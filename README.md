# BASEmesh Repository

for now we have two different versions of *BASEmesh*

* a QGIS version (working for QGIS version < 3.0)
* and a python tool to create meshes script-based (see `MeshTool`)

These two meshing tools shall be merged for QGIS v3.x!


## Installation for QGIS v3.x

Do the checkout of this repository in you local plugin directory of QGIS v3.x

*  on Windows: `<your-home-directory>\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\BASEmesh`
*  on Linux: `<your-home-direcotry>/.local/share/QGIS/QGIS3/profiles/default/python/plugins/BASEmesh`
*  on MacOSX: `<your-home-directory>/Library/Application Support/QGIS/QGIS3/profiles/default/python/plugins/BASEmesh`

Afterwards you should see a BASEMENT icon in the toolbar. Clicking on the icon adds the BASEmesh main widget to the left dock. You can close it and open it again.