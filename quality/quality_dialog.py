# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

import os
import sys
import platform

# ui from external file
from ui_quality_widget import Ui_qualMeshQt

# modules for triangulation and conversion of results
from ..tools import commonFunctions
from ..tools.runTriangle import RUNTRIANGLE
from ..tools.meshConversion import MESHCONVERSION
from ..tools.nodeManipulation import extractStringdefNodes


class QualMeshDialog (QDialog, Ui_qualMeshQt):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
        self.mWindow = iface.mainWindow()
        self.progressBar.setValue(0)
        # creation of QProcess(), for future execution of Triangle
        self.process = QProcess()
        # ensure that checkboxes are set to their default value
        self.triangleDelaunayCheckBox.setCheckState(Qt.Checked)
        self.triangleQualityCheckBox.setCheckState(Qt.Checked)
        self.loadMeshPolygonCheckBox.setCheckState(Qt.Checked)
        self.loadMeshPointsCheckBox.setCheckState(Qt.Checked)
        self.deltempCheckBox.setCheckState(Qt.Checked)
        
        # populate combo boxes
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Polygon:
                self.boundaryComboBox.addItem( layer.name() )
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Line:
                self.breaklinesComboBox.addItem( layer.name() )
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                self.regionComboBox.addItem( layer.name() )
                self.verticesComboBox.addItem( layer.name() )
        # function is called for the first time, to assure combobox is not empty
        self.readAttributeREGION()
        self.readAttributeDIV()
        #connections
        self.connect(self.breaklinesCheckBox, SIGNAL("toggled(bool)"), self.baseInputToggledBREAKLINES)
        self.connect(self.regionCheckBox, SIGNAL("toggled(bool)"), self.baseInputToggledREGIONS)
        self.connect(self.regionComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeREGION)
        self.connect(self.breaklinesComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeDIV)
        self.connect(self.browseButton, SIGNAL("clicked()"), self.defineOutputFile)
        self.connect(self.process, SIGNAL("readyReadStandardOutput()"), self.readTriangleOutput)
        self.connect(self.process, SIGNAL("readyReadStandardError()"), self.readTriangleErrors)
        self.connect(self.snappingToleranceCheckBox, SIGNAL("toggled(bool)"), self.onSnappingToleranceToggled)

    # ---------------------------
    # - definition of functions -
    # ---------------------------
    def defineOutputFile (self): # display file dialog for output shapefile, return selected file path
        commonFunctions.defineOutput(self, self.outputShapeLineEdit,".shp", commonFunctions.getProjectName())
    
    def baseInputToggledBREAKLINES (self):
        commonFunctions.baseInputToggled (self.breaklinesDivCheckBox, self.breaklinesDivComboBox, self.breaklinesCheckBox.isChecked())

    def baseInputToggledREGIONS (self):
        commonFunctions.baseInputToggled (self.areaCheckBox, self.areaComboBox, self.regionCheckBox.isChecked())
        commonFunctions.baseInputToggled (self.materialCheckBox, self.materialComboBox, self.regionCheckBox.isChecked())
        commonFunctions.baseInputToggled (self.holesCheckBox, self.holesComboBox, self.regionCheckBox.isChecked())

    def readAttributeREGION (self):
        commonFunctions.readAttribute (self.regionComboBox, self.areaComboBox)
        commonFunctions.readAttribute (self.regionComboBox, self.materialComboBox)
        commonFunctions.readAttribute (self.regionComboBox, self.holesComboBox)

    def readAttributeDIV (self):
        commonFunctions.readAttribute (self.breaklinesComboBox, self.breaklinesDivComboBox)

    def readTriangleOutput (self):
        self.textStatusBox.append(str(self.process.readAllStandardOutput()))

    def readTriangleErrors (self):
        self.textStatusBox.append("Triangle error:" + QString(self.process.readAllStandardError()))

    def onSnappingToleranceToggled (self):
        self.snappingToleranceSpinBox.setReadOnly( not self.snappingToleranceCheckBox.isChecked() )

    # -------------------------------------------------------------
    # - accept - block: loaded, when "generate"-button is pressed -
    # -------------------------------------------------------------
    def accept (self):
        
        # DO SOME PREPERATION STUFF
        self.textStatusBox.clear()
        self.textStatusBox.setFontItalic (1)
        # used for definition of Triangle output filenames
        meshingtype = 'Quality'
        # check if output file is defined
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.mWindow, "Error","Please specify output shp-file.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            projectPath, fileName = os.path.split(outputFile)
            projectName = os.path.splitext(fileName)[0]
            self.textStatusBox.append ("prefix of triangle output-files: " + projectName)
            self.textStatusBox.append ("plugin directory: " + commonFunctions.getPluginDir())

        # get bounary layer to retreive mesh extent
        if self.boundaryComboBox.count()==0:
            QMessageBox.warning(self.mWindow, "Error","pls define a shapefile containing the boundary polygon!")
            return
        else:
            boundaryLayer = commonFunctions.readSelection (self.boundaryComboBox)
        # triangle files ID
        triangleFileID = "%s_%s"%(projectName, meshingtype)
        # set up TRIANGLE
        # first get the relative precision for snapping
        precisionExp = self.snappingToleranceSpinBox.value()
        triangle = RUNTRIANGLE(boundaryLayer,10**precisionExp)
        success = triangle.initialization(triangleFileID)
        if success != True:
            QMessageBox.critical(self.mWindow, "Error", success)
            return
        else:
            self.textStatusBox.append("\n'triangle' successfully initialized...\n")

        # CONVERSION TO TRIANGLE INPUTS
        self.textStatusBox.append ('\nPROCESSING INPUT DATA...')
        # layer containing the boundary
        boundary,_ = commonFunctions.extractLines(boundaryLayer)
        if type(boundary) != list:
            QMessageBox.critical(self.mWindow, meshingtype +" meshing", edges)
        else:
            triangle.addLines(boundary)
        # breakline layer (optional)
        if self.breaklinesCheckBox.isChecked():
            if self.breaklinesComboBox.count()==0:
                self.textStatusBox.append ("\n> no optional breaklines layer found.")
            else:
                breaklinesLayer = commonFunctions.readSelection (self.breaklinesComboBox)
                self.textStatusBox.append ("\n> layer with breaklines specified: " + breaklinesLayer.name())
                # eventually prescribe number of cells on breaklines
                nCells = None
                if self.breaklinesDivCheckBox.isChecked():
                    nCells = self.breaklinesDivComboBox.currentText()
                    self.textStatusBox.append (" - field with number of cells for breaklines: " + nCells)
                lines,nSegments = commonFunctions.extractLines(breaklinesLayer,nCells)
                if type(lines) != list:
                    QMessageBox.critical(self.mWindow, meshingtype +" meshing", lines)
                else:
                    triangle.addLines(lines,nSegments)
        else:
            self.textStatusBox.append ('\n> no breaklines specified <')
        # layer containing the vertices (optional)
        if self.verticesCheckBox.isChecked():
            if self.verticesComboBox.count()==0:
                self.textStatusBox.append ("\n> no optional vertices layer found.")
            else:
                verticesLayer = commonFunctions.readSelection (self.verticesComboBox)
                self.textStatusBox.append ("\n> layer with vertices specified: " + verticesLayer.name())
                nodes = commonFunctions.extractPoints(verticesLayer)
                if type(nodes) != list:
                    QMessageBox.critical(self.mWindow, meshingtype +" meshing", nodes)
                else:
                    triangle.addVertices(nodes)
        # regional attributes (optional)
        if self.regionCheckBox.isChecked():
            if self.regionComboBox.count()==0:
                self.textStatusBox.append ("\n> no optional region layer found.")
            else:
                regionLayer = commonFunctions.readSelection (self.regionComboBox)
                self.textStatusBox.append ("\n> layer with regions specified: " + regionLayer.name())
                area = None
                matID = None
                hole = None
                # maximum element area information (optional)
                if self.areaCheckBox.isChecked():
                    area = self.areaComboBox.currentText()
                    self.textStatusBox.append (" - field with maximum element size data: " + area)
                    triangle.setOption('a', True)
                else:
                    self.textStatusBox.append (" - no optional element size field specified.")
                    triangle.setOption('a', False)
                # material index (optional)
                if self.materialCheckBox.isChecked():
                    matID = self.materialComboBox.currentText()
                    self.textStatusBox.append (" - field with material index data: " + matID)
                    triangle.setOption('A', True)
                else:
                    self.textStatusBox.append (" - no optional material index field specified.")
                    triangle.setOption('A', False)
                # holes information (optional)
                if self.holesCheckBox.isChecked():
                    hole = self.holesComboBox.currentText()
                    self.textStatusBox.append (" - field with hole marker: " + hole)
                else:
                    self.textStatusBox.append (" - no optional holes layer specified.")
                # put the regional attributes to the triangle data
                regions = commonFunctions.extractRegions(regionLayer, area, matID, hole)
                if type(regions) != list:
                    QMessageBox.critical(self.mWindow, meshingtype +" meshing", regions)
                else:
                    triangle.addRegions(regions)
        else:
            self.textStatusBox.append ("\n> no optional region layer found.")
            triangle.setOption('A', False)
            triangle.setOption('a', False)
        # finally write .poly file
        success = triangle.writePolyfile()
        if success != True:
            QMessageBox.critical(self.mWindow, meshingtype +" meshing", success)
        else:
            self.textStatusBox.append ('\nTRIANGLE INPUT file %s.poly successfully written'%triangleFileID)
        # has the VERBOSE-option been checked?
        triangle.setOption('V', self.triangleVerboseCheckBox.checkState() == Qt.Checked)
        # delaunay triangulation instead of just constraint delaunay?
        triangle.setOption('D', self.triangleDelaunayCheckBox.checkState() == Qt.Checked)
        # prohibition of steiner points?
        triangle.setOption('Y', self.triangleSteinerCheckBox.checkState() == Qt.Checked)
        # quality mesh and value of max angle?
        if self.triangleQualityCheckBox.checkState() == Qt.Checked:
            triangle.setQuality(self.minimumAngleSpinBox.value())
        # expert mode?
        if self.expertCheckBox.checkState() == Qt.Checked:
            triangle.addExpertArgs(self.expertLineEdit.text())

        # EXECUTION OF TRIANGLE
        # Triangle is executed from it's location
        self.textStatusBox.append ('\n > EXECUTION OF TRIANGLE... <')
        self.process.start(triangle.getExecutable(), triangle.getParameters())
        # definition of output files created by triangle, to check their existance after meshing
        nodeResult, elementResult = triangle.getResultfiles()
        triangleFileID = os.path.splitext(nodeResult)[0]
        # check if result files are present
        if self.process.waitForFinished() == True:
            if not os.path.exists(nodeResult):
                QMessageBox.critical(self.mWindow, meshingtype +" meshing", "Meshing via Triangle failed! \nNode result file not found.")
            elif not os.path.exists(elementResult):
                QMessageBox.critical(self.mWindow, meshingtype +" meshing", "Meshing via Triangle failed! \nElement result file not found.")
            else:
                self.textStatusBox.append('\nMeshing via Triangle succesfull. Results are converted to shapefiles...')
        else:
            self.textStatusBox.append("\nTriangle: MESHING FAILED.")
            QMessageBox.critical(self.mWindow, meshingtype +" meshing", "Meshing failed. \nDetails can be found in the status messages.")
            return

        # CONVERT TRIANGLE RESULTS TO SHAPE FILES
        # build mesh from .node and .ele file
        meshConv = MESHCONVERSION()
        success = meshConv.readTriangle(triangleFileID, self.progressBar)
        if success != True:
            QMessageBox.warning( self.mWindow, "Error", success )
            return
        else:
            self.textStatusBox.append("\nMesh successfully loaded.")
        # write shapefiles from generated mesh
        shapefileID = os.path.join(projectPath,'%s_%s'%(projectName, meshingtype))
        success = meshConv.writeShape( shapefileID )
        if success != True:
            QMessageBox.warning( self.mWindow, "Error", success )
            return
        else:
            shapefileNodes = shapefileID+'_nodes.shp'
            shapefileCells = shapefileID+'_elements.shp'
            self.textStatusBox.append ('\nWriting shape files finished succesfully!')
        # reporting success
        self.textStatusBox.append ('\nLoading mesh finished succesfully. Shape files were created.')
        
        # DO SOME AFTERMATH
        # after meshing, the name of the output-file is cleared, so that user cannot overwrite previous mesh by accident
        self.outputShapeLineEdit.clear()
        # loading of result files
        if self.loadMeshPolygonCheckBox.checkState() == Qt.Checked:
            layer = QgsVectorLayer(shapefileCells, unicode(os.path.basename(shapefileCells)), "ogr")
            if layer.isValid():
                # add the layer to the map
                QgsMapLayerRegistry.instance().addMapLayer(layer)
                # get all the material indices
                renderer = commonFunctions.createMatidRenderer(layer)
                layer.setRendererV2(renderer)
            else:
                QMessageBox.warning(self.mWindow, "Quality meshing", "Error loading mesh polygon layer")
        if self.loadMeshPointsCheckBox.checkState() == Qt.Checked:
            layer = QgsVectorLayer(shapefileNodes, unicode(os.path.basename(shapefileNodes)), "ogr")
            if layer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(layer)
            else:
                QMessageBox.warning(self.mWindow, "Quality meshing", "Error loading mesh points layer")
        # deletion of temporary triangle files
        if self.deltempCheckBox.checkState() == Qt.Checked:
            success = triangle.deleteFiles()
            if success != True:
                QMessageBox.critical(self.mWindow, meshingtype +" meshing", success)
            else:
                self.textStatusBox.append("\nTemporary files created by Triangle during meshing have been deleted...\n")