# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'importmesh/ui_importmesh_widget.ui'
#
# Created: Fri Mar 11 11:06:34 2016
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_importmeshQT(object):
    def setupUi(self, importmeshQT):
        importmeshQT.setObjectName(_fromUtf8("importmeshQT"))
        importmeshQT.resize(850, 350)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(importmeshQT.sizePolicy().hasHeightForWidth())
        importmeshQT.setSizePolicy(sizePolicy)
        importmeshQT.setMinimumSize(QtCore.QSize(850, 350))
        importmeshQT.setMaximumSize(QtCore.QSize(16777215, 16777215))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/BASEmesh/icons/importmesh.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        importmeshQT.setWindowIcon(icon)
        self.verticalLayout_5 = QtGui.QVBoxLayout(importmeshQT)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.groupBox = QtGui.QGroupBox(importmeshQT)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.groupBox)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.inputMeshLineEdit = QtGui.QLineEdit(self.groupBox)
        self.inputMeshLineEdit.setObjectName(_fromUtf8("inputMeshLineEdit"))
        self.horizontalLayout_3.addWidget(self.inputMeshLineEdit)
        self.browseInputButton = QtGui.QPushButton(self.groupBox)
        self.browseInputButton.setObjectName(_fromUtf8("browseInputButton"))
        self.horizontalLayout_3.addWidget(self.browseInputButton)
        self.verticalLayout_3.addWidget(self.groupBox)
        self.groupBox_2 = QtGui.QGroupBox(importmeshQT)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.checkBoxRotate = QtGui.QCheckBox(self.groupBox_2)
        self.checkBoxRotate.setEnabled(False)
        self.checkBoxRotate.setObjectName(_fromUtf8("checkBoxRotate"))
        self.horizontalLayout_5.addWidget(self.checkBoxRotate)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.groupBox_2)
        self.label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.lineEditDegree = QtGui.QLineEdit(self.groupBox_2)
        self.lineEditDegree.setEnabled(False)
        self.lineEditDegree.setObjectName(_fromUtf8("lineEditDegree"))
        self.verticalLayout.addWidget(self.lineEditDegree)
        spacerItem = QtGui.QSpacerItem(20, 25, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_5.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label_2 = QtGui.QLabel(self.groupBox_2)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_2.addWidget(self.label_2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_3 = QtGui.QLabel(self.groupBox_2)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout.addWidget(self.label_3)
        self.lineEditX = QtGui.QLineEdit(self.groupBox_2)
        self.lineEditX.setEnabled(False)
        self.lineEditX.setObjectName(_fromUtf8("lineEditX"))
        self.horizontalLayout.addWidget(self.lineEditX)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_4 = QtGui.QLabel(self.groupBox_2)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_4.addWidget(self.label_4)
        self.lineEditY = QtGui.QLineEdit(self.groupBox_2)
        self.lineEditY.setEnabled(False)
        self.lineEditY.setObjectName(_fromUtf8("lineEditY"))
        self.horizontalLayout_4.addWidget(self.lineEditY)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addWidget(self.groupBox_2)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.horizontalLayout_7.addLayout(self.verticalLayout_3)
        self.tabWidget_2 = QtGui.QTabWidget(importmeshQT)
        self.tabWidget_2.setObjectName(_fromUtf8("tabWidget_2"))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.horizontalLayout_8 = QtGui.QHBoxLayout(self.tab_4)
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.textStatusBox = QtGui.QTextEdit(self.tab_4)
        self.textStatusBox.setObjectName(_fromUtf8("textStatusBox"))
        self.horizontalLayout_8.addWidget(self.textStatusBox)
        self.tabWidget_2.addTab(self.tab_4, _fromUtf8(""))
        self.tab_5 = QtGui.QWidget()
        self.tab_5.setObjectName(_fromUtf8("tab_5"))
        self.horizontalLayout_9 = QtGui.QHBoxLayout(self.tab_5)
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.textBrowser = QtGui.QTextBrowser(self.tab_5)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.horizontalLayout_9.addWidget(self.textBrowser)
        self.tabWidget_2.addTab(self.tab_5, _fromUtf8(""))
        self.horizontalLayout_7.addWidget(self.tabWidget_2)
        self.verticalLayout_5.addLayout(self.horizontalLayout_7)
        self.groupBox_3 = QtGui.QGroupBox(importmeshQT)
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        self.outputShapeLineEdit = QtGui.QLineEdit(self.groupBox_3)
        self.outputShapeLineEdit.setObjectName(_fromUtf8("outputShapeLineEdit"))
        self.horizontalLayout_11.addWidget(self.outputShapeLineEdit)
        self.browseOutputButton = QtGui.QPushButton(self.groupBox_3)
        self.browseOutputButton.setMaximumSize(QtCore.QSize(150, 16777215))
        self.browseOutputButton.setObjectName(_fromUtf8("browseOutputButton"))
        self.horizontalLayout_11.addWidget(self.browseOutputButton)
        self.verticalLayout_4.addLayout(self.horizontalLayout_11)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.loadMeshCheckBox = QtGui.QCheckBox(self.groupBox_3)
        self.loadMeshCheckBox.setChecked(True)
        self.loadMeshCheckBox.setObjectName(_fromUtf8("loadMeshCheckBox"))
        self.horizontalLayout_6.addWidget(self.loadMeshCheckBox)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem2)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.loadProgressBar = QtGui.QProgressBar(self.groupBox_3)
        self.loadProgressBar.setMinimumSize(QtCore.QSize(235, 0))
        self.loadProgressBar.setProperty("value", 0)
        self.loadProgressBar.setObjectName(_fromUtf8("loadProgressBar"))
        self.horizontalLayout_2.addWidget(self.loadProgressBar)
        self.closeButton = QtGui.QPushButton(self.groupBox_3)
        self.closeButton.setMinimumSize(QtCore.QSize(0, 0))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout_2.addWidget(self.closeButton)
        self.importButton = QtGui.QPushButton(self.groupBox_3)
        self.importButton.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.importButton.setFont(font)
        self.importButton.setObjectName(_fromUtf8("importButton"))
        self.horizontalLayout_2.addWidget(self.importButton)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.verticalLayout_5.addWidget(self.groupBox_3)

        self.retranslateUi(importmeshQT)
        self.tabWidget_2.setCurrentIndex(0)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), importmeshQT.reject)
        QtCore.QObject.connect(self.importButton, QtCore.SIGNAL(_fromUtf8("clicked()")), importmeshQT.accept)
        QtCore.QObject.connect(self.checkBoxRotate, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.lineEditY.setEnabled)
        QtCore.QObject.connect(self.checkBoxRotate, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.lineEditDegree.setEnabled)
        QtCore.QObject.connect(self.checkBoxRotate, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.lineEditX.setEnabled)
        QtCore.QMetaObject.connectSlotsByName(importmeshQT)

    def retranslateUi(self, importmeshQT):
        importmeshQT.setWindowTitle(QtGui.QApplication.translate("importmeshQT", "BASEmesh - Import Mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("importmeshQT", "INPUT MESH", None, QtGui.QApplication.UnicodeUTF8))
        self.browseInputButton.setText(QtGui.QApplication.translate("importmeshQT", "Browse", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("importmeshQT", "Mesh rotation", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBoxRotate.setText(QtGui.QApplication.translate("importmeshQT", "Rotate", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("importmeshQT", "Degree rotation", None, QtGui.QApplication.UnicodeUTF8))
        self.lineEditDegree.setText(QtGui.QApplication.translate("importmeshQT", "0.0", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("importmeshQT", "Coordinates rotation", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("importmeshQT", "X:", None, QtGui.QApplication.UnicodeUTF8))
        self.lineEditX.setText(QtGui.QApplication.translate("importmeshQT", "0.0", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("importmeshQT", "Y:", None, QtGui.QApplication.UnicodeUTF8))
        self.lineEditY.setText(QtGui.QApplication.translate("importmeshQT", "0.0", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_4), QtGui.QApplication.translate("importmeshQT", "Status messages", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("importmeshQT", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">PURPOSE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">This dialog allows for loading </span><span style=\" font-size:8pt; font-weight:600;\">2dm</span><span style=\" font-size:8pt;\">-mesh files (BASEplane, 2D). The 2dm-mesh file is converted to two shapefiles: </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">- a polygon shapefile containing the mesh elements and their connectivity  information (node-IDs, material indices)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">- a point shapefile containing the mesh nodes and nodal information (nodeIDs, elevation).</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Furthermore, this dialog allows for loading </span><span style=\" font-size:8pt; font-weight:600;\">bmg</span><span style=\" font-size:8pt;\">-mesh files (BASEchain, 1D) and </span><span style=\" font-size:8pt; font-weight:600;\">g</span><span style=\" font-size:8pt;\">-mesh files (HEC-RAS, 1D). The mesh file is converted to two shapefiles, containing the cross-section points with elevation data and cross-section lines with additional cross-section information.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">USAGE:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; text-decoration: underline;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">1.</span><span style=\" font-size:8pt; color:#0000ff;\"> </span><span style=\" font-size:8pt;\">A </span><span style=\" font-size:8pt; font-weight:600;\">2dm mesh file </span><span style=\" font-size:8pt;\">or </span><span style=\" font-size:8pt; font-weight:600;\">1D mesh file </span><span style=\" font-size:8pt;\">has to be selected.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">2.</span><span style=\" font-size:8pt;\"> The output names of the imported 2dm-mesh are by default set to the prefix \'mesh\'. The corresponding nodes- and element files are markes with the suffixes \'_elements\' and \'-nodes.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">After clicking the \'</span><span style=\" font-size:8pt; font-weight:600;\">Import mesh</span><span style=\" font-size:8pt;\">\' - button, the conversion process is started. A progress bar informs about the current status of the process. Until completion, no status messages are displayed. If the import mesh-checkbos is activated, the result files are automatically loaded into map canvas.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">ADVICE:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; text-decoration: underline; color:#da9100;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; color:#000000;\">- The Mesh rotation options are mainly used for debugging and not of use in everyday work. As a resullt, they are currently deactivated.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">- The conversion process can be time-consuming and last for several minutes. Please be patient.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_5), QtGui.QApplication.translate("importmeshQT", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("importmeshQT", "Shapefile OUTPUT", None, QtGui.QApplication.UnicodeUTF8))
