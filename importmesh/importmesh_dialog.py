# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar
import math

# ui from external file
from ui_importmesh_widget import Ui_importmeshQT

# common functions that are used in several scripts of this plugin
from ..tools import commonFunctions
from ..tools.meshConversion import MESHCONVERSION
from ..tools.crossSectionConversion import CROSSSECTIONCONVERSION

class ImportMeshDialog (QDialog, Ui_importmeshQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
        self.mWindow = iface.mainWindow()

        # connection of signal for definition of input file
        QObject.connect(self.browseInputButton, SIGNAL("clicked()"), self.defineInputFile)

        # connection of signal for definition of output file
        QObject.connect(self.browseOutputButton, SIGNAL("clicked()"), self.defineOutputFile)

    # ---------------------------
    # - definition of functions -
    # ---------------------------

    def defineInputFile (self): # display file dialog for input file, return selected file path
        commonFunctions.defineInput( self,self.inputMeshLineEdit,[".2dm", ".bmg", ".g0*"])
        pass

    def defineOutputFile (self): # display file dialog for output shapefile, return selected file path
        commonFunctions.defineOutput(self, self.outputShapeLineEdit,".shp", "mesh.shp")

    # -------------------------------------------------------------
    # - accept - block: loaded, when "Load"-button is pressed -
    # ------------------------------------------------------------

    def accept (self):
        # do some initialization stuff
        self.textStatusBox.clear()
        self.loadProgressBar.setValue(0)

        # check if input file is defined
        if self.inputMeshLineEdit.text() == "":
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please specify the input file.")
            return
        else:
            inputFile = str(self.inputMeshLineEdit.text())
            fileName, fileExtension = os.path.splitext(inputFile)

        # check if output file is defined
        shapefileID = ""
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.mWindow, "Error","Please specify output shp-file.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            projectPath, outputFileName = os.path.split(outputFile)
            projectName = os.path.splitext(outputFileName)[0]
            shapefileID = os.path.join(projectPath,'%s'% projectName)
            self.textStatusBox.append ("prefix of output-files: " + projectName)

        # LOAD BASEplane FILE
        if fileExtension=='.2dm':
            # create mesh from .2dm-file
            meshConv = MESHCONVERSION()
            self.textStatusBox.append ('\nReading the 2dm-mesh file...')
            success = meshConv.read2dm( fileName )
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                nNodes = len(meshConv.getMesh().getNodeIDs())
                nCells = len(meshConv.getMesh().getElementIDs())
                self.textStatusBox.append ( '\nimported mesh with %i nodes and %i elements'%(nNodes, nCells) )
            # initialize the progress bar
            totalFeatures = nNodes+nCells
            self.loadProgressBar.setRange(0, totalFeatures)

            # write shapefiles from generated mesh
            shapefileNodes = shapefileID+'_nodes.shp'
            shapefileCells = shapefileID+'_elements.shp'
            success = meshConv.writeShape(shapefileID, self.loadProgressBar)
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                self.textStatusBox.append ('\nMesh imported finished succesfully. Shapefiles created successfully.')
            # reporting success
            # iface.messageBar().pushMessage("Importing 2dm mesh", "Mesh imported finished succesfully. Shape files were created.", QgsMessageBar.INFO, 10)

        # LOAD BASEchain FILE
        elif fileExtension=='.bmg' or fileExtension.find(".g") >= 0:
            # create mesh from .bmg-file
            csConv = CROSSSECTIONCONVERSION()
            self.textStatusBox.append ('\nReading the 1D cross sections...')
            text = []
            if fileExtension=='.bmg':
                success = csConv.readBMG( fileName, text )
            else:
                success = csConv.readHECRAS( fileName + fileExtension, text )
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                nNodes = len(csConv.getMesh().getNodeIDs())
                nCells = len(csConv.getMesh().getElementIDs())
                self.textStatusBox.append ( '\nImported mesh with %i nodes and %i cross sections'%(nNodes, nCells) )

           # print some stats
            tmpString = ""
            for string in text:
                tmpString += string + "\n"
            self.textStatusBox.append(tmpString)

            # initialize the progress bar
            totalFeatures = nNodes+nCells
            self.loadProgressBar.setRange(0, totalFeatures)
            # write shapefiles from generated mesh
            shapefileNodes = shapefileID+'_points.shp'
            shapefileCells = shapefileID+'_crossSections.shp'
            self.textStatusBox.append ('\nReading the .bmg cross sections..%s.' % shapefileID)
            success = csConv.writeShape(shapefileID, self.loadProgressBar)
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                self.textStatusBox.append ('\nbmg Mesh imported finished succesfully. Shapefiles created successfully.')
            # reporting success
            # iface.messageBar().pushMessage("Importing bmg file", "Mesh imported finished succesfully. Shapefiles created successfully.", QgsMessageBar.INFO, 10)

        # ERROR
        else:
            QMessageBox.warning( self.iface.mainWindow(), "Error", 'unknown file extension!' )
            return

        # LOAD LAYERS (optional)
        if self.loadMeshCheckBox.checkState() == Qt.Checked:
            # layer containing the cells
            meshlayer = QgsVectorLayer(shapefileCells, unicode(os.path.basename(shapefileCells)), "ogr")
            if meshlayer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(meshlayer)
                if fileExtension=='.2dm':
                    # get all the material indices
                    renderer = commonFunctions.createMatidRenderer(meshlayer)
                    meshlayer.setRendererV2(renderer)
            else:
                QMessageBox.warning(self.iface.mainWindow(), "Error", "loading mesh polygon layer %s"%shapefileCells)
                pass
            # layer containing the nodes
            meshlayer = QgsVectorLayer(shapefileNodes, unicode(os.path.basename(shapefileNodes)), "ogr")
            if meshlayer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(meshlayer)
            else:
                QMessageBox.warning(self.iface.mainWindow(), "Error", "loading mesh point layer %s"%shapefileNodes)
                pass
        return
