# define all ui files
FILES="
quality/ui_quality_widget
elevation/ui_elevation_widget
interpolation/ui_interpolation_widget
importmesh/ui_importmesh_widget
renumber/ui_renumber_widget
checkMesh/ui_checkMesh_widget
stringdef/ui_stringdef_widget
exportmesh/ui_exportmesh_widget"
# build all the ui forms
# and remove all import resources_rc from the just generated files
for f in $FILES
do
	echo "Processing $f.ui"
	pyuic4 $f.ui -o $f.py
	head -n -2 $f.py > temp.py
	mv temp.py $f.py
done
# build resources
echo "Processing resources.qrc"
pyrcc4 resources.qrc -o resources.py