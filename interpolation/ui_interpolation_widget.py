# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interpolation/ui_interpolation_widget.ui'
#
# Created: Fri Mar 11 11:06:34 2016
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_interpolQT(object):
    def setupUi(self, interpolQT):
        interpolQT.setObjectName(_fromUtf8("interpolQT"))
        interpolQT.resize(570, 455)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(interpolQT.sizePolicy().hasHeightForWidth())
        interpolQT.setSizePolicy(sizePolicy)
        interpolQT.setMinimumSize(QtCore.QSize(570, 455))
        interpolQT.setMaximumSize(QtCore.QSize(16777215, 455))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/BASEmesh/icons/interpolation.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        interpolQT.setWindowIcon(icon)
        self.verticalLayout_6 = QtGui.QVBoxLayout(interpolQT)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.tabWidget = QtGui.QTabWidget(interpolQT)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setMinimumSize(QtCore.QSize(485, 305))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.tab)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.groupBox = QtGui.QGroupBox(self.tab)
        self.groupBox.setFlat(False)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.labelqualnodes = QtGui.QLabel(self.groupBox)
        self.labelqualnodes.setMinimumSize(QtCore.QSize(150, 0))
        self.labelqualnodes.setObjectName(_fromUtf8("labelqualnodes"))
        self.horizontalLayout_2.addWidget(self.labelqualnodes)
        self.qualpointsComboBox = QtGui.QComboBox(self.groupBox)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.qualpointsComboBox.sizePolicy().hasHeightForWidth())
        self.qualpointsComboBox.setSizePolicy(sizePolicy)
        self.qualpointsComboBox.setMinimumSize(QtCore.QSize(300, 0))
        self.qualpointsComboBox.setObjectName(_fromUtf8("qualpointsComboBox"))
        self.horizontalLayout_2.addWidget(self.qualpointsComboBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.verticalLayout_4.addWidget(self.groupBox)
        self.groupBox_2 = QtGui.QGroupBox(self.tab)
        self.groupBox_2.setFlat(False)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.elevMeshButton = QtGui.QRadioButton(self.groupBox_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.elevMeshButton.sizePolicy().hasHeightForWidth())
        self.elevMeshButton.setSizePolicy(sizePolicy)
        self.elevMeshButton.setChecked(True)
        self.elevMeshButton.setObjectName(_fromUtf8("elevMeshButton"))
        self.verticalLayout.addWidget(self.elevMeshButton)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem1 = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.labelelevnodes = QtGui.QLabel(self.groupBox_2)
        self.labelelevnodes.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelelevnodes.sizePolicy().hasHeightForWidth())
        self.labelelevnodes.setSizePolicy(sizePolicy)
        self.labelelevnodes.setMinimumSize(QtCore.QSize(150, 0))
        self.labelelevnodes.setObjectName(_fromUtf8("labelelevnodes"))
        self.horizontalLayout_6.addWidget(self.labelelevnodes)
        self.elevpointsComboBox = QtGui.QComboBox(self.groupBox_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.elevpointsComboBox.sizePolicy().hasHeightForWidth())
        self.elevpointsComboBox.setSizePolicy(sizePolicy)
        self.elevpointsComboBox.setMinimumSize(QtCore.QSize(300, 0))
        self.elevpointsComboBox.setObjectName(_fromUtf8("elevpointsComboBox"))
        self.horizontalLayout_6.addWidget(self.elevpointsComboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem2 = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.labelelevelements = QtGui.QLabel(self.groupBox_2)
        self.labelelevelements.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelelevelements.sizePolicy().hasHeightForWidth())
        self.labelelevelements.setSizePolicy(sizePolicy)
        self.labelelevelements.setMinimumSize(QtCore.QSize(150, 0))
        self.labelelevelements.setObjectName(_fromUtf8("labelelevelements"))
        self.horizontalLayout_7.addWidget(self.labelelevelements)
        self.elevmeshComboBox = QtGui.QComboBox(self.groupBox_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.elevmeshComboBox.sizePolicy().hasHeightForWidth())
        self.elevmeshComboBox.setSizePolicy(sizePolicy)
        self.elevmeshComboBox.setMinimumSize(QtCore.QSize(300, 0))
        self.elevmeshComboBox.setObjectName(_fromUtf8("elevmeshComboBox"))
        self.horizontalLayout_7.addWidget(self.elevmeshComboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        spacerItem3 = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.demMeshButton = QtGui.QRadioButton(self.groupBox_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.demMeshButton.sizePolicy().hasHeightForWidth())
        self.demMeshButton.setSizePolicy(sizePolicy)
        self.demMeshButton.setChecked(False)
        self.demMeshButton.setObjectName(_fromUtf8("demMeshButton"))
        self.verticalLayout.addWidget(self.demMeshButton)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        spacerItem4 = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem4)
        self.labelraster = QtGui.QLabel(self.groupBox_2)
        self.labelraster.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelraster.sizePolicy().hasHeightForWidth())
        self.labelraster.setSizePolicy(sizePolicy)
        self.labelraster.setMinimumSize(QtCore.QSize(150, 0))
        self.labelraster.setObjectName(_fromUtf8("labelraster"))
        self.horizontalLayout_8.addWidget(self.labelraster)
        self.elevRasterComboBox = QtGui.QComboBox(self.groupBox_2)
        self.elevRasterComboBox.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.elevRasterComboBox.sizePolicy().hasHeightForWidth())
        self.elevRasterComboBox.setSizePolicy(sizePolicy)
        self.elevRasterComboBox.setMinimumSize(QtCore.QSize(300, 0))
        self.elevRasterComboBox.setObjectName(_fromUtf8("elevRasterComboBox"))
        self.horizontalLayout_8.addWidget(self.elevRasterComboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_8)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem5 = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.labelband = QtGui.QLabel(self.groupBox_2)
        self.labelband.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelband.sizePolicy().hasHeightForWidth())
        self.labelband.setSizePolicy(sizePolicy)
        self.labelband.setMinimumSize(QtCore.QSize(150, 0))
        self.labelband.setMaximumSize(QtCore.QSize(90, 16777215))
        self.labelband.setObjectName(_fromUtf8("labelband"))
        self.horizontalLayout.addWidget(self.labelband)
        self.bandComboBox = QtGui.QComboBox(self.groupBox_2)
        self.bandComboBox.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.bandComboBox.sizePolicy().hasHeightForWidth())
        self.bandComboBox.setSizePolicy(sizePolicy)
        self.bandComboBox.setMinimumSize(QtCore.QSize(250, 0))
        self.bandComboBox.setObjectName(_fromUtf8("bandComboBox"))
        self.horizontalLayout.addWidget(self.bandComboBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_4.addWidget(self.groupBox_2)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.tab_2)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.textBrowser = QtGui.QTextBrowser(self.tab_2)
        self.textBrowser.setMinimumSize(QtCore.QSize(465, 0))
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.horizontalLayout_3.addWidget(self.textBrowser)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.verticalLayout_6.addWidget(self.tabWidget)
        self.groupBox_3 = QtGui.QGroupBox(interpolQT)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy)
        self.groupBox_3.setMinimumSize(QtCore.QSize(485, 0))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_15 = QtGui.QHBoxLayout()
        self.horizontalLayout_15.setObjectName(_fromUtf8("horizontalLayout_15"))
        self.outputShapeLineEdit = QtGui.QLineEdit(self.groupBox_3)
        self.outputShapeLineEdit.setReadOnly(True)
        self.outputShapeLineEdit.setObjectName(_fromUtf8("outputShapeLineEdit"))
        self.horizontalLayout_15.addWidget(self.outputShapeLineEdit)
        self.browseButton = QtGui.QPushButton(self.groupBox_3)
        self.browseButton.setObjectName(_fromUtf8("browseButton"))
        self.horizontalLayout_15.addWidget(self.browseButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_15)
        self.loadResultCheckBox = QtGui.QCheckBox(self.groupBox_3)
        self.loadResultCheckBox.setObjectName(_fromUtf8("loadResultCheckBox"))
        self.verticalLayout_2.addWidget(self.loadResultCheckBox)
        self.horizontalLayout_18 = QtGui.QHBoxLayout()
        self.horizontalLayout_18.setObjectName(_fromUtf8("horizontalLayout_18"))
        self.progressBar = QtGui.QProgressBar(self.groupBox_3)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.horizontalLayout_18.addWidget(self.progressBar)
        self.closeButton = QtGui.QPushButton(self.groupBox_3)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout_18.addWidget(self.closeButton)
        self.executeButton = QtGui.QPushButton(self.groupBox_3)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.executeButton.setFont(font)
        self.executeButton.setObjectName(_fromUtf8("executeButton"))
        self.horizontalLayout_18.addWidget(self.executeButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_18)
        self.verticalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout_6.addWidget(self.groupBox_3)

        self.retranslateUi(interpolQT)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.elevRasterComboBox.setEnabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.bandComboBox.setEnabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.labelraster.setEnabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.labelband.setEnabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.elevpointsComboBox.setDisabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.elevmeshComboBox.setDisabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.labelelevnodes.setDisabled)
        QtCore.QObject.connect(self.demMeshButton, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.labelelevelements.setDisabled)
        QtCore.QObject.connect(self.executeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), interpolQT.accept)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), interpolQT.reject)
        QtCore.QMetaObject.connectSlotsByName(interpolQT)

    def retranslateUi(self, interpolQT):
        interpolQT.setWindowTitle(QtGui.QApplication.translate("interpolQT", "BASEmesh - Interpolation", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("interpolQT", "Quality mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.labelqualnodes.setText(QtGui.QApplication.translate("interpolQT", "Nodes vector layer", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("interpolQT", "Elevation data", None, QtGui.QApplication.UnicodeUTF8))
        self.elevMeshButton.setText(QtGui.QApplication.translate("interpolQT", "elevation mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.labelelevnodes.setText(QtGui.QApplication.translate("interpolQT", "Nodes vector layer", None, QtGui.QApplication.UnicodeUTF8))
        self.labelelevelements.setText(QtGui.QApplication.translate("interpolQT", "Elements vector layer", None, QtGui.QApplication.UnicodeUTF8))
        self.demMeshButton.setText(QtGui.QApplication.translate("interpolQT", "digital elevation map", None, QtGui.QApplication.UnicodeUTF8))
        self.labelraster.setText(QtGui.QApplication.translate("interpolQT", "Raster layer", None, QtGui.QApplication.UnicodeUTF8))
        self.labelband.setText(QtGui.QApplication.translate("interpolQT", "Band", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QtGui.QApplication.translate("interpolQT", "INPUT", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("interpolQT", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">PURPOSE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">In this tool, elevation information is interpolated on an existing quality mesh. The quality mesh would already be suitable for numeric applications, but in most cases, the addition of elevatio information to the mesh nodes is required.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Two data sources can be chosen:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">A</span><span style=\" font-size:8pt; font-weight:600; color:#000000;\"> - </span><span style=\" font-size:8pt; color:#000000;\">based on an </span><span style=\" font-size:8pt; font-weight:600; color:#000000;\">elevation mesh in vector format</span><span style=\" font-size:8pt; color:#000000;\">. This mesh was most likely created with the \'elevation meshing\' tool of this plugin.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; color:#000000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">B</span><span style=\" font-size:8pt; color:#000000;\"> - based on a </span><span style=\" font-size:8pt; font-weight:600; color:#000000;\">digital elevation map in raster format</span><span style=\" font-size:8pt; color:#000000;\">. Such datasets already exist in different resolutions for many areas or can be created in QGIS by various tools.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">PROCESS DESCRIPTION:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">A - </span><span style=\" font-size:8pt;\">The routine identifies the coordinates of each quality mesh node and determines any underlying elevation mesh elements.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt; If a corresponding elevation mesh element is found, the elevation of the quality mesh node is interpolated. </span><span style=\" font-size:8pt; color:#000000;\">Nodes that were interpolated this way are marked by a \'1\' in the \'element\'-field of the result attribute table.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt; If the quality mesh node is located directly on an elevation point, it\'s height value is used without any interpolation. By this mean, the elevation of specific mesh vertices and breaklines is preserved.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; color:#000000;\">&gt; If no underlying elevation mesh element is found, the elevation is interpolated from the neighboring nodes of the elevation mesh. Nodes that were interpolated this way are marked by a \'0\' in the \'element\'-field of the result attribute table. In the status messages, those nodes are named \'with special treatment\'.</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">B - </span><span style=\" font-size:8pt; color:#000000;\">The elevation value of the underlying raster cell is directly used for the quality mesh node without interpolation. Therefore, the resolution of the elevation map has a big influence on the correct representation of elevation values in the mesh.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; color:#000000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#000000;\">Please note that the mesh detail and the raster resolution must be chosen wisely.</span><span style=\" font-size:8pt; color:#000000;\"> It is for example not advisable to combine a raster dataset with a cell size of 20 x 20 m with a very dence mesh with nodes distances in the range of 1m. The representation of elevations in the mesh would be quite poor and sudden steps are likely to be found in the mesh when reaching the next raster cell. Depending on available raster resolutions, Option B may be useful for meshes of bigger areas, where terrain representation must not be higly accurate.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; color:#000000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#000000;\">The extent of the raster file should be bigger than the model boundary.</span><span style=\" font-size:8pt; color:#000000;\"> If no underlying raster cell is found, the elevation of the mesh node is set to </span><span style=\" font-size:8pt; font-weight:600; color:#000000;\">-999</span><span style=\" font-size:8pt; color:#000000;\">.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">USAGE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">The shapefiles needed have to be loaded into the QGIS-project before executing this tool - It is not possible to select files directly on the hard drive by browsing. Futhermore, the files used for meshing must be activated in the QGIS table of contents on the left side of the screen. </span><span style=\" font-size:8pt; text-decoration: underline;\">Only data that is displayed on the map can be used for meshing.</span><span style=\" font-size:8pt;\"> To prevent the selection of wrong shapetypes, the available fields are populated with the corresponding data type.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline;\"><br /></span><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">1.</span><span style=\" font-size:8pt;\"> Three</span><span style=\" font-size:8pt; font-weight:600;\"> inputs layers</span><span style=\" font-size:8pt;\"> have to be selected. There are no other parameters available. </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Quality mesh nodes:</span><span style=\" font-size:8pt;\"> vector point layer containing mesh nodes of a quality mesh which has been created using specific quality criteria (as created by the tool \'quality meshing\' of this plugin).</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">2.</span><span style=\" font-size:8pt;\"> </span><span style=\" font-size:8pt; font-weight:600;\">Selection of elevation data:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">A</span><span style=\" font-size:8pt;\"> -  When using an elevation mesh as data source, click the corresponding radio button and select the following layers:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">- Elevation mesh elements: </span><span style=\" font-size:8pt;\">point layer containing elevation mesh elements as polygons with their element ID and three adjacent nodes stored in the attribute table .(as created by the tool \'elevation meshing\' of this plugin).</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">- Elevation mesh nodes:</span><span style=\" font-size:8pt;\"> layer containing elevation mesh nodes and their coordinates as attributes (as created by the tool \'elevation meshing\' of this plugin).</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">B</span><span style=\" font-size:8pt;\"> -  When using a digital elevation map as data source, click the corresponding radio button and select the following layers:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">- raster layer: </span><span style=\" font-size:8pt;\">raster file containing elevation values.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">- band: </span><span style=\" font-size:8pt;\">many raster formats allow multiple bands, where each band stores different values or data.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">3.</span><span style=\" font-size:8pt;\"> </span><span style=\" font-size:8pt; color:#000000;\">BASEmesh automatically inserts the name of the current project as prefix for the output file. The suffic \'Interpolated\' is added automatically to the resulting shapefile: </span><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">PROJECTNAME</span><span style=\" font-size:8pt; font-weight:600;\">_Interpolated.shp</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; color:#000000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">After selecting the output location, the interpolation process is started by clicking on \'</span><span style=\" font-size:8pt; font-weight:600;\">Interpolate elevations</span><span style=\" font-size:8pt;\">\'. A progress bar informs about the current status of the process. Until completion, no status messages are displayed.The </span><span style=\" font-size:8pt; font-weight:600;\">Interpolation results</span><span style=\" font-size:8pt;\"> are by default loaded into map canvas.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">ADVICE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">The interpolation can be time-consuming and last for several minutes. Please be patient.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">At the moment, QGIS doesn\'t support 3d visualizations. To check the interpolation results, we recommend to use the labeling function integrated in QGIS. By activating the elevation labels for the elevation source file and the interpolation results, implausible results can quickly be detected.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QtGui.QApplication.translate("interpolQT", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("interpolQT", "Shapefile OUTPUT", None, QtGui.QApplication.UnicodeUTF8))
