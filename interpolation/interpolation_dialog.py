# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

import os,  sys,  math
from ..tools import commonFunctions
from ..tools import nodeManipulation
from ..tools.meshConversion import MESHCONVERSION

# ui from external file
from ui_interpolation_widget import Ui_interpolQT

class InterpolDialog (QDialog, Ui_interpolQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
        self.mWindow = iface.mainWindow()


        # set default values
        self.loadResultCheckBox.setCheckState(Qt.Checked) #check the mesh polygon checkbox

        # populate combo box of the quality mesh
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                self.qualpointsComboBox.addItem( layer.name() )

        # connecttion of signal for the defintion of interpolation method
        QObject.connect( self.elevMeshButton, SIGNAL("toggled(bool)"), self.elevMeshButtonToggled )
        self.elevMeshButtonToggled(True)

        # connection of signal for the band selection
        QObject.connect(self.elevRasterComboBox, SIGNAL("currentIndexChanged(QString)"), self.readBand)

        # connection of signal for definition of output file
        QObject.connect( self.browseButton, SIGNAL("clicked()"), self.defineOutputFile )

    # ---------------------------
    # - definition of functions -
    # ---------------------------
    def defineOutputFile (self):
        commonFunctions.defineOutput(self, self.outputShapeLineEdit, ".shp", commonFunctions.getProjectName())

    def readBand (self):
        commonFunctions.readAttribute(self.elevRasterComboBox, self.bandComboBox)

    def elevMeshButtonToggled (self, bool):
        if bool:
            # elevation mesh is used for interpolation
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Polygon:
                    self.elevmeshComboBox.addItem( layer.name() )
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                    self.elevpointsComboBox.addItem( layer.name() )
            self.elevRasterComboBox.clear()
            self.bandComboBox.clear()
        else:
            # raster data (DEM) is directly used for interpolation
            self.elevmeshComboBox.clear()
            self.elevpointsComboBox.clear()
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.RasterLayer:
                    self.elevRasterComboBox.addItem( layer.name() )

    # ------------------------------------------------------------
    # - accept - block: loaded, when "execute"-button is pressed -
    # ------------------------------------------------------------
    def accept (self):
    
    	# so far hardcoded
    	precExponent = -6

        # DO SOME PREPERATION STUFF
        # used for definition of Triangle output filenames
        interpolatedName = '_Interpolated_nodes'
        # check if output file is defined
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.mWindow, "Error","Please specify output shp-file.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            fileName, fileExtension = os.path.splitext(outputFile)
            fileName += interpolatedName
        
        # reading comboboxes and data
        # quality mesh
        if self.qualpointsComboBox.count()==0:
            QMessageBox.warning(self.mWindow, "Error","pls define a shapefile containing the nodes of the quality mesh!")
            return
        else:
            qualityPoints = commonFunctions.readSelection (self.qualpointsComboBox)
        if self.elevMeshButton.isChecked():
            # elevation mesh
            if self.elevpointsComboBox.count()==0:
                QMessageBox.warning(self.mWindow, "Error","pls define a shapefile containing the nodes of the elevation mesh!")
                return
            else:
                elevationPoints = commonFunctions.readSelection (self.elevpointsComboBox)
            if self.elevmeshComboBox.count()==0:
                QMessageBox.warning(self.mWindow, "Error","pls define a shapefile containing the elements of the elevation mesh!")
                return
            else:
                elevationPolygons = commonFunctions.readSelection (self.elevmeshComboBox)
            # create mesh object
            meshConv = MESHCONVERSION()
            success = meshConv.readShape( elevationPoints, 'Z', elevationPolygons, None )
            if success != True:
                QMessageBox.critical(self.mWindow, "Error", success)
                return
            else:
                elevMesh = meshConv.getMesh()
        else:
            # raster data (DEM)
            if self.elevRasterComboBox.count()==0:
                QMessageBox.warning(self.mWindow, "Error","pls define raster file containing the elevation data!")
                return
            else:
                elevationData = commonFunctions.readSelection (self.elevRasterComboBox)
                elevationBand = int(self.bandComboBox.currentText())

        # EXECUTE INTERPOLATION
        numberQualityPoints = commonFunctions.featureCount(qualityPoints)
        iface.messageBar().pushMessage("Interpolation", "Number of points to be interpolated: %d" %numberQualityPoints, QgsMessageBar.INFO, 3)
        self.progressBar.setRange(0, numberQualityPoints)
        self.progressBar.setValue(0)
        if self.elevMeshButton.isChecked():
            # elevation mesh
            fileName += "_elevMesh"
            result = nodeManipulation.interpolationFromMesh(fileName, elevMesh, qualityPoints, 10**precExponent, self.progressBar)
            if type(result) in [type(str()), type(unicode())]:
                QMessageBox.warning(self.mWindow, "Error", result)
                return
            else:
                if result>0:
                    iface.messageBar().pushMessage("Interpolation", "Nr points with special treatment: %d." % result, QgsMessageBar.INFO, 3)
        else:
            # raster data
            fileName += "_Raster"
            result = nodeManipulation.interpolationFromRaster(fileName, elevationData, elevationBand, qualityPoints, self.progressBar)
            if type(result) in [type(str()), type(unicode())]:
                QMessageBox.warning(self.mWindow, "Error", result)
                return
            else:
                if result>0:
                    iface.messageBar().pushMessage("Interpolation", "Nr points with special treatment: %d." % result, QgsMessageBar.INFO, 3)

        # DO SOME AFTERMATH
        fileName += ".shp"
        # after meshing, the name of the output-file is cleared, so that user cannot overwrite previous mesh by accident
        self.outputShapeLineEdit.clear()
        # loading of result files
        if self.loadResultCheckBox.checkState() == Qt.Checked:
            layer = QgsVectorLayer(fileName, unicode(os.path.basename(fileName)), "ogr")
            if layer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(layer)
            else:
                QMessageBox.warning(self.mWindow, "Error loading interpolation result layer")
        else:
            pass
