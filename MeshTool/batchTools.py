# by Samuel J. Peter, 2015

import os, getopt, sys

# base class for MESHMODEL and BASEMENT class, to handle the working directory issue
class WORKINGDIRECTORY:
	def __init__ (self,directory):
		if not os.path.exists(directory):
			try:
				os.mkdir(directory)
			except:
				print 'directory %s does not exist!'%directory
		self.__cwd = os.path.abspath(directory)

	def getDirectory (self):
		return self.__cwd

	def to (self, path):
		return os.path.join(self.__cwd,path)


# class to parse command line options and create an ID
class OPTIONPARSING:
	def __init__ (self):
		self.__N = 0
		self.__values = {}
		self.__fix = {}

	def __str__ (self):
		out = ''
		for k,v in self.__values.iteritems():
			out += '%s: %f\n'%(k,v)
		return out

	def addOption (self,name,defaultValue=False,fixSize=None):
		self.__values[name] = defaultValue
		self.__fix[name] = fixSize
		self.__N += 1

	def parseOptions (self,optsFromCommandLine):
		optsList = ''
		for oo in self.__values.keys():
			optsList += '%s'%oo
			if self.__values[oo] != False:
				optsList += ':'
		try:
			#print optsList
			#print optsFromCommandLine
			opts,args = getopt.getopt(optsFromCommandLine,optsList)
		except getopt.GetoptError:
			sys.exit(2)
		if len(opts) > 0:
			tags,vals =  map(list,zip(*opts))
			for oo in self.__values.keys():
				tag = '-%s'%oo
				if tag in tags:
					ind = tags.index(tag)
					if vals[ind] != '':
						self.__values[oo] = vals[ind]
					else:
						self.__values[oo] = True

	def getID (self):
		modelID = ''
		for oo in self.__values.keys():
			v = float(self.__value[oo])
			modelID += oo
			modelID += '%i'%round(v*10**self.__fix[oo],0)
			modelID += 'e'
			if cmp(v,0):
				modelID += '-'
			else:
				modelID += '+'
			modelID += '%i'%abs(self.__fix[oo])
			modelID +='_'
		return modelID[:-1]

	def getValue (self,name):
		return self.__values[name]


def printSuccess():
	print "\n\
     @@@@              \n\
     @  `@         ,,  \n\
     @   @,       @';@ \n\
     @`  :@      @+  @`\n\
     #@   @      @   @ \n\
      @   @.    @#   @ \n\
      @   '@    @   ## \n\
      #@   @   :@   @  \n\
       @   @   @`   @  \n\
       @`  #+  @   +#  \n\
       +#   @ @'   @   \n\
        @   @ @    @   \n\
        @   @@@   '#   \n\
    `   #@        @    \n\
    @@@@#@@@@@@'  @    \n\
   `@  @@@     @' @    \n\
   `@   @@,     @.@    \n\
 ;@@@   +@@@'.   @@    \n\
 @  @#   @` '@   #@    \n\
 @   @   '@  @    @`   \n\
 @+  +@   @  @    #@   \n\
 @@   @,  @  '`    @   \n\
 @@#  @@@@@        @   \n\
 @#@' @           @+   \n\
 ,@ @@`           @    \n\
  @              @,    \n\
  @:            '@     \n\
   @`          `@      \n\
    @:         @`      \n\
    `@         @       \n\
    `@         @       \n\
     #@@@@@@@@@+      \n\
"
