#!/usr/bin/env python

# mesh generator based on ChannelGen by S. Peter

# new features: 
#  - movable widening
#  - additional cross-sections around the wider area
#  - rectangular cross-section
# by Matteo Facchini, 2018
# mailto: matteo.facchini@unitn.it

import math, argparse
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class PiGreco2D(MESHMODEL):
	def __init__(self, directory, rect,
		length, width, height, chanSlope, latSlope, 
		nCS, ksBank, ksBed, nEl, qA):
		# call initialization of meshmodel
		MESHMODEL.__init__(self, directory)
		# this class does the meshing
		self.initialization('pi_greco_tests')
		# store input argument as member variables (hidden)
		self.__iR = rect
		self.__lc = length
		self.__sc = chanSlope
		self.__sl = latSlope
		self.__wc = width
		self.__hc = height
		self.__n  = nCS
		self.__kB = ksBank
		self.__ks = ksBed
		self.__nE = nEl
		self.__q  = qA

	def buildModel(self):
		# check if the cross-section is rectangular
		if self.__sl <= 1e-8:
			self.__iR = True # default is false
		# needed coordinates: mesh with cross-section centered on (x,y)=(0,0)
		xUS, xDS = (0.0, self.__lc) # x UpStream and x DownStream
		yBl, yBr = (0.5*self.__wc, -0.5*self.__wc) # y Bottom left and right
		yTl, yTr = (yBl+(self.__sl*self.__hc), yBr-(self.__sl*self.__hc)) #
		if self.__iR:
			totArea = self.__lc*self.__wc
			maxAmain = totArea/self.__nE
		else:
			# quality mesh parameters
			totArea = 2.0*yTr*self.__lc
			mainchArea = self.__lc*self.__wc
			banksArea = totArea-mainchArea
			maxAmain = 1.25*totArea/self.__nE
			maxAbanks = 10.0*maxAmain
		# bed lines
		self.addLine( [NODE(xUS, yBl), NODE(xDS, yBl)], name='bottomLeft' )
		self.addLine( [NODE(xUS, yBr), NODE(xDS, yBr)], name='bottomRight' )
		# boundary
		if self.__iR:
			boundaryLine = [NODE(0.0, 0.0), NODE(xUS, yBl)]
			boundaryLine.extend( [NODE(xDS, yBl), NODE(xDS, yBr)] )
			boundaryLine.extend( [NODE(xUS, yBr), NODE(0.0, 0.0)] )
		else:
			self.addLine( [NODE(xUS, yTl), NODE(xDS, yTl)], name='topLeft' )
			self.addLine( [NODE(xUS, yTr), NODE(xDS, yTr)], name='topRight' )
			boundaryLine = [NODE(0.0, 0.0), NODE(xUS, yBl), NODE(xUS, yTl)]
			boundaryLine.extend( [NODE(xDS, yTl), NODE(xDS, yBl), NODE(xDS, yBr), NODE(xDS, yTr)] )
			boundaryLine.extend( [NODE(xUS, yTr), NODE(xUS, yBr), NODE(0.0, 0.0)] )
		self.addLine(boundaryLine)
		# define matID for illustration purposes
		self.addRegion(NODE(0.5*self.__lc, 0.0), matID=1, maxArea=maxAmain) # main channel
		if not self.__iR:
			self.addRegion(NODE(0.5*self.__lc, yBl+0.5*(yTl-yBl)), matID=2, maxArea=maxAbanks) # left bank
			self.addRegion(NODE(0.5*self.__lc, yBr+0.5*(yTr-yBr)), matID=2, maxArea=maxAbanks) # right bank
		# define number of cross sections
		dx = self.__lc/self.__n
		for ii in range(self.__n+1):
			self.addCrossSection(NODE(ii*dx,0.0)) # ranges the whole channel length
		# define frction ranges
		if not self.__iR:
			friction = {'default': self.__ks,
						'topLeft-bottomLeft': self.__kB,
						'bottomLeft-bottomRight': self.__ks,
						'topRight-bottomRight': self.__kB}
		else:
			friction = {'default': self.__ks,
						'bottomLeft-bottomRight': self.__ks,}
		self.setFriction(friction)
		# set if cross-section is rectangular
		self.setRectangle(self.__iR, self.__hc)
		# write the .bmg file
		self.createMesh()
		# deleting the triangle files
		self.deleteFiles()

	def elevationFunction(self, x, y):
		zBed = (self.__lc-x)*self.__sc
		# application of simmetry
		x = min(x, self.__lc-x)
		dy = abs(y)-0.5*self.__wc
		if dy > 1e-8: # we are on the banks
			zBank = min(self.__hc, dy/self.__sl)
		else:
			zBank = 0.0
		return zBed+zBank

class PiGreco2Dw(MESHMODEL):
	def __init__(self, directory, rect,
		length, width, height, chanSlope, latSlope, 
		wideningStartLocation, wideningLength, transitionLength, wideningBedWidth, ksWid,  
		nCS, ksBank, ksBed, nEl, qA, nCSW):
		# call initialization of meshmodel
		MESHMODEL.__init__(self, directory)
		# this class does the meshing
		self.initialization('pi_greco_tests')
		# store input argument as member variables (hidden)
		self.__iR = rect
		self.__lc = length
		self.__sc = chanSlope
		self.__sl = latSlope
		self.__wc = width
		self.__hc = height
		self.__ws = wideningStartLocation
		self.__lw = wideningLength
		self.__lt = transitionLength
		self.__ww = wideningBedWidth
		self.__kW = ksWid 
		self.__n  = nCS
		self.__kB = ksBank
		self.__ks = ksBed
		self.__nE = nEl
		self.__q  = qA
		self.__nW = nCSW

	def buildModel(self):
		# check if the cross-section is rectangular
		if self.__sl <= 1e-8:
			self.__iR = True # default is false
		# check the transition length
		if 2.0*self.__lt+self.__lw > self.__lc:
			print 'ERROR: channel too short to model the widening including transition!'
		else:
			# set up boundaries for the starting location (xWSL) of a movable widening
			dx = self.__lc/self.__n
			usdsL = 0.5*(self.__lc - (2*self.__lt+self.__lw)) # length of the us/ds reaches
			xWSL_max = 2*usdsL
			xWSL_min = 0.0 
			xStart   = self.__ws + usdsL
			if xStart <= xWSL_min:
				print 'the wider reach is upstream the domain: setting its start at the upstream end'
				xWSL = dx - usdsL
			elif xStart >= xWSL_max:
				print 'the wider reach is downstream the domain: setting its end at the downstream end'
				xWSL = usdsL - dx
			else:
				xWSL = self.__ws
			# needed coordinates: mesh with cross-section centered on (x,y)=(0,0)
			xUS, xDS = (0.0, self.__lc) # x UpStream and x DownStream
			yBl, yBr = (-0.5*self.__wc, 0.5*self.__wc) # y Bottom left and right
			xUSt = xWSL+0.5*(self.__lc-self.__lw)-self.__lt
			xDSt = xWSL+0.5*(self.__lc+self.__lw)+self.__lt
			xUSw = xWSL+0.5*(self.__lc-self.__lw)
			xDSw = xWSL+0.5*(self.__lc+self.__lw)
			yBlw, yBrw = (-0.5*self.__ww, 0.5*self.__ww)
			yTl, yTr = (yBl-(self.__sl*self.__hc), yBr+(self.__sl*self.__hc)) # y Top left and righ
			yTlw, yTrw = (yBlw-(self.__sl*self.__hc), yBrw+(self.__sl*self.__hc))
			if not self.__iR:
				# parameters for quality meshing
				boundaryArea = 2*yTrw*self.__lc
				totArea = boundaryArea-4*((xUSw+xUSt)*(yTrw-yTr)*0.5)
				mainchArea = (self.__lc*self.__ww)-4*((xUSw+xUSt)*(yBrw-yBr)*0.5)
				banksArea = totArea-mainchArea
				maxAmain = 1.5*totArea/self.__nE
				maxAbanks = 10.0*maxAmain
				maxAwide = maxAmain
			else:
				totArea = (self.__lc*self.__ww)-4*((xUSw+xUSt)*(yBrw-yBr)*0.5)
				maxAmain = totArea/self.__nE
				maxAwide = maxAmain
			# bed lines
			self.addLine( [NODE(xUS, yBl), NODE(xUSt, yBl)], name='bottomLeft' )
			self.addLine( [NODE(xUSt, yBl), NODE(xUSw, yBlw), NODE(xDSw, yBlw), NODE(xDSt, yBl)], name='wideBotLeft' )
			self.addLine( [NODE(xDSt, yBl), NODE(xDS, yBl)], name='bottomLeft' )
			self.addLine( [NODE(xUS, yBr), NODE(xUSt, yBr)], name='bottomRight' )
			self.addLine( [NODE(xUSt, yBr), NODE(xUSw, yBrw), NODE(xDSw, yBrw), NODE(xDSt, yBr)], name='wideBotRight' )
			self.addLine( [NODE(xDSt, yBr), NODE(xDS, yBr)], name='bottomRight' )
			# bank lines
			if not self.__iR:
				self.addLine( [NODE(xUS, yTl), NODE(xUSt, yTl), NODE(xUSw, yTlw), NODE(xDSw, yTlw), NODE(xDSt, yTl), NODE(xDS, yTl)], name='topLeft' )
				self.addLine( [NODE(xUS, yTr), NODE(xUSt, yTr), NODE(xUSw, yTrw), NODE(xDSw, yTrw), NODE(xDSt, yTr), NODE(xDS, yTr)], name='topRight' )
				# boundary lines
				boundaryLine = [NODE(0.0,0.0), NODE(xUS, yBl), NODE(xUS, yTl)]
				boundaryLine.extend( [NODE(xUSt, yTl), NODE(xUSw, yTlw), NODE(xDSw, yTlw), NODE(xDSt, yTl)] )
				boundaryLine.extend( [NODE(xDS, yTl), NODE(xDS, yBl), NODE(xDS, yBr), NODE(xDS, yTr)] )
				boundaryLine.extend( [NODE(xDSt, yTr), NODE(xDSw, yTrw), NODE(xUSw, yTrw), NODE(xUSt, yTr)] )
				boundaryLine.extend( [NODE(xUS, yTr), NODE(xUS, yBr), NODE(0.0,0.0)] )
			else:
				# boundary line
				boundaryLine = [NODE(0.0,0.0), NODE(xUS, yBl)]
				boundaryLine.extend( [NODE(xUSt, yBl), NODE(xUSw, yBlw), NODE(xDSw, yBlw), NODE(xDSt, yBl)] )
				boundaryLine.extend( [NODE(xDS, yBl), NODE(xDS, yBr)] )
				boundaryLine.extend( [NODE(xDSt, yBr), NODE(xDSw, yBrw), NODE(xUSw, yBrw), NODE(xUSt, yBr)] )
				boundaryLine.extend( [NODE(xUS, yBr), NODE(0.0,0.0)] )
			self.addLine(boundaryLine)
			# define matID for illustration purposes
			# add two lines to divide the main channel in and outside the widening region
			self.addLine( [NODE(xUSt, yBl), NODE(xUSt, yBr)] ) 
			self.addLine( [NODE(xDSt, yBl), NODE(xDSt, yBr)] )
			if not self.__iR:
				self.addRegion(NODE(0.5*(xUSt+xUS), 0.0), matID=1, maxArea=maxAmain) # main channel upstream
				self.addRegion(NODE(0.5*(xUSw+xDSw), 0.0), matID=3, maxArea=maxAwide) # main channel, widening region
				self.addRegion(NODE(0.5*(xDS+xDSt), 0.0), matID=1, maxArea=maxAmain) # main channel downstream
				self.addRegion(NODE(0.5*(xUSw+xDSw), 0.5*(yBlw+yTlw)), matID=2, maxArea=maxAbanks) # left bank widening
				self.addRegion(NODE(0.5*(xUSw+xDSw), 0.5*(yBrw+yTrw)), matID=2, maxArea=maxAbanks) # right bank widening
			else:
				self.addRegion(NODE(xUS+0.5*(xUSt-xUS), 0.0), matID=1, maxArea=maxAmain) # main channel upstream
				self.addRegion(NODE(0.5*(xUSw+xDSw), 0.0), matID=2, maxArea=maxAwide) # main channel, widening region
				self.addRegion(NODE(xDSt+0.5*(xDS-xDSt), 0.0), matID=1, maxArea=maxAmain) # main channel downstream
			# define number of cross sections
			if self.__nW:
				# add more cross-sections around the wider area
				dx  = self.__lc/self.__n
				# set dx in the wider region (dxW) as half of the regular dx 
				dxW = dx/2.0
				# calculate the upstream reach length
				usL = xUSt
				# calculate the downstream reach length
				dsL = self.__lc - xDSt
				# calculate the total length of the wider region
				totWideningL = self.__lw + 2.0*self.__lt
				# calculate the length of the regions with different cross-sections
				if usL < dsL and usL < totWideningL:
					finerLength = usL + 2.0*totWideningL
				elif usL > dsL and dsL < totWideningL:
					finerLength = dsL + 2.0*totWideningL
				else:
					finerLength = 3.0*totWideningL
				# in the case of a finer zone longer than the channel make everything fine
				coarserLength = max(self.__lc-finerLength, 0)
				# calculate the number of cross-sections in the wider area
				# calculate the number of cross-sections outside the wider area
				nCSout = int(coarserLength/dx)
				# check if nCSout is odd
				if usL < dsL and usL < totWideningL:
					nCSus = int(usL/dxW)
					nCSds = int((dsL-totWideningL)/dx)+1
					nCSw  = int((finerLength-usL)/dxW)
					dxus  = dxW
					dxds  = dx
				elif usL > dsL and dsL < totWideningL:
					nCSus = int((usL-totWideningL)/dx)+1
					nCSds = int(dsL/dxW)
					nCSw  = int((finerLength-dsL)/dxW)
					dxus  = dx
					dxds  = dxW
				else:
					nCSus = int((usL-totWideningL)/dx)
					nCSds = int((dsL-totWideningL)/dx)
					nCSw  = int(finerLength/dxW)+1
					dxus  = dx
					dxds  = dx
				# set the new cross-section numbers as integers
				nCSdsW = nCSus+nCSw # number of CS at the end of the widening
				nCSnew = nCSdsW+nCSds # number of CS at the end of the domain
				# range in the channel (NB: range(N) goes from 0 to N-1)
				for ii in range(nCSus+1):
					# print NODE(ii*dxus,0.0).getX()
					self.addCrossSection(NODE(ii*dxus,0.0)) # ranges the ups reach
					Lus = NODE(nCSus*dxus,0.0).getX()
				for ii in range(1,nCSw+1):
					# print NODE(Lus+ii*dxW,0.0).getX()
					self.addCrossSection(NODE(Lus+ii*dxW,0.0)) # ranges the finer reach
					LdsW = NODE(Lus+nCSw*dxW).getX()
				for ii in range(1,nCSds+1):
					# print NODE(LdsW+ii*dxds,0.0).getX()
					self.addCrossSection(NODE(LdsW+ii*dxds,0.0)) # ranges the dws reach
					lastCSx = NODE(LdsW+ii*dxds,0.0).getX()
			else:
				dx  = self.__lc/self.__n
				for ii in range(self.__n+1):
					self.addCrossSection(NODE(ii*dx,0.0)) # ranges the whole channel length
			# define friction regions
			if not self.__iR:
				friction = {'default': self.__ks,
							'topLeft-bottomLeft': self.__kB,
							'bottomLeft-bottomRight': self.__ks,
							'topRight-bottomRight': self.__kB,
							'wideningLeft-topLeft': self.__kB,
							'wideningLeft-wideningRight': self.__kW,
							'wideningRight-topRight': self.__kB}
			else:
				friction = {'default': self.__ks,
							'bottomLeft-bottomRight': self.__ks,
							'wideningLeft-wideningRight': self.__kW}
			self.setFriction(friction)
			# set the rectangular cross-section switch
			self.setRectangle(self.__iR,self.__hc)
			# write the .bmg file
			self.createMesh()
			# deleting the triangle files
			self.deleteFiles()

	def elevationFunction(self, x, y):
		zBed = (self.__lc-x)*self.__sc
		# application of simmetry
		# x  = min(x, self.__lc-x)
		# set x coordinates taking into account movable widening
		dx = self.__lc/self.__n
		usdsL = 0.5*(self.__lc - (2*self.__lt+self.__lw)) # length of the us/ds reaches
		xWSL_max = 2*usdsL
		xWSL_min = 0.0 
		xStart   = self.__ws + usdsL
		if xStart <= xWSL_min:
			xWSL = dx - usdsL
		elif xStart >= xWSL_max:
			xWSL = usdsL - dx
		else:
			xWSL = self.__ws
		xUSt = xWSL+0.5*(self.__lc-self.__lw)-self.__lt
		xDSt = xWSL+0.5*(self.__lc+self.__lw)+self.__lt
		xUSw = xWSL+0.5*(self.__lc-self.__lw)
		xDSw = xWSL+0.5*(self.__lc+self.__lw)
		# calculate dy based on position
		if x<xUSt or x>xDSt:
			dy = abs(y)-0.5*self.__wc
		elif x>=xUSt and x<=xUSw:
			w  = self.__wc + (self.__ww-self.__wc)*(x-xUSt)/self.__lt
			dy = abs(y) - 0.5*w
		elif x>xUSw and x<xDSw:
			dy = abs(y)-0.5*self.__ww
		elif x>=xDSw and x<=xDSt:
			w  = self.__ww + (self.__ww-self.__wc)*(x-xDSw)/self.__lt
			dy = abs(y) - 0.5*w
		# calculate zBank based on dy
		if dy > 1e-8:
			zBank = min( self.__hc, dy/self.__sl)
		else:
			zBank = 0.0
		return zBed+zBank

if __name__ != 'main':
	sketch = '\n\
    <------------------------LC------------------------>\n\
                       **************\n\
                    **        |       **\n\
    ***************           |          ***************\n\
          |                   |                 |\n\
          WC                  WW                WC\n\
          |                   |                 |\n\
    ***************           |          ***************\n\
                    **        |       **\n\
                       **************\n\
                   <LT><-----LW-----><LT>'
	parser = argparse.ArgumentParser(prog='PiGreco',
		formatter_class=argparse.RawDescriptionHelpFormatter,
		description='Generation of simple channel geometries.\n'+sketch)
	parser.add_argument('-LC', metavar='length', default=24.0, type=float, help='total length of the channel [m] (default: %(default)s)')
	parser.add_argument('-WC', metavar='width', default=0.4, type=float, help='bed width of the channel [m] (default: %(default)s)')
	parser.add_argument('-S', metavar='chanSlope', default=0.01, type=float, help='slope of the channel [-] (default: %(default)s)')
	parser.add_argument('-B', metavar='latSlope', default=0.0, type=float, help='slope of the banks [h:v] (default: %(default)s)')
	parser.add_argument('-WSL', metavar='wideningStartLocation', default=0.0, type=float, help='widening start location [m] (default is calculated based on channel mid location)')
	parser.add_argument('-H', metavar='height', default=200.0, type=float, help='height of the floodplain above the bed [m] (default: %(default)s)')
	parser.add_argument('-KS', metavar='ksBed', default=70.0, type=float, help='strickler value of bed [...] (default: %(default)s)')
	parser.add_argument('-KSB', metavar='ksBank', default=20.0, type=float, help='strickler value of bank [...] (default: %(default)s)')
	parser.add_argument('-X', metavar='referenceDistance', default=0.0, type=float, help='offset in x-direction [m] (default: %(default)s)')
	parser.add_argument('-Z', metavar='referenceElevation', default=0.0, type=float, help='offset in z-direction [m] (default: %(default)s)')
	parser.add_argument('-N', metavar='nCS', default=100, type=int, help='number of cross sections to generate [-] (default: %(default)s)')
	parser.add_argument('-CS', metavar='csNamePrefix', default='CS', type=str, help='prefix of cross section names (default: %(default)s)')
	parser.add_argument('--R', dest='reverseList', action='store_true', help='reverse cross section order (default: %(default)s)')
	parser.add_argument('--widening', dest='widening', action='store_true', help='tag to model a widening (default: %(default)s)')
	parser.add_argument('--CSW', dest='CSW', action='store_true', help='tag to add cross-sections around the wider area (default: %(default)s)')
	parser.add_argument('-LW', metavar='wideningLength', default=5.65, type=float, help='length of the widening only [m] (default: %(default)s)')
	parser.add_argument('-LT', metavar='transitionLength', default=0.175, type=float, help='length of the transition between channel and widenening (>0) [m] (default: %(default)s)')
	parser.add_argument('-WW', metavar='wideningBedWidth', default=0.8, type=float, help='bed width of the widening [m] (default: %(default)s)')
	parser.add_argument('-KSW', metavar='kstrickler', default=70.0, type=float, help='strickler value of widening [...] (default: %(default)s)')
	parser.add_argument('-NE', metavar='nEl', default=10000, type=int, help='number of meshing elements [-] (default: %(default)s)')
	parser.add_argument('-QA', metavar='qA', default=31.0, type=float, help='minimum triangle angle [deg] (default: %(default)s)')
	parser.add_argument('--2D', dest='2d', action='store_true', help='tag to generate 2d mesh (default: %(default)s)')
	parser.add_argument('--BED', dest='bed', action='store_false', help='tag to define a bottom with soil 1 (default: %(default)s)')
	parser.add_argument('--FN', dest='filename', default='', type=str, help='tag to define a suffix for the file name (default: %(default)s)')
	parser.add_argument('--SJP', dest='sjp', action='store_true', help=argparse.SUPPRESS)
	args = vars(parser.parse_args())
	# create model directory
	wd = WORKINGDIRECTORY('PiGreco')
	# initialize switch for rectangular cross-section
	rect = False
	# if args['B'] <= 1e-8:
	# 	rect = True
	# cretae the mesh model
	if args['sjp']:
		printSuccess()
	if args['widening']:
		nm = PiGreco2Dw(wd, rect,
			length = args['LC'],
			width = args['WC'],
			height = args['H'],
			chanSlope = args['S'],
			latSlope = args['B'],
			wideningStartLocation = args['WSL'],
			wideningLength = args['LW'],
			transitionLength = args['LT'],
			wideningBedWidth = args['WW'],
			ksWid = args['KSW'],
			nCS = args['N'],
			ksBank = args['KSB'],
			ksBed = args['KS'],
			nEl = args['NE'],
			qA = args['QA'],
			nCSW = args['CSW'])
		if args['filename']:
			modID = args['filename']
		else:
			modID = 'PiGreco2Dw' if args['2d'] else 'PiGreco1Dw'
	else:
		nm = PiGreco2D(wd, rect,
			length = args['LC'],
			width = args['WC'],
			height = args['H'],
			chanSlope = args['S'],
			latSlope = args['B'],
			nCS = args['N'],
			ksBank = args['KSB'],
			ksBed = args['KS'],
			nEl = args['NE'],
			qA = args['QA'])
		if args['filename']:
			modID = args['filename']
		else:
			modID = 'PiGreco2D' if args['2d'] else 'PiGreco1D'
	mesh = 'plane2x' if args['2d'] else 'chain'
	nm.setQuality(args['QA'])
	nm.initialization(modID,mtype=mesh)
	nm.setOffset(args['X'],args['Z'])
	nm.setCSnamePrefix(args['CS'])
	nm.setReverseList(args['reverseList'])
	nm.setHasBed(args['bed'])
	nm.buildModel()
