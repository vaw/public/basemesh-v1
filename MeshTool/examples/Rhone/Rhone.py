#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL, MESH2DM

class Rhone (MESHMODEL):
	def __init__ (self, directory,
		domainFile=None,
		maxArea=100.0,
		quality=30.0,
		elevMeshFile=None,
		mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('rhone_prototype',mType)
		# store input argument as member variable
		self.__A = maxArea
		self.__q = quality
		# domain information
		try:
			self.__domain = np.genfromtxt(domainFile, delimiter=',')
		except:
			print 'error reading domain file...'
			self.__domain = None
		# load the elevation mesh
		m = MESH2DM(elevMeshFile)
		self.__elevMesh = m.getElevationInterpolator()
		# activate bandwidth reduction
		self.setReduction(True)

	def elevationFunction (self, x, y):
		return self.__elevMesh(x,y) if self.__elevMesh else 0.0

	# this function actually does all the work
	def build (self):
		nodes = []
		for n in self.__domain:
			nodes.append( NODE(n[0],n[1]) )
		# now define the domain
		self.addLine(nodes,True)
		# add some stringdefs
		#self.addStringdef( 'inflow',[nodes[-2],nodes[0]] )
		#self.addStringdef( 'zero',nodes[4:85] )
		# set triangle options
		self.setQuality(self.__q)
		self.setOption('-Da%f'%self.__A)
		# let's go...
		self.createMesh()
		
print '*** Rhone mesh ***'
wd = WORKINGDIRECTORY('rhone_prototype')
icold = Rhone(wd,
	maxArea=1e1,
	quality=30.0,
	domainFile='domain.csv',
	elevMeshFile='Rhone_ElvMesh.2dm',
	mType='plane3x'
	)
icold.build()