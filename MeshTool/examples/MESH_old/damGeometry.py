import math, time, os, subprocess
import sys, getopt
from meshModel import NODE, MESHMODEL

class DAMGEOMETRY (MESHMODEL):
	def __init__ (self, directory,
		meshName='DamReservoirSystem',
		height=5,
		volume=5e4,
		crestWidth=6.0,
		embankmentSlope=2.5,
		alpha=1.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		self.__modelID = meshName
		# this class does all the meshing stuff
		self.initialization(self.__modelID)
		# input arguments
		self.__Hd = height
		self.__V = volume
		self.__wc = crestWidth
		self.__se = embankmentSlope
		self.__alpha = alpha
		# constants, so far. evetnually make dependent on other variables
		# initial breach geometry -> trapezoid
		self.__Y0 = 0.9 # initial breach level, relative to dam height
		self.__B0 = 1.0 # initial breach width at the bottom of the breach, relative to initial breach depth
		self.__beta = 1.0 # initial breach side slope
		# dam-reservoir system
		# make the reservoir horizontal shape quadratic
		length = self.__solveQGL(1.0,0.5*self.__alpha*self.__se*self.__Hd,-self.__alpha*self.__V/self.__Hd)
		self.__ld = length/self.__Hd # dam length, relative to dam height
		self.__Vcorr = self.__V - 0.5*self.__ld*self.__se*self.__Hd**3 # the part of the reservoir volume above the dam's embankment is neglected for the V-h relation
		self.__maxSurface = self.__alpha * self.__Vcorr / self.__Hd # reservoir surface at full level
		# calculate all the important coordinates
		# x-coordinates of the nodes
		self.__x3 = -0.5*self.__wc
		self.__x2 = self.__x3 - self.__se*self.__Hd
		self.__x2i = self.__x3 - self.__se*self.__Hd*(1.0-self.__Y0)
		self.__x1 = self.__x2 - self.__maxSurface/(self.__ld*self.__Hd)
		self.__x4 = 0.5*self.__wc
		self.__x5 = self.__x4 + self.__se*self.__Hd
		self.__x5i = self.__x4 + self.__se*self.__Hd*(1.0-self.__Y0)
		# y-coordinates of the nodes
		initialBreachDepth = self.__Hd*(1.0-self.__Y0)
		self.__ym = -0.5*self.__ld*self.__Hd
		self.__ymi = -0.5*self.__B0*initialBreachDepth
		self.__ymmi = self.__ymi - self.__beta*initialBreachDepth
		self.__yp = 0.5*self.__ld*self.__Hd
		self.__ypi = 0.5*self.__B0*initialBreachDepth
		self.__yppi = self.__ypi + self.__beta*initialBreachDepth

	def getUpstreamFoot (self):
		return self.__x2

	# process everything
	def build (self):
		# defining the nodes
		m1 = NODE(self.__x1,self.__ym)
		c1 = NODE(self.__x1,0.0)
		p1 = NODE(self.__x1,self.__yp)
		m2 = NODE(self.__x2,self.__ym)
		#c2 = NODE(self.__x2,0.0)
		p2 = NODE(self.__x2,self.__yp)
		m3 = NODE(self.__x3,self.__ym)
		p3 = NODE(self.__x3,self.__yp)
		m4 = NODE(self.__x4,self.__ym)
		p4 = NODE(self.__x4,self.__yp)
		m5 = NODE(self.__x5,self.__ym)
		c5 = NODE(self.__x5,0.0)
		p5 = NODE(self.__x5,self.__yp)
		im2 = NODE(self.__x2i,self.__ymi)
		#ic2 = NODE(self.__x2i,0.0)
		ip2 = NODE(self.__x2i,self.__ypi)
		im3 = NODE(self.__x3,self.__ymmi)
		ip3 = NODE(self.__x3,self.__yppi)
		im4 = NODE(self.__x4,self.__ymmi)
		ip4 = NODE(self.__x4,self.__yppi)
		im5 = NODE(self.__x5i,self.__ymi)
		#ic5 = NODE(self.__x5i,0.0)
		ip5 = NODE(self.__x5i,self.__ypi)
		# now define the breaklines
		self.addLine([m1,c1,p1,p2,p3,p4,p5,c5,m5,m4,m3,m2],True)
		self.addLine([m2,p2])
		self.addLine([m3,im3])
		self.addLine([ip3,p3])
		self.addLine([m4,im4])
		self.addLine([ip4,p4])
		self.addLine([im2,im3,im4,im5])
		self.addLine([ip2,ip3,ip4,ip5])
		self.addLine([im2,ip2,ip5,im5],True)
		self.addLine([c1,c5])
		# and the regions with max area and material indices
		reservoirCellSize = self.__maxSurface/1e3
		damCellSize = self.__se*self.__ld*self.__Hd**2/5e3
		self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*self.__ym),maxArea=reservoirCellSize,matID=1)
		self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*self.__yp),maxArea=reservoirCellSize,matID=1)
		self.addRegion(NODE(0.5*(self.__x2+self.__x3),0.5*self.__ym),maxArea=damCellSize,matID=2)
		self.addRegion(NODE(0.5*(self.__x2+self.__x3),0.5*self.__yp),maxArea=damCellSize,matID=2)
		self.addRegion(NODE(0.0,0.5*(self.__ymmi+self.__ymi)),maxArea=damCellSize,matID=3)
		self.addRegion(NODE(0.0,0.5*self.__ymi),maxArea=damCellSize,matID=3)
		self.addRegion(NODE(0.0,0.5*self.__ypi),maxArea=damCellSize,matID=3)
		self.addRegion(NODE(0.0,0.5*(self.__yppi+self.__ypi)),maxArea=damCellSize,matID=3)
		self.addRegion(NODE(0.0,0.5*(self.__ym+self.__ymi)),maxArea=damCellSize,matID=4)
		self.addRegion(NODE(0.0,0.5*(self.__yp+self.__ypi)),maxArea=damCellSize,matID=4)
		self.addRegion(NODE(0.5*(self.__x4+self.__x5),0.5*self.__ym),maxArea=damCellSize,matID=4)
		self.addRegion(NODE(0.5*(self.__x4+self.__x5),0.5*self.__yp),maxArea=damCellSize,matID=4)
		# define stringdefs
		self.addStringdef( 'inflow', [p1,c1,m1] )
		self.addStringdef( 'outflow', [p5,c5,m5] )
		self.addStringdef( 'waterSurface', [c1,c5])
		# set the minimum triangle angle
		self.setQuality(31)

	def generate2dm(self,fixed=False):
		self.__fixed = fixed
		# let's go...
		self.createMesh()
		# deleting the triangle files
		self.deleteFiles()

	# function that describes the topography of the model
	def elevationFunction (self, x, y):
		# assign the location
		where = None
		if x < self.__x2:
			# reservoir
			where = 'reservoir'
		else:
			# dam
			if self.__x3 <= x <= self.__x4:
				# crest
				if self.__ymmi <= y <= self.__yppi:
					where = 'breach'
				else:
					where = 'crest'
			else:
				# body
				if self.__x2i <= x <= self.__x5i:
					if self.__ymmi <= y <= self.__yppi:
						m = (self.__ypi-self.__yppi)/(self.__x5i-self.__x4)
						k = self.__yppi - self.__x4*m
						if abs(y) <= k+abs(x)*m:
							where = 'breach'
						else:
							where = 'body'
					else:
						where = 'body'
				else:
					where = 'body'
		# now calculate the elevation
		if where == 'reservoir':
			if self.__alpha <= 1.0:
				return 0.0
			else:
				surface = abs(x-self.__x2)*self.__ld*self.__Hd
				coeff = self.__Vcorr/math.pow(self.__Hd,self.__alpha)
				return math.pow( surface/(self.__alpha*coeff), 1.0/(self.__alpha-1.0) )
		elif self.__fixed:
			return 0.0
		else:
			if where == 'crest':
				return self.__Hd
			elif where == 'breach':
				if self.__ymi <= y <= self.__ypi:
					return self.__Hd*self.__Y0
				else:
					if y < 0:
						p1 = NODE(self.__x3,self.__ymi,self.__Hd*self.__Y0)
						p2 = NODE(self.__x3,self.__ymmi,self.__Hd)
						p3 = NODE(self.__x4,self.__ymi,self.__Hd*self.__Y0)
					else:
						p1 = NODE(self.__x3,self.__ypi,self.__Hd*self.__Y0)
						p2 = NODE(self.__x3,self.__yppi,self.__Hd)
						p3 = NODE(self.__x4,self.__ypi,self.__Hd*self.__Y0)
					return self.__interpolateInsidePlane(p1,p2,p3,x,y)
			elif where == 'body':
				if x < 0:
					p1 = NODE(self.__x2,self.__yp,0.0)
					p2 = NODE(self.__x2,self.__ym,0.0)
					p3 = NODE(self.__x3,self.__yp,self.__Hd)
				else:
					p1 = NODE(self.__x5,self.__yp,0.0)
					p2 = NODE(self.__x5,self.__ym,0.0)
					p3 = NODE(self.__x4,self.__yp,self.__Hd)
				return self.__interpolateInsidePlane(p1,p2,p3,x,y)

	def __interpolateInsidePlane (self,P,Q,R,x,y):
		a =  -(R.getY()*Q.getZ() - P.getY()*Q.getZ() - R.getY()*P.getZ() + Q.getY()*P.getZ() + P.getY()*R.getZ() - Q.getY()*R.getZ())
		b =   P.getY()*R.getX() + Q.getY()*P.getX() + R.getY()*Q.getX() - Q.getY()*R.getX() - P.getY()*Q.getX() - R.getY()*P.getX()
		c =   Q.getZ()*R.getX() + P.getZ()*Q.getX() + R.getZ()*P.getX() - P.getZ()*R.getX() - Q.getZ()*P.getX() - Q.getX()*R.getZ()
		d = -a*P.getX() - b*P.getZ() - c*P.getY()
		if b == 0.0:
			if ((x == P.getX()) and (y == P.getY)):
				return P.getZ()
			if (x == Q.getX() and y == Q.getY()):
				return Q.getZ()
			if (x == R.getX() and y == R.getY()):
				return R.getZ()
			return -9999999 # cv: hack - to prevent division by zero - better do something other?
		return -(a*x+c*y+d) / b

	def __solveQGL (self,a,b,c):
		# compute m = (-b+sqrt(b^2-4ac))/(2a)
		try:
			det = math.sqrt(b*b - 4.0*a*c)
		except:
			return -999
		return (-b+det)/(2*a)
