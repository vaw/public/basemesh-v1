
from basement import *
from batchTools import *
from NLSolve import *
from postprocessing import *
import math
import random
from meshModel import NODE, MESHMODEL

class FREECHANNEL (MESHMODEL):
    """class defines the geometry of a trapezoidal channel with free erodable forelands"""
    def __init__ (self, directory,
        meshName='freeChannel',
        modelLength=28.5,
        modelWidth=3.2,
        channelWidth=0.8,
        channelHeight=0.13,
        channelBankSlope=1.0/1.0,
        channelBedSlope=0.004,
        numberOfCells=10000,
        randomPert=0.0):
        # call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory,absolutePrecision=1e-8)
        self.setStringdefDistance(True)
        # this class does all the meshing stuff
        self.initialization(meshName)  # This is the name used for the output-files
        # input arguments
        self.__L = modelLength
        self.__B = modelWidth
        self.__W = channelWidth
        self.__H = channelHeight
        self.__S = channelBankSlope
        self.__J = channelBedSlope
        self.__N = numberOfCells
        self.__R = randomPert
        # bank width:
        self.__BW = self.__H / self.__S
        # suface of laboratory channel:
        self.__surface = self.__L * self.__B
        # calculate all the important coordinates
        # x-coordinates of the nodes
        self.__x1 = -self.__L / 2.0
        self.__x2 = -self.__L / 2.0
        self.__x3 = -self.__L / 2.0
        self.__x4 = -self.__L / 2.0
        self.__x5 = -self.__L / 2.0
        self.__x6 = -self.__L / 2.0
        self.__x7 = self.__L / 2.0
        self.__x8 = self.__L / 2.0
        self.__x9 = self.__L / 2.0
        self.__x10 = self.__L / 2.0
        self.__x11 = self.__L / 2.0
        self.__x12 = self.__L / 2.0
        # x-coordinates of the nodes for the breaklines
        self.__xbl1 = -0.5*(self.__L / 2.0)     # nach einviertel Laenge
        self.__xbl2 = 0.0                       # Mitte
        self.__xbl3 = 0.5*(self.__L / 2.0)      # nach dreiviertel Laenge
        self.__xbf1 = -self.__L / 2.0 + 100.0    # fixed bed in the first 50 meters
        self.__xbf2 = -self.__L / 2.0 + 150.0   # bed for the next 50 meter
        self.__xbf3 = -self.__L / 2.0 + 200.0   # bed for the next 50 meter
        self.__xbf4 = -self.__L / 2.0 + 250.0   # bed for the next 50 meter
        # y-coordinates of the nodes
        self.__y1 = -self.__B / 2.0
        self.__y2 = -self.__W / 2.0 - self.__BW
        self.__y3 = -self.__W / 2.0
        self.__y4 = self.__W / 2.0
        self.__y5 = self.__W / 2.0 + self.__BW
        self.__y6 = self.__B / 2.0
        self.__y7 = -self.__B / 2.0
        self.__y8 = -self.__W / 2.0 - self.__BW
        self.__y9 = -self.__W / 2.0
        self.__y10 = self.__W / 2.0
        self.__y11 = self.__W / 2.0 + self.__BW
        self.__y12 = self.__B / 2.0
        # y-coordinates of the nodes for the breaklines
        self.__ybl1 = -self.__B / 2.0
        self.__ybl2 = self.__B / 2.0
        # triangle for sediment source
        self.__trx1 = -self.__L / 2.0 + 33.0
        self.__trx2 = -self.__L / 2.0 + 66.0
        self.__trx3 = -self.__L / 2.0 + 66.0
        self.__try1 = 0.0
        self.__try2 = -20.0
        self.__try3 = +20.0

    # process everything
    def build (self):
        # defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)
        n9 = NODE(self.__x9,self.__y9)
        n10 = NODE(self.__x10,self.__y10)
        n11 = NODE(self.__x11,self.__y11)
        n12 = NODE(self.__x12,self.__y12)
        b1 = NODE(self.__xbl1,self.__ybl1)
        b2 = NODE(self.__xbl1,self.__ybl2)
        b3 = NODE(self.__xbl2,self.__ybl1)
        b4 = NODE(self.__xbl2,self.__ybl2)
        b5 = NODE(self.__xbl3,self.__ybl1)
        b6 = NODE(self.__xbl3,self.__ybl2)
        bf1 = NODE(self.__xbf1,self.__ybl1)
        bf2 = NODE(self.__xbf1,self.__ybl2)
        bf3 = NODE(self.__xbf2,self.__ybl1)
        bf4 = NODE(self.__xbf2,self.__ybl2)
        bf5 = NODE(self.__xbf3,self.__ybl1)
        bf6 = NODE(self.__xbf3,self.__ybl2)
        bf7 = NODE(self.__xbf4,self.__ybl1)
        bf8 = NODE(self.__xbf4,self.__ybl2)
        tr1 = NODE(self.__trx1,self.__try1)
        tr2 = NODE(self.__trx2,self.__try2)
        tr3 = NODE(self.__trx3,self.__try3)
        # now define the breaklines
        self.addLine([n1,n2,n3,n4,n5,n6,n12,n11,n10,n9,n8,n7],True) # model boundary
        self.addLine([tr1,tr2,tr3],True)                            # triangle for external sediment source
        self.addLine([n2,n8])                                       
        self.addLine([n3,n9])
        self.addLine([n4,n10])
        self.addLine([n5,n11])
        self.addLine([b1,b2])
        self.addLine([b3,b4])
        self.addLine([b5,b6])
        self.addLine([bf1,bf2])
        self.addLine([bf3,bf4])
        self.addLine([bf5,bf6])
        self.addLine([bf7,bf8])
        # and the regions with max area and material indices
        channelCellSize = self.__surface/self.__N
        bankCellSize = 0.3*self.__surface/self.__N
        forelandCellSize = 0.9*self.__surface/self.__N
        # triangle for external sediment source
        self.addRegion(NODE(0.5*(self.__trx1+self.__trx2),0.5*(self.__try2+self.__try3)),maxArea=channelCellSize,matID=7) # triangle for external sediment source
        # foreland left:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=6) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=6) # fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=6) # fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf3+self.__xbf4),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=10) # -0.05 m fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf4+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__x7),0.5*(self.__y1+self.__y2)),maxArea=forelandCellSize,matID=3)        
        # bank left:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=5) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=5) # fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=11) # -0.05 m fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf3+self.__xbf4),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=12) # -0.10 m fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf4+self.__xbl1),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__x7),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        # channel bed:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbf1)-40.0,0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=4) # fixed bed layer at channel entry (sediment source)
        self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=8) # -0.05 m fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=9) # -0.10 m fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf3+self.__xbf4),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=1) # mobile bed
        self.addRegion(NODE(0.5*(self.__xbf4+self.__xbl1),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__x7),0.5*(self.__y3+self.__y4)),maxArea=channelCellSize,matID=1)
        # bank right:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=5) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=5) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=11) # -0.05 m fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf3+self.__xbf4),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=12) # -0.10 m fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf4+self.__xbl1),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__x7),0.5*(self.__y4+self.__y5)),maxArea=bankCellSize,matID=2)
        # foreland right:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=6) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=6) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=6) # fixed bed layer at channel entry
        self.addRegion(NODE(0.5*(self.__xbf3+self.__xbf4),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=10) # -0.05 m fixed bed layer
        self.addRegion(NODE(0.5*(self.__xbf4+self.__xbl1),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__x7),0.5*(self.__y5+self.__y6)),maxArea=forelandCellSize,matID=3)
        # define stringdefs
        self.addStringdef( 'inflow', [n2,n5] )
        self.addStringdef( 'outflow', [n8,n11] )
        self.addStringdef( 'bed_inflow', [n3,n4] )
        self.addStringdef( 'bed_outflow', [n8,n11] )
        self.addStringdef( 'CS0', [bf1,bf2])
        self.addStringdef( 'CS1', [b1,b2])
        self.addStringdef( 'CS2', [b3,b4])
        self.addStringdef( 'CS3', [b5,b6])

        # set the minimum triangle angle
        self.setQuality(31)
        # let's go...
        self.createMesh()
        # deleting the triangle files
        self.deleteFiles()

    # function that describes the topography of the model
    def elevationFunction (self, x, y):
        # assign location
        where = None
        if self.__y1 <= y <= self.__y2:
             where = 'foreland_left'
        elif self.__y2 < y < self.__y3:
             where = 'bank_left'
        elif (self.__y3 <= y <= self.__y4) and (x <= self.__L/2.0 -10.0):
             where = 'bed'
        elif (self.__y3 <= y <= self.__y4) and (x > self.__L/2.0 -10.0): # last 10 meters no bed pertubation
             where = 'bed_end'
        elif self.__y4 < y < self.__y5:
             where = 'bank_right'
        elif self.__y5 <= y <= self.__y6:
             where = 'foreland_right'

        # calculate elevation
        if where == 'bed':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + random.random()*self.__R
        elif where == 'bed_end':
            return ((self.__L/2.0) * self.__J) - (x * self.__J)
        elif where == 'bank_left':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + (-y+self.__y3)*self.__S
        elif where == 'bank_right':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + (y-self.__y4)*self.__S
            #       Koordinaten Nullpunkt          Sohlgefaelle     bank slope
        elif where == 'foreland_left':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + self.__H
        elif where == 'foreland_right':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + self.__H
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999
