#!/usr/bin/env python

# by Samuel J. Peter, 2015
# mailto: peter@vaw.baug.ethz.ch

from batchTools import *
from meshModel import NODE, MESHMODEL
import argparse, math

class DELTAEXPGRID (MESHMODEL):
	"""class defines the geometry of a trapezoidal channel"""
	def __init__ (self, directory,
		channelLength,
		channelWidth,
		channelSlope,
		damSlope,
		damHeight,
		kStrickler,
		nQP):
		# call init of the meshmodel (base)-class, this class does all the meshing stuff
		MESHMODEL.__init__(self, directory)
		# store parameters
		self.__lc = channelLength
		self.__wc = channelWidth
		self.__sc = channelSlope
		self.__hd = damHeight
		self.__sd = damSlope
		self.__ks = kStrickler
		self.__N = nQP

	def buildModel(self):
		# needed coordinates
		xU,xM,xD = (0.0, (self.__lc*(self.__sc+self.__sd)-self.__hd)/(self.__sc+self.__sd), self.__lc)
		yL,yR = (0.5*self.__wc, -0.5*self.__wc)
		# break lines
		self.addLine( [NODE(xU,yL),NODE(xU,yR)], name='inflow' )
		self.addLine( [NODE(xM,yL),NODE(xM,yR)], name='bottom' )
		self.addLine( [NODE(xD,yL),NODE(xD,yR)], name='outflow' )
		# boundary
		self.addLine( [NODE(xU,yL),NODE(xU,yR),NODE(xD,yR),NODE(xD,yL),NODE(xU,yL)] )			
		# matid only for illustration purposes
		self.addRegion(NODE(0.5*(xU+xM),0.0),matID=1,maxArea=self.__lc*self.__wc/1e3)
		self.addRegion(NODE(0.5*(xM+xD),0.0),matID=2,maxArea=self.__lc*self.__wc/1e3)
		# add stringdefs
		self.addStringdef('inflow_boundary',[NODE(xU,yL),NODE(xU,yR)])
		self.addStringdef('outflow_boundary',[NODE(xD,yL),NODE(xD,yR)])
		# define number of cross sections
		dx = self.__lc/self.__N
		for ii in range(self.__N+1):
			self.addCrossSection(NODE(ii*dx,0.0))
		# define the friction ranges
		friction = {'default': self.__ks}
		self.setFriction(friction)
		# write the .bmg file
		self.createMesh()

	def elevationFunction(self, x, y):
		zChannel = self.__sc*(self.__lc-x)
		zDam = self.__hd+self.__sd*(x-self.__lc)
		return max(zChannel,zDam)


if __name__ != 'main':
	parser = argparse.ArgumentParser(prog='ChannelGen',
		formatter_class=argparse.RawDescriptionHelpFormatter,
		description='Generation of computational grid to model delta development.')
	parser.add_argument('-LC', metavar='channelLength', default=50, type=float, help='total length of the channel [m] (default: %(default)s)')
	parser.add_argument('-WC', metavar='channelWidth', default=2, type=float, help='bed width of the channel [m] (default: %(default)s)')
	parser.add_argument('-SC', metavar='channelSlope', default=0.01, type=float, help='slope of the channel [-] (default: %(default)s)')
	parser.add_argument('-HD', metavar='damHeight', default=0.2, type=float, help='height of the dam downstream [m] (default: %(default)s)')
	parser.add_argument('-SD', metavar='damSlope', default=0.5, type=float, help='slope of the dam embankment [-] (default: %(default)s)')
	parser.add_argument('-KS', metavar='kStrickler', default=35, type=float, help='strickler value of bed [...] (default: %(default)s)')
	parser.add_argument('-N', metavar='nCrossSections', default=100, type=int, help='number of cross sections to generate [-] (default: %(default)s)')
	parser.add_argument('--2D', dest='2d', action='store_true', help='tag to generate 2d mesh (default: %(default)s)')
	args = vars(parser.parse_args())
	# create model directory
	wd = WORKINGDIRECTORY('DeltaExperimentGrid')
	# cretae the mesh model
	nm = DELTAEXPGRID(wd,
		channelLength = args['LC'],
		channelWidth = args['WC'],
		channelSlope = args['SC'],
		damHeight = args['HD'],
		damSlope = args['SD'],
		kStrickler = args['KS'],
		nQP = args['N'])
	modID = 'geometry'
	mesh = 'plane2x' if args['2d'] else 'chain'
	nm.initialization(modID,mtype=mesh)
	nm.setCSnamePrefix('CS')
	nm.setOffset(0.0,0.0)
	nm.setQuality(33.0)
	nm.buildModel()