# Island Deposit in Lab Channel

import math
from meshModel import NODE, MESHMODEL

class ISLANDDEPOSIT (MESHMODEL):
    """class defines the geometry of a trapezoidal laboratory channel with narrowing or deposit"""
    def __init__ (self, directory, meshName, channelLength, channelWidth, channelHeight, channelBankSlope, channelBedSlope, numberOfCells, depositLength, depositWidth, depositHeight, depositBankSlope, depositYoffset):
		# call init of the meshmodel (base)-class
        MESHMODEL.__init__(self, directory)
        self.setStringdefDistance(True)
        # this class does all the meshing stuff
        self.initialization(meshName) # This is the name used for the output-files
        self.__Epsilon = 0.00001      # [used length units e.g. meter] Epsilon is used for function accuratePointInTriangle to determine if point is on edge of triangle
        # input arguments
        self.__L = channelLength
        self.__W = channelWidth
        self.__H = channelHeight
        self.__S = channelBankSlope
        self.__J = channelBedSlope
        self.__N = numberOfCells
        self.__dL= depositLength
        self.__dW= depositWidth
        self.__dH= depositHeight
        self.__dS= depositBankSlope
        self.__dY= depositYoffset # y-offset of gravel deposit
        # bank width:
        self.__BW = self.__H / self.__S
        # suface of laboratory channel:
        self.__surface = self.__L * (self.__W + 2.0*self.__BW)
		# calculate all the important coordinates
		# x-coordinates of the nodes
        # channel:
        self.__x1 = -self.__L / 2.0
        self.__x2 = -self.__L / 2.0
        self.__x3 = -self.__L / 2.0
        self.__x4 = -self.__L / 2.0
        self.__x5 = self.__L / 2.0
        self.__x6 = self.__L / 2.0
        self.__x7 = self.__L / 2.0
        self.__x8 = self.__L / 2.0
        # gravel deposit:
        self.__xz1 = -self.__dL/2.0
        self.__xz2 = -self.__dL/2.0
        self.__xz3 = self.__dL/2.0
        self.__xz4 = self.__dL/2.0
        self.__xz5 = -self.__dL/2.0 + self.__dS*self.__dH
        self.__xz6 = -self.__dL/2.0 + self.__dS*self.__dH
        self.__xz7 = self.__dL/2.0 - self.__dS*self.__dH
        self.__xz8 = self.__dL/2.0 - self.__dS*self.__dH
        # breaklines:
        self.__xb0 = -7.0
        self.__xb1 = -5.0
        self.__xb3 = -1.5
        self.__xb5 = -1.0
        self.__xb7 = -0.5
        self.__xb9 = 0.0
        self.__xb11 = 0.5
        self.__xb13 = 1.0
        self.__xb15 = 1.5
        self.__xb17 = 2.0
        self.__xb19 = 2.5
        self.__xb21 = 3.5
        self.__xb23 = 4.5
        self.__xb25 = 6.5
        self.__xb27 = 8.5
        self.__xb29 = 10.5
        # y-coordinates of the nodes
        # channel:
        self.__y1 = self.__BW
        self.__y2 = 0.0
        self.__y3 = -self.__W
        self.__y4 = -self.__W - self.__BW
        self.__y5 = self.__BW
        self.__y6 = 0.0
        self.__y7 = -self.__W
        self.__y8 = -self.__W - self.__BW
        # gravel deposit:
        self.__yz1 = -self.__dY
        self.__yz2 = self.__dY - self.__dW
        self.__yz3 = self.__dY - self.__dW
        self.__yz4 = -self.__dY
        self.__yz5 = self.__dY - self.__dH*self.__dS
        self.__yz6 = self.__dY - self.__dW + self.__dH*self.__dS
        self.__yz7 = self.__dY - self.__dW + self.__dH*self.__dS
        self.__yz8 = self.__dY - self.__dH*self.__dS
        # breaklines (here we need just two y-values!)
        self.__yb1 = self.__BW
        self.__yb2 = -self.__W - self.__BW

	# process everything
    def build (self):
		# defining the nodes
        # channel:
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)
        # gravel deposit:
        z1 = NODE(self.__xz1,self.__yz1)
        z2 = NODE(self.__xz2,self.__yz2)
        z3 = NODE(self.__xz3,self.__yz3)
        z4 = NODE(self.__xz4,self.__yz4)
        z5 = NODE(self.__xz5,self.__yz5)
        z6 = NODE(self.__xz6,self.__yz6)
        z7 = NODE(self.__xz7,self.__yz7)
        z8 = NODE(self.__xz8,self.__yz8)
        # breaklines:
        b00 = NODE(self.__xb0,self.__yb1)
        b0 = NODE(self.__xb0,self.__yb2)
        b1 = NODE(self.__xb1,self.__yb1)
        b2 = NODE(self.__xb1,self.__yb2)
        b3 = NODE(self.__xb3,self.__yb1)
        b4 = NODE(self.__xb3,self.__yb2)
        b5 = NODE(self.__xb5,self.__yb1)
        b6 = NODE(self.__xb5,self.__yb2)
        b7 = NODE(self.__xb7,self.__yb1)
        b8 = NODE(self.__xb7,self.__yb2)
        b9 = NODE(self.__xb9,self.__yb1)
        b10 = NODE(self.__xb9,self.__yb2)
        b11 = NODE(self.__xb11,self.__yb1)
        b12 = NODE(self.__xb11,self.__yb2)
        b13 = NODE(self.__xb13,self.__yb1)
        b14 = NODE(self.__xb13,self.__yb2)
        b15 = NODE(self.__xb15,self.__yb1)
        b16 = NODE(self.__xb15,self.__yb2)
        b17 = NODE(self.__xb17,self.__yb1)
        b18 = NODE(self.__xb17,self.__yb2)
        b19 = NODE(self.__xb19,self.__yb1)
        b20 = NODE(self.__xb19,self.__yb2)
        b21 = NODE(self.__xb21,self.__yb1)
        b22 = NODE(self.__xb21,self.__yb2)
        b23 = NODE(self.__xb23,self.__yb1)
        b24 = NODE(self.__xb23,self.__yb2)
        b25 = NODE(self.__xb25,self.__yb1)
        b26 = NODE(self.__xb25,self.__yb2)
        b27 = NODE(self.__xb27,self.__yb1)
        b28 = NODE(self.__xb27,self.__yb2)
        b29 = NODE(self.__xb29,self.__yb1)
        b30 = NODE(self.__xb29,self.__yb2)
        L1 = NODE(self.__xb0,-self.__W/2.0)
        L2 = NODE(self.__x6,-self.__W/2.0)
        # now define the breaklines
        # channel:
        self.addLine([n1,n2,n3,n4,n8,n7,n6,n5],True) # model boundary
        self.addLine([n2,n6])
        self.addLine([n3,n7])
        # gravel deposit:
        self.addLine([z1,z2])
        self.addLine([z2,z3])
        self.addLine([z3,z4])
        self.addLine([z4,z1])
        self.addLine([z5,z6])
        self.addLine([z6,z7])
        self.addLine([z7,z8])
        self.addLine([z8,z5])
        self.addLine([z1,z5])
        self.addLine([z2,z6])
        self.addLine([z3,z7])
        self.addLine([z4,z8])
        # observation breaklines:
        self.addLine([b0,b00])
        self.addLine([b1,b2])
        self.addLine([b3,b4])
        #self.addLine([b5,z5,z6,b6])
        #self.addLine([b7,b8])
        self.addLine([b9,b10])
        #self.addLine([b11,b12])
        #self.addLine([b13,z8,z7,b14])
        self.addLine([b15,b16])
        self.addLine([b17,b18])
        self.addLine([b19,b20])
        self.addLine([b21,b22])
        self.addLine([b23,b24])
        self.addLine([b25,b26])
        self.addLine([b27,b28])
        # breakline for mesh density
        self.addLine([b29,b30])
        self.addLine([L1,L2])
		# and the regions with max area and material indices
        tinyCellSize = 0.05*self.__surface/self.__N
        verySmallCellSize = 0.15*self.__surface/self.__N
        SmallCellSize = 0.3*self.__surface/self.__N
        smallCellSize = 0.5*self.__surface/self.__N
        MediumCellSize = 0.75*self.__surface/self.__N
        mediumCellSize = 1.0*self.__surface/self.__N
        normalCellSize = 1.5*self.__surface/self.__N
        bigCellSize = 2.0*self.__surface/self.__N
        # bank left (von oben nach unten):
        self.addRegion(NODE(0.5*(self.__x1+self.__xb0),0.5*(self.__y2+self.__y1)),maxArea=mediumCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb0+self.__xb1),0.5*(self.__y2+self.__y1)),maxArea=mediumCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb1+self.__xb3),0.5*(self.__y2+self.__y1)),maxArea=mediumCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb3+self.__xb9),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        #self.addRegion(NODE(0.5*(self.__xb5+self.__xb7),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        #self.addRegion(NODE(0.5*(self.__xb9+self.__xb15),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        #self.addRegion(NODE(0.5*(self.__xb13+self.__xb11),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb15+self.__xb9),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb17+self.__xb15),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb19+self.__xb17),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)        
        self.addRegion(NODE(0.5*(self.__xb21+self.__xb19),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb23+self.__xb21),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb25+self.__xb23),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb27+self.__xb25),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xb29+self.__xb27),0.5*(self.__y2+self.__y1)),maxArea=verySmallCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__x5+self.__xb29),0.5*(self.__y2+self.__y1)),maxArea=SmallCellSize,matID=2)
        # channel bed oben:
        self.addRegion(NODE(0.5*(self.__x1+self.__xb0),0.5*(self.__y2+self.__y3)),maxArea=bigCellSize,matID=1)       
        # channel bed, left (von oben nach unten):
        self.addRegion(NODE(0.5*(self.__xb0+self.__xb1),0.5*(self.__y2-self.__W/2.0)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb1+self.__xb3),0.5*(self.__y2-self.__W/2.0)),maxArea=mediumCellSize,matID=1)
        #self.addRegion(NODE(0.5*(self.__xb3+self.__xb5),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb3+self.__xz1),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        #self.addRegion(NODE(0.5*self.__xb7,0.5*(self.__y2+self.__y3)),maxArea=SmallCellSize,matID=1)
        #self.addRegion(NODE(0.5*self.__xb11,0.5*(self.__y2+self.__y3)),maxArea=SmallCellSize,matID=1)
        #self.addRegion(NODE(self.__xb11-0.5*abs(self.__xb13-self.__xb11),0.5*(self.__y2+self.__y3)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xz3+self.__xb15),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb15+self.__xb17),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb17+self.__xb19),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb19+self.__xb21),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb21+self.__xb23),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb23+self.__xb25),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb25+self.__xb27),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb27+self.__xb29),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb29+self.__x6),0.5*(self.__y2-self.__W/2.0)),maxArea=SmallCellSize,matID=1)
        # channel bed, right (von oben nach unten):
        self.addRegion(NODE(0.5*(self.__xb0+self.__xb1),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb1+self.__xb3),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        #self.addRegion(NODE(0.5*(self.__xb3+self.__xb5),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb3+self.__xz1),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        #self.addRegion(NODE(0.5*self.__xb7,0.5*(self.__y2+self.__y3)),maxArea=normalCellSize,matID=1)
        #self.addRegion(NODE(0.5*self.__xb11,0.5*(self.__y2+self.__y3)),maxArea=normalCellSize,matID=1)
        #self.addRegion(NODE(self.__xb11-0.5*abs(self.__xb13-self.__xb11),0.5*(self.__y2+self.__y3)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xz3+self.__xb15),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb15+self.__xb17),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb17+self.__xb19),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb19+self.__xb21),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb21+self.__xb23),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb23+self.__xb25),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb25+self.__xb27),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb27+self.__xb29),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xb29+self.__x6),0.5*(self.__y3-self.__W/2.0)),maxArea=normalCellSize,matID=1)
        # bank right (von oben nach unten):
        self.addRegion(NODE(0.5*(self.__x1+self.__xb0),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb0+self.__xb1),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb1+self.__xb3),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb3+self.__xb9),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        #self.addRegion(NODE(0.5*(self.__xb5+self.__xb7),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        #self.addRegion(NODE(0.5*(self.__xb9+self.__xb15),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        #self.addRegion(NODE(0.5*(self.__xb13+self.__xb11),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb15+self.__xb9),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb17+self.__xb15),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb19+self.__xb17),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)        
        self.addRegion(NODE(0.5*(self.__xb21+self.__xb19),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb23+self.__xb21),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb25+self.__xb23),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb27+self.__xb25),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xb29+self.__xb27),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__x5+self.__xb29),0.5*(self.__y3+self.__y4)),maxArea=bigCellSize,matID=3)
        # gravel deposit
        # top:
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz5),0.5*(self.__yz6 + self.__yz5)),maxArea=verySmallCellSize,matID=4)
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz8),0.5*(self.__yz6 + self.__yz5)),maxArea=verySmallCellSize,matID=4)
        # right side:
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz5),0.5*(self.__yz3 + self.__yz7)),maxArea=verySmallCellSize,matID=4)
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz8),0.5*(self.__yz3 + self.__yz7)),maxArea=verySmallCellSize,matID=4)
        # left side:
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz5),0.5*(self.__yz8 + self.__yz4)),maxArea=verySmallCellSize,matID=4)
        self.addRegion(NODE(0.5*(self.__xb9+self.__xz8),0.5*(self.__yz8 + self.__yz4)),maxArea=verySmallCellSize,matID=4)
        # front:
        self.addRegion(NODE(0.5*(self.__xz1 + self.__xz5),0.5*(self.__yz2 + self.__yz1)),maxArea=verySmallCellSize,matID=4)
        # back:
        self.addRegion(NODE(0.5*(self.__xz4 + self.__xz8),0.5*(self.__yz2 + self.__yz1)),maxArea=verySmallCellSize,matID=4)

		# define stringdefs
        self.addStringdef( 'inflow', [n1,n4] )
        self.addStringdef( 'outflow', [n5,n8] )
        self.addStringdef( 'b1-b2', [NODE(self.__xb1,self.__yb1),NODE(self.__xb1,self.__yb2)] )
        self.addStringdef( 'b3-b4', [NODE(self.__xb3,self.__yb1),NODE(self.__xb3,self.__yb2)] )
        #self.addStringdef( 'b5-b6', [NODE(self.__xb5,self.__yb1),NODE(self.__xb5,self.__yb2)] )
        #self.addStringdef( 'b7-b8', [NODE(self.__xb7,self.__yb1),NODE(self.__xb7,self.__yb2)] )
        self.addStringdef( 'b9-b10', [NODE(self.__xb9,self.__yb1),NODE(self.__xb9,self.__yb2)] )
        #self.addStringdef( 'b11-b12', [NODE(self.__xb11,self.__yb1),NODE(self.__xb11,self.__yb2)] )
        #self.addStringdef( 'b13-b14', [NODE(self.__xb13,self.__yb1),NODE(self.__xb13,self.__yb2)] )
        self.addStringdef( 'b15-b16', [NODE(self.__xb15,self.__yb1),NODE(self.__xb15,self.__yb2)] )
        self.addStringdef( 'b17-b18', [NODE(self.__xb17,self.__yb1),NODE(self.__xb17,self.__yb2)] )
        self.addStringdef( 'b19-b20', [NODE(self.__xb19,self.__yb1),NODE(self.__xb19,self.__yb2)] )
        self.addStringdef( 'b21-b22', [NODE(self.__xb21,self.__yb1),NODE(self.__xb21,self.__yb2)] )
        self.addStringdef( 'b23-b24', [NODE(self.__xb23,self.__yb1),NODE(self.__xb23,self.__yb2)] )
        self.addStringdef( 'b25-b26', [NODE(self.__xb25,self.__yb1),NODE(self.__xb25,self.__yb2)] )
        self.addStringdef( 'b27-b28', [NODE(self.__xb27,self.__yb1),NODE(self.__xb27,self.__yb2)] )
        # additional stringdefs for gravel deposit
        self.addStringdef( 'top', [NODE(self.__xz1,self.__yz1),NODE(self.__xz2,self.__yz2)] )
        self.addStringdef( 'down', [NODE(self.__xz4,self.__yz4),NODE(self.__xz3,self.__yz3)] )
        self.addStringdef( 'left', [NODE(self.__xz1,self.__yz1),NODE(self.__xz4,self.__yz4)] )
        self.addStringdef( 'right', [NODE(self.__xz2,self.__yz2),NODE(self.__xz3,self.__yz3)] )

        # set the minimum triangle angle
        self.setQuality(29)
		
    def generate2dm(self,fixed=False):
        self.__fixed = fixed
		# let's go...
        self.createMesh()
        # deleting the triangle files
        self.deleteFiles()


    # fixed bed and mobile bed (combined without headache)
    # function describing the topography of the trapezoidal channel with gravel deposit topography
    def elevationFunction (self, x, y):
        # assign location
        where = None        
        if self.__fixed:
            if self.__y1 >= y > self.__y2:
                where = 'bank_left'
            elif self.__y2 >= y >= self.__y3:
                 where = 'bed'
            elif self.__y3 > y >= self.__y4:
                 where = 'bank_right'
        else:
            if self.__y1 >= y >= self.__y2:
                where = 'bank_left'
            elif self.__y3 >= y >= self.__y4:
                 where = 'bank_right'
            elif (self.__y3 <= y <= self.__y2) and (self.__xz1 >= x >= self.__x2):
                where = 'bed' # oben
            elif (self.__y3 <= y <= self.__y2) and (self.__x6 >= x >= self.__xz4):
                where = 'bed' # unten
            elif (self.__y3 <= y <= self.__yz2) and (self.__xz3 >= x >= self.__xz2):
                where = 'bed' # mitte
            elif (self.__yz6 <= y <= self.__yz5) and (self.__xz8 >= x >= self.__xz5):
                where = 'deposit_top'
            elif (self.__yz5 < y < self.__yz1) and (self.__xz8 >= x >= self.__xz5):
                where = 'deposit_left'
            elif (self.__yz2 < y < self.__yz6) and (self.__xz7 >= x >= self.__xz6):
                where = 'deposit_right'
            elif (self.__yz6 <= y <= self.__yz5) and (self.__xz5 > x > self.__xz1):
                where = 'deposit_front'
            elif (self.__yz7 <= y <= self.__yz8) and (self.__xz4 > x > self.__xz8):
                where = 'deposit_back'
            elif (self.__yz8 < y < self.__yz4) and (self.__xz4 >= x >= self.__xz8):
                # kleines Quadrat I
                pt = NODE(x,y)
                nz4 = NODE(self.__xz4,self.__yz4)
                nz8 = NODE(self.__xz8,self.__yz8)
                nz48 = NODE(self.__xz8,self.__yz4)
                if self.__accuratePointInTriangle(nz4, nz8, nz48, pt):
                    where = 'deposit_left'
                else:
                    where = 'deposit_back'
            elif (self.__yz3 < y < self.__yz7) and (self.__xz3 >= x >= self.__xz7):
                # kleines Quadrat II
                pt = NODE(x,y)
                nz3 = NODE(self.__xz3,self.__yz3)
                nz7 = NODE(self.__xz7,self.__yz7)
                nz37 = NODE(self.__xz7,self.__yz3)
                if self.__accuratePointInTriangle(nz3, nz7, nz37, pt):
                    where = 'deposit_right'
                else:
                    where = 'deposit_back'            
            elif (self.__yz2 < y < self.__yz6) and (self.__xz6 >= x >= self.__xz2):
                # kleines Quadrat III
                pt = NODE(x,y)
                nz2 = NODE(self.__xz2,self.__yz2)
                nz6 = NODE(self.__xz6,self.__yz6)
                nz26 = NODE(self.__xz6,self.__yz2)
                if self.__accuratePointInTriangle(nz2, nz6, nz26, pt):
                    where = 'deposit_right'
                else:
                    where = 'deposit_front'
            elif (self.__yz5 < y < self.__yz1) and (self.__xz5 >= x >= self.__xz1):
                # kleines Quadrat IV
                pt = NODE(x,y)
                nz1 = NODE(self.__xz1,self.__yz1)
                nz5 = NODE(self.__xz5,self.__yz5)
                nz15 = NODE(self.__xz5,self.__yz1)
                if self.__accuratePointInTriangle(nz1, nz5, nz15, pt):
                    where = 'deposit_left'
                else:
                    where = 'deposit_front' 

        # calculate elevation
        if where == 'bed':
            return ((self.__L/2.0) * self.__J) - (x * self.__J)
        elif where == 'bank_left':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + y*self.__S
        elif where == 'bank_right':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) - (y-self.__y3)*self.__S
            #       Koordinaten Nullpunkt          Sohlgefaelle     bank slope
        elif where == 'deposit_top':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + self.__dH
        elif where == 'deposit_right':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) - (self.__yz3-y)/self.__dS
        elif where == 'deposit_left':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + (self.__yz1-y)/self.__dS
        elif where == 'deposit_front':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) - (self.__xz1-x)/self.__dS
        elif where == 'deposit_back':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + (self.__xz4-x)/self.__dS
        else:
            print "There is no elevation found for x = ", x, " and y = ", y, "."
            print "Therefore, the elevation is set to -99"
            return -99


    def __sign (self, p1, p2, p3):
        return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY())

    def __PointInTriangle (self, pt, v1, v2, v3):
        b1 = self.__sign(pt, v1, v2) < 0.0
        b2 = self.__sign(pt, v2, v3) < 0.0
        b3 = self.__sign(pt, v3, v1) < 0.0
        return ((b1 == b2) and (b2 == b3))

    def __interpolateInsidePlane (self,P,Q,R,x,y):
        a =  -(R.getY()*Q.getZ() - P.getY()*Q.getZ() - R.getY()*P.getZ() + Q.getY()*P.getZ() + P.getY()*R.getZ() - Q.getY()*R.getZ())
        b =   P.getY()*R.getX() + Q.getY()*P.getX() + R.getY()*Q.getX() - Q.getY()*R.getX() - P.getY()*Q.getX() - R.getY()*P.getX()
        c =   Q.getZ()*R.getX() + P.getZ()*Q.getX() + R.getZ()*P.getX() - P.getZ()*R.getX() - Q.getZ()*P.getX() - Q.getX()*R.getZ()
        d = -a*P.getX() - b*P.getZ() - c*P.getY()
        if b == 0.0:
            if ((x == P.getX()) and (y == P.getY)):
                return P.getZ()
            if (x == Q.getX() and y == Q.getY()):
                return Q.getZ()
            if (x == R.getX() and y == R.getY()):
                return R.getZ()
            return -9999999 # cv: hack - to prevent division by zero - better do something other?
        return -(a*x+c*y+d) / b


    def __distanceSquarePointToSegment(self, n1, n2, pt):
        p1_p2_squareLength = (n2.getX() - n1.getX())*(n2.getX() - n1.getX()) + (n2.getY() - n1.getY())*(n2.getY() - n1.getY())
        dotProduct = ((pt.getX() - n1.getX())*(n2.getX() - n1.getX()) + (pt.getY() - n1.getY())*(n2.getY() - n1.getY())) / p1_p2_squareLength
        if (dotProduct < 0.0):
            return (pt.getX() - n1.getX())*(pt.getX() - n1.getX()) + (pt.getY() - n1.getY())*(pt.getY() - n1.getY())
        elif ( dotProduct <= 1.0 ):
            p_p1_squareLength = (n1.getX() - pt.getX())*(n1.getX() - pt.getX()) + (n1.getY() - pt.getY())*(n1.getY() - pt.getY())
            return p_p1_squareLength - dotProduct * dotProduct * p1_p2_squareLength
        else:
            return (pt.getX() - n2.getX())*(pt.getX() - n2.getX()) + (pt.getY() - n2.getY())*(pt.getY() - n2.getY())


    def __accuratePointInTriangle(self, n1, n2, n3, pt):
        "gets all points inside and on the edge of the triangle (within distance of EpsilonSquare)"
        if self.__PointInTriangle(pt, n1, n2, n3):
            return True
        if (self.__distanceSquarePointToSegment(n1, n2, pt) <= self.__Epsilon):
            return True
        elif (self.__distanceSquarePointToSegment(n2, n3, pt) <= self.__Epsilon):
            return True
        elif (self.__distanceSquarePointToSegment(n3, n1, pt) <= self.__Epsilon):
            return True
        else:
            return False