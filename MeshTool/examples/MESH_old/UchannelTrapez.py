#!/usr/bin/env python

from basement import *
from batchTools import *
from NLSolve import *
from postprocessing import *
import math
from meshModel import NODE, MESHMODEL

class UCHANNELTRAPEZ (MESHMODEL):
    """class defines the geometry of a rectangular/trapezoidal U-turn (180 degrees) laboratory channel"""
    def __init__ (self,directory,
    	meshName='UchannelTrapez', 
    	channelLength=5.0,
    	channelBedWidth=1.0,
        channelRadius=10.0,
        channelHeight=0.3,
        channelBankSlope=2.0/3.0,
    	channelBedSlope=0.2,
        observationAngles=[30.0],
        sedimentLayer=0.23,
    	numberOfCells=5000.0):
		# call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory,absolutePrecision=1e-8)
        self.setStringdefDistance(True)
        # this class does all the meshing stuff
        self.initialization(meshName) # name used for output-files
        # input arguments
        self.__L = channelLength
        self.__W = channelBedWidth
        self.__bW = channelHeight/channelBankSlope # bank width
        self.__sW = sedimentLayer/channelBankSlope # sediment layer width
        self.__sL = sedimentLayer # sediment layer thickness
        self.__R = channelRadius
        self.__J = channelBedSlope
        self.__H = channelHeight
        self.__S = channelBankSlope
        self.__oD = observationAngles # angle after which an observation line is made
        self.__N = numberOfCells
        # Different radius of laboratory channel
        self.__outerRadius = self.__R + self.__W/2.0
        self.__innerRadius = self.__R - self.__W/2.0
        self.__outerBankRadius = self.__R + self.__W/2.0 + self.__bW
        self.__innerBankRadius = self.__R - self.__W/2.0 - self.__bW
        self.__outerSedimentRadius = self.__R + self.__W/2.0 + self.__sW
        self.__innerSedimentRadius = self.__R - self.__W/2.0 - self.__sW
        # suface of laboratory channel
        areaChannel = 2.0* ((self.__L * self.__W) + (2.0 * self.__bW * self.__L))
        areaCurve = math.pi * (self.__outerRadius*self.__outerRadius - self.__innerRadius*self.__innerRadius)
        self.__surface = areaChannel + areaCurve

        # list with angles (theta) defined by the user
        self.__thetaObs_list = []
        if (len(self.__oD) == 1):           
            for ii in range(1,int(180.0/self.__oD[0])):
                self.__thetaObs_list.append(ii*self.__oD[0])
        else:
            for ii in self.__oD:
                self.__thetaObs_list.append(180.0-ii)
            self.__thetaObs_list.reverse()

		# calculate all the important coordinates
		# x-coordinates of the nodes
        self.__x1 = 0.0
        self.__x2 = self.__W
        self.__x3 = 0.0
        self.__x4 = self.__W
        self.__x5 = 2.0*self.__R
        self.__x6 = 2.0*self.__R + self.__W
        self.__x7 = 2.0*self.__R
        self.__x8 = 2.0*self.__R + self.__W       
        self.__xR = self.__R + 0.5*self.__W
        
        self.__xa = - self.__bW
        self.__xb = self.__W + self.__bW
        self.__xc = - self.__bW
        self.__xd = self.__W + self.__bW
        self.__xe = 2.0*self.__R - self.__bW
        self.__xf = 2.0*self.__R + self.__W + self.__bW
        self.__xg = 2.0*self.__R - self.__bW
        self.__xh = 2.0*self.__R + self.__W + self.__bW
        
        self.__xs1 = - self.__sW
        self.__xs2 = self.__W + self.__sW
        self.__xs3 = - self.__sW
        self.__xs4 = self.__W + self.__sW 
        self.__xs5 = 2.0*self.__R - self.__sW
        self.__xs6 = 2.0*self.__R + self.__W + self.__sW
        self.__xs7 = 2.0*self.__R - self.__sW
        self.__xs8 = 2.0*self.__R + self.__W + self.__sW 

        # y-coordinates of the nodes
        self.__y1 = 0.0
        self.__y2 = 0.0
        self.__y3 = self.__L
        self.__y4 = self.__L
        self.__y5 = self.__L
        self.__y6 = self.__L
        self.__y7 = 0.0
        self.__y8 = 0.0
        self.__yR = self.__L
        self.__ya = 0.0
        self.__yb = 0.0
        self.__yc = self.__L
        self.__yd = self.__L
        self.__ye = self.__L
        self.__yf = self.__L
        self.__yg = 0.0
        self.__yh = 0.0
        self.__ys1 = 0.0
        self.__ys2 = 0.0
        self.__ys3 = self.__L
        self.__ys4 = self.__L
        self.__ys5 = self.__L
        self.__ys6 = self.__L
        self.__ys7 = 0.0
        self.__ys8 = 0.0

	# process everything
    def build (self):
		# defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)

        na = NODE(self.__xa, self.__ya)
        nb = NODE(self.__xb, self.__yb)
        nc = NODE(self.__xc, self.__yc)
        nd = NODE(self.__xd, self.__yd)
        ne = NODE(self.__xe, self.__ye)
        nf = NODE(self.__xf, self.__yf)
        ng = NODE(self.__xg, self.__yg)
        nh = NODE(self.__xh, self.__yh)

        ns1 = NODE(self.__xs1,self.__ys1)
        ns2 = NODE(self.__xs2,self.__ys2)
        ns3 = NODE(self.__xs3,self.__ys3)
        ns4 = NODE(self.__xs4,self.__ys4)
        ns5 = NODE(self.__xs5,self.__ys5)
        ns6 = NODE(self.__xs6,self.__ys6)
        ns7 = NODE(self.__xs7,self.__ys7)
        ns8 = NODE(self.__xs8,self.__ys8)

        # and the regions with max area and material indices
        # Note: this is used here to create the nodes in the curvature, and later to define grid size
        curvatureCellSize = 1.0*self.__surface/self.__N
        dLCurvature = math.sqrt(2.0*curvatureCellSize)
        halfPerimeter = math.pi # [rad]

        innerTheta = 2.0*math.asin(dLCurvature/(2.0*self.__innerRadius))
        outerTheta = 2.0*math.asin(dLCurvature/(2.0*self.__outerRadius))
        innerBankTheta = 2.0*math.asin(dLCurvature/(2.0*self.__innerBankRadius))
        outerBankTheta = 2.0*math.asin(dLCurvature/(2.0*self.__outerBankRadius))
        innerSedimentTheta = 2.0*math.asin(dLCurvature/(2.0*self.__innerSedimentRadius))
        outerSedimentTheta = 2.0*math.asin(dLCurvature/(2.0*self.__outerSedimentRadius))
  
        # number of sections and angle for curvature
        numInnerSections = halfPerimeter/innerTheta
        numInnerSections = round(numInnerSections)       
        innerTheta = halfPerimeter/numInnerSections
        numOuterSections = halfPerimeter/outerTheta
        numOuterSections = round(numOuterSections)       
        outerTheta = halfPerimeter/numOuterSections

        numInnerBankSections = halfPerimeter/innerBankTheta
        numInnerBankSections = round(numInnerBankSections)       
        innerBankTheta = halfPerimeter/numInnerBankSections
        numOuterBankSections = halfPerimeter/outerBankTheta
        numOuterBankSections = round(numOuterBankSections)       
        outerBankTheta = halfPerimeter/numOuterBankSections

        numInnerSedimentSections = halfPerimeter/innerSedimentTheta
        numInnerSedimentSections = round(numInnerSedimentSections)       
        innerSedimentTheta = halfPerimeter/numInnerSedimentSections
        numOuterSedimentSections = halfPerimeter/outerSedimentTheta
        numOuterSedimentSections = round(numOuterSedimentSections)       
        outerSedimentTheta = halfPerimeter/numOuterSedimentSections

        innerCircleNodeList = []
        outerCircleNodeList = []
        innerBankCircleNodeList = []
        outerBankCircleNodeList = []
        innerSedimentCircleNodeList = []
        outerSedimentCircleNodeList = []
        
        innerBreakLines = []
        outerBreakLines = []
        innerBankBreakLines = []
        outerBankBreakLines = []
        innerSedimentBreakLines = []
        outerSedimentBreakLines = []

        # make vectors with angles on these breaklines
        innerTheta_vec = []       
        theta_sum = 0.0
        for ii in range(1,int(numInnerSections)):
            theta_sum += innerTheta
            innerTheta_vec.append(theta_sum)
 
        outerTheta_vec = []
        theta_sum = 0.0
        for ii in range(1,int(numOuterSections)):
            theta_sum += outerTheta
            outerTheta_vec.append(theta_sum)

        innerBankTheta_vec = []       
        theta_sum = 0.0
        for ii in range(1,int(numInnerBankSections)):
            theta_sum += innerBankTheta
            innerBankTheta_vec.append(theta_sum)
               
        outerBankTheta_vec = []
        theta_sum = 0.0
        for ii in range(1,int(numOuterBankSections)):
            theta_sum += outerBankTheta
            outerBankTheta_vec.append(theta_sum)

        innerSedimentTheta_vec = []       
        theta_sum = 0.0
        for ii in range(1,int(numInnerSedimentSections)):
            theta_sum += innerSedimentTheta
            innerSedimentTheta_vec.append(theta_sum)
               
        outerSedimentTheta_vec = []
        theta_sum = 0.0
        for ii in range(1,int(numOuterSedimentSections)):
            theta_sum += outerSedimentTheta
            outerSedimentTheta_vec.append(theta_sum)


        # now create the nodes for all these breaklines/half-circles
        delta = 0.4 # limit elements near user defined observation breaklines
        # nodes for inner half-circle        
        tt = 0
        moreObs = True
        for idx, ii in enumerate(innerTheta_vec):
            # determine distance to next possible node
            if tt == 0:
                x1 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerRadius * math.cos(ii)
                y2 = self.__yR + self.__innerRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerRadius * math.cos(ii)
                y2 = self.__yR + self.__innerRadius * math.sin(ii)
                x3 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            # creater nodes for observation breaklines
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                innerCircleNodeList.append(NODE(x,y))
                innerBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            # create all other nodes (only if not too near to the nodes of observation breaklines)
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__innerRadius * math.cos(ii)
                y = self.__yR + self.__innerRadius * math.sin(ii)
                innerCircleNodeList.append(NODE(x,y))

        # nodes for inner bank half-circle        
        tt = 0
        moreObs = True
        for idx, ii in enumerate(innerBankTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__innerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerBankRadius * math.cos(ii)
                y2 = self.__yR + self.__innerBankRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__innerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerBankRadius * math.cos(ii)
                y2 = self.__yR + self.__innerBankRadius * math.sin(ii)
                x3 = self.__xR + self.__innerBankRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__innerBankRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__innerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__innerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                innerBankCircleNodeList.append(NODE(x,y))
                innerBankBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__innerBankRadius * math.cos(ii)
                y = self.__yR + self.__innerBankRadius * math.sin(ii)
                innerBankCircleNodeList.append(NODE(x,y))

        # nodes for inner sediment half-circle        
        tt = 0
        moreObs = True
        for idx, ii in enumerate(innerSedimentTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__innerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerSedimentRadius * math.cos(ii)
                y2 = self.__yR + self.__innerSedimentRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__innerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerSedimentRadius * math.cos(ii)
                y2 = self.__yR + self.__innerSedimentRadius * math.sin(ii)
                x3 = self.__xR + self.__innerSedimentRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__innerSedimentRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__innerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__innerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                innerSedimentCircleNodeList.append(NODE(x,y))
                innerSedimentBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__innerSedimentRadius * math.cos(ii)
                y = self.__yR + self.__innerSedimentRadius * math.sin(ii)
                innerSedimentCircleNodeList.append(NODE(x,y))
            
        # nodes for outer half-circle
        tt = 0
        moreObs = True
        for idx, ii in enumerate(outerTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerRadius * math.cos(ii)
                y2 = self.__yR + self.__outerRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerRadius * math.cos(ii)
                y2 = self.__yR + self.__outerRadius * math.sin(ii)
                x3 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                outerCircleNodeList.append(NODE(x,y))
                outerBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__outerRadius * math.cos(ii)
                y = self.__yR + self.__outerRadius * math.sin(ii)
                outerCircleNodeList.append(NODE(x,y))

        # nodes for outer bank half-circle
        tt = 0
        moreObs = True
        for idx, ii in enumerate(outerTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__outerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerBankRadius * math.cos(ii)
                y2 = self.__yR + self.__outerBankRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__outerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerBankRadius * math.cos(ii)
                y2 = self.__yR + self.__outerBankRadius * math.sin(ii)
                x3 = self.__xR + self.__outerBankRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__outerBankRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__outerBankRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__outerBankRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                outerBankCircleNodeList.append(NODE(x,y))
                outerBankBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__outerBankRadius * math.cos(ii)
                y = self.__yR + self.__outerBankRadius * math.sin(ii)
                outerBankCircleNodeList.append(NODE(x,y))

        # nodes for outer sediment half-circle
        tt = 0
        moreObs = True
        for idx, ii in enumerate(outerTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__outerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerSedimentRadius * math.cos(ii)
                y2 = self.__yR + self.__outerSedimentRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__outerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerSedimentRadius * math.cos(ii)
                y2 = self.__yR + self.__outerSedimentRadius * math.sin(ii)
                x3 = self.__xR + self.__outerSedimentRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__outerSedimentRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__outerSedimentRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__outerSedimentRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                outerSedimentCircleNodeList.append(NODE(x,y))
                outerSedimentBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__outerSedimentRadius * math.cos(ii)
                y = self.__yR + self.__outerSedimentRadius * math.sin(ii)
                outerSedimentCircleNodeList.append(NODE(x,y))


        # Define the breaklines
        # mesh boundary
        nodeList = [nc,na,ns1,n1,n2,ns2,nb,nd]
        innerBankCircleNodeList.reverse()
        nodeList.extend(innerBankCircleNodeList)
        nodeList.extend([ne,ng,ns7,n7,n8,ns8,nh,nf])
        nodeList.extend(outerBankCircleNodeList)
        self.addLine(nodeList,True)
        # observation break lines in the curvatue
        for idx, ii in enumerate(innerBankBreakLines):
            self.addLine([ii,innerSedimentBreakLines[idx],innerBreakLines[idx],outerBreakLines[idx],outerSedimentBreakLines[idx],outerBankBreakLines[idx]])
        self.addLine([nc,ns3,n3,n4,ns4,nd])
        self.addLine([ne,ns5,n5,n6,ns6,nf])

        # break lines in the channel
        nodeListBedLeft = [n1,n3]
        outerCircleNodeList.reverse()
        nodeListBedLeft.extend(outerCircleNodeList)
        nodeListBedLeft.extend([n6,n8])
        self.addLine(nodeListBedLeft)

        nodeListBedRight = [n2,n4]
        innerCircleNodeList.reverse()
        nodeListBedRight.extend(innerCircleNodeList)
        nodeListBedRight.extend([n5,n7])       
        self.addLine(nodeListBedRight)

        nodeListSedimentLeft = [ns1,ns3]
        outerSedimentCircleNodeList.reverse()
        nodeListSedimentLeft.extend(outerSedimentCircleNodeList)
        nodeListSedimentLeft.extend([ns6,ns8])
        self.addLine(nodeListSedimentLeft)

        nodeListSedimentRight = [ns2,ns4]
        innerSedimentCircleNodeList.reverse()
        nodeListSedimentRight.extend(innerSedimentCircleNodeList)
        nodeListSedimentRight.extend([ns5,ns7])       
        self.addLine(nodeListSedimentRight)

        # Add regions with max area and material indices
        smallCellSize = 1.0*self.__surface/self.__N
        normalCellSize = 2.0*self.__surface/self.__N
        # channel before curve:
        self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*(self.__y1+self.__y3)),maxArea=normalCellSize,matID=3)    # bed
        self.addRegion(NODE(0.5*(self.__xs1+self.__x1),0.5*(self.__ya+self.__yc)),maxArea=normalCellSize,matID=3)   # bed
        self.addRegion(NODE(0.5*(self.__x2+self.__xs2),0.5*(self.__y2+self.__y4)),maxArea=normalCellSize,matID=3)   # bed
        self.addRegion(NODE(0.5*(self.__xa+self.__xs1),0.5*(self.__ya+self.__yc)),maxArea=normalCellSize,matID=4)   # left bank
        self.addRegion(NODE(0.5*(self.__xs2+self.__xb),0.5*(self.__y2+self.__y4)),maxArea=normalCellSize,matID=4)   # right bank
        # channel after curve:
        self.addRegion(NODE(0.5*(self.__x7+self.__x8),0.5*(self.__y7+self.__y5)),maxArea=normalCellSize,matID=3)    # bed
        self.addRegion(NODE(0.5*(self.__xs7+self.__x7),0.5*(self.__yg+self.__ye)),maxArea=normalCellSize,matID=3)   # bed
        self.addRegion(NODE(0.5*(self.__x8+self.__xs8),0.5*(self.__y8+self.__y6)),maxArea=normalCellSize,matID=3)   # bed
        self.addRegion(NODE(0.5*(self.__xg+self.__xs7),0.5*(self.__yg+self.__ye)),maxArea=normalCellSize,matID=4)   # left bank
        self.addRegion(NODE(0.5*(self.__xs8+self.__xh),0.5*(self.__y8+self.__y6)),maxArea=normalCellSize,matID=4)   # right bank
        # case of equal distance between observation arcs:
        if (len(self.__oD) == 1):
            # outer bank in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(outerBankBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(outerBankBreakLines)):           
                angleRegion_sum += math.pi/(len(outerBankBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(outerBankBreakLines)+1):
                x = self.__xR + (self.__outerBankRadius-0.5*(self.__outerBankRadius-self.__outerSedimentRadius)) * math.cos(angleRegion_list[pp])
                y = self.__yR + (self.__outerBankRadius-0.5*(self.__outerBankRadius-self.__outerSedimentRadius)) * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=2)
                pp += 1 
            # outer sediment bank in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(outerSedimentBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(outerSedimentBreakLines)):           
                angleRegion_sum += math.pi/(len(outerSedimentBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(outerSedimentBreakLines)+1):
                x = self.__xR + (self.__outerSedimentRadius-0.5*(self.__outerSedimentRadius-self.__outerRadius)) * math.cos(angleRegion_list[pp])
                y = self.__yR + (self.__outerSedimentRadius-0.5*(self.__outerSedimentRadius-self.__outerRadius)) * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)
                pp += 1         
            # inner bank in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(innerBankBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(innerBankBreakLines)):           
                angleRegion_sum += math.pi/(len(innerBankBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(innerBankBreakLines)+1):
                x = self.__xR + (self.__innerSedimentRadius-0.5*(self.__innerSedimentRadius-self.__innerBankRadius)) * math.cos(angleRegion_list[pp])
                y = self.__yR + (self.__innerSedimentRadius-0.5*(self.__innerSedimentRadius-self.__innerBankRadius)) * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=2)
                pp += 1
            # inner sediment bank in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(innerSedimentBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(innerSedimentBreakLines)):           
                angleRegion_sum += math.pi/(len(innerSedimentBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(innerSedimentBreakLines)+1):
                x = self.__xR + (self.__innerRadius-0.5*(self.__innerRadius-self.__innerSedimentRadius)) * math.cos(angleRegion_list[pp])
                y = self.__yR + (self.__innerRadius-0.5*(self.__innerRadius-self.__innerSedimentRadius)) * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)
                pp += 1
            # channel in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(innerBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(innerBreakLines)):           
                angleRegion_sum += math.pi/(len(innerBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(innerBreakLines)+1):
                x = self.__xR + self.__R * math.cos(angleRegion_list[pp])
                y = self.__yR + self.__R * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)
                pp += 1
        else: # case of non-equal distance between observation arcs:
            # all regions
            angleRegion_list = []
            for ii in self.__thetaObs_list:         
                angleRegion_list.append((ii-5.0)*math.pi/180.0)
            angleRegion_list.append((self.__thetaObs_list[-1]+5.0)*math.pi/180.0)
            # outer bank in curvature:
            for ii in angleRegion_list:
                x = self.__xR + (self.__outerBankRadius-0.5*(self.__outerBankRadius-self.__outerSedimentRadius)) * math.cos(ii)
                y = self.__yR + (self.__outerBankRadius-0.5*(self.__outerBankRadius-self.__outerSedimentRadius)) * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=2)   
            # outer sediment bank in curvature:
            for ii in angleRegion_list:
                x = self.__xR + (self.__outerSedimentRadius-0.5*(self.__outerSedimentRadius-self.__outerRadius)) * math.cos(ii)
                y = self.__yR + (self.__outerSedimentRadius-0.5*(self.__outerSedimentRadius-self.__outerRadius)) * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)  
            # inner sediment bank in curvature:
            for ii in angleRegion_list:
                x = self.__xR + (self.__innerRadius-0.5*(self.__innerRadius-self.__innerSedimentRadius)) * math.cos(ii)
                y = self.__yR + (self.__innerRadius-0.5*(self.__innerRadius-self.__innerSedimentRadius)) * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)
            # inner bank in curvature:
            for ii in angleRegion_list:
                x = self.__xR + (self.__innerSedimentRadius-0.5*(self.__innerSedimentRadius-self.__innerBankRadius)) * math.cos(ii)
                y = self.__yR + (self.__innerSedimentRadius-0.5*(self.__innerSedimentRadius-self.__innerBankRadius)) * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=2)
            # channel in curvature:
            for ii in angleRegion_list:
                x = self.__xR + self.__R * math.cos(ii)
                y = self.__yR + self.__R * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)

		# define stringdefs
        self.addStringdef( 'inflow', [na,ns1,n1,n2,ns2,nb] )
        self.addStringdef( 'outflow', [ng,ns7,n7,n8,ns8,nh] )

        string_list = ['CS1','CS2','CS3','CS4','CS5','CS6','CS7','CS8','CS9','CS10']
        for idx, ii in enumerate(self.__thetaObs_list):
            self.addStringdef(string_list[idx],[innerBankBreakLines[idx],innerBreakLines[idx],outerBreakLines[idx],outerBankBreakLines[idx]])
	
        # set the minimum triangle angle
        self.setQuality(30)

    def generate2dm(self,fixed=False):
        self.__fixed = fixed
        # let's go...
        self.createMesh()
        # deleting the triangle files
        self.deleteFiles()


	# function that describes the topography of the model
    def elevationFunction (self, x, y):
        # idea: first assign location, then calcualte elevation
        epsilon = 0.1
        Rx = self.__R+0.5*self.__W   
        Ry = self.__L  
        where = None
        if self.__fixed:
            # assign location (fixed bed)  
            if (self.__y1 <= y <= self.__y3) and (self.__x1 <= x <= self.__x2):
                where = 'channelBegin'
            elif (self.__ya <= y <= self.__yc) and (self.__xa <= x <= self.__x1):
                where = 'channelBeginBankLeft'
            elif (self.__y2 <= y <= self.__y4) and (self.__x2 <= x <= self.__xb):
                where = 'channelBeginBankRight'
            elif (self.__y7 <= y <= self.__y5) and (self.__x7 <= x <= self.__x8):
                where = 'channelEnd'
            elif (self.__yg <= y <= self.__ye) and (self.__xg <= x <= self.__x7):
                where = 'channelEndBankLeft'
            elif (self.__y8 <= y <= self.__y6) and (self.__x8 <= x <= self.__xh):
                where = 'channelEndBankRight'          
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__outerRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__innerRadius)**2 ):
                where = 'curveBed'
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__outerBankRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__outerRadius)**2 ):
                where = 'curveBankLeft'
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__innerRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__innerBankRadius)**2 ):
                where = 'curveBankRight'
            # calculate elevation (fixed bed)
            totalLength = 2.0*self.__L + math.pi*self.__R
            if where == 'channelBegin':
                return self.__J*(totalLength - y)
            elif where == 'channelBeginBankLeft':
                return self.__J*(totalLength - y) - x * self.__S
            elif where == 'channelBeginBankRight':
                return self.__J*(totalLength - y) + (x-self.__x2) * self.__S
            elif where == 'channelEnd':
                return y * self.__J
            elif where == 'channelEndBankLeft':
                return y * self.__J - (x-self.__x7) * self.__S
            elif where == 'channelEndBankRight':
                return y * self.__J + (x-self.__x8) * self.__S
            elif where == 'curveBed':
                v0x = self.__x4 - Rx
                v0y = self.__y4 - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                return zBegin - self.__R*phi*self.__J           
            elif where == 'curveBankLeft':
                v0x = self.__x3 - Rx
                v0y = self.__y3 - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                xB = self.__xR + self.__outerRadius * math.cos(math.pi-phi)
                yB = self.__yR + self.__outerRadius * math.sin(math.pi-phi) 
                distance = math.sqrt((x-xB)**2+(y-yB)**2)
                return zBegin - self.__R*phi*self.__J + distance*self.__S
            elif where == 'curveBankRight':
                v0x = self.__xd - Rx
                v0y = self.__yd - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                xB = self.__xR + self.__innerRadius * math.cos(math.pi-phi)
                yB = self.__yR + self.__innerRadius * math.sin(math.pi-phi) 
                distance = math.sqrt((x-xB)**2+(y-yB)**2)
                return zBegin - self.__R*phi*self.__J + distance*self.__S
            else:
                print "There is no elevation found for x =", x, " and y =", y
                print "Therefore, the elevation is set to 0.0"
                return 0.0
        else:
            # mobile bed with a layer thickness (input parameter)
            # assign location (mobile bed)  
            if (self.__y1 <= y <= self.__y3) and (self.__xs1 <= x <= self.__xs2):
                where = 'channelBegin'
            elif (self.__ya <= y <= self.__yc) and (self.__xa <= x <= self.__xs1):
                where = 'channelBeginBankLeft'
            elif (self.__y2 <= y <= self.__y4) and (self.__xs2 <= x <= self.__xb):
                where = 'channelBeginBankRight'
            elif (self.__y7 <= y <= self.__y5) and (self.__xs7 <= x <= self.__xs8):
                where = 'channelEnd'
            elif (self.__yg <= y <= self.__ye) and (self.__xg <= x <= self.__xs7):
                where = 'channelEndBankLeft'
            elif (self.__y8 <= y <= self.__y6) and (self.__xs8 <= x <= self.__xh):
                where = 'channelEndBankRight'          
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__outerSedimentRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__innerSedimentRadius)**2 ):
                where = 'curveBed'
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__outerBankRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__outerSedimentRadius)**2 ):
                where = 'curveBankLeft'
            elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__innerSedimentRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__innerBankRadius)**2 ):
                where = 'curveBankRight'
            # calculate elevation (mobile bed)
            totalLength = 2.0*self.__L + math.pi*self.__R
            if where == 'channelBegin':
                return self.__sL + self.__J*(totalLength - y)
            elif where == 'channelBeginBankLeft':
                return self.__J*(totalLength - y) - x * self.__S
            elif where == 'channelBeginBankRight':
                return self.__J*(totalLength - y) + (x-self.__x2) * self.__S
            elif where == 'channelEnd':
                return self.__sL + y * self.__J
            elif where == 'channelEndBankLeft':
                return y * self.__J - (x-self.__x7) * self.__S
            elif where == 'channelEndBankRight':
                return y * self.__J + (x-self.__x8) * self.__S
            elif where == 'curveBed':
                v0x = self.__x4 - Rx
                v0y = self.__y4 - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                return zBegin + self.__sL - self.__R*phi*self.__J           
            elif where == 'curveBankLeft':
                v0x = self.__x3 - Rx
                v0y = self.__y3 - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                xB = self.__xR + self.__outerRadius * math.cos(math.pi-phi)
                yB = self.__yR + self.__outerRadius * math.sin(math.pi-phi) 
                distance = math.sqrt((x-xB)**2+(y-yB)**2)
                return zBegin - self.__R*phi*self.__J + distance*self.__S
            elif where == 'curveBankRight':
                v0x = self.__xd - Rx
                v0y = self.__yd - Ry
                vx = x-Rx
                vy = y-Ry
                phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
                zBegin = self.__J*(totalLength - self.__L)
                xB = self.__xR + self.__innerRadius * math.cos(math.pi-phi)
                yB = self.__yR + self.__innerRadius * math.sin(math.pi-phi) 
                distance = math.sqrt((x-xB)**2+(y-yB)**2)
                return zBegin - self.__R*phi*self.__J + distance*self.__S
            else:
                print "There is no elevation found for x =", x, " and y =", y
                print "Therefore, the elevation is set to 0.0"
                return 0.0




