#!/usr/bin/env python
from testMeshes import *
from batchTools import *

def run_internalBC_channel ():
    qin=30.0
    tfin=200.0
    gateElev=2
    ibed=30
    print '*** SIMPLE CHANNEL with INTERNAL BCs TEST ***'
    wd = WORKINGDIRECTORY('testIBCsChannel')
    sc = IBCsCHANNEL(wd)
    sc.build()
    modelID = 'IBCs_channel'
    bm = BASEMENT(wd)
    bm.setProjectTitle('this_is_another_test')
    bm.setModelID(modelID)
    bm.setNumThreads(1)
    bm.set2dmFile('%s.2dm'%modelID)
    bm.setTimeStep({'total_run_time':tfin,'CFL':0.95})
    bm.setHydInitialCondition({'type':'dry'})
    bm.setHydFriction({'type':'strickler', 'default_friction':'25.0', 'wall_friction':'off'})
    bm.setStringdefFile ('%s_stringdefs.txt'%modelID)
    inflowFile = 'inflow.txt'
    bm.writeTimeSeriesFile(inflowFile,[0.0,tfin*0.5,tfin],[qin,qin,qin])
    gateFile = 'gate.txt'
    bm.writeTimeSeriesFile(gateFile,[0.0,tfin],[gateElev,gateElev])
    bm.addHydBoundaryCondition('inflow',{'string_name':'inflow_boundary', 'type':'hydrograph', 'file':inflowFile, 'slope':ibed})
    bm.addHydBoundaryCondition('outflow',{'string_name':'outflow_boundary', 'type':'hqrelation', 'slope':ibed})
    bm.addIntBoundaryCondition('gate',{'string_name1':'gate_out','string_name2':'gate_in', 'type':'gate','file':gateFile,'my':'0.6'})   
    bm.setConsoleTimeStep(10.0)
    bm.addCenteredOutput (10.0,'element',['wse','velocity','depth','z_element'],'vtk')
    #bm.addBASEvizOutput(1)
    #bm.addHistoryOutput(60,'stringdef',['outflow_boundary'],['wse'])
    bm.run()
    print '*** SUCCESS ***\n\n\n'
    #p = POSTPROCESSING(bm.getResultNameSuffix())
    #p.readTimeHistory('stringdef','outflow_boundary')
    #wseTimeHistory = p.getData('wse')
    #return wseTimeHistory[-1]

def run_Cproperty ():
    #by Davide Vanzo
    #C-property test: set wse > 1 (e.g. 1.5)
    #extended C-property test: set wse < 1 (e.g. 0.5)
    print '*** C Property TEST ***'
    wd = WORKINGDIRECTORY('testCproperty')
    cp = CPROPERTY(wd)
    cp.build()
    modelID = 'C_property'
    bm = BASEMENT(wd)
    bm.setProjectTitle('this_is_a_test')
    bm.setModelID(modelID)
    bm.set2dmFile('%s.2dm'%modelID)
    bm.setNumThreads(1)
    bm.setTimeStep({'total_run_time':3600.0,'CFL':0.95})
    bm.setHydInitialCondition({'type':'index_table', 'index':'(0 5)', 'wse':'(0.5 0.5)', 'u':'(0 0)', 'v':'(0 0)'})
    bm.setHydParameters({'minimum_water_depth':'0.01', 'velocity_update_partial':'depth','dynamic_depth_solver':'off'})
    bm.setHydFriction({'type':'frictionless'})
    bm.setConsoleTimeStep(600)
    bm.addCenteredOutput (600,'element',['wse','depth','velocity'],'ascii')
    bm.addCenteredOutput (600,'element',['wse','velocity','depth','z_element'],'vtk')
    bm.run()
    print '*** SUCCESS ***\n\n\n'

def run_half_sphere ():
    print '*** HALF SPHERE TEST ***'
    wd = WORKINGDIRECTORY('testHalfSphere')
    hs = HALFSPHERE(wd)
    hs.build()
    modelID = 'half_sphere'
    bm = BASEMENT(wd)
    bm.setProjectTitle('this_is_a_test')
    bm.setModelID(modelID)
    bm.set2dmFile('%s.2dm'%modelID)
    bm.setTimeStep({'total_run_time':60.0,'CFL':0.95})
    bm.setHydInitialCondition({'type':'index_table', 'index':'(0 5 7)', 'wse':'(0.5 0.5 0.5)', 'u':'(0 0 0)', 'v':'(0 0 0)'})
    bm.setHydFriction({'type':'strickler', 'default_friction':'25.0', 'wall_friction':'off'})
    bm.addBASEvizOutput(1)
    bm.addCenteredOutput(1,'element',['wse','depth'],'ascii')
    bm.addBalanceOutput(1,['water_volume'])
    bm.run()
    print '*** SUCCESS ***\n\n\n'
    p = POSTPROCESSING(bm.getResultNameSuffix())
    p.readBalance()
    vol = p.getData('water_vol')
    p.readCentered('element')
    h =  p.getData('depth')
    wse =  p.getData('wse')

def run_simple_channel (qin=100.0):
    print '*** SIMPLE CHANNEL TEST ***'
    wd = WORKINGDIRECTORY('testSimpleChannel')
    sc = SIMPLECHANNEL(wd)
    sc.build()
    modelID = 'simple_channel'
    bm = BASEMENT(wd)
    bm.setProjectTitle('this_is_another_test')
    bm.setModelID(modelID)
    bm.setNumThreads(4)
    bm.set2dmFile('%s.2dm'%modelID)
    bm.setTimeStep({'total_run_time':180.0,'CFL':0.95})
    bm.setHydInitialCondition({'type':'dry'})
    bm.setHydFriction({'type':'strickler', 'default_friction':'25.0', 'wall_friction':'on'})
    bm.setStringdefFile ('%s_stringdefs.txt'%modelID)
    inflowFile = 'inflow.txt'
    bm.writeTimeSeriesFile(inflowFile,[0.0,60.0],[0.0,qin])
    bm.addHydBoundaryCondition('inflow',{'string_name':'inflow_boundary', 'type':'hydrograph', 'file':inflowFile, 'slope':'1'})
    bm.addHydBoundaryCondition('outflow',{'string_name':'outflow_boundary', 'type':'zero_gradient'})
    bm.addBASEvizOutput(1)
    bm.addHistoryOutput(60,'stringdef',['outflow_boundary'],['wse'])
    bm.run()
    print '*** SUCCESS ***\n\n\n'
    p = POSTPROCESSING(bm.getResultNameSuffix())
    p.readTimeHistory('stringdef','outflow_boundary')
    wseTimeHistory = p.getData('wse')
    return wseTimeHistory[-1]


def run_lab_channel (qin=0.5):
    print '*** LAB CHANNEL TEST ***'
    wd = WORKINGDIRECTORY('LabChannel_fixbed')
    modelID = 'straightLabChannel'
    lc = LABCHANNEL(wd,meshName=modelID,
        channelLength=10.0,
        channelWidth=1.0,
        channelBedSlope=0.002,
        lateralchannelBedSlope=0.0,
        numberOfCells=1000.0)
    lc.build()
    bm = BASEMENT(wd)
    bm.setProjectTitle('laboratory_channel_fixbed')
    bm.setModelID(modelID)
    bm.set2dmFile('%s.2dm'%modelID)
    bm.setTimeStep({'start_time':'0.0','total_run_time':50.0,'CFL':0.95})
    # Hydraulics
    #bm.setHydInitialCondition({'type':'continue', 'file':'labChannel_Initial_Condition.cgns'})
    bm.setHydInitialCondition({'type':'index_table','index':'(1 2)','u':'(0.0 0.0)','v':'(0.0 0.0)','h':'(0.4 0.4)'})
    bm.setHydFriction({'type':'chezy', 'default_friction':'0.004','fixed_bed_d90':'1.5', 'index':'(1 2)', 'friction':'(0.004 0.004)','wall_friction':'off', 'grain_size_friction':'yes'})
    bm.setStringdefFile ('%s_stringdefs.txt'%modelID)
    inflowFile = 'inflow.txt'   
    bm.writeTimeSeriesFile(inflowFile,[0.0,1000.0],[qin,qin])
    bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'2.0'})
    bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'2.0'})
    #bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'0.02'})
    # Morphology
    bm.setMorphInitialCondition({'type':'initial_mesh'})
    bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'37.0','control_volume_type':'constant','control_volume_thickness':'0.003'})
    bm.setGrainClass([0.2, 0.4, 1.0, 1.5, 2.0])
    bm.addMixture('multi_grain',[20.0, 20.0, 20.0, 20.0, 20])
    bm.addSoilDefinition('soil_uniform',[{'mixture':'multi_grain', 'bottom_elevation':-1.0}])
    bm.addSoilDefinition('soil_fixbed',[])
    #bm.assignSoils({'1':'soil_fixbed','2':'soil_uniform'})
    bm.assignSoils({'1':'soil_fixbed','2':'soil_fixbed'})
    bm.setBedloadFormula({'bedload_formula':'parker','bedload_factor':'1.0','critical_shear_stress_calibration':'0.90'})
    bm.setBedloadParameters({'limit_bedload_wetted':'on'})
    bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_index':'(1 2)','lateral_transport_factor':'1.5'})
    #bm.addBedloadBoundaryCondition('bed_inflow',{'type':'IOUp','string_name':'inflow','fraction_boundary':'1.0','mixture':'multi_grain'})
    bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow'})
    fileQSed = 'QSed.txt'   
    bm.writeTimeSeriesFile(fileQSed,[0.0,1000.0],[0.0005,0.0005])
    bm.addBedloadBoundaryCondition('bed_inflow',{'type':'sediment_discharge','string_name':'inflow','mixture':'multi_grain','file':fileQSed})
    # Output
    bm.setRestartTimeStep(1e32)
    bm.setConsoleTimeStep(10.0)
    bm.addBASEvizOutput(10)
    #bm.addBalanceOutput(1.0,['sediment','timestep','water_volume'])
    bm.addHistoryOutput(100.0,'element',[182, 295, 480],['depth','velocity'],True)
    #bm.addHistoryOutput(1.0,'node',[8, 132, 185, 222, 299, 317],['z_node','deltaz'],True)
    #bm.addHistoryOutput(1.0,'stringdef',['cs1','cs2','cs3'],['zbed','Q','Qsed','u','wse'])
    bm.addHistoryOutput(100.0,'stringdef',['cs1','cs2','cs3'],['Q','u','wse'])
    bm.addHistoryOutput(100.0,'boundary',[],['Q'])
    bm.addCenteredOutput (10.0,'node',['z_node','depth','velocity','deltaz','grain_size'],'vtk')
    bm.run()
    print '*** SUCCESS ***\n\n\n'

def run_trapezoidal_channel ():
    print '*** TRAPEZOIDAL CHANNEL TEST ***'
    wd = WORKINGDIRECTORY('testTrapezoidalChannel')
    tc = TRAPEZOIDALCHANNEL(wd)
    tc.build()
    # TODO: basement setup ...
    print '*** SUCCESS ***\n\n\n'

"""
class CALIBRATION (NLSOLVE):
    def __init__ (self, refValue):
        self.__refValue = refValue
        # call init of the meshmodel class
        NLSOLVE.__init__(self)

    def functionToBeSolved (self, x):
        return run_simple_channel(x)-self.__refValue
"""


def run_calibration():
    ref = 1.35
    c = CALIBRATION(ref)
    c.setFunctionTolerance(ref*1e-6)
    c.setRootTolerance(1e-16)
    c.init(0.5,100)
    c.solve()
    print c.getRoot()



if __name__ == "__main__":
    #run_lab_channel()

    print '*** Trapez channel mesh generator ***'
    wd = WORKINGDIRECTORY('TrapezChannel_v2x')
    lc = TRAPEZOIDALCHANNEL(wd,
        channelLength=200.0,
        channelWidth=20.0,
        channelHeight=2.0,
        channelBedSlope=0.005,
        channelBankSlope=2.0/3.0,
        maxArea=3.0,
        quality=32.0,
        mType='plane2x')
    lc.build()