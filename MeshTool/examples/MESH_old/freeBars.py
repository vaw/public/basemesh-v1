# Free bars (triggered by initial perturbation)

import math
import random
from meshModel import NODE, MESHMODEL
from batchTools import *
import numpy as np

class FREEBARS (MESHMODEL):
    """class defines the geometry of a rectangular laboratory channel (no side walls) with obstacle"""
    def __init__ (self,directory,
        meshName='freeBars',
        channelLength=18000.0,
        channelWidth=60.0,
        channelBedSlope=0.005,
        numberOfCells=10000.0,
        randomPert=0.0,
        cosineAmplitude=0.0):
        # call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory,absolutePrecision=1e-5)
        self.setStringdefDistance(True)
        # activate bandwidth reduction
        self.setReduction(True)
        # this class does all the meshing stuff
        self.initialization(meshName,'plane3x') # This is the name used for the output-files
        # input arguments
        self.__L = channelLength
        self.__W = channelWidth
        self.__J = channelBedSlope
        self.__N = numberOfCells
        self.__Rand = randomPert
        self.__Amplitude = cosineAmplitude
        # suface of laboratory channel:
        self.__surface = self.__L * self.__W
        # calculate all the important coordinates
        # x-coordinates of the nodes
        self.__x1 = 0.0
        self.__x2 = 0.0
        self.__x3 = self.__L
        self.__x4 = self.__L
        self.__xblRPoben = 0.05*self.__L  # break line for no random perturbation at beginning
        self.__xblRPunten = 0.95*self.__L  # break line for no random perturbation at beginning
        self.__xbl1 = 0.1*self.__L  # 1 oberservation line (breakline)
        self.__xbl2 = 0.2*self.__L  # 2 oberservation line (breakline)
        self.__xbl3 = 0.3*self.__L  # 3 oberservation line (breakline)
        self.__xbl4 = 0.4*self.__L  # 4 oberservation line (breakline)
        self.__xbl5 = 0.5*self.__L  # 5 oberservation line (breakline) Middle!
        self.__xbl6 = 0.6*self.__L  # 6 oberservation line (breakline)
        self.__xbl7 = 0.7*self.__L  # 7 oberservation line (breakline)
        self.__xbl8 = 0.8*self.__L  # 8 oberservation line (breakline)
        self.__xbl9 = 0.9*self.__L  # 9 oberservation line (breakline)
        # y-coordinates of the nodes
        self.__y1 = self.__W
        self.__y2 = 0.0
        self.__y3 = 0.0
        self.__y4 = self.__W
        self.__ybl1 = self.__W
        self.__ybl2 = 0.0
        self.__yhalf = self.__W/2.0


    # process everything
    def build (self):
        self.setOption('-DpaA')
        # defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)

        self.addLine([n1,n2,n3,n4],True)
        self.addLine([n1,n2])
        self.addLine([n3,n4])

        # and the regions with max area and material indices
        mediumCellSize = 1.5*self.__surface/self.__N
        # channel bed:
        self.addRegion(NODE(0.5*(self.__x1+self.__x3),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)

        # define stringdefs
        self.addStringdef( 'inflow', [n1,n2] )
        self.addStringdef( 'outflow', [n3,n4] )

        # set the minimum triangle angle
        self.setQuality(30)
        self.setOption('-D')
        # let's go...
        self.createMesh()

    # function that describes the topography of the model
    def elevationFunction (self, x, y):
        if (self.__y2 <= y <= self.__y1) and (self.__xblRPoben <= x <= self.__xblRPunten):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) + random.uniform(-1.0,1.0)*self.__Rand
        elif (self.__y2 <= y <= self.__y1) and (self.__x2 <= x < self.__xblRPoben):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # oben keine random perturbation
        elif (self.__y2 <= y <= self.__y1) and (self.__xblRPunten < x <= self.__x3):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # unten keine random perturbation
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999


modelID = 'FreeBars'
Amplitude = 0.0
B = 60.0
J = 0.005       # bed slope [-]
# setup the model
wd = WORKINGDIRECTORY(modelID)
uc = FREEBARS(wd,meshName=modelID,
    channelLength=300.0*B,
    channelWidth=B,
    channelBedSlope=J,
    numberOfCells=100000.0,
    randomPert=0.01*2.92, # 0.01*h
    cosineAmplitude=Amplitude)  
uc.build()