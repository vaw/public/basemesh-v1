# by Samuel J. Peter, 2015

import math
import numpy as np
from meshModel import NODE, MESHMODEL

class BELLAL (MESHMODEL):
	def __init__ (self, directory,
		length=6.9,
		width=0.5,
		slope=0.0302):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('Bellal_channel')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return self.__s * (self.__l - x)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		n2 = NODE(self.__l,-0.5*self.__w)
		n3 = NODE(self.__l,0.5*self.__w)
		n4 = NODE(0.0,0.5*self.__w)
		# now define the breaklines
		self.addLine([n2,n3])
		self.addLine([n4,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.5*self.__l,0.0),maxArea=self.__w*self.__l/(25000))

		# define stringdef lines
		self.addStringdef('inflow_boundary',[n4,n1])
		self.addStringdef('outflow_boundary',[n2,n3])
		# set some triangle options
		self.setQuality(34)
		self.setOption('-D')
		# let's go...
		self.createMesh()

class CONICAL_DUNE (MESHMODEL):
	def __init__ (self, directory,
		radius=5.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('Conical_dune')
		# store input argument as member variable
		self.__r = radius

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		#expression to described bottom elevation eta=f(x,y)
		if ((x>300 and x<500) and (y>400 and y<600)):
			eta = 0.1 + (math.sin(3.14159*(x-300)/200)**2)*(math.sin(3.14159*(y-400)/200)**2)
		else:
			eta = 0.1
		return eta
			


	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0,0)
		n2 = NODE(1000,0)
		n3 = NODE(1000,1000)
		n4 = NODE(0,1000)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4],True)
		# and the regions with max area and material indices
		self.addRegion(NODE(500,500),maxArea=80,matID=1)
		# define stringdef lines
		self.addStringdef('inflow_boundary',[n1,n4])
		self.addStringdef('outflow_boundary',[n2,n3])
		# set some triangle options
		self.setQuality(34)
		self.setOption('-D')
		# let's go...
		self.createMesh()

class XIA_DAMB (MESHMODEL):
	def __init__ (self, directory,
		length=100.0,
		width=10.0,
		slope=0.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('XIA_DAMB')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope
	
	def MaterialIDFunction(self, matID, x, y):
		#d = (x**2 + y**2)**0.5
		if x < 3:
			return 1
		else:
			return 2
	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return 0.1

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,   0.0)
		n2 = NODE(3.0,   0.0)
		n3 = NODE(6.0,   0.0)
		n4 = NODE(6.0,   0.5)
		n5 = NODE(4.0,    0.5)
		n6 = NODE(4.0,   0.25)
		n7 = NODE(3.0,    0.25)
		n8 = NODE(0.00000,    0.25000)
		
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n5,n6,n7,n8,n1])
		self.addLine([n2,n7])
		# and the regions with max area and material indices
		self.addRegion(NODE(1.0,0.1),maxArea=3.0/25e3)
		self.addRegion(NODE(4.0,0.1),maxArea=3.0/25e3)
		# define stringdef line
		self.addStringdef('outflow_boundary',[n3,n4])
		# set some triangle options
		self.setQuality(34)
		self.setOption('-D')
		# let's go...
		self.createMesh()

class CIRC_DAM_BREAK (MESHMODEL):
	def __init__ (self, directory,
		radius=2.5, extent = 20.0, maxArea=0.006, quality=29.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('circ_dam_break')
		# store input argument as member variable
		self.__r = radius
		self.__l = extent
		self.__A = maxArea
		self.__q = quality
		
	def MaterialIDFunction(self, matID, x, y):
		d = (x**2 + y**2)**0.5
		if d > self.__r:
			return 1
		else:
			return 2

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(-self.__l,-self.__l)
		n2 = NODE(-self.__l,self.__l)
		n3 = NODE(self.__l,self.__l)
		n4 = NODE(self.__l,-self.__l)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.0,0.0),maxArea=self.__A)
		# set triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		#for icell in 

class THREE_HUMPS (MESHMODEL):
	def __init__ (self, directory,
		length=75.0,
		width=30.0,
		dam=16.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('3_humps_test')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__d = dam

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		#expression to described bottom elevation eta=f(x,y)
		a=(1.0-1.0/8.0*((x-30.0)**2+(y+9.0)**2)**0.5)
		b=(1.0-1.0/8.0*((x-30.0)**2+(y-9.0)**2)**0.5)
		c=(3.0-3.0/10.0*((x-47.5)**2+y**2)**0.5)
		eta =max(0.0, a, b, c) # max(0.0, 1-1/8*((x-30)**2+(y+9)**2)**0.5, 1-1/8*((x-30)**2+(y-9)**2)**0.5, 3-3/10*((x-47.5)**2+y**2)**0.5)
		return eta

	# this function actually does all the work
	def build (self):
		## nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		#n2 = NODE(self.__d,-0.5*self.__w)
		n3 = NODE(self.__l,-0.5*self.__w)
		n4 = NODE(self.__l,0.5*self.__w)
		#n5 = NODE(self.__d,0.5*self.__w)
		n6 = NODE(0.0,0.5*self.__w)
		# now define the breaklines
		self.addLine([n1,n3,n4,n6,n1])
		#self.addLine([n2,n5])
		# and the regions with max area and material indices
		#self.addRegion(NODE(0.5*self.__d,0.0),maxArea=self.__w*self.__l/0.65e4,matID=5) #10665 cells
		#self.addRegion(NODE(0.5*self.__d,0.0),maxArea=self.__w*self.__l/2.45e4,matID=5) #40188 cells
		self.addRegion(NODE(0.5*self.__d,0.0),maxArea=self.__w*self.__l/9.8e4,matID=5) #160150 cells
		#self.addRegion(NODE(1.5*self.__d,0.0),maxArea=self.__w*self.__l/1e4,matID=6)
		# set some triangle options
		self.setQuality(32)
		# let's go...
		self.createMesh()

class IBCsCHANNEL (MESHMODEL):
	def __init__ (self, directory,
		length=100.0,
		width=10.0,
		slope=0.03):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('IBCs_channel')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return self.__s * (self.__l - x)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,0.0)
		n2 = NODE(0.0,self.__w)
		n3 = NODE(self.__l,self.__w)
		n4 = NODE(self.__l,0.0)
		n5 = NODE(0.5*self.__l-0.2,self.__w)
		n6 = NODE(0.5*self.__l-0.2,0.0)
		n7 = NODE(0.5*self.__l+0.2,self.__w)
		n8 = NODE(0.5*self.__l+0.2,0.0)
		# now define the breaklines
		self.addLine([n1,n2,n5,n6,n1])
		self.addLine([n8,n7,n3,n4,n8])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.25*self.__l,0.5*self.__w),maxArea=self.__w*self.__l/1e3)
		self.addRegion(NODE(0.75*self.__l,0.5*self.__w),maxArea=self.__w*self.__l/1e3)
		# define stringdef lines
		self.addStringdef('inflow_boundary',[n2,n1])
		self.addStringdef('outflow_boundary',[n3,n4])
		self.addStringdef('gate_in',[n5,n6])
		self.addStringdef('gate_out',[n7,n8])
		# set some triangle options
		self.setQuality(34)
		# let's go...
		self.createMesh()

class CPROPERTY (MESHMODEL):
	def __init__ (self, directory,
		radius=5.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('C_property')
		# store input argument as member variable
		self.__r = radius

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		#expression to described bottom elevation eta=f(x,y)
		eta = math.exp(-(x+1.5)**2-(y+1.5)**2)-math.exp(-(x-1.5)**2-(y-1.5)**2)
		return eta

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(-self.__r,-self.__r)
		n2 = NODE(-self.__r,self.__r)
		n3 = NODE(self.__r,self.__r)
		n4 = NODE(self.__r,-self.__r)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(-0.5*self.__r,-0.5*self.__r),maxArea=0.06,matID=5)
		# set some triangle options
		self.setQuality(34)
		# let's go...
		self.createMesh()

class HALFSPHERE (MESHMODEL):
	def __init__ (self, directory,
		radius=1.0):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('half_sphere')
		# store input argument as member variable
		self.__r = radius

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		z2 = self.__r**2 - x**2 - y**2
		if z2 < 0.0:
			return 0.0
		else:
			return math.sqrt(z2)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(-self.__r,-self.__r)
		n2 = NODE(-self.__r,self.__r)
		n3 = NODE(self.__r,self.__r)
		n4 = NODE(self.__r,-self.__r)
		m12 = NODE(-self.__r,0)
		m23 = NODE(0,self.__r)
		m34 = NODE(self.__r,0)
		m41 = NODE(0,-self.__r)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		self.addLine([m12,m34])
		self.addLine([m41,m23])
		# and the regions with max area and material indices
		self.addRegion(NODE(-0.5*self.__r,-0.5*self.__r),maxArea=self.__r**2/1e3,matID=5)
		self.addRegion(NODE(-0.5*self.__r,0.5*self.__r),maxArea=self.__r**2/1e2)
		self.addRegion(NODE(0.5*self.__r,0.5*self.__r))
		self.addRegion(NODE(0.5*self.__r,-0.5*self.__r),matID=7)
		# set some triangle options
		self.setQuality(34)
		# let's go...
		self.createMesh()


class CONSTRAINTBOUNDARY (MESHMODEL):
	def __init__ (self, directory,
		meshName='constraint_boundary'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization(meshName)

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return 0.0

	# this function actually does all the work
	def build (self,nCells=100,nBoundaryCells=5):
		size = 1.0
		x = np.linspace(-size,size,nBoundaryCells+1)
		# nodes to define the breaklines
		n1 = NODE(-size,-size)
		n2 = NODE(-size,size)
		n3 = NODE(size,size)
		n4 = NODE(size,-size)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		# and the nodes on the boundary
		for xi in x:
			self.addVertices([NODE(xi,-size)])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.0,0.0),maxArea=size**2/nCells)
		# set some triangle options
		self.setQuality(31)
		# let's go...
		self.createMesh()


class SIMPLECHANNEL (MESHMODEL):
	def __init__ (self, directory,
		length=50.0,
		width=10.0,
		slope=0.002):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('simple_channel')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return self.__s * (self.__l - x)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,0.0)
		n2 = NODE(0.0,self.__w)
		n3 = NODE(self.__l,self.__w)
		n4 = NODE(self.__l,0.0)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		self.addLine([n6,n7])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.5*self.__l,0.5*self.__w),maxArea=self.__w*self.__l/1e3)
		# define stringdef lines
		self.addStringdef('inflow_boundary',[n2,n1])
		self.addStringdef('outflow_boundary',[n3,n4])
		# set some triangle options
		self.setQuality(34)
		# let's go...
		self.createMesh()


class LABCHANNEL (MESHMODEL):
	"""class defines the geometry of a rectangular laboratory channel (no side walls)"""
	def __init__ (self,directory,
		meshName='labChannel',
		channelLength=31.45,
		channelWidth=2.0,
		channelBedSlope=0.00172,
		lateralchannelBedSlope=0.0,
		numberOfCells=5000.0):
		# call init of the meshmodel (base)-class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization(meshName)          #This is the name used for the output-files
		# input arguments
		self.__L = channelLength
		self.__W = channelWidth
		self.__J = channelBedSlope
		self.__JL = lateralchannelBedSlope
		self.__N = numberOfCells
		# suface of laboratory channel:
		self.__surface = self.__L * self.__W
		# calculate all the important coordinates
		# x-coordinates of the nodes
		self.__x1 = 0.0
		self.__x2 = 0.0
		self.__x3 = self.__L
		self.__x4 = self.__L
		self.__xbl1 = 1.0  # first oberservation line (breakline)
		self.__xbl2 = 4.0  # second oberservation line (breakline)
		self.__xbl3 = 7.0  # third oberservation line (breakline)
		# y-coordinates of the nodes
		self.__y1 = 0.0
		self.__y2 = self.__W
		self.__y3 = self.__W
		self.__y4 = 0.0
		self.__ybl1 = self.__W
		self.__ybl2 = 0.0

	# process everything
	def build (self):
		# defining the nodes
		n1 = NODE(self.__x1,self.__y1)
		n2 = NODE(self.__x2,self.__y2)
		n3 = NODE(self.__x3,self.__y3)
		n4 = NODE(self.__x4,self.__y4)
		b1 = NODE(self.__xbl1,self.__ybl1)
		b2 = NODE(self.__xbl1,self.__ybl2)
		b3 = NODE(self.__xbl2,self.__ybl1)
		b4 = NODE(self.__xbl2,self.__ybl2)
		b5 = NODE(self.__xbl3,self.__ybl1)
		b6 = NODE(self.__xbl3,self.__ybl2)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4],True)
		self.addLine([n1,n2])
		self.addLine([n3,n4])
		self.addLine([b1,b2])
		self.addLine([b3,b4])
		self.addLine([b5,b6])
		# and the regions with max area and material indices
		channelSmallCellSize = 1.0*self.__surface/self.__N
		channelCellSize = 2.0*self.__surface/self.__N
		channelBigCellSize = 3.0*self.__surface/self.__N
		# channel bed:
		self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=channelBigCellSize,matID=1)
		self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=2)
		self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=2)
		self.addRegion(NODE(0.5*(self.__xbl3+self.__x3),0.5*(self.__y1+self.__y2)),maxArea=channelBigCellSize,matID=2)
		# define stringdefs
		self.addStringdef( 'inflow', [n1,n2] )
		self.addStringdef( 'outflow', [n3,n4] )
		self.addStringdef( 'cs1', [b1,b2] )
		self.addStringdef( 'cs2', [b3,b4] )
		self.addStringdef( 'cs3', [b5,b6] )
		# set the minimum triangle angle
		self.setQuality(30)
		# let's go...
		self.createMesh()

	# function that describes the topography of the model
	def elevationFunction (self, x, y):
		if (self.__y1 <= y <= self.__y2) and (self.__x1 <= x <= self.__x3):
			return self.__J*(self.__L - x) + self.__JL*(-1.0)*(y - self.__W)
		else:
			print "There is no elevation found for x =", x, " and y =", y
			print "Therefore, the elevation is set to -999"
			return -999


class TRAPEZOIDALCHANNEL (MESHMODEL):
	"""class defines the geometry of a trapezoidal (laboratory) channel"""
	def __init__ (self, directory,
		meshName='trapezoidalChannel',
		channelLength=31.45,
		channelWidth=2.0,
		channelHeight=0.3,
		channelBankSlope=2.0/3.0,
		channelBedSlope=0.00172,
		numberOfCells=10000.0,
		maxArea=0.01,
        quality=34.0,
        mType='plane2x'):
		# call init of the meshmodel (base)-class
		MESHMODEL.__init__(self,directory,absolutePrecision=1e-8)
		self.setStringdefDistance(True)
		# this class does all the meshing stuff
		self.initialization(meshName)  # This is the name used for the output-files
		# input arguments
		self.__L = channelLength
		self.__W = channelWidth
		self.__H = channelHeight
		self.__S = channelBankSlope
		self.__J = channelBedSlope
		self.__N = numberOfCells
		self.__q = quality
		self.__A = maxArea
		# bank width:
		self.__BW = self.__H / self.__S
		# suface of laboratory channel:
		self.__surface = self.__L * (self.__W + 2.0*self.__BW)
		# calculate all the important coordinates
		# x-coordinates of the nodes
		self.__x1 = -self.__L / 2.0
		self.__x2 = -self.__L / 2.0
		self.__x3 = -self.__L / 2.0
		self.__x4 = -self.__L / 2.0
		self.__x5 = self.__L / 2.0
		self.__x6 = self.__L / 2.0
		self.__x7 = self.__L / 2.0
		self.__x8 = self.__L / 2.0
		self.__xbl1 = -5.0*25.0
		self.__xbl2 = 0.0
		self.__xbl3 = 5.0*25.0
		# y-coordinates of the nodes
		self.__y1 = -self.__BW
		self.__y2 = 0.0
		self.__y3 = self.__W
		self.__y4 = self.__W + self.__BW
		self.__y5 = -self.__BW
		self.__y6 = 0.0
		self.__y7 = self.__W
		self.__y8 = self.__W + self.__BW
		self.__ybl1 = -self.__BW
		self.__ybl2 = self.__W + self.__BW

	# process everything
	def build (self):
		# defining the nodes
		n1 = NODE(self.__x1,self.__y1)
		n2 = NODE(self.__x2,self.__y2)
		n3 = NODE(self.__x3,self.__y3)
		n4 = NODE(self.__x4,self.__y4)
		n5 = NODE(self.__x5,self.__y5)
		n6 = NODE(self.__x6,self.__y6)
		n7 = NODE(self.__x7,self.__y7)
		n8 = NODE(self.__x8,self.__y8)
		b1 = NODE(self.__xbl1,self.__ybl1)
		b2 = NODE(self.__xbl1,self.__ybl2)
		b3 = NODE(self.__xbl2,self.__ybl1)
		b4 = NODE(self.__xbl2,self.__ybl2)
		b5 = NODE(self.__xbl3,self.__ybl1)
		b6 = NODE(self.__xbl3,self.__ybl2)
		#bf1 = NODE(self.__xbf1,self.__ybl1)
		#bf2 = NODE(self.__xbf1,self.__ybl2)
		obs1 = NODE(0.0,0.0)
		obs2 = NODE(0.0,self.__W)
		# now define the breaklines
		self.addLine([n1,n4,n8,n5],True)
		#self.addLine([bf1,bf2])
		self.addLine([n2,n6])
		self.addLine([n3,n7])
		self.addLine([b1,b2])
		self.addLine([b3,b4])
		self.addLine([b5,b6])
		self.addLine([b5,b6])

		# and the regions with max area and material indices
		# bank left:
		#self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y1+self.__y2)),maxArea=self.__A,matID=4) # ev. fixed bed layer at channel entry
		self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=self.__A,matID=2)
		self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y2)),maxArea=self.__A,matID=2)
		self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y1+self.__y2)),maxArea=self.__A,matID=2)
		self.addRegion(NODE(0.5*(self.__xbl3+self.__x5),0.5*(self.__y1+self.__y2)),maxArea=self.__A,matID=2)
		# channel bed:
		#self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y1+self.__y4)),maxArea=self.__A,matID=4) # ev. fixed bed layer at channel entry
		self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y1+self.__y4)),maxArea=self.__A,matID=1)
		self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y4)),maxArea=self.__A,matID=1)
		self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y1+self.__y4)),maxArea=self.__A,matID=1)
		self.addRegion(NODE(0.5*(self.__xbl3+self.__x5),0.5*(self.__y1+self.__y4)),maxArea=self.__A,matID=1)
		# bank right:
		#self.addRegion(NODE(0.5*(self.__x1+self.__xbf1),0.5*(self.__y3+self.__y4)),maxArea=self.__A,matID=4) # ev. fixed bed layer at channel entry
		self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y3+self.__y4)),maxArea=self.__A,matID=3)
		self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y3+self.__y4)),maxArea=self.__A,matID=3)
		self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y3+self.__y4)),maxArea=self.__A,matID=3)
		self.addRegion(NODE(0.5*(self.__xbl3+self.__x5),0.5*(self.__y3+self.__y4)),maxArea=self.__A,matID=3)
		# define stringdefs
		self.addStringdef( 'inflow', [NODE(self.__x1,self.__y1),NODE(self.__x4,self.__y4)] )
		self.addStringdef( 'outflow', [NODE(self.__x5,self.__y5),NODE(self.__x8,self.__y8)] )
		self.addStringdef( 'bed_inflow', [NODE(self.__x1,self.__y2),NODE(self.__x4,self.__y3)] )
		self.addStringdef( 'bed_outflow', [NODE(self.__x5,self.__y6),NODE(self.__x8,self.__y7)] )
		#self.addStringdef( 'CS0', [bf1,bf2])
		self.addStringdef( 'CS1', [b1,b2])
		self.addStringdef( 'CS2', [b3,b4])
		self.addStringdef( 'CS3', [b5,b6])
		self.addStringdef( 'CSobs', [obs1,obs2])


		# set the minimum triangle angle
		self.setQuality(self.__q)
		# let's go...
		self.createMesh()
		# deleting the triangle files
		self.deleteFiles()

	# function that describes the topography of the model
	def elevationFunction (self, x, y):
		# assign location
		where = None
		if self.__y1 <= y < self.__y2:
			 where = 'bank_left'
		elif self.__y2 <= y <= self.__y3:
			 where = 'bed'
		elif self.__y3 < y <= self.__y4:
			 where = 'bank_right'

		# calculate elevation
		if where == 'bed':
			return ((self.__L/2.0) * self.__J) - (x * self.__J)
		elif where == 'bank_left':
			return ((self.__L/2.0) * self.__J) - (x * self.__J) + (-y)*self.__S
		elif where == 'bank_right':
			return ((self.__L/2.0) * self.__J) - (x * self.__J) + (y-self.__y3)*self.__S
			#       Koordinaten Nullpunkt          Sohlgefaelle     bank slope
		else:
			print "There is no elevation found for x =", x, " and y =", y
			print "Therefore, the elevation is set to -999"
			return -999

