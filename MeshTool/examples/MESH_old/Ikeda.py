
from basement import *
from batchTools import *
from NLSolve import *
from postprocessing import *
import math
import random
from meshModel import NODE, MESHMODEL

class IKEDA (MESHMODEL):
    """class defines the geometry of a half trapezoidal channel with free erodable foreland (Ikeda)"""
    def __init__ (self, directory,
        meshName='Ikdeda',
        modelLength=15.0,
        modelWidth=0.5,
        channelWidth=0.11,
        channelHeight=0.07,
        channelBankSlope=0.063/0.125,
        channelBedSlope=0.00215,
        numberOfCells=10000,
        randomPert=0.0):
        # call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory,absolutePrecision=1e-8)
        self.setStringdefDistance(True)
        # this class does all the meshing stuff
        self.initialization(meshName)  # This is the name used for the output-files
        # input arguments
        self.__L = modelLength
        self.__B = modelWidth
        self.__W = channelWidth
        self.__H = channelHeight
        self.__S = channelBankSlope
        self.__J = channelBedSlope
        self.__N = numberOfCells
        self.__R = randomPert
        # bank width:
        self.__BW = self.__H / self.__S
        # suface of laboratory channel:
        self.__surface = self.__L * self.__B
        # calculate all the important coordinates
        # x-coordinates of the nodes
        self.__x1 = -self.__L / 2.0
        self.__x2 = -self.__L / 2.0
        self.__x3 = -self.__L / 2.0
        self.__x4 = -self.__L / 2.0
        self.__x5 = self.__L / 2.0
        self.__x6 = self.__L / 2.0
        self.__x7 = self.__L / 2.0
        self.__x8 = self.__L / 2.0
        # x-coordinates of the nodes for the breaklines
        self.__xbl1 = 1.5
        self.__xbl2 = 3.5
        self.__xbf1 = -self.__L/2.0 + 1.0 # fixed bed in the first meter
        self.__xbf2 = -self.__L/2.0 + 2.0 # fixed bed in the second meters
        self.__xbf3 = -self.__L/2.0 + 3.0 # fixed bed in the third meters
        # y-coordinates of the nodes
        self.__y1 = 0.0
        self.__y2 = -self.__W
        self.__y3 = -self.__W - self.__BW
        self.__y4 = -self.__B
        self.__y5 = 0.0
        self.__y6 = -self.__W
        self.__y7 = -self.__W - self.__BW
        self.__y8 = -self.__B
        # y-coordinates of the nodes for the breaklines
        self.__ybl1 = 0.0
        self.__ybl2 = -self.__B
        
    # process everything
    def build (self):
        # defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)

        b1 = NODE(self.__xbl1,self.__ybl1)
        b2 = NODE(self.__xbl1,self.__ybl2)
        b3 = NODE(self.__xbl2,self.__ybl1)
        b4 = NODE(self.__xbl2,self.__ybl2)
        b5 = NODE(self.__xbl1,self.__y2)
        b6 = NODE(self.__xbl2,self.__y2)
        b7 = NODE(self.__xbl1,self.__y3)
        b8 = NODE(self.__xbl2,self.__y3)

        #bf1 = NODE(self.__xbf1,self.__ybl1)
        #bf2 = NODE(self.__xbf1,self.__ybl2)
        #bf3 = NODE(self.__xbf2,self.__ybl1)
        #bf4 = NODE(self.__xbf2,self.__ybl2)
        #bf5 = NODE(self.__xbf3,self.__ybl1)
        #bf6 = NODE(self.__xbf3,self.__ybl2)

        # now define the breaklines
        self.addLine([n1,n2,n3,n4,b2,b4,n8,n7,n6,n5,b3,b1],True) # model boundary
        self.addLine([n2,b5,b6,n6])   #longitudinal                                     
        self.addLine([n3,b7,b8,n7])   #longitudinal
        # CS9 defining number of elements
        self.addLine([b1,b5])
        self.addLines([[b5,b7]],[5])    # 5, 7, 9
        self.addLines([[b7,b2]],[8])   # 8, 11, 14
        # CS11 defining number of elements
        self.addLine([b3,b6])
        self.addLines([[b6,b8]],[5])    # 5, 7, 9
        self.addLines([[b8,b4]],[8])   # 8, 11, 14
        #self.addLine([bf1,bf2])
        #self.addLine([bf3,bf4])
        #self.addLine([bf5,bf6])
        # and the regions with max area and material indices
        channelCellSize = self.__surface/self.__N
        bankCellSize = self.__surface/self.__N

        # channel bed:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1) # fixed bed layer at channel entry (sediment source)
        #self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1) # -0.05 m fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1) # -0.10 m fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf3+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__x5),0.5*(self.__y1+self.__y2)),maxArea=channelCellSize,matID=1)
        # bank right:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2) # fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2) # fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2) # -0.05 m fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf3+self.__xbl1),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__x5),0.5*(self.__y2+self.__y3)),maxArea=bankCellSize,matID=2)
        # foreland right:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3) # fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf1+self.__xbf2),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3) # fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf2+self.__xbf3),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3) # -0.05 m fixed bed layer at channel entry
        #self.addRegion(NODE(0.5*(self.__xbf3+self.__xbl1),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__x5),0.5*(self.__y3+self.__y4)),maxArea=bankCellSize,matID=3)
        # define stringdefs
        self.addStringdef('inflow', [n1,n3])
        self.addStringdef('outflow', [n5,n7])
        #self.addStringdef('CS1', [bf1,bf2])
        #self.addStringdef('CS2', [bf1,bf2])
        #self.addStringdef('CS3', [bf1,bf2])
        self.addStringdef('CS9', [b1,b2])
        self.addStringdef('CS11', [b3,b4])
        self.addStringdef('CSChannelNine', [b1,b5])
        self.addStringdef('CSChannelEleven', [b3,b6])

        # set the minimum triangle angle
        self.setQuality(29)
        # let's go...
        self.createMesh()
        # deleting the triangle files
        self.deleteFiles()

    # function that describes the topography of the model
    def elevationFunction (self, x, y):
        # assign location
        where = None
        if (self.__y1 >= y >= self.__y2) and (x <= self.__L/2.0 -1.0):
             where = 'bed'
        elif (self.__y1 >= y >= self.__y2) and (x > self.__L/2.0 -1.0):
             where = 'bed_end'
        elif self.__y2 > y > self.__y3:
             where = 'bank_right'
        elif self.__y3 >= y >= self.__y4:
             where = 'foreland_right'

        # calculate elevation
        if where == 'bed':
            return ((self.__L/2.0)*self.__J) - (x * self.__J) + random.random()*self.__R
        elif where == 'bed_end':
            return ((self.__L/2.0)*self.__J) - (x * self.__J)
        elif where == 'bank_right':
            return ((self.__L/2.0)*self.__J) - (x * self.__J) + (self.__y2-y)*self.__S
            #       Koordinaten Nullpunkt          Sohlgefaelle     bank slope
        elif where == 'foreland_right':
            return ((self.__L/2.0) * self.__J) - (x * self.__J) + self.__H
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999
