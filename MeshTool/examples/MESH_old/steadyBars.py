# Steady bars (forced by groyne)

import math
import random
from meshModel import NODE, MESHMODEL
import numpy as np
from batchTools import *

class STEADYBARS (MESHMODEL):
    """class defines the geometry of a rectangular laboratory channel (no side walls) with obstacle"""
    def __init__ (self,directory,
        meshName='steadyBars',
        channelLength=10000.0,
        channelWidth=60.0,
        channelBedSlope=0.00172,
        groyneLength=20.0,
        groyneWidth=3.0,
        groyneTrapezWidth=34.64,
        groynePlace=0.1,
        numberOfCells=10000.0,
        randomPert=0.0,
        cosineAmplitude=0.0,
        scour=0.0):
        # call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory)
        self.setStringdefDistance(True)
        # activate bandwidth reduction
        self.setReduction(True)
        # this class does all the meshing stuff
        self.initialization(meshName) # This is the name used for the output-files
        # input arguments
        self.__L = channelLength
        self.__W = channelWidth
        self.__J = channelBedSlope
        self.__gL = groyneLength
        self.__gW = groyneWidth
        self.__gWT = groyneTrapezWidth
        self.__gP = groynePlace
        self.__N = numberOfCells
        self.__Rand = randomPert
        self.__Amplitude = cosineAmplitude
        self.__Scour = scour # scour at obstacle
        # suface of laboratory channel:
        self.__surface = self.__L * self.__W
        # calculate all the important coordinates
        # x-coordinates of the nodes
        self.__x1 = 0.0
        self.__x2 = 0.0
        self.__x3 = self.__L
        self.__x4 = self.__L
        self.__x5 = self.__gP*self.__L + 0.5*self.__gW + self.__gWT
        self.__x6 = self.__gP*self.__L + 0.5*self.__gW
        self.__x7 = self.__gP*self.__L - 0.5*self.__gW
        self.__x8 = self.__gP*self.__L - 0.5*self.__gW - self.__gWT
        self.__xblRPoben = 0.05*self.__L  # break line for no random perturbation at beginning
        self.__xblRPunten = 0.95*self.__L  # break line for no random perturbation at beginning
        self.__xbl1 = 0.1*self.__L  # 1 oberservation line (breakline)
        self.__xbl2 = 0.2*self.__L  # 2 oberservation line (breakline)
        self.__xbl3 = 0.3*self.__L  # 3 oberservation line (breakline)
        self.__xbl4 = 0.45*self.__L  # 4 oberservation line (breakline)
        self.__xbl5 = 0.55*self.__L  # 5 oberservation line (breakline)
        self.__xbl6 = 0.6*self.__L  # 6 oberservation line (breakline)
        self.__xbl7 = 0.7*self.__L  # 7 oberservation line (breakline)
        self.__xbl8 = 0.8*self.__L  # 8 oberservation line (breakline)
        self.__xbl9 = 0.9*self.__L  # 9 oberservation line (breakline)
        # y-coordinates of the nodes
        self.__y1 = self.__W
        self.__y2 = 0.0
        self.__y3 = 0.0
        self.__y4 = self.__W
        self.__y5 = self.__W
        self.__y6 = self.__W - self.__gL
        self.__y7 = self.__W - self.__gL
        self.__y8 = self.__W
        self.__ybl1 = self.__W
        self.__ybl2 = 0.0
        self.__yhalf = self.__W/2.0

    # process everything
    def build (self):
        self.setOption('-DpaA')
        # defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)
        b1 = NODE(self.__xbl1,self.__ybl1)
        b2 = NODE(self.__xbl1,self.__ybl2)
        b3 = NODE(self.__xbl2,self.__ybl1)
        b4 = NODE(self.__xbl2,self.__ybl2)
        b5 = NODE(self.__xbl3,self.__ybl1)
        b6 = NODE(self.__xbl3,self.__ybl2)
        b7 = NODE(self.__xbl4,self.__ybl1)
        b8 = NODE(self.__xbl4,self.__ybl2)
        b9 = NODE(self.__xbl5,self.__ybl1)
        b10 = NODE(self.__xbl5,self.__ybl2)
        b11 = NODE(self.__xbl6,self.__ybl1)
        b12 = NODE(self.__xbl6,self.__ybl2)
        b13 = NODE(self.__xbl7,self.__ybl1)
        b14 = NODE(self.__xbl7,self.__ybl2)
        b15 = NODE(self.__xbl8,self.__ybl1)
        b16 = NODE(self.__xbl8,self.__ybl2)
        b17 = NODE(self.__xbl9,self.__ybl1)
        b18 = NODE(self.__xbl9,self.__ybl2)
        #r1 = NODE(self.__xblRPoben,self.__ybl1)
        #r2 = NODE(self.__xblRPoben,self.__ybl2)
        #r3 = NODE(self.__xblRPunten,self.__ybl1)
        #r4 = NODE(self.__xblRPunten,self.__ybl2)
        #r5 = NODE(self.__xblRPoben,self.__yhalf)
        #r6 = NODE(self.__xbl1,self.__yhalf)
        # now define the breaklines
        self.addLine([n1,n2,n3,n4,n5,n6,n7,n8],True)
        self.addLine([n1,n2])
        self.addLine([n4,n3])
        self.addLine([b1,b2])
        self.addLine([b3,b4])
        self.addLine([b5,b6])
        self.addLine([b7,b8])
        self.addLine([b9,b10])
        self.addLine([b11,b12])
        self.addLine([b13,b14])
        self.addLine([b15,b16])
        self.addLine([b17,b18])
        #self.addLine([r1,r2])
        #self.addLine([r3,r4])
        #self.addLine([r5,r6])
        # and the regions with max area and material indices
        smallCellSize = 0.5*self.__surface/self.__N
        SmallCellSize = 1.5*self.__surface/self.__N
        mediumCellSize = 1.5*self.__surface/self.__N
        bigCellSize = 3.0*self.__surface/self.__N
        # channel bed:
        self.addRegion(NODE(0.5*(self.__x1+self.__xbl1),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        #self.addRegion(NODE(0.5*(self.__xblRPoben+self.__xbl1),0.25*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=3)
        #self.addRegion(NODE(0.5*(self.__xblRPoben+self.__xbl1),0.75*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=4)
        self.addRegion(NODE(0.5*(self.__xbl1+self.__xbl2),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl2+self.__xbl3),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl3+self.__xbl4),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl4+self.__xbl5),0.45*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl5+self.__xbl6),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl6+self.__xbl7),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl7+self.__xbl8),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        self.addRegion(NODE(0.5*(self.__xbl8+self.__xbl9),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        #self.addRegion(NODE(0.5*(self.__xbl9+self.__xblRPunten),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=2)
        self.addRegion(NODE(0.5*(self.__xbl9+self.__x3),0.5*(self.__y1+self.__y2)),maxArea=mediumCellSize,matID=1)
        # define stringdefs
        self.addStringdef( 'inflow', [n1,n2] )
        self.addStringdef( 'outflow', [n3,n4] )
        self.addStringdef( 'rightbank', [n2,n3] )
        self.addStringdef( 'leftbankOben', [n1,n8] )
        self.addStringdef( 'leftbankUnten', [n5,n4] )
        self.addStringdef( 'cs1', [b1,b2] )
        self.addStringdef( 'cs2', [b3,b4] )
        self.addStringdef( 'cs3', [b5,b6] )
        self.addStringdef( 'cs4', [b7,b8] )
        self.addStringdef( 'cs5', [b9,b10] )
        self.addStringdef( 'cs6', [b11,b12] )
        self.addStringdef( 'cs7', [b13,b14] )
        self.addStringdef( 'cs8', [b15,b16] )
        self.addStringdef( 'cs9', [b17,b18] )
        # set the minimum triangle angle
        self.setQuality(28)
        # let's go...
        self.createMesh()

    # function that describes the topography of the model
    def elevationFunction (self, x, y):
        devDown=50.0
        devUp=10.0
        if (self.__y2 <= y <= self.__y1) and (self.__xblRPoben <= x <= self.__xbl4):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) + random.uniform(-1.0,1.0)*self.__Rand
        elif (self.__y2 <= y <= self.__y1) and (self.__xbl5 <= x <= self.__xblRPunten):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) + random.uniform(-1.0,1.0)*self.__Rand
        elif (self.__y2 <= y <= self.__y1) and (self.__xbl4 < x < self.__xbl5):
            if ( (self.__L/2.0 + devDown) < x  or x < (self.__L/2.0 - devUp) ):
                return self.__J*(self.__L - x)
            else:
                return self.__J*(self.__L - x) - self.__Scour*math.sin( 2.0*math.pi*( (x-(0.5*self.__L - devUp)) / ( (devDown+devUp)*2.0)) )
        elif (self.__y2 <= y <= self.__y1) and (self.__x2 <= x < self.__xblRPoben):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # oben keine random perturbation
        elif (self.__y2 <= y <= self.__y1) and (self.__xblRPunten < x <= self.__x3):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # unten keine random perturbation
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999

"""

        if (self.__y2 <= y <= self.__y1) and (self.__xblRPoben <= x <= self.__xblRPunten):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) + random.uniform(-1.0,1.0)*self.__Rand
        elif (self.__y2 <= y <= self.__y1) and (self.__x2 <= x < self.__xblRPoben):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # oben keine random perturbation
        elif (self.__y2 <= y <= self.__y1) and (self.__xblRPunten < x <= self.__x3):
            return self.__J*(self.__L - x) + self.__Amplitude*math.sin(2.0*math.pi*x/(self.__W*6.0))*math.cos(2.0*math.pi*y/(self.__W*2.0)) # unten keine random perturbation
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999

     
        if (self.__y2 <= y <= self.__y1) and (self.__xblRPoben <= x <= self.__xblRPunten):
            return self.__J*(self.__L - x) + random.uniform(-1.0,1.0)*self.__Rand   # Hauptteil Kanal
        elif (self.__y2 <= y <= self.__y1) and (self.__x2 <= x < self.__xblRPoben):
            return self.__J*(self.__L - x) # oben keine random perturbation
        elif (self.__y2 <= y <= self.__y1) and (self.__xblRPunten < x <= self.__x3):
            return self.__J*(self.__L - x) # unten keine random perturbation
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999
"""