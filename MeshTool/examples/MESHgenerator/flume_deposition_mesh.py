#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class TFLUME (MESHMODEL):
	def __init__ (self, directory,
		length=1000.0,
		width=10.0,
		slope=0.01, maxArea=0.01, quality=34.0,mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('flume_deposition_channel',mType)
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope
		self.__A = maxArea
		self.__q = quality
		# activate bandwidth reduction
		self.setReduction(True)

	def MaterialIDFunction(self, matID, x, y):
			return 1
	
	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		deposit = 0.0
		if x > (0.5*self.__l-2.0*self.__w) and x < (0.5*self.__l+2.0*self.__w) and y > 0.33*0.5*self.__w:
			deposit = 5.0
		else:
			deposit = 0.0
		return self.__s * (self.__l - x)+deposit

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		n2 = NODE(self.__l,-0.5*self.__w)
		n3 = NODE(self.__l,0.5*self.__w)
		n8 = NODE(0.0,0.5*self.__w)
		# now define the domain
		self.addLine([n1,n2,n3,n8,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.1*self.__l,0.0),maxArea=self.__A)
		# define stringdef lines
		self.addStringdef('1',[n8,n1])
		self.addStringdef('2',[n2,n3])
		# set some triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		
print '*** flume+deposition mesh generator ***'
wd = WORKINGDIRECTORY('flume_depo')
th = TFLUME(wd,
	length=6000.0,
	width=60.0,
	slope=0.0025,
	maxArea=15.0,
	quality=32.0,
	mType='plane3x')
th.build()
