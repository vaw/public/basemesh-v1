#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class BELLAL (MESHMODEL):
	def __init__ (self, directory,
		length=6.9,
		width=0.5,
		slope=0.0302):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('Bellal_channel')
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return self.__s * (self.__l - x)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		n2 = NODE(self.__l,-0.5*self.__w)
		n3 = NODE(self.__l,0.5*self.__w)
		n4 = NODE(0.0,0.5*self.__w)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.5*self.__l,0.0),maxArea=self.__w*self.__l/(15e03))

		# define stringdef lines
		self.addStringdef('inflow_boundary',[n4,n1])
		self.addStringdef('outflow_boundary',[n2,n3])
		# set some triangle options
		self.setQuality(32)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		
print '*** bellal test mesh generator ***'
wd = WORKINGDIRECTORY('Bellal')
mod = BELLAL(wd)
mod.build()

