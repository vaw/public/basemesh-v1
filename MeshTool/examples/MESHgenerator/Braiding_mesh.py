#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class BRAIDING (MESHMODEL):
	def __init__ (self, directory,
		length=1000.0,
		width=10.0,
		slope=0.01, maxArea=0.01, quality=34.0,mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('braiding_channel',mType)
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope
		self.__A = maxArea
		self.__q = quality
		# activate bandwidth reduction
		self.setReduction(True)

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		return self.__s * (self.__l - x)

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		n2 = NODE(self.__w*10,-5*self.__w)
		n3 = NODE(self.__l-self.__w*10,-5*self.__w)
		n4 = NODE(self.__l,-0.5*self.__w)
		n5 = NODE(self.__l,0.5*self.__w)
		n6 = NODE(self.__l-self.__w*10,5*self.__w)
		n7 = NODE(self.__w*10,5*self.__w)
		n8 = NODE(0.0,0.5*self.__w)
		# now define the domain
		self.addLine([n1,n2,n3,n4,n5,n6,n7,n8,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.1*self.__l,0.0),maxArea=self.__A)
		# define stringdef lines
		self.addStringdef('1',[n8,n1])
		self.addStringdef('2',[n4,n5])
		# set some triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		
print '*** Braiding mesh generator ***'
wd = WORKINGDIRECTORY('Braid')
th = BRAIDING(wd,
	length=12000.0,
	width=60.0,
	slope=0.0025,
	maxArea=50.0,
	quality=32.0,
	mType='plane3x')
th.build()
