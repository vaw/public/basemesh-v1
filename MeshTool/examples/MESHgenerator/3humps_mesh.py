#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL


class THREE_HUMPS (MESHMODEL):
	def __init__ (self, directory,
		length=75.0,
		width=30.0,
		dam=16.0,
		mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('3_humps_test',mType)
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__d = dam

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		#expression to described bottom elevation eta=f(x,y)
		a=(1.0-1.0/8.0*((x-30.0)**2+(y+9.0)**2)**0.5)
		b=(1.0-1.0/8.0*((x-30.0)**2+(y-9.0)**2)**0.5)
		c=(3.0-3.0/10.0*((x-47.5)**2+y**2)**0.5)
		eta =max(0.0, a, b, c) # max(0.0, 1-1/8*((x-30)**2+(y+9)**2)**0.5, 1-1/8*((x-30)**2+(y-9)**2)**0.5, 3-3/10*((x-47.5)**2+y**2)**0.5)
		return eta

	# this function actually does all the work
	def build (self):
		## nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__w)
		n2 = NODE(self.__d,-0.5*self.__w)
		n3 = NODE(self.__l,-0.5*self.__w)
		n4 = NODE(self.__l,0.5*self.__w)
		n5 = NODE(self.__d,0.5*self.__w)
		n6 = NODE(0.0,0.5*self.__w)
		# now define the breaklines
		self.addLine([n1,n3,n4,n6,n1])
		self.addLine([n2,n5])
		self.addRegion(NODE(0.5*self.__d,0.0),maxArea=self.__w*self.__l/1.5e4,matID=5)
		self.addRegion(NODE(1.5*self.__d,0.0),maxArea=self.__w*self.__l/1.5e4,matID=6)
		# set some triangle options
		self.setQuality(32)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		
print '*** 3humps test mesh generator ***'
wd = WORKINGDIRECTORY('3humps_test')
th = THREE_HUMPS(wd,mType='plane2x')
th.build()
