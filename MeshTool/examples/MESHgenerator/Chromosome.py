#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class CHROMOSOME (MESHMODEL):
	def __init__ (self, directory,
		length=1000.0,
		width=10.0,
		slope=0.01,
		gap=1.0,
		junction=10.0,
		height=0.0,
		CSexp=0.0,
		maxArea=0.01,
		quality=34.0,
		mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory,absolutePrecision=1e-3)
		# this class does all the meshing stuff
		self.initialization('chromosome',mType)
		# store input argument as member variable
		self.__l = length
		self.__w = width
		self.__s = slope
		self.__g = gap
		self.__j = junction
		self.__h = height
		self.__e = CSexp
		self.__A = maxArea
		self.__q = quality
		# activate bandwidth reduction
		self.setReduction(True)

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		hx = self.__s * (self.__l - x)
		if (math.fabs(y) >= 0.5*self.__g):
			# channel
			y = math.fabs(y)-0.5*(self.__w+self.__g)
			hy = self.__h * math.pow(2.0*y/self.__w,self.__e)
		else:
			# junction
			hy = self.__h
		return hx+hy

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0.0,-0.5*self.__g)
		n2 = NODE(0.0,-(self.__w+0.5*self.__g))
		n3 = NODE(self.__l,-(self.__w+0.5*self.__g))
		n4 = NODE(self.__l,-0.5*self.__g)
		n5 = NODE(0.5*(self.__l+self.__j),-0.5*self.__g)
		n6 = NODE(0.5*(self.__l+self.__j),0.5*self.__g)
		n7 = NODE(self.__l,0.5*self.__g)
		n8 = NODE(self.__l,self.__w+0.5*self.__g)
		n9 = NODE(0.0,self.__w+0.5*self.__g)
		n10 = NODE(0.0,0.5*self.__g)
		n11 = NODE(0.5*(self.__l-self.__j),0.5*self.__g)
		n12 = NODE(0.5*(self.__l-self.__j),-0.5*self.__g)
		# now define the domain
		self.addLine([n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12],True)
		# add line
		self.addLine([n5,n12])
		self.addLine([n6,n11])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.5*self.__l,-self.__w),maxArea=self.__A)
		self.addRegion(NODE(0.5*self.__l,0.0),maxArea=self.__A)
		self.addRegion(NODE(0.5*self.__l,self.__w),maxArea=self.__A)
		# define stringdef lines
		self.addStringdef('11',[n1,n2])
		self.addStringdef('12',[n3,n4])
		self.addStringdef('21',[n9,n10])
		self.addStringdef('22',[n7,n8])
		self.addStringdef('31',[n5,n12])
		self.addStringdef('32',[n6,n11])
		# set some triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		
print '*** chromosome channel mesh generator ***'
wd = WORKINGDIRECTORY('ChromosomeTest')
th = CHROMOSOME(wd,
	length=200.0,
	width=10.0,
	slope=0.01,
	height=1.0,
	gap=1.0,
	junction=9.0,
	CSexp=2.0,
	maxArea=0.2,
	quality=32.0,
	mType='plane3x')
th.build()
