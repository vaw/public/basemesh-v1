#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class SIMPLECHANNEL (MESHMODEL):
    def __init__ (self, directory,
        length=1000.0,
        width=10.0,
        slope=0.01,
        height=0.0,
        CSexp=0.0,
        maxArea=0.01,
        quality=34.0,
        mType='plane2x'):
        # call init of the meshmodel class
        MESHMODEL.__init__(self,directory)
        # this class does all the meshing stuff
        self.initialization('simple_channel',mType)
        # store input argument as member variable
        self.__l = length
        self.__w = width
        self.__s = slope
        self.__h = height
        self.__e = CSexp
        self.__A = maxArea
        self.__q = quality
        # activate bandwidth reduction
        self.setReduction(True)

    # this function must be implemented (abstract function in base class)
    def elevationFunction (self, x, y):
        hx = self.__s * (self.__l - x)
        hy = self.__h * math.pow(2.0*math.fabs(y)/self.__w,self.__e)
        return hx+hy

    # this function actually does all the work
    def build (self):
        # nodes to define the breaklines
        n1  = NODE(0.0,-0.5*self.__w)
        n2  = NODE(0.65*self.__l,-0.5*self.__w)
        n3  = NODE(0.7*self.__l,-0.5*self.__w)
        n4  = NODE(self.__l,-0.5*self.__w)
        n5  = NODE(self.__l,0.5*self.__w)
        n6  = NODE(0.3*self.__l,0.5*self.__w)
        n7  = NODE(0.35*self.__l,0.5*self.__w)
        n8  = NODE(0.0,0.5*self.__w)
        n9  = NODE(0.5*self.__l,0.5*self.__w)
        n10 = NODE(0.5*self.__l,-0.5*self.__w)
        # now define the domain
        self.addLine([n1,n2,n3,n4,n5,n6,n7,n8],True)
        self.addLines([[n9,n10]],[10])
        self.addLines([[n8,n1]],[10])
        self.addLines([[n4,n5]],[10])
        # and the regions with max area and material indices
        self.addRegion(NODE(0.1*self.__l,0.0),maxArea=self.__A,matID=1)
        self.addRegion(NODE(0.9*self.__l,0.0),maxArea=self.__A,matID=2)
        # define stringdef lines
        self.addStringdef('inflow_boundary',[n8,n1])
        self.addStringdef('outflow_boundary',[n4,n5])
        # set some triangle options
        self.setQuality(self.__q)
        self.setOption('-D')
        # let's go...
        self.createMesh()

class TRAPEZCHANNEL (MESHMODEL):
    def __init__ (self, directory,
        length=1000.0,
        width=10.0,
        slope=0.01,
        bankSlope=2.0/3.0,
        height=0.0,
        maxArea=0.01,
        quality=34.0,
        mType='plane2x'):
        # call init of the meshmodel class
        MESHMODEL.__init__(self,directory)
        # this class does all the meshing stuff
        self.initialization('trapez_channel',mType)
        # store input argument as member variable
        self.__l = length
        self.__w = width
        self.__s = slope
        self.__b = bankSlope
        self.__h = height
        self.__bw = self.__h / self.__b
        self.__A = maxArea
        self.__q = quality
        self.__y1 = -0.5*self.__w
        self.__y2 = -0.5*self.__w-self.__bw
        self.__y8 = 0.5*self.__w
        self.__y7 = 0.5*self.__w+self.__bw
        self.setReduction(True)

    # this function must be implemented (abstract function in base class)
    def elevationFunction (self, x, y):
        # assign location
        where = None
        if self.__y8 < y <= self.__y7:
             where = 'bank_left'
        elif self.__y1 <= y <= self.__y8:
             where = 'bed'
        elif self.__y2 <= y < self.__y1:
             where = 'bank_right'

        # calculate elevation
        if where == 'bed':
            return self.__s * (self.__l - x)
        elif where == 'bank_left':
            return self.__s * (self.__l - x) +(y-self.__y8)*self.__b
        elif where == 'bank_right':
            return self.__s * (self.__l - x) -(y-self.__y1)*self.__b
            #      bed slope                  bank slope
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to -999"
            return -999

    # this function actually does all the work
    def build (self):
        # nodes to define the breaklines
        n1  = NODE(0.0,-0.5*self.__w)
        n2  = NODE(0.0,-0.5*self.__w-self.__bw)
        n3  = NODE(self.__l,-0.5*self.__w-self.__bw)
        n4  = NODE(self.__l,-0.5*self.__w)
        n5  = NODE(self.__l,0.5*self.__w)
        n6  = NODE(self.__l,0.5*self.__w+self.__bw)
        n7  = NODE(0.0,0.5*self.__w+self.__bw)
        n8  = NODE(0.0,0.5*self.__w)
        n9  = NODE(0.5*self.__l,0.5*self.__w+self.__bw)
        n10 = NODE(0.5*self.__l,-0.5*self.__w-self.__bw)
        # now define the domain
        self.addLine([n1,n2,n3,n4,n5,n6,n7,n8],True)
        #self.addLines([[n9,n10]],[10])
        self.addLines([[n1,n8]],[10])
        self.addLines([[n8,n7]],[4])
        self.addLines([[n2,n1]],[4])
        self.addLines([[n4,n5]],[10])
        self.addLines([[n5,n6]],[4])
        self.addLines([[n3,n4]],[4])
        self.addLine([n8,n5])
        self.addLine([n1,n4])
        # and the regions with max area and material indices
        self.addRegion(NODE(0.4*self.__l,0.0),maxArea=self.__A,matID=1)
        self.addRegion(NODE(0.4*self.__l,-0.55*self.__w),maxArea=self.__A,matID=2)
        self.addRegion(NODE(0.4*self.__l,0.55*self.__w),maxArea=self.__A,matID=2)
        # define stringdef lines
        self.addStringdef('inflow_boundary',[n1,n8])
        self.addStringdef('outflow_boundary',[n4,n5])
        # set some triangle options
        self.setQuality(self.__q)
        self.setOption('-D')
        # let's go...
        self.createMesh()



print '*** Simple channel mesh generator ***'
wd = WORKINGDIRECTORY('SimpleChannel_v2x3x')
th = SIMPLECHANNEL(wd,
    length=200.0,
    width=20.0,
    slope=0.005,
    height=2.0,
    CSexp=2.0,
    maxArea=3.0,
    quality=32.0,
    mType='plane2x3x')
th.build()


print '*** Trapezoidal channel mesh generator ***'
wd = WORKINGDIRECTORY('TrapezChannel_v2x3x')
th = TRAPEZCHANNEL(wd,
    length=200.0,
    width=20.0,
    slope=0.005,
    bankSlope=2.0/3.0,
    height=6.0,
    maxArea=3.0,
    quality=32.0,
    mType='plane2x3x')
th.build()
