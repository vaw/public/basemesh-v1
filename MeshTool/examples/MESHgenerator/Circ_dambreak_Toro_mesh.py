#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

# default input parameters
optsPars = OPTIONPARSING()
optsPars.addOption('a',0.25,4)

# actual meshing class
class CIRC_DAM_BREAK (MESHMODEL):
	def __init__ (self, directory,meshname,
		radius=2.5, extent = 20.0, maxArea=0.05, quality=34.0,mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization(meshname,mType)
		# store input argument as member variable
		self.__r = radius
		self.__l = extent
		self.__A = maxArea
		self.__q = quality
		# activate bandwidth reduction
		self.setReduction(True)
		
	def MaterialIDFunction(self, matID, x, y):
		d = (x**2 + y**2)**0.5
		if d > self.__r:
			return 1
		else:
			return 2

	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(-self.__l,-self.__l)
		n2 = NODE(-self.__l,self.__l)
		n3 = NODE(self.__l,self.__l)
		n4 = NODE(self.__l,-self.__l)
		# now define the breaklines
		self.addLine([n1,n2,n3,n4,n1])
		# and the regions with max area and material indices
		self.addRegion(NODE(0.0,0.0),maxArea=self.__A)
		# set triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()
		#for icell in 

# here we run everything
print '*** circular dam break Toro mesh generator ***'
wd = WORKINGDIRECTORY('circ_damb_toro')
optsPars.parseOptions(sys.argv[1:]) # parse command line options
th = CIRC_DAM_BREAK(wd,
	meshname = 'circ_dam_break',
	maxArea=float(optsPars.getValue('a')),
	mType='plane3x')
th.build()

