#AFK TEST
#!/usr/bin/env python


from batchTools import *
from meshModel import NODE, MESHMODEL
import argparse, math

class ChannelTestMigrationV30 (MESHMODEL):
	"""class defines the geometry of a trapezoidal channel"""
	def __init__ (self, directory,meshname,
		maxArea = 1.0,
		channelLength=5000,
		channelBedWidth=100,
		channelHeight=50,
		bedSlope=0.02,
		bankSlope=2,
		nQP=10,
		ksBed=0.6,
		ksBank=1.2,
		quality=32.0,
		mType='plane2x'):
		# call init of the meshmodel (base)-class, this class does all the meshing stuff
		MESHMODEL.__init__(self, directory)
		# this class does all the meshing stuff
		self.initialization(meshname,mType)
		# store parameters
		self.__A = maxArea
		self.__q = quality
		self.__lc = channelLength
		self.__h = channelHeight
		self.__wc = channelBedWidth
		self.__s = bedSlope
		self.__b = bankSlope
		self.__N = nQP
		self.__ksBed = ksBed
		self.__ksBank = ksBank


	def build(self):
		# needed coordinates
		xB,xU = (0.0, self.__lc)
		ybL,ybR = (-0.5*self.__wc, 0.5*self.__wc)
		ytL,ytR = (-(0.5*self.__wc+self.__b*self.__h), 0.5*self.__wc+self.__b*self.__h)
		# bed lines
		self.addLine( [NODE(xB,ybL),NODE(xU,ybL)], name='bottomLeft' )
		self.addLine( [NODE(xB,ybR),NODE(xU,ybR)], name='bottomRight' )
		# bank lines
		self.addLine( [NODE(xB,ytL),NODE(xU,ytL)], name='topLeft' )
		self.addLine( [NODE(xB,ytR),NODE(xU,ytR)], name='topRight' )
		# boundary
		boundaryLine = [NODE(0.0,0.0),NODE(xB,ybL),NODE(xB,ytL)]
		boundaryLine.extend( [NODE(xU,ytL),NODE(xU,ybL),NODE(xU,ybR),NODE(xU,ytR)] )
		boundaryLine.extend( [NODE(xB,ytR),NODE(xB,ybR),NODE(0.0,0.0)] )
		self.addLine(boundaryLine)
		# matid only for illustration purposes
		self.addRegion(NODE(0.5*self.__lc,0.0),matID=1,maxArea=self.__A)
		self.addRegion(NODE(0.5*self.__lc,0.5*(self.__wc+self.__h*self.__b)),matID=2,maxArea=self.__A)
		self.addRegion(NODE(0.5*self.__lc,-0.5*(self.__wc+self.__h*self.__b)),matID=2,maxArea=self.__A)
		# define number of cross sections
		dx = self.__lc/self.__N
		for ii in range(self.__N+1):
			self.addCrossSection(NODE(ii*dx,0.0))
		# define the friction ranges
		friction = {'default': self.__ksBank,
					'topLeft-bottomLeft': self.__ksBank,
					'bottomLeft-bottomRight': self.__ksBed,
					'bottomRight-topRight': self.__ksBank}
		self.setFriction(friction)

		# define stringdef lines
		self.addStringdef('inflow_boundary',[NODE(0.0,0.0),NODE(xB,ybL),NODE(xB,ytL),NODE(xB,ytR),NODE(xB,ybR)])
		self.addStringdef('outflow_boundary',[NODE(xU,ytL),NODE(xU,ybL),NODE(xU,ybR),NODE(xU,ytR)])
		# set some triangle options
		self.setQuality(self.__q)
		self.setOption('-D')
		# let's go...
		self.createMesh()

	def elevationFunction(self, x, y):
		zBed = x*self.__s
		# because of symmetry
		x = min(x,self.__lc-x)
		dy = abs(y)-0.5*self.__wc
		if dy > 0.0:
			zBank = min(self.__h, dy/self.__b)
		else:
			zBank = 0.0
		return zBed + zBank



print '*** channel_test mesh generator ***'
 #create model directory
wd = WORKINGDIRECTORY('Channel_test_v30')
	# cretae the mesh model
th = ChannelTestMigrationV30(wd,
	meshname = 'channel_mesh_v30',
	maxArea=100.0,
	channelLength=5000,
	channelBedWidth=100,
	channelHeight=50,
	bedSlope=0.02,
	bankSlope=1.5,
	quality=34.0,
	mType='plane3x')
th.build()