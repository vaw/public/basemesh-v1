#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class CONICAL_DUNE (MESHMODEL):
	def __init__ (self, directory,
		radius=5.0,mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory)
		# this class does all the meshing stuff
		self.initialization('Conical_dune',mType)
		# store input argument as member variable
		self.__r = radius
		# activate bandwidth reduction
		self.setReduction(True)

	# this function must be implemented (abstract function in base class)
	def elevationFunction (self, x, y):
		#expression to described bottom elevation eta=f(x,y)
		if ((x>300 and x<500) and (y>400 and y<600)):
			eta = 0.1 + (math.sin(3.14159*(x-300)/200)**2)*(math.sin(3.14159*(y-400)/200)**2)
		else:
			eta = 0.1
		return eta



	# this function actually does all the work
	def build (self):
		# nodes to define the breaklines
		n1 = NODE(0,0)
		n2 = NODE(1000,0)
		n3 = NODE(1000,1000)
		n4 = NODE(0,1000)
		#adding nodes to reduce the length of stringdefs (number of nodes)
		n5 = NODE(0, 125)
		n6 = NODE(0,250)
		n7 = NODE(0, 375)
		n8 = NODE(0, 500)
		n9 = NODE(0,625)
		n10 = NODE(0, 750)
		n11 = NODE(0, 875)
		n12 = NODE(1000, 125)
		n13 = NODE(1000, 250)
		n14 = NODE(1000, 375)
		n15 = NODE(1000, 500)
		n16 = NODE(1000, 625)
		n17 = NODE(1000, 750)
		n18 = NODE(1000, 875)
		# now define the breaklines
		self.addLine([n1,n2,n12,n13,n14,n15,n16,n17,n18,n3,n4,n11,n10,n9,n8,n7,n6,n5],True)
		# and the regions with max area and material indices
		#change maxArea to change size, maxArea=54 for 30k, maxArea=27 for 60k, maxArea= 18 for 90k and maxArea=13 for 120k
		self.addRegion(NODE(500,500),maxArea=27,matID=1)

		# define stringdef lines
		self.addStringdef('1i',[n1,n5])
		self.addStringdef('2i',[n5,n6])
		self.addStringdef('3i',[n6,n7])
		self.addStringdef('4i',[n7,n8])
		self.addStringdef('5i',[n8,n9])
		self.addStringdef('6i',[n9,n10])
		self.addStringdef('7i',[n10,n11])
		self.addStringdef('8i',[n11,n4])
		self.addStringdef('1o',[n2,n12])
		self.addStringdef('2o',[n12,n13])
		self.addStringdef('3o',[n13,n14])
		self.addStringdef('4o',[n14,n15])
		self.addStringdef('5o',[n15,n16])
		self.addStringdef('6o',[n16,n17])
		self.addStringdef('7o',[n17,n18])
		self.addStringdef('8o',[n18,n3])

		# set some triangle options
		self.setQuality(32) #set to 32 initially
		self.setOption('-D')
		# let's go...
		self.createMesh()

print '*** conical dune mesh generator ***'
wd = WORKINGDIRECTORY('conical_dune')
th = CONICAL_DUNE(wd, mType='plane3x')
th.build()