#!/usr/bin/env python
import math
import random
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL

class UCHANNEL (MESHMODEL):
    """class defines the geometry of a rectangular/trapezoidal U-turn (180 degrees) laboratory channel"""
    def __init__ (self,directory,
    	meshName='Uchannel', 
    	channelLength=5.0,
    	channelBedWidth=1.0,
        channelRadius=10.0,
    	channelBedSlope=0.2,
        observationAngles=[30.0],
    	numberOfCells=5000.0,
        randomPert=0.0, quality=34.0,mType='plane2x'):
		# call init of the meshmodel (base)-class
        MESHMODEL.__init__(self,directory,absolutePrecision=1e-8)
        self.setStringdefDistance(True)
        # this class does all the meshing stuff
        self.initialization(meshName,mType) # name used for output-files
        # input arguments
        self.__q = quality
        self.__L = channelLength
        self.__W = channelBedWidth
        self.__R = channelRadius
        self.__J = channelBedSlope
        self.__JB = channelBedSlope*1.0 # Just for testing: Slope at boundary reach (not used)
        self.__oD = observationAngles # angle after which an observation line is made
        self.__N = numberOfCells
        self.__Rand = randomPert
        # Different radius of laboratory channel
        self.__outerRadius = self.__R + self.__W/2.0
        self.__innerRadius = self.__R - self.__W/2.0
        # suface of laboratory channel
        areaChannel = 2.0 * (self.__L * self.__W)
        areaCurve = math.pi * (self.__outerRadius*self.__outerRadius - self.__innerRadius*self.__innerRadius)
        self.__surface = areaChannel + areaCurve

        # list with angles (theta) defined by the user
        self.__thetaObs_list = []
        if (len(self.__oD) == 1):           
            for ii in range(1,int(180.0/self.__oD[0])):
                self.__thetaObs_list.append(ii*self.__oD[0])
        else:
            for ii in self.__oD:
                self.__thetaObs_list.append(180.0-ii)
            self.__thetaObs_list.reverse()

        cellSize = 1.0*self.__surface/self.__N
        dLBoundary = math.sqrt(2.0*cellSize)

		# calculate all the important coordinates
		# x-coordinates of the nodes
        self.__x1 = 0.0
        self.__x2 = self.__W
        self.__x3 = 0.0
        self.__x4 = self.__W
        self.__x5 = 2.0*self.__R
        self.__x6 = 2.0*self.__R + self.__W
        self.__x7 = 2.0*self.__R
        self.__x8 = 2.0*self.__R + self.__W       
        self.__xR = self.__R + 0.5*self.__W
        self.__x9 = 0.0
        self.__x10 = self.__W  
        self.__x11 = 0.0
        self.__x12 = self.__W      

        # y-coordinates of the nodes
        self.__y1 = 0.0
        self.__y2 = 0.0
        self.__y3 = self.__L
        self.__y4 = self.__L
        self.__y5 = self.__L
        self.__y6 = self.__L
        self.__y7 = 0.0
        self.__y8 = 0.0
        self.__yR = self.__L
        self.__y9 = 2.0*dLBoundary
        self.__y10 = 2.0*dLBoundary
        self.__y11 = 10.0*dLBoundary
        self.__y12 = 10.0*dLBoundary
        # activate bandwidth reduction
	self.setReduction(True)

	# process everything
    def build (self):
		# defining the nodes
        n1 = NODE(self.__x1,self.__y1)
        n2 = NODE(self.__x2,self.__y2)
        n3 = NODE(self.__x3,self.__y3)
        n4 = NODE(self.__x4,self.__y4)
        n5 = NODE(self.__x5,self.__y5)
        n6 = NODE(self.__x6,self.__y6)
        n7 = NODE(self.__x7,self.__y7)
        n8 = NODE(self.__x8,self.__y8)
        n9 = NODE(self.__x9,self.__y9)
        n10 = NODE(self.__x10,self.__y10)
        n13 = NODE(self.__x7,self.__y9)
        n14 = NODE(self.__x8,self.__y10)
        n11 = NODE(self.__x11,self.__y11)
        n12 = NODE(self.__x12,self.__y12)
        # and the regions with max area and material indices
        # Note: this is used here to create the nodes in the curvature, and later to define grid size
        curvatureCellSize = 1.0*self.__surface/self.__N
        dLCurvature = math.sqrt(2.0*curvatureCellSize)
        halfPerimeter = math.pi # [rad]

        innerTheta = 2.0*math.asin(dLCurvature/(2.0*self.__innerRadius))
        outerTheta = 2.0*math.asin(dLCurvature/(2.0*self.__outerRadius))
  
        # number of sections and angle for curvature
        numInnerSections = halfPerimeter/innerTheta
        numInnerSections = round(numInnerSections)       
        innerTheta = halfPerimeter/numInnerSections
        numOuterSections = halfPerimeter/outerTheta
        numOuterSections = round(numOuterSections)       
        outerTheta = halfPerimeter/numOuterSections

        innerCircleNodeList = []
        outerCircleNodeList = []
        
        innerBreakLines = []
        outerBreakLines = []

        # make vectors with angles on these breaklines
        innerTheta_vec = []       
        theta_sum = 0.0
        for ii in range(1,int(numInnerSections)):
            theta_sum += innerTheta
            innerTheta_vec.append(theta_sum)
 
        outerTheta_vec = []
        theta_sum = 0.0
        for ii in range(1,int(numOuterSections)):
            theta_sum += outerTheta
            outerTheta_vec.append(theta_sum)

        # now create the nodes for all these breaklines/half-circles
        delta = 0.4 # limit elements near user defined observation breaklines
        # nodes for inner half-circle        
        tt = 0
        moreObs = True
        for idx, ii in enumerate(innerTheta_vec):
            # determine distance to next possible node
            if tt == 0:
                x1 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerRadius * math.cos(ii)
                y2 = self.__yR + self.__innerRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__innerRadius * math.cos(ii)
                y2 = self.__yR + self.__innerRadius * math.sin(ii)
                x3 = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            # creater nodes for observation breaklines
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__innerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__innerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                innerCircleNodeList.append(NODE(x,y))
                innerBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            # create all other nodes (only if not too near to the nodes of observation breaklines)
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__innerRadius * math.cos(ii)
                y = self.__yR + self.__innerRadius * math.sin(ii)
                innerCircleNodeList.append(NODE(x,y))

            
        # nodes for outer half-circle
        tt = 0
        moreObs = True
        for idx, ii in enumerate(outerTheta_vec):
            if tt == 0:
                x1 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerRadius * math.cos(ii)
                y2 = self.__yR + self.__outerRadius * math.sin(ii)
                distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if tt > 0:
                x1 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y1 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                x2 = self.__xR + self.__outerRadius * math.cos(ii)
                y2 = self.__yR + self.__outerRadius * math.sin(ii)
                x3 = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt-1]*math.pi/180.0)
                y3 = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt-1]*math.pi/180.0)
                distance = min(math.sqrt((x1-x2)**2 + (y1-y2)**2),math.sqrt((x3-x2)**2 + (y3-y2)**2))
            if ii > self.__thetaObs_list[tt]*math.pi/180.0 and moreObs:
                x = self.__xR + self.__outerRadius * math.cos(self.__thetaObs_list[tt]*math.pi/180.0)
                y = self.__yR + self.__outerRadius * math.sin(self.__thetaObs_list[tt]*math.pi/180.0)
                outerCircleNodeList.append(NODE(x,y))
                outerBreakLines.append(NODE(x,y))
                if self.__thetaObs_list[tt] != self.__thetaObs_list[-1]:
                    tt += 1
                else:
                    moreObs = False
            if distance > delta*dLCurvature: 
                x = self.__xR + self.__outerRadius * math.cos(ii)
                y = self.__yR + self.__outerRadius * math.sin(ii)
                outerCircleNodeList.append(NODE(x,y))

        # Define the breaklines
        # mesh boundary
        nodeList = [n3,n11,n9,n1,n2,n10,n12,n4]
        innerCircleNodeList.reverse()
        nodeList.extend(innerCircleNodeList)
        nodeList.extend([n5,n13,n7,n8,n14,n6])
        nodeList.extend(outerCircleNodeList)
        self.addLine(nodeList,True)
        # observation break lines in the curvatue
        for idx, ii in enumerate(innerBreakLines):
            self.addLines([[ii,outerBreakLines[idx]]],[13])
        #self.addLines([[n3,n4]],[13])
        #self.addLines([[n5,n6]],[13])
        #self.addLine([n9,n10])
        #self.addLine([n11,n12])
        #self.addLine([n13,n14])

        # Add regions with max area and material indices
        smallCellSize = 1.0*self.__surface/self.__N
        normalCellSize = 1.0*self.__surface/self.__N
        # channel before curve:
        self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*(self.__y1+self.__y3)),maxArea=normalCellSize,matID=1)    # mobile bed
        #self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*(self.__y9+self.__y11)),maxArea=normalCellSize,matID=5)    # -0.1 m fixed bed
        #self.addRegion(NODE(0.5*(self.__x1+self.__x2),0.5*(self.__y1+self.__y9)),maxArea=normalCellSize,matID=1)     # fixed bed
        # channel after curve:
        self.addRegion(NODE(0.5*(self.__x7+self.__x8),0.5*(self.__y5+self.__y7)),maxArea=normalCellSize,matID=1)    # mobile bed
        #self.addRegion(NODE(0.5*(self.__x7+self.__x8),0.5*(self.__y9+self.__y7)),maxArea=normalCellSize,matID=3)    # last meter
        # case of equal distance between observation arcs:
        if (len(self.__oD) == 1):
            # channel in curvature:
            angleRegion_list = []
            halfAngleRegion = 0.5*math.pi/(len(innerBreakLines)+1)
            angleRegion_list.append(halfAngleRegion)
            angleRegion_sum = halfAngleRegion
            for ii in range(0,len(innerBreakLines)):           
                angleRegion_sum += math.pi/(len(innerBreakLines)+1)
                angleRegion_list.append(angleRegion_sum)
            pp = 0
            for ii in range(0,len(innerBreakLines)+1):
                x = self.__xR + self.__R * math.cos(angleRegion_list[pp])
                y = self.__yR + self.__R * math.sin(angleRegion_list[pp])
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)
                pp += 1
        else: # case of non-equal distance between observation arcs:
            # all regions
            angleRegion_list = []
            for ii in self.__thetaObs_list:         
                angleRegion_list.append((ii-5.0)*math.pi/180.0)
            angleRegion_list.append((self.__thetaObs_list[-1]+5.0)*math.pi/180.0)
            # channel in curvature:
            for ii in angleRegion_list:
                x = self.__xR + self.__R * math.cos(ii)
                y = self.__yR + self.__R * math.sin(ii)
                self.addRegion(NODE(x,y),maxArea=curvatureCellSize,matID=1)

		# define stringdefs
        self.addStringdef( '1', [n1,n2] )
        self.addStringdef( '2', [n7,n8] )
        #self.addStringdef( 'curve_start', [n3,n4] )
        #self.addStringdef( 'curve_end', [n5,n6] )

        #string_list = ['CS1','CS2','CS3','CS4','CS5','CS6','CS7','CS8','CS9','CS10','CS11','CS12']
        #for idx, ii in enumerate(self.__thetaObs_list):
        #    self.addStringdef(string_list[idx],[innerBreakLines[idx],outerBreakLines[idx]])
	
	# set some triangle options
	self.setQuality(self.__q)
	self.setOption('-D')
	# let's go...
	self.createMesh()
	
	# function that describes the topography of the model
    def elevationFunction (self, x, y):
        # idea: first assign location, then calcualte elevation
        epsilon = 0.1
        Rx = self.__R+0.5*self.__W   
        Ry = self.__L  
        where = None
        # mobile bed with a layer thickness (input parameter)
        # assign location (mobile bed)
        if (self.__y1 <= y < self.__y9) and (self.__x1 <= x <= self.__x2):
            where = 'channelBeginSedBoundary' # here i dont want random bed perturbation
        elif (self.__y9 <= y < self.__y11) and (self.__x1 <= x <= self.__x2):
            where = 'channelBeginWeak' # here i dont want random bed perturbation
        elif (self.__y11 <= y <= self.__y3) and (self.__x1 <= x <= self.__x2):
            where = 'channelBegin'
        elif (self.__y11 <= y <= self.__y5) and (self.__x7 <= x <= self.__x8):
            where = 'channelEnd'
        elif (self.__y9 <= y < self.__y11) and (self.__x7 <= x <= self.__x8):
            where = 'channelEndWeak'   # here i want weak random bed perturbation  
        elif (self.__y7 <= y < self.__y9) and (self.__x7 <= x <= self.__x8):
            where = 'channelEndSedBoundary'   # here i dont want random bed perturbation       
        elif ( (x - Rx)**2 + (y - Ry)**2 - epsilon <= (self.__outerRadius)**2 ) and ( (x-Rx)**2 + (y-Ry)**2 + epsilon >= (self.__innerRadius)**2 ):
            where = 'curveBed'

        # calculate elevation (mobile bed)
        zNull = 0.0
        totalLength = 2.0*self.__L + math.pi*self.__R
        if where == 'channelBeginSedBoundary':
            return zNull + self.__J*(totalLength - y)
            #return zNull + self.__J*(totalLength - self.__y9) + self.__JB*(self.__y9 - y)
        elif where == 'channelBeginWeak':
            return zNull + self.__J*(totalLength - y) + random.uniform(-1.0,1.0)*self.__Rand*0.5
        elif where == 'channelBegin':
            return zNull + self.__J*(totalLength - y) + random.uniform(-1.0,1.0)*self.__Rand
        elif where == 'channelEnd':
            return zNull + y * self.__J + random.uniform(-1.0,1.0)*self.__Rand
        elif where == 'channelEndWeak':
            return zNull + y * self.__J + random.uniform(-1.0,1.0)*self.__Rand*0.33       
        elif where == 'channelEndSedBoundary':
            return zNull + y * self.__J 
        elif where == 'curveBed':
            v0x = self.__x4 - Rx
            v0y = self.__y4 - Ry
            vx = x-Rx
            vy = y-Ry
            phi = math.acos(((v0x*vx)+(v0y*vy))/(math.sqrt(vx*vx+vy*vy)*math.sqrt(v0x*v0x+v0y*v0y)))
            zBegin = self.__J*(totalLength - self.__L)
            return zNull + zBegin - self.__R*phi*self.__J + random.uniform(-1.0,1.0)*self.__Rand          
        else:
            print "There is no elevation found for x =", x, " and y =", y
            print "Therefore, the elevation is set to 0.0"
            return 0.0

print '*** U flume mesh generator ***'
wd = WORKINGDIRECTORY('Uflume')
th = UCHANNEL(wd,
	mType='plane3x',
	quality=32.0,
	channelLength=3000.0,
    	channelBedWidth=60.0,
        channelRadius=360.0,
    	channelBedSlope=0.0025,
        observationAngles=[90.0],
    	numberOfCells=50000.0)
th.build()


