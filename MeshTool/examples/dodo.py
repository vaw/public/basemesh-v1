from testRuns import *
from testMeshes import *

def task_internalBC():
	return {
		'actions': [run_internalBC_channel]
		}

def task_Cproperty():
	return {
		'actions': [run_Cproperty]
		}

def task_halfSphere():
	return {
		'actions': [run_half_sphere],
		'verbosity': 2
		}

def task_labChannel():
	return {
		'actions': [run_lab_channel],
		'verbosity': 2
		}

def task_trapezoidalChannel():
	return {
		'actions': [run_trapezoidal_channel]
		}

def task_simpleChannel():
	return {
		'actions': [(run_simple_channel,[50])]
		}

def task_calibration():
	return {
		'actions': [run_calibration],
		'verbosity': 2
		}

def task_constraintBoundaryMesh():
	def buildMesh(nCells,nBoundaryCells):
		wd = WORKINGDIRECTORY('testConstrainBoundary')
		meshName = 'constraint_boundary'
		cb = CONSTRAINTBOUNDARY(wd,meshName=meshName)
		cb.build(nCells,nBoundaryCells)
	return {
		'actions': [(buildMesh,),'../viewMesh.py testConstrainBoundary/constraint_boundary.2dm'],
		'params':[{'name':'nCells',
                       'short':'n',
                       'type':int,
                       'default':100},
                  {'name':'nBoundaryCells',
                       'short':'b',
                       'type':int,
                       'default':5}],
		'verbosity': 2
	}