from os.path import join
from batchTools import *
from meshModel import MESHMODEL, RASTER



class POLYTEST (MESHMODEL):
	def __init__ (self, directory,
		maxArea=100.0,
		quality=30.0,
		demFile=None,
		landuseFile=None,
		mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory,absolutePrecision=1e-1)
		# this class does all the meshing stuff
		self.initialization('pocEXAR',mType)
		# store input argument as member variable
		self.__A = maxArea
		self.__q = quality
		# load the DEM data
		dem = RASTER(demFile)
		self.__DEM = dem.getInterpolator()
		# load the landuse data
		lulc = RASTER(landuseFile)
		lulc.setInterpolation('nearest')
		self.__lulc = lulc.getInterpolator()
		# activate bandwidth reduction
		self.setReduction(True)
		
	def MaterialIDFunction (self, matID, x, y):
		return self.__lulc([x,y]) if self.__lulc else 0

	def elevationFunction (self, x, y):
		return self.__DEM([x,y]) if self.__DEM else 0.0

	def build (self,polyfile):
		# set triangle options
		self.setQuality(self.__q)
		self.setArea(self.__A)
		# let's go...
		self.createMesh(polyfile)



print '*** TEST for creating directly from POLYFILE ***'
wd = WORKINGDIRECTORY('fromPolyfile')
icold = POLYTEST(wd,
	maxArea=1.0e5,
	quality=30.0,
	demFile='dem.txt',
	landuseFile='lulc.txt',
	mType='plane3x'
	)
polyfile = join(wd.getDirectory(),'fromQGIS.poly')
icold.build(polyfile)

