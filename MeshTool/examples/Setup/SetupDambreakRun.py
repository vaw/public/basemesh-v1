#!/usr/bin/env python

from damGeometry import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil, sys

# default input parameters
optsPars = OPTIONPARSING()
optsPars.addOption('h',4.0,3)
optsPars.addOption('v',60000.0,1)
optsPars.addOption('w',2.5,3)
optsPars.addOption('s',2.0,3)
optsPars.addOption('a',1.0,3)

# parse command line options
optsPars.parseOptions(sys.argv[1:])
modelID = optsPars.getID()
wd = WORKINGDIRECTORY(modelID)

# setup the mesh properties
dg = DAMGEOMETRY(wd,meshName=modelID,
	height=optsPars.getValue('h'),
	volume=optsPars.getValue('v'),
	crestWidth=optsPars.getValue('w'),
	embankmentSlope=optsPars.getValue('s'),
	alpha=optsPars.getValue('a'))
dg.build()
filename = dg.get2dmFilename()
# create the mesh for mobile bed
dg.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(filename,filename2dm)
# and the fixed bed mesh
dg.generate2dm(fixed=True)
filename2dmFixed = '%s_fixed.2dm'%os.path.splitext(filename)[0]
shutil.move(filename,filename2dmFixed)

# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle('Dam_Break_Model')
bm.setModelID(modelID)
bm.setNumThreads(8)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile (os.path.split(dg.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':30.0,'CFL':0.9})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':0.05})
strickler = 28.0
bm.setHydFriction({'type':'strickler',
	'default_friction':strickler,
	'input_type':'index_table',
	'index':'(1 2 3 4)',
	'friction':'(%f %f %f %f)'%(strickler,strickler,strickler,strickler),
	'wall_friction':'off'})
h = optsPars.getValue('h')
bm.setHydInitialCondition({'type':'index_table',
	'index':'(1 2 3 4)',
	'wse':'(%f %f %f 0)'%(h,h,h),
	'u':'(0 0 0 0)',
	'v':'(0 0 0 0)'})
bm.addHydBoundaryCondition('Outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'10'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setGrainClass([8.0])
bm.addMixture('d_m',[100.0])
bm.addSoilDefinition('Einkorn',[{'mixture':'d_m', 'bottom_elevation':-50.0}])
bm.addSoilDefinition('fixed_bed')
bm.assignSoils({'1':'fixed_bed','2':'Einkorn','3':'Einkorn','4':'Einkorn'})
bm.setFixedBed(os.path.split(filename2dmFixed)[1])
# transport
bm.setBedloadParameters({'upwind_index':'(1)',
	'upwind':'(0.5)',
	'limit_bedload_wetted':'off'})
bm.setBedloadFormula({'bedload_transport':'mpm',
	'lateral_transport_factor':1.0,
	'critical_shear_stress_calibration':2.0})
bm.addBedloadBoundaryCondition('Outflow',{'type':'IODown','string_name':'outflow'})
bm.setGravitationTransport({'index':'(1 2 3 4)',
	'angle_failure_dry':'(70 70 70 70)',
	'angle_failure_wetted':'(40 40 40 40)',
	'angle_failure_deposited':'(15 15 15 15)',
	'angle_wetted_criterion':'partially_wetted'})
# Output
bm.addBASEvizOutput(1)
bm.addHistoryOutput(1.0,'stringdef',['outflow'],['Q','Qsed'])
bm.addBalanceOutput(1.0,['sediment','water_volume'])
#bm.addHistoryOutput(10.0,'element',dg.getMonitoringElements(),['wse'])
# finally run the model
bm.run()

# postprocessing
pp = POSTPROCESSING(bm.getResultNameSuffix())
pp.readTimeHistory('stringdef','outflow')
print pp.getFilename()
print pp.getData('Q')

pp = POSTPROCESSING(bm.getResultNameSuffix())
pp.readBalance()
print pp.getFilename()
print pp.getData('water_vol')