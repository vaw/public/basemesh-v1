#!/usr/bin/env python
from freeBars import *
from basement import *
from batchTools import *
from postprocessing import *
from uniformflow import *
import os, shutil
import re
import math
import numpy as np

def SetupFreeBars(Q,dm,modelID,Amplitude):
	"""Setup Free Bars"""
	# define Geometry (rectangular channel)
	B = 60.0
	J = 0.005		# bed slope [-]

	# define simulation time
	Tage = 86400
	tSim = 100*Tage

	Jp = J*1000.0	# bed slope [per mil]
	dmm = dm*1000.0	# [mm]
	ks = 3.0*dm 	# equivalent sand roughness [m], used for Chezy friction

	# estimate bedload transport (mpm) 
	#kst = 21.1/math.pow(2.0*dm,1.0/6.0)
	kst = 21.1/math.pow(dm,1.0/6.0)
	h = calculateUniformFlowDepth(Q,B,J,dm,kst,False)
	u = Q/(h*B)
	theta = h*J/(1.65*dm)
	print "theta = ", theta
	theta=0.1
	phi = 4.93*math.pow((theta-0.047),1.6) # Wong and Parker (2006)
	qb = math.sqrt(1.65*9.81)*math.pow(dm,1.5)*phi
	Qb = qb*B
	print "phi (Wong and Parker, 2006) = ", phi
	print "qb (Wong and Parker, 2006) = ", qb
	print "Qb (Wong and Parker, 2006) = ", Qb
	# intial water depth
	h0 = h

	# setup the model
	wd = WORKINGDIRECTORY(modelID)
	uc = FREEBARS(wd,meshName=modelID,
		channelLength=300.0*B,
		channelWidth=B,
	    channelBedSlope=J,
	    numberOfCells=50000.0,
	    randomPert=0.0, # 0.01*h
	    cosineAmplitude=Amplitude) 	
	uc.build()
	# create the mesh for mobile bed
	filename = uc.get2dmFilename()
	#uc.generate2dm()
	filename2dm = '%s.2dm'%os.path.splitext(filename)[0]
	shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))


	# First, we create the initial condition
	# Note: starting dry we don't apply a turbulence model
	init = BASEMENT(wd)
	init.setProjectTitle(modelID)
	init.setModelID(modelID)
	init.setNumThreads(2)
	# Geometry
	init.set2dmFile(filename2dm)
	init.setStringdefFile(uc.getStringdefFilename())
	# Timestep
	init.setTimeStep({'total_run_time':3600.0,'start_time':'0.0','CFL':'0.95'})
	# Hydraulics
	init.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
	#init.setHydInitialCondition({'type':'dry'})
	init.setHydInitialCondition({'type':'index_table','index':'(1)','h':'(%f)'%(h0),'u':'(%f)'%(u),'v':'(0.0)'})
	init.setHydFriction({'type':'chezy', 'default_friction':'%f'%(ks), 'index':'(1)', 'friction':'(%f)'%(ks), 'wall_friction':'off', 'input_type':'index_table'})
	# Inflow input file	
	inflowFile = 'inflow.txt'   
	init.writeTimeSeriesFile(inflowFile,[0.0,tSim],[Q,Q]) 
	init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'%f'%(Jp), 'weighting_type':'area'})
	init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'%f'%(Jp)})
	# Output
	init.setRestartTimeStep(1e32)
	init.setConsoleTimeStep(100.0)
	#init.addBASEvizOutput(1)
	init.addBalanceOutput(100.0,['timestep','water_volume'])
	init.addHistoryOutput(100.0,'boundary',[],['Q'])
	init.run()

	#################################################################
	# Rename restart file for initial condition
	RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
	# get the path of this script
	root = os.path.split(os.path.realpath(__file__))[0]
	strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
	strRestart = os.path.join(wd.getDirectory(), RestartFileName)
	# Check if an old file already exists
	if os.path.exists(strRestart):
		print "Restart file already exists and will be replaced with new restart file"
		os.remove(strRestart)
		os.rename(strRestartDefault, strRestart)
	else:
	    print "strRestartDefault", strRestartDefault
	    os.rename(strRestartDefault, strRestart)
	# Rename log file of hydraulic simulation (just used for later checking hydraulic run)
	strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
	strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
	if os.path.exists(strLogFileHyd):
		os.remove(strLogFileHyd)
		os.rename(strLogFile, strLogFileHyd)
	else:
		os.rename(strLogFile, strLogFileHyd)


	#################################################################
	# Test restart file and provoke error with available restart time
	pe = BASEMENT(wd)
	pe.setProjectTitle(modelID)
	pe.setModelID(modelID)
	pe.setNumThreads(2)
	# Geometry
	pe.set2dmFile(os.path.split(filename2dm)[1])
	pe.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
	# Timestep
	pe.setTimeStep({'total_run_time':10.0,'start_time':'0.0','CFL':'0.95'})
	# Hydraulics
	pe.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
	pe.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':'3.1456'})
	pe.setHydFriction({'type':'chezy', 'default_friction':'%f'%(ks), 'index':'(1)', 'friction':'(%f)'%(ks), 'wall_friction':'off', 'input_type':'index_table'})
	# Output
	pe.setRestartTimeStep(1e32)
	pe.setConsoleTimeStep(2)
	#pe.addBASEvizOutput(1)
	pe.run()


	##################################################################
	# Parse .err file to get valid time steps to restart
	errFile = os.path.join(wd.getDirectory(), modelID+'.err')
	fid = open(errFile,'r')
	for line in fid:
		if 'Valid times are:' in line:
			data = line.split()
			cgnsTime = data[-1]
	fid.close()


	#################################################################
	# Now we setup the sediment model
	# BASEMENT object:
	bm = BASEMENT(wd)
	bm.setProjectTitle(modelID)
	bm.setModelID(modelID)
	bm.setNumThreads(8)
	# Geometry
	bm.set2dmFile(os.path.split(filename2dm)[1])
	bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
	# Timestep
	bm.setTimeStep({'total_run_time':tSim,'start_time':'0.0','CFL':'0.95','morph_cycle':'constant','cycle_step':'20'})
	#bm.setTimeStep({'total_run_time':172800.0,'start_time':'0.0','CFL':'0.95'})
	# Hydraulics
	bm.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc','dynamic_depth_solver':'off'})
	bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTime})
	#bm.setHydInitialCondition({'type':'dry'})
	bm.setHydFriction({'type':'chezy', 'default_friction':'%f'%(ks), 'index':'(1)', 'friction':'(%f)'%(ks), 'wall_friction':'off', 'input_type':'index_table'})
	# Inflow input file
	bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'%f'%(Jp), 'weighting_type':'area'})
	bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'%f'%(Jp)})
	# External Water Sources to trigger free bars
	"""
	fileExtHydSourceSine = 'fileExtHydSourceSine.txt'
	fileExtHydSourceCosine = 'fileExtHydSourceCosine.txt' 
	fhs = open(os.path.join(wd.getDirectory(),fileExtHydSourceSine),'w')
	fhc = open(os.path.join(wd.getDirectory(),fileExtHydSourceCosine),'w')
	A = 0.2*Q		# max amplitude as a fraction of the inflow 
	ff = 1.0/600.0	# frequenzy
	for ii in range(0,tSim+100,10):
		vs = A*math.sin(2.0*math.pi*ff*ii)
		vc = A*math.cos(2.0*math.pi*ff*ii + (math.pi/2.0))
		fhs.write('%f\t%f\n'%(ii,vs))
		fhc.write('%f\t%f\n'%(ii,vc))
	fhs.close()
	fhc.close()
	bm.addHydExternalSource('index',[3],fileExtHydSourceSine)
	bm.addHydExternalSource('index',[4],fileExtHydSourceCosine)
	bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'2.0'})
	"""
	# Morphology
	bm.setMorphInitialCondition({'type':'initial_mesh'})
	bm.setMorphParameters({'local_slope_angle_repose':'34.0','porosity':'40.0','control_volume_type':'constant', 'control_volume_thickness':'0.2','distortion_offset':'%f'%(0.01*h0),'distortion_time_interval':'3600','distortion_index':'(1)'})
	bm.setGrainClass([dmm])
	bm.addMixture('single_grain',[100])
	#bm.setGrainClass([0.5, 1.0, 1.5, 2.2, 3.2])
	#bm.addMixture('multi_grain',[28, 24, 16, 22, 10])
	#bm.setGrainClass([0.5, 1.0, 1.5, 2.2, 3.2])
	#bm.addMixture('multi_grain_armor',[10, 20, 20, 30, 20])
	bm.addSoilDefinition('soil_uniform',[{'mixture':'single_grain', 'bottom_elevation':-200.0}])
	bm.addSoilDefinition('soil_fixbed',[])
	#bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform','4':'soil_uniform'})
	bm.assignSoils({'1':'soil_uniform'})

	# bedload transport
	bm.setBedloadParameters({'limit_bedload_wetted':'on'}) # ,'use_cell_averaged_bedload_flux':'off','upwind':'(1.0 1.0)','upwind_index':'(1 2)'
	bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'1.35','lateral_index':'(1)'})
	#bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'1.0','critical_shear_stress_calibration':'0.85'}) # ,'local_slope_flowdirection':'off','local_slope_lateral':'off'
	bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'0.61625','bedload_exponent':'1.6','theta_critical_index':'(1)','theta_critical':'(0.047)','local_slope_correction':'no_local_slope'})
	fileQSed = 'QSed.txt'   
	bm.writeTimeSeriesFile(fileQSed,[0.0,tSim+100.0],[Qb,Qb])
	bm.addBedloadBoundaryCondition('bed_inflow',{'type':'sediment_discharge','string_name':'inflow','mixture':'single_grain','file':fileQSed})
	#bm.addBedloadBoundaryCondition('bed_inflow',{'type':'IOUp','string_name':'inflow','fraction_boundary':'1.0'})
	bm.addBedloadBoundaryCondition('bed_outflow',{'type':'IODown','string_name':'outflow'})
	# External Sediment Source to trigger free bars
	"""
	fileExtSourceSine = 'fileExtSourceSine.txt'
	fileExtSourceCosine = 'fileExtSourceCosine.txt' 
	fileExtSourceRandom =  'fileExtSourceRandom.txt' 
	fs = open(os.path.join(wd.getDirectory(),fileExtSourceSine),'w')
	fc = open(os.path.join(wd.getDirectory(),fileExtSourceCosine),'w')
	fr = open(os.path.join(wd.getDirectory(),fileExtSourceRandom),'w')
	A = 0.5*Qb		# max amplitude of half of the sediment inflow 
	f = 1.0/600.0	# frequenzy
	for ii in range(0,tSim+100,10):
		vs = A*math.sin(2.0*math.pi*f*ii)
		vc = A*math.cos(2.0*math.pi*f*ii + (math.pi/2.0))
		vr = A*random.uniform(-1.0,1.0)
		fs.write('%f\t%f\n'%(ii,vs))
		fc.write('%f\t%f\n'%(ii,vc))
		fr.write('%f\t%f\n'%(ii,vr))
	fs.close()
	fc.close()
	fr.close()
	bm.addMorphExternalSource('index',[3],fileExtSourceSine,'single_grain')
	bm.addMorphExternalSource('index',[4],fileExtSourceCosine,'single_grain')
	"""
	# Output
	bm.setRestartTimeStep(86400.0)
	bm.setConsoleTimeStep(86400.0)
	#bm.addBASEvizOutput(1)
	bm.addBalanceOutput(86400.0,['sediment','timestep','water_volume'])
	# Stringdef output
	bm.addHistoryOutput(86400.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse','zbed'])
	# Node history output at the breaklines
	for ii in range(0,len(uc.getStringdefNames())):
		bm.addHistoryOutput(86400.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node'],True)
	bm.addHistoryOutput(86400.0,'boundary',[],['Qsed'])
	#bm.addCenteredOutput (172800.0,'element',['depth','velocity'],'vtk')
	#bm.addCenteredOutput (172800.0,'node',['z_node','deltaz'],'vtk')
	bm.addCenteredOutput (86400.0,'node',['deltaz', 'z_node', 'grain_bedload', 'grain_size', 'bedload_vec', 'depth', 'velocity'],'vtk')
	#bm.addCenteredOutput (86400.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
	#bm.addCenteredOutput (86400.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

	# finally run the model
	bm.build()
	#bm.run()