#!/usr/bin/env python

from UchannelTrapez import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re

# setup the model
modelID = 'UChannelTrapez'
wd = WORKINGDIRECTORY(modelID)
uc = UCHANNELTRAPEZ(wd,meshName=modelID,
	channelLength=20.0,
	channelBedWidth=1.44,
	channelRadius=13.11,
    channelHeight=0.33,
    channelBankSlope=2.0/3.0,
   	channelBedSlope=0.001,
    observationAngles=[31.0,61.0,90.0,120.0,149.0],
    sedimentLayer=0.23,
   	numberOfCells=10000.0)
uc.build()

# create the mesh for mobile bed
filename = uc.get2dmFilename()
uc.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))
# and the fixed bed mesh
uc.generate2dm(fixed=True)
filename2dmFixed = '%s_fixed.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dmFixed))


# First, we create the initial condition
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(4)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(uc.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':150.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.001'})
init.setHydInitialCondition({'type':'dry'})
init.setHydFriction({'type':'chezy', 'default_friction':'0.001', 'index':'(1 2 3 4)', 'friction':'(0.001 0.001 0.001 0.001)', 'wall_friction':'on', 'input_type':'index_table'})
# Inflow input file	
inflowFile = 'inflow.txt'   
init.writeTimeSeriesFile(inflowFile,[0,3600.0],[0.153,0.153])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'0.75'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.16'})
# Output
init.setRestartTimeStep(1e32)
init.setOutputTimeStep(1800)
init.setConsoleTimeStep(30)
init.addBASEvizOutput(1)
# Run simulation for initial condition
init.run()

# Process restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)

# Rename log file of hydraulic simulation
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

# Parse hydraulic log file for CGNS output time step
fid = open(strLogFileHyd,'r')
for line in fid:
	if 'CGNS' in line:
		data = line.split()
		cgnsTime = data[-1]
count = 0
cgnsTimeP = ''
for ii in cgnsTime:
	if ii == '.':
		count +=1
	if count <=1:
		cgnsTimeP = cgnsTimeP + ii
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(4)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':3600.0,'start_time':'0.0'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy', 'default_friction':'0.001', 'index':'(1 2 3 4)', 'friction':'(0.001 0.001 0.001 0.001)', 'wall_friction':'on', 'input_type':'index_table'})	
# Inflow input file
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'0.75','weighting_type':'area'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.16'})
#bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'5.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'37.0'})
bm.setGrainClass([0.3])
bm.addMixture('single_grain',[100.0])
bm.addSoilDefinition('soil_uniform',[{'mixture':'single_grain', 'bottom_elevation':-2.0}])
bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform','4':'soil_uniform'})
bm.setFixedBed(os.path.split(filename2dmFixed)[1])
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'off'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'2.0','lateral_index':'(1 2 3 4)'})
bm.setBedloadDirection({'lateral_transport_type':'curvature_effect_dynamic','radius_calculation_type':'water_surface_elevation','lateral_index':'(1 2)'})
bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'0.7','critical_shear_stress_calibration':'0.90'})
bm.addBedloadBoundaryCondition('inflow',{'type':'IOUp','string_name':'inflow'})
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow'})
bm.setGravitationTransport(
	{'index':'(1 2 3 4)',
	'angle_failure_dry':'(40 40 40 40)',
	'angle_failure_wetted':'(30 30 30 30)',
	'angle_failure_deposited':'(30 30 30 30)',
	'angle_wetted_criterion':'partially_wetted',
	'min_changed_volume': '0.0000001',
	'max_delta_z':'0.001'})
# Output
bm.setRestartTimeStep(1e32)
bm.setOutputTimeStep(100)
bm.setConsoleTimeStep(100)
bm.addBASEvizOutput(1)
bm.addBalanceOutput(60.0,['sediment','timestep','water_volume'])
# Stringdef output
bm.addHistoryOutput(60.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse'])
# Node history output at the breaklines
for ii in range(0,len(uc.getStringdefNames())):
	bm.addHistoryOutput(60.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node'],True)

#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])

bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
bm.addCenteredOutput (60.0,'element',['wse','depth','velocity','radius_curvature'],'vtk')
bm.addCenteredOutput (60.0,'node',['z_node','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
#bm.build()
bm.run()