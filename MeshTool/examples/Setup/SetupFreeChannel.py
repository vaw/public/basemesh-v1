#!/usr/bin/env python
from FreeChannel import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re


modelID_v = ['Run9_Requena_2_1','Run10_Requena_2_2','Run11_Requena_2_3','Run12_Requena_2_4']
channelBedSlope_v = [0.0055,0.004,0.006,0.0035]
slope_string_up_v = ['5.5','4.0','6.0','3.5']
slope_string_down_v = ['5.5','4.0','6.0','3.5']
inflow_const_v = [837,558,1115,837]
#sediment_const_v = [0.157842,0.131535,0.341992,0.131535]
sediment_const_v = [0.078921,0.065768,0.170996,0.065768]
ks = 0.10 # [m]
scale = 60.0 # Naturmassstab

for jj,tt in enumerate(modelID_v):
	
	# setup the model:
	modelID = tt
	
	wd = WORKINGDIRECTORY(modelID)
	uc = FREECHANNEL(wd,meshName=modelID,
		modelLength=32.0*scale,
		modelWidth=3.2*scale,
		channelWidth=0.8*scale,
		channelHeight=0.08*scale,
		channelBankSlope=1.0/1.0,
		channelBedSlope=channelBedSlope_v[jj],
		numberOfCells=25000,
		randomPert=0.2)
	uc.build()
	# constant water and sediment inflow:
	slope_string_up = slope_string_up_v[jj]
	slope_string_down = slope_string_down_v[jj]
	inflow_const = inflow_const_v[jj]
	sediment_const = sediment_const_v[jj]

	# create the mesh for mobile bed
	filename = uc.get2dmFilename()
	#uc.generate2dm()
	filename2dm = '%s.2dm'%os.path.splitext(filename)[0]
	shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))

	# First, we create the initial condition
	# Note: starting dry we don't apply a turbulence model
	init = BASEMENT(wd)
	init.setProjectTitle(modelID)
	init.setModelID(modelID)
	init.setNumThreads(1)
	# Geometry
	init.set2dmFile(filename2dm)
	init.setStringdefFile(uc.getStringdefFilename())
	# Timestep
	init.setTimeStep({'total_run_time':400.0,'start_time':'0.0'})
	# Hydraulics
	init.setHydParameters({'minimum_water_depth':'0.05'})
	#init.setHydInitialCondition({'type':'dry'})
	init.setHydInitialCondition({'type':'index_table','index':'(1 2 3 4 5 6 7 8 9 10 11 12)','h':'(1.0 0.0 0.0 1.0 0.0 0.0 1.0 1.0 1.0 0.0 0.0 0.0)','u':'(0.5 0.0 0.0 0.5 0.0 0.0 0.5 0.5 0.5 0.0 0.0 0.0)','v':'(0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0)'})
	init.setHydFriction({'type':'chezy', 'default_friction':'%s' %(ks), 'index':'(1 2 3 4 5 6 7 8 9 10 11 12)', 'friction':'(%s %s %s %s %s %s %s %s %s %s %s %s)' %(ks,ks,ks,ks,ks,ks,ks,ks,ks,ks,ks,ks), 'wall_friction':'off', 'input_type':'index_table'})
	# Inflow input file	
	inflowFile = 'inflow.txt'   
	init.writeTimeSeriesFile(inflowFile,[0,150000.0],[inflow_const,inflow_const])
	init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':slope_string_up})
	init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':slope_string_down, 'weighting_type':'conveyance'})
	# Output
	init.setRestartTimeStep(1e10)
	init.setConsoleTimeStep(1)
	init.addBASEvizOutput(1)
	# Run simulation for initial condition
	#init.build()
	init.run()


	# Process restart file for initial condition
	RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
	# get the path of this script
	root = os.path.split(os.path.realpath(__file__))[0]
	strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
	strRestart = os.path.join(wd.getDirectory(), RestartFileName)
	# Check if an old file already exists
	if os.path.exists(strRestart):
		print "Restart file already exists and will be replaced with new restart file"
		os.remove(strRestart)
		os.rename(strRestartDefault, strRestart)
	else:
	    print "strRestartDefault", strRestartDefault
	    os.rename(strRestartDefault, strRestart)

	# Rename log file of hydraulic simulation
	strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
	strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
	if os.path.exists(strLogFileHyd):
		os.remove(strLogFileHyd)
		os.rename(strLogFile, strLogFileHyd)
	else:
		os.rename(strLogFile, strLogFileHyd)

	# Parse hydraulic log file for CGNS output time step
	fid = open(strLogFileHyd,'r')
	for line in fid:
		if 'CGNS' in line:
			data = line.split()
			cgnsTime = data[-1]
	count = 0
	cgnsTimeP = ''
	for ii in cgnsTime:
		if ii == '.':
			count +=1
		if count <=1:
			cgnsTimeP = cgnsTimeP + ii
	fid.close()


	# Now we setup the sediment model
	# BASEMENT object:
	bm = BASEMENT(wd)
	bm.setProjectTitle(modelID)
	bm.setModelID(modelID)
	bm.setNumThreads(8)
	# Geometry
	bm.set2dmFile(os.path.split(filename2dm)[1])
	bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
	# Timestep
	#bm.setTimeStep({'total_run_time':86400.0,'start_time':'0.0'})
	bm.setTimeStep({'total_run_time':150000.0,'start_time':'0.0'})
	# Hydraulics
	bm.setHydParameters({'minimum_water_depth':'0.05'})
	bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
	bm.setHydFriction({'type':'chezy', 'fixed_bed_d90':'55.0','grain_size_friction':'yes','default_friction':'%s' %(ks), 'index':'(1 2 3 4 5 6 7 8 9 10 11 12)', 'friction':'(%s %s %s %s %s %s %s %s %s %s %s %s)' %(ks,ks,ks,ks,ks,ks,ks,ks,ks,ks,ks,ks), 'wall_friction':'off', 'input_type':'index_table'})	
	# Inflow input file
	bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':slope_string_up})
	#bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'zero_gradient'})
	bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':slope_string_down})
	bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'1.0'})
	# Morphology
	bm.setMorphInitialCondition({'type':'initial_mesh'})
	bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'37.0','control_volume_type':'borah'})
	# Kiesmischung 1:
	#bm.setGrainClass([0.17, 0.45, 1.22, 3.34, 9.09]) # Kiesmischung 1
	#bm.addMixture('multi_grain',[8.6, 7.8, 19.9, 33.0, 30.7]) # Kiesmischung 1
	# Kiesmischung 2:
	#bm.setGrainClass([0.21, 0.41, 0.80, 1.57, 3.07]) # Kiesmischung 2
	# Kiesmischung 2 (Naturmassstab):
	bm.setGrainClass([4.2, 8.2, 16.0, 31.4, 60.4]) # Kiesmischung 2 fuer Naturmassstab (lambda = 20?)
	bm.addMixture('multi_grain',[12.9, 29.2, 21.6, 16.8, 19.5]) # Kiesmischung 2
	bm.addSoilDefinition('soil_uniform',[{'mixture':'multi_grain', 'bottom_elevation':-8.0}])
	bm.addSoilDefinition('soil_uniform_1',[{'mixture':'multi_grain', 'bottom_elevation':-0.5}])
	bm.addSoilDefinition('soil_uniform_2',[{'mixture':'multi_grain', 'bottom_elevation':-1.0}])
	bm.addSoilDefinition('soil_uniform_3',[{'mixture':'multi_grain', 'bottom_elevation':-1.5}])
	bm.addSoilDefinition('soil_fixbed',[])
	bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform','4':'soil_fixbed','5':'soil_fixbed','6':'soil_fixbed','7':'soil_fixbed','8':'soil_uniform_1','9':'soil_uniform_2','10':'soil_uniform_2','11':'soil_uniform_1','12':'soil_uniform_3'})
	# bedload transport
	bm.setBedloadParameters({'limit_bedload_wetted':'off','use_cell_averaged_bedload_flux':'off'}) # ,'upwind':'(1.0 1.0 1.0)','upwind_index':'(1 2 3)'
	bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'2.0','lateral_index':'(1 2 3 4 5 6 7 8 9 10 11 12)'})
	bm.setBedloadFormula({'bedload_formula':'mpm_multi','bedload_factor':'1.0','critical_shear_stress_calibration':'0.8'}) # ,'local_slope_flowdirection':'on','local_slope_lateral':'off'
	bedloadFile = 'bedloadInflow.txt'   
	init.writeTimeSeriesFile(bedloadFile,[0,86400.0],[sediment_const,sediment_const])
	# sediment inflow with external sources (index based), rather than at the boundary
	#bm.addBedloadBoundaryCondition('bed_inflow',{'type':'sediment_discharge','string_name':'bed_inflow','file':bedloadFile,'mixture':'multi_grain'})
	bm.addMorphExternalSource('index',[7],bedloadFile,'multi_grain')
	bm.addBedloadBoundaryCondition('bed_outflow',{'type':'IODown','string_name':'bed_outflow','fraction_boundary':'1.0'})
	bm.setGravitationTransport(
		{'index':'(1 2 3 4 5 6 7 8 9 10 11 12)',
		'angle_failure_dry':'(40 40 40 1.0 40 40 0.5 5.0 10.0 40 40 40)',
		'angle_failure_wetted':'(30 30 30 1.0 30 30 0.5 5.0 10.0 30 30 30)',
		'angle_failure_deposited':'(30 30 30 1.0 30 30 0.5 5.0 10.0 30 30 30)',
		'angle_wetted_criterion':'partially_wetted',
		'min_changed_volume': '0.00000001',
		'max_delta_z':'0.01'})
	# Output
	bm.setRestartTimeStep(1e10)
	bm.setConsoleTimeStep(100)
	#bm.addBASEvizOutput(1)
	bm.addBalanceOutput(1000.0,['sediment','timestep','water_volume'])
	# Stringdef output
	bm.addHistoryOutput(1000.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse'])
	# Node history output at the breaklines
	for ii in range(0,len(uc.getStringdefNames())):
		bm.addHistoryOutput(1000.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node'],True)

	#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])

	#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
	#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
	#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
	bm.addCenteredOutput (1000.0,'element',['wse','depth','velocity'],'vtk')
	bm.addCenteredOutput (1000.0,'node',['z_node','deltaz'],'vtk')
	#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
	#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

	# finally run the model
	bm.build()
	#bm.run()
