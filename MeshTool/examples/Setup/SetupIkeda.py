#!/usr/bin/env python
from Ikeda import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re


# setup the model:
modelID = 'Ikeda'
wd = WORKINGDIRECTORY(modelID)
uc = IKEDA(wd,meshName=modelID,
	modelLength=15.0,
	modelWidth=0.5,
	channelWidth=0.11,
	channelHeight=0.07,
	channelBankSlope=0.063/0.125,
	channelBedSlope=0.00215, ###
	numberOfCells=14000, # 22000, 35000
	randomPert=0.0)
uc.build()
# constant water and sediment inflow:
slope_string = '2.15' 		###
inflow_const = 0.00413 		###
sediment_const = 0.0 		###

# create the mesh for mobile bed
filename = uc.get2dmFilename()
#uc.generate2dm()
filename2dm = '%s.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))

# First, we create the initial condition
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(1)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(uc.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':120.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.001'})
init.setHydInitialCondition({'type':'index_table','index':'(1 2 3)','h':'(0.063 0.02 0.0)','u':'(0.5 0.5 0.5)','v':'(0.0 0.0 0.0)'})
init.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'index':'(1 2 3)', 'friction':'(0.003 0.003 0.003)', 'wall_friction':'off', 'input_type':'index_table'})
# Inflow input file	
inflowFile = 'inflow.txt'   
init.writeTimeSeriesFile(inflowFile,[0,86400.0],[inflow_const,inflow_const])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':slope_string})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':slope_string})
# Output
init.setRestartTimeStep(1e10)
init.setConsoleTimeStep(1)
init.addBASEvizOutput(1)
init.addHistoryOutput(1.0,'stringdef',['CSChannelNine'],['u','wse','zbed'])
# Run simulation for initial condition
#init.build()
init.run()

#################################################################
# Rename restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)
# Rename log file of hydraulic simulation (just used for later checking hydraulic run)
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

#################################################################
# Test restart file and provoke error with available restart time
pe = BASEMENT(wd)
pe.setProjectTitle(modelID)
pe.setModelID(modelID)
pe.setNumThreads(2)
# Geometry
pe.set2dmFile(os.path.split(filename2dm)[1])
pe.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
pe.setTimeStep({'total_run_time':10.0,'start_time':'0.0','CFL':'0.95'})
# Hydraulics
pe.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
pe.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':'3.1456'})
pe.setHydFriction({'type':'chezy', 'default_friction':'%f'%(0.002), 'index':'(1)', 'friction':'(%f)'%(0.002), 'wall_friction':'off', 'input_type':'index_table'})
# Output
pe.setRestartTimeStep(1e32)
pe.setConsoleTimeStep(1)
#pe.addBASEvizOutput(1)
pe.run()

##################################################################
# Parse .err file to get valid time steps to restart
errFile = os.path.join(wd.getDirectory(), modelID+'.err')
fid = open(errFile,'r')
for line in fid:
	if 'Valid times are:' in line:
		data = line.split()
		cgnsTime = data[-1]
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(8)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':43200.0,'start_time':'0.0','CFL':'0.95','morph_cycle':'constant','cycle_step':'10'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTime})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'index':'(1 2 3)', 'friction':'(0.003 0.003 0.003)', 'wall_friction':'off', 'input_type':'index_table'})	
# Inflow input file
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':slope_string})
#bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'zero_gradient'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':slope_string})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'5.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'34.0','porosity':'37.0'})
# Kiesmischung single grain:
bm.setGrainClass([1.3]) # Kiesmischung 1
bm.addMixture('single_grain',[100.0]) # Kiesmischung 1
# Kiesmischung multi grain (to be determined):
#bm.setGrainClass([0.21, 0.41, 0.80, 1.57, 3.07]) # Kiesmischung 2
#bm.addMixture('multi_grain',[12.9, 29.2, 21.6, 16.8, 19.5]) # Kiesmischung 2
bm.addSoilDefinition('soil_uniform',[{'mixture':'single_grain', 'bottom_elevation':-1.0}])
bm.addSoilDefinition('soil_uniform_1',[{'mixture':'single_grain', 'bottom_elevation':-0.05}])
bm.addSoilDefinition('soil_uniform_2',[{'mixture':'single_grain', 'bottom_elevation':-0.10}])
bm.addSoilDefinition('soil_fixbed',[])
bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform'})
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'on','use_cell_averaged_bedload_flux':'off'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'1.4','lateral_index':'(1 2 3)'})
bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'0.2','critical_shear_stress_calibration':'0.8'}) # ,'local_slope_flowdirection':'on','local_slope_lateral':'off'
#bedloadFile = 'bedloadInflow.txt'   
#init.writeTimeSeriesFile(bedloadFile,[0,86400.0],[sediment_const,sediment_const])
# sediment inflow with external sources (index based), rather than at the boundary
#bm.addBedloadBoundaryCondition('bed_inflow',{'type':'sediment_discharge','string_name':'bed_inflow','file':bedloadFile,'mixture':'single_grain'})
#bm.addMorphExternalSource('index',[7],bedloadFile,'single_grain')
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow','fraction_boundary':'1.0'})
bm.setGravitationTransport(
	{'index':'(1 2 3)',
	'angle_failure_dry':'(34 34 34)',
	'angle_failure_wetted':'(30 30 30)',
	'angle_failure_deposited':'(30 30 30)',
	'angle_wetted_criterion':'partially_wetted',
	'min_changed_volume': '0.0000001',
	'max_delta_z':'0.01'})
# Output
bm.setRestartTimeStep(1e10)
bm.setConsoleTimeStep(100)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(1800.0,['sediment','timestep','water_volume'])
# Stringdef output
bm.addHistoryOutput(1800.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse','zbed'])
# Node history output at the breaklines
for ii in range(0,len(uc.getStringdefNames())):
	bm.addHistoryOutput(1800.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node'],True)
bm.addHistoryOutput(1800.0,'boundary',[],['Q','Qsed'])
#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])
#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
bm.addCenteredOutput (1800.0,'element',['wse','depth','velocity'],'vtk')
bm.addCenteredOutput (1800.0,'node',['z_node','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
#bm.run()
