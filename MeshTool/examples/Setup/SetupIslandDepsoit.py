#!/usr/bin/env python

from islandDeposit import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re

# default input parameters
optsPars = OPTIONPARSING()
optsPars.addOption('N',1,0)
optsPars.addOption('Q',0.114,3)
optsPars.addOption('W',0.5,1)
optsPars.addOption('H',0.15,2)
optsPars.addOption('L',2.0,0)

# parse command line options
optsPars.parseOptions(sys.argv[1:])
#print sys.argv[1:]

#modelID = 'GravelDeposit_Nr'+re.split('[.]', str(optsPars.getValue('N')))[0]+'_LF'+re.split('[.]', str(optsPars.getValue('Q')))[1]+'_Wb0'+re.split('[.]', str(optsPars.getValue('W')/2.0))[1]
modelID = 'IslandDeposit_Run3'
wd = WORKINGDIRECTORY(modelID)

Q = 0.225

# setup the mesh properties
td = ISLANDDEPOSIT(wd,meshName=modelID,
channelLength=31.45,
channelWidth=2.0,
channelHeight=0.25,
channelBankSlope=2.0/3.0,
channelBedSlope=0.00172,
numberOfCells=10000.0,
#depositLength=optsPars.getValue('L'),
#depositWidth=optsPars.getValue('W'),
#depositHeight=optsPars.getValue('H'), 
depositLength=2.0,
depositWidth=0.6,
depositHeight=0.15,
depositBankSlope=0.7,
depositYoffset=0.0)

td.build()

# create the mesh for mobile bed
filename = td.get2dmFilename()
td.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))


# and the fixed bed mesh
td.generate2dm(fixed=True)
filename2dmFixed = '%s_fixed.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dmFixed))

# First, we create the initial condition
#inflowFile = 'Inflow'+re.split('[.]', str(optsPars.getValue('Q')))[1]+'.txt'
inflowFile = 'Inflow.txt'
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(1)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(td.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':120.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.001'})
init.setHydInitialCondition({'type':'dry'})
init.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'index':'(1 2 3 4)', 'friction':'(0.004 0.003 0.003 0.004)', 'wall_friction':'off', 'input_type':'index_table'})
#																1: bed, 2: bank left, 3: bank right, 4: island
# Inflow input file	
#init.writeTimeSeriesFile(inflowFile,[0.0,10800.0],[optsPars.getValue('Q'),optsPars.getValue('Q')])
init.writeTimeSeriesFile(inflowFile,[0.0,10800.0],[Q,Q])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'1.72'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.72'})
# Output
init.setRestartTimeStep(1e32)
init.setConsoleTimeStep(10)
init.addBalanceOutput(20.0,['timestep','water_volume'])
#init.addHistoryOutput(10.0,'stringdef',['inflow','outflow'],['Q','u','wse','zbed'])
#init.addCenteredOutput (50.0,'element',['wse','depth','velocity'],'vtk')
init.addBASEvizOutput(1)
# Run simulation for initial condition
init.run()

RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)

# Rename log file of hydraulic simulation
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

# Parse hydraulic log file for CGNS output time step
fid = open(strLogFileHyd,'r')
for line in fid:
	if 'CGNS' in line:
		data = line.split()
		cgnsTime = data[-1]
count = 0
cgnsTimeP = ''
for ii in cgnsTime:
	if ii == '.':
		count +=1
	if count <=1:
		cgnsTimeP = cgnsTimeP + ii
fid.close()

# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(8)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile (os.path.split(td.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':10800.0,'start_time':'0.0'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'index':'(1 2 3 4)', 'friction':'(0.004 0.003 0.003 0.004)', 'wall_friction':'off', 'input_type':'index_table'})	
																		#1: bed, 2: bank left, 3: bank right, 4: island
# Inflow input file
#bm.writeTimeSeriesFile(inflowFile,[0.0,10800.0],[optsPars.getValue('Q'),optsPars.getValue('Q')])
bm.writeTimeSeriesFile(inflowFile,[0.0,10800.0],[Q,Q])
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'1.72'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.72'})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'2.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'40.0','control_volume_type':'borah'})
bm.setGrainClass([0.5, 1.0, 1.5, 2.2, 3.2])
bm.addMixture('multi_grain',[28, 24, 16, 22, 10])
bm.addSoilDefinition('soil_mixture',[{'mixture':'multi_grain', 'bottom_elevation':-2.0}])
bm.assignSoils({'1':'soil_mixture','2':'soil_mixture','3':'soil_mixture','4':'soil_mixture'})
bm.setFixedBed(os.path.split(filename2dmFixed)[1])
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'on','use_cell_averaged_bedload_flux':'off'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'2.0','lateral_index':'(1 2 3 4)'})
bm.setBedloadFormula({'bedload_formula':'mpm_multi','bedload_factor':'0.7','critical_shear_stress_calibration':'0.90'})
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow'})
bm.setGravitationTransport(
	{'index':'(1 2 3 4)',
	'angle_failure_dry':'(40 40 40 40)',
	'angle_failure_wetted':'(30 30 30 30)',
	'angle_failure_deposited':'(30 30 30 30)',
	'angle_wetted_criterion':'partially_wetted',
	'min_changed_volume': '0.0000001',
	'max_delta_z':'0.001'})
# Output
bm.setRestartTimeStep(1e32)
bm.setConsoleTimeStep(100)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(60.0,['sediment','timestep','water_volume'])
# Stringdef output
bm.addHistoryOutput(60.0,'stringdef',td.getStringdefNames(),['Q','Qsed','u','wse'])
bm.addHistoryOutput(60.0,'stringdef',['top'],['Qsed'])
bm.addHistoryOutput(60.0,'stringdef',['down'],['Qsed'])
bm.addHistoryOutput(60.0,'stringdef',['left'],['Qsed'])
bm.addHistoryOutput(60.0,'stringdef',['right'],['Qsed'])
# Node history output at the breaklines
#bm.addHistoryOutput(100.0,'node',td.getStringdefNodeIDs('b1-b2'),['z_node'],True)
for ii in range(0,len(td.getStringdefNames())):
	bm.addHistoryOutput(300.0,'node',td.getStringdefNodeIDs(td.getStringdefNames()[ii]),['z_node'],True)

# Node centred output (node: node centred, element: element centred)
#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
bm.addCenteredOutput (600.0,'node',['z_node','wse','depth','velocity','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
bm.run()
