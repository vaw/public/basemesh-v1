#!/usr/bin/env python
from testMeshes import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re

# setup the model
modelID = 'TrapezChannel_H'
wd = WORKINGDIRECTORY(modelID)
uc = TRAPEZOIDALCHANNEL(wd,meshName=modelID,
	channelLength=32.0*25.0,
	channelWidth=2.0*25.0,
	channelHeight=0.5*25.0,
	channelBankSlope=2.0/3.0,
   	channelBedSlope=0.00172,
   	numberOfCells=12000.0)
uc.build()

# create the mesh for mobile bed
filename = uc.get2dmFilename()
#uc.generate2dm()
filename2dm = '%s.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))

# First, we create the initial condition
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(3)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(uc.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':1000.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
init.setHydInitialCondition({'type':'dry'})
init.setHydFriction({'type':'yalin', 'default_friction':'0.125', 'index':'(1 2 3 4)', 'friction':'(0.125 0.125 0.125 0.125)', 'wall_friction':'off', 'input_type':'index_table'})
# Inflow input file	
inflowFile = 'inflow.txt'   
init.writeTimeSeriesFile(inflowFile,[0,3600.0],[356.25,356.25])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'1.72'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.72'})
# Output
init.setRestartTimeStep(1e32)
init.setConsoleTimeStep(30)
init.addBASEvizOutput(1)
init.addBalanceOutput(10.0,['timestep','water_volume'])
# Stringdef output
init.addHistoryOutput(10.0,'stringdef',uc.getStringdefNames(),['Q','u','wse','zbed'])
# Run simulation for initial condition
init.run()


"""
#################################################################
# Rename restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)
# Rename log file of hydraulic simulation (just used for later checking hydraulic run)
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

#################################################################
# Test restart file and provoke error with available restart time
pe = BASEMENT(wd)
pe.setProjectTitle(modelID)
pe.setModelID(modelID)
pe.setNumThreads(3)
# Geometry
pe.set2dmFile(os.path.split(filename2dm)[1])
pe.setStringdefFile(os.path.split(td.getStringdefFilename())[1])
# Timestep
pe.setTimeStep({'total_run_time':10.0,'start_time':'0.0','CFL':'0.95'})
# Hydraulics
pe.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
pe.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':'3.1456'})
pe.setHydFriction({'type':'chezy', 'default_friction':'0.08', 'wall_friction':'off'})
# Output
pe.setRestartTimeStep(1e32)
pe.setConsoleTimeStep(2)
#pe.addBASEvizOutput(1)
pe.run()

##################################################################
# Parse .err file to get valid time steps to restart
errFile = os.path.join(wd.getDirectory(), modelID+'.err')
fid = open(errFile,'r')
for line in fid:
	if 'Valid times are:' in line:
		data = line.split()
		cgnsTime = data[-1]
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(4)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':86400.0,'start_time':'0.0'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'index':'(1 2 3 4)', 'friction':'(0.003 0.0015 0.0015 0.003)', 'wall_friction':'off', 'input_type':'index_table'})	
# Inflow input file
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'2.5'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'zero_gradient'})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'2.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'37.0','control_volume_type':'constant', 'control_volume_thickness':'0.2'})
bm.setGrainClass([0.5, 1.0, 1.5, 2.2, 3.2])
bm.addMixture('multi_grain',[28, 24, 16, 22, 10])
bm.setGrainClass([0.5, 1.0, 1.5, 2.2, 3.2])
bm.addMixture('multi_grain_armor',[10, 20, 20, 30, 20])
#bm.setGrainClass([1.5])
#bm.addMixture('single_grain',[100.0])
bm.addSoilDefinition('soil_uniform',[{'mixture':'multi_grain', 'bottom_elevation':-0.5}])
bm.addSoilDefinition('soil_fixbed',[])
bm.assignSoils({'1':'soil_uniform','2':'soil_fixbed','3':'soil_fixbed','4':'soil_fixbed'})

# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'off'}) # ,'use_cell_averaged_bedload_flux':'off','upwind':'(1.0 1.0 1.0 1.0)','upwind_index':'(1 2 3 4)'
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'2.0','lateral_index':'(1 2 3 4)'})
bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'1.0'}) # ,'local_slope_flowdirection':'on','local_slope_lateral':'off'
bm.addBedloadBoundaryCondition('bed_inflow',{'type':'IOUp','string_name':'bed_inflow','fraction_boundary':'1.0'})
bm.addBedloadBoundaryCondition('bed_outflow',{'type':'IODown','string_name':'bed_outflow','fraction_boundary':'0.7'})

# Output
bm.setRestartTimeStep(1e32)
bm.setConsoleTimeStep(100)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(600.0,['sediment','timestep','water_volume'])
# Stringdef output
bm.addHistoryOutput(600.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse'])
# Node history output at the breaklines
for ii in range(0,len(uc.getStringdefNames())):
	bm.addHistoryOutput(600.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node'],True)

#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])

#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
bm.addCenteredOutput (600.0,'element',['wse','depth','velocity'],'vtk')
bm.addCenteredOutput (600.0,'node',['z_node','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
#bm.run()
"""