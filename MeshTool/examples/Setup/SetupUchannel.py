#!/usr/bin/env python
from Uchannel import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re

# setup the model
modelID = 'YenAndLee_extended'
wd = WORKINGDIRECTORY(modelID)
uc = UCHANNEL(wd,meshName=modelID,
	channelLength=11.5,
	channelBedWidth=1.0,
	channelRadius=4.0,
   	channelBedSlope=0.002,
    observationAngles=[90.0],
   	numberOfCells=10000.0)
uc.build()

# create the mesh for mobile bed
filename = uc.get2dmFilename()
uc.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))


# First, we create the initial condition
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(2)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(uc.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':360.0,'start_time':'0.0','CFL':'0.95'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.001'})
init.setHydInitialCondition({'type':'index_table','index':'(1 2 3)','h':'(0.05 0.05 0.05)','u':'(0.0 0.0 0.0)','v':'(0.0 0.0 0.0)'})
init.setHydFriction({'type':'chezy', 'default_friction':'0.003', 'fixed_bed_d90':'1.0','index':'(1 2 3)', 'friction':'(0.003 0.003 0.003)', 'wall_friction':'off','input_type':'index_table'})
# Inflow input file	
inflowFile = 'inflow.txt'   
init.writeTimeSeriesFile(inflowFile,[0,86400.0],[0.02,0.02])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'2.0'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'2.0'})
# Output
init.setRestartTimeStep(1e32)
init.setConsoleTimeStep(10)
init.addBASEvizOutput(1)
# Run simulation for initial condition
init.run()

# Process restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)

# Rename log file of hydraulic simulation
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

# Parse hydraulic log file for CGNS output time step
fid = open(strLogFileHyd,'r')
for line in fid:
	if 'CGNS' in line:
		data = line.split()
		cgnsTime = data[-1]
count = 0
cgnsTimeP = ''
for ii in cgnsTime:
	if ii == '.':
		count +=1
	if count <=1:
		cgnsTimeP = cgnsTimeP + ii
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(8)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':24000.0,'start_time':'0.0','CFL':'0.95','morph_cycle':'constant','cycle_step':'5'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001','riemann_solver':'hllc'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy', 'roughness_factor':'3.0','grain_size_friction':'yes','default_friction':'0.003', 'index':'(1 2 3)', 'friction':'(0.003 0.003 0.003 0.003 0.003)', 'wall_friction':'off', 'wall_friction_factor':'0.0001','input_type':'index_table'})	
# Inflow input file	
inflowFileHydrograph = 'inflowHyd.txt'   
init.writeTimeSeriesFile(inflowFileHydrograph,[0.0,6000.0,18000.0,24000.0],[0.02,0.053,0.02,0.02])
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFileHydrograph, 'slope':'2.0'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'2.0'})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'1.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'34.0','control_volume_type':'constant'})
bm.setGrainClass([0.20, 0.29, 0.44, 0.67, 1.0, 1.50, 2.26, 3.39, 5.10])
bm.addMixture('multi_grain',[6.0, 7.3, 11.9, 16.0, 17.6, 16.0, 11.9, 7.3, 6.0])
bm.addSoilDefinition('soil_fixbed',[])
bm.addSoilDefinition('soil_uniform',[{'mixture':'multi_grain', 'bottom_elevation':-0.2}])
bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform'})
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'on'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'1.4','lateral_index':'(1 2 3)'})
bm.setBedloadDirection({'lateral_transport_type':'curvature_effect_dynamic','curvature_transport_factor':'9.0','radius_calculation_type':'water_surface_elevation','lateral_index':'(2)','min_abs_radius':'3.5','update_timestep_radius':'30.0'})
#bm.setBedloadDirection({'lateral_transport_type':'curvature_effect_static','radius_static':'(-4.0)','lateral_index':'(2)','curvature_transport_factor':'9.0','min_abs_radius':'3.5'})
bm.setBedloadFormula({'bedload_formula':'mpm_multi','bedload_factor':'1.0','critical_shear_stress_calibration':'1.0'})
#bm.addBedloadBoundaryCondition('inflow',{'type':'IOUp','string_name':'inflow'})
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow'})
# Output
bm.setRestartTimeStep(1e32)
bm.setConsoleTimeStep(100)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(600.0,['sediment','timestep','water_volume'])
# Stringdef history output
bm.addHistoryOutput(600.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse','zbed'])
# Node history output at the breaklines
for ii in range(0,len(uc.getStringdefNames())):
	bm.addHistoryOutput(1800.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node','deltaz','grain_size'],True)

#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])

#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
bm.addCenteredOutput (18000.0,'node',['z_node','deltaz','grain_size'],'sms')
bm.addCenteredOutput (1800.0,'element',['wse','depth','velocity','radius_curvature','radius_curvature_abs'],'vtk')
bm.addCenteredOutput (1800.0,'node',['z_node','deltaz','grain_size'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
#bm.run()