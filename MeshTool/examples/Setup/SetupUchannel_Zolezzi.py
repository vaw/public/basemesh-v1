#!/usr/bin/env python
from Uchannel import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re


# setup the model
#------------------------
modelID = 'Zolezzi_Run29'
Qin = 0.0105
h0 = 0.05
#------------------------

wd = WORKINGDIRECTORY(modelID)
uc = UCHANNEL(wd,meshName=modelID,
	channelLength=100.0,
	channelBedWidth=0.6,
	channelRadius=2.5,
   	channelBedSlope=0.011,
    observationAngles=[45.0,90.0,135.0],
   	numberOfCells=40000.0,
   	randomPert=0.0)
uc.build()

# create the mesh for mobile bed
filename = uc.get2dmFilename()
uc.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))


# First, we create the initial condition
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(1)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(uc.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':300.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.001'})
init.setHydInitialCondition({'type':'index_table','index':'(1 2 3)','h':'(%.2f %.2f %.2f)'%(h0,h0,h0),'u':'(0.0 0.0 0.0)','v':'(0.5 0.0 -0.5)'})
init.setHydFriction({'type':'chezy', 'default_friction':'0.0025','index':'(1 2 3)', 'friction':'(0.0025 0.0025 0.0025)', 'wall_friction':'off', 'wall_friction_factor':'0.0005','input_type':'index_table'})
# Inflow input file	
inflowFile = 'inflow.txt'   
init.writeTimeSeriesFile(inflowFile,[0,86400.0],[Qin,Qin])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'4.0', 'weighting_type':'area'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'zero_gradient'})
# Output
init.setRestartTimeStep(1e32)
init.setConsoleTimeStep(10)
init.addBASEvizOutput(1)
# Stringdef history output
init.addHistoryOutput(10.0,'stringdef',uc.getStringdefNames(),['Q','u','wse','zbed'])
# Run simulation for initial condition
init.run()

# Process restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)

# Rename log file of hydraulic simulation
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

# Parse hydraulic log file for CGNS output time step
fid = open(strLogFileHyd,'r')
for line in fid:
	if 'CGNS' in line:
		data = line.split()
		cgnsTime = data[-1]
count = 0
cgnsTimeP = ''
for ii in cgnsTime:
	if ii == '.':
		count +=1
	if count <=1:
		cgnsTimeP = cgnsTimeP + ii
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(8)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile(os.path.split(uc.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':21600.0,'start_time':'0.0'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.001'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTimeP})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'chezy','default_friction':'0.0025', 'index':'(1 2 3)', 'friction':'(0.0025 0.0025 0.0025)', 'wall_friction':'off', 'wall_friction_factor':'0.0001','input_type':'index_table'})	
# Inflow input file	
inflowFileHydrograph = 'inflowHyd.txt'   
bm.writeTimeSeriesFile(inflowFileHydrograph,[0.0,23000.0],[Qin,Qin])
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFileHydrograph, 'slope':'4.0', 'weighting_type':'area'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'zero_gradient'})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'1.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'34.0','min_theta_critic':'0.001'})
bm.setGrainClass([1.0])
bm.addMixture('single_grain',[100.0])
bm.addSoilDefinition('soil_fixbed',[])
bm.addSoilDefinition('soil_uniform_1',[{'mixture':'single_grain', 'bottom_elevation':-0.05}])
bm.addSoilDefinition('soil_uniform',[{'mixture':'single_grain', 'bottom_elevation':-0.2}])
bm.assignSoils({'1':'soil_uniform','2':'soil_uniform','3':'soil_uniform'})
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'on'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'1.35','lateral_index':'(1 2 3)'})
#bm.setBedloadDirection({'lateral_transport_type':'curvature_effect_dynamic','curvature_transport_factor':'7.0','lateral_index':'(2)','min_abs_radius':'2.5','radius_calculation_type':'water_surface_elevation'})
bm.setBedloadDirection({'lateral_transport_type':'curvature_effect_static','radius_static':'(-2.5)','lateral_index':'(2)','curvature_transport_factor':'7.0'})
bm.setBedloadFormula({'bedload_formula':'mpm','bedload_factor':'1.0','critical_shear_stress_calibration':'0.80'})
bm.addBedloadBoundaryCondition('inflow',{'type':'IOUp','string_name':'inflow','fraction_boundary':'0.07'})
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow','fraction_boundary':'0.9'})
bm.setGravitationTransport(
	{'index':'(1 2 3)',
	'angle_failure_dry':'(31.0 31.0 31.0)',
	'angle_failure_wetted':'(30.0 30.0 30.0)',
	'angle_failure_deposited':'(30.0 30.0 30.0)',
	'angle_wetted_criterion':'partially_wetted',
	'min_changed_volume': '0.0000001',
	'max_delta_z':'0.001'})
# Output
bm.setRestartTimeStep(1e32)
bm.setConsoleTimeStep(100)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(600.0,['sediment','timestep','water_volume'])
# Stringdef history output
bm.addHistoryOutput(600.0,'stringdef',uc.getStringdefNames(),['Q','Qsed','u','wse','zbed'])
# Node history output at the breaklines
for ii in range(0,len(uc.getStringdefNames())):
	bm.addHistoryOutput(1800.0,'node',uc.getStringdefNodeIDs(uc.getStringdefNames()[ii]),['z_node','deltaz'],True)

#bm.addHistoryOutput(5.0,'element',['60'],['velocity'])

#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz','radius_curvature'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
bm.addCenteredOutput (1800.0,'node',['z_node','deltaz'],'sms')
bm.addCenteredOutput (1800.0,'element',['wse','depth','velocity','radius_curvature'],'vtk')
bm.addCenteredOutput (900.0,'node',['z_node','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
bm.run()