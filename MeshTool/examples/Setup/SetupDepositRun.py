#!/usr/bin/env python

from trapezoidalChannelDeposits import *
from trapezoidalChannelDepositsL8m import *
from basement import *
from batchTools import *
from postprocessing import *
import os, shutil
import re

# default input parameters
optsPars = OPTIONPARSING()
optsPars.addOption('N',1,0)
optsPars.addOption('Q',0.114,3)
optsPars.addOption('W',0.5,1)
optsPars.addOption('H',0.15,2)
optsPars.addOption('L',2.0,0)

# parse command line options
optsPars.parseOptions(sys.argv[1:])
print sys.argv[1:]

#modelID = 'GravelDeposit_'+optsPars.getID()
modelID = 'GravelDeposit_Nr'+re.split('[.]', str(optsPars.getValue('N')))[0]+'_LF'+re.split('[.]', str(optsPars.getValue('Q')))[0]+'_Wb0'+re.split('[.]', str(optsPars.getValue('W')/50.0))[1]
wd = WORKINGDIRECTORY(modelID)

# setup the mesh properties
if optsPars.getValue('L') == 200.0:
	td = TRAPEZOIDALCHANNELDEPOSITL8M(wd,meshName=modelID,
	channelLength=31.45*25.0,
	channelWidth=2.0*25.0,
	channelHeight=0.25*25.0,
	channelBankSlope=2.0/3.0,
	channelBedSlope=0.00172,
	numberOfCells=12000.0,
	depositLength=optsPars.getValue('L'),
	depositAngle=60.0,
	depositWidth=optsPars.getValue('W'),
	depositHeight=optsPars.getValue('H'), 
	depositBankSlope=0.7)
else:
	td = TRAPEZOIDALCHANNELDEPOSIT(wd,meshName=modelID,
	channelLength=31.45*25.0,
	channelWidth=2.0*25.0,
	channelHeight=0.25*25.0,
	channelBankSlope=2.0/3.0,
	channelBedSlope=0.00172,
	numberOfCells=12000.0,
	depositLength=optsPars.getValue('L'),
	depositAngle=60.0,
	depositWidth=optsPars.getValue('W'),
	depositHeight=optsPars.getValue('H'), 
	depositBankSlope=0.7)

td.build()

# create the mesh for mobile bed
filename = td.get2dmFilename()
td.generate2dm()
filename2dm = '%s_mobile.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dm))
# and the fixed bed mesh
td.generate2dm(fixed=True)
filename2dmFixed = '%s_fixed.2dm'%os.path.splitext(filename)[0]
shutil.move(os.path.join(wd.getDirectory(),filename),os.path.join(wd.getDirectory(),filename2dmFixed))

# First, we create the initial condition
inflowFile = 'Inflow'+re.split('[.]', str(optsPars.getValue('Q')))[0]+'.txt'
# Note: starting dry we don't apply a turbulence model
init = BASEMENT(wd)
init.setProjectTitle(modelID)
init.setModelID(modelID)
init.setNumThreads(3)
# Geometry
init.set2dmFile(filename2dm)
init.setStringdefFile(td.getStringdefFilename())
# Timestep
init.setTimeStep({'total_run_time':1200.0,'start_time':'0.0'})
# Hydraulics
init.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
init.setHydInitialCondition({'type':'dry'})
init.setHydFriction({'type':'yalin', 'default_friction':'0.125', 'index':'(1 2 3 4)', 'friction':'(0.125 0.125 0.125 0.125)', 'wall_friction':'off', 'input_type':'index_table'})
# Inflow input file	
init.writeTimeSeriesFile(inflowFile,[0.0,54000.0],[optsPars.getValue('Q'),optsPars.getValue('Q')])
init.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'1.72'})
init.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.72'})
# Output
init.setRestartTimeStep(1e32)
init.setConsoleTimeStep(60)
init.addBalanceOutput(100.0,['timestep','water_volume'])
init.addBASEvizOutput(1)
# Run simulation for initial condition
#init.build()
init.run()


#################################################################
# Rename restart file for initial condition
RestartFileName = 'Hydraulics_Inital_Condition_'+inflowFile.split('.')[0]+'.cgns'
# get the path of this script
root = os.path.split(os.path.realpath(__file__))[0]
strRestartDefault = os.path.join(wd.getDirectory(), modelID+'_region_restart.cgns')
strRestart = os.path.join(wd.getDirectory(), RestartFileName)
# Check if an old file already exists
if os.path.exists(strRestart):
	print "Restart file already exists and will be replaced with new restart file"
	os.remove(strRestart)
	os.rename(strRestartDefault, strRestart)
else:
    print "strRestartDefault", strRestartDefault
    os.rename(strRestartDefault, strRestart)
# Rename log file of hydraulic simulation (just used for later checking hydraulic run)
strLogFile = os.path.join(wd.getDirectory(), modelID+'.log')
strLogFileHyd = os.path.join(wd.getDirectory(), modelID+'hydraulic.log')
if os.path.exists(strLogFileHyd):
	os.remove(strLogFileHyd)
	os.rename(strLogFile, strLogFileHyd)
else:
	os.rename(strLogFile, strLogFileHyd)

#################################################################
# Test restart file and provoke error with available restart time
pe = BASEMENT(wd)
pe.setProjectTitle(modelID)
pe.setModelID(modelID)
pe.setNumThreads(3)
# Geometry
pe.set2dmFile(os.path.split(filename2dm)[1])
pe.setStringdefFile(os.path.split(td.getStringdefFilename())[1])
# Timestep
pe.setTimeStep({'total_run_time':10.0,'start_time':'0.0','CFL':'0.95'})
# Hydraulics
pe.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
pe.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':'3.1456'})
pe.setHydFriction({'type':'yalin', 'default_friction':'0.08', 'wall_friction':'off'})
# Output
pe.setRestartTimeStep(1e32)
pe.setConsoleTimeStep(2)
#pe.addBASEvizOutput(1)
pe.run()

##################################################################
# Parse .err file to get valid time steps to restart
errFile = os.path.join(wd.getDirectory(), modelID+'.err')
fid = open(errFile,'r')
for line in fid:
	if 'Valid times are:' in line:
		data = line.split()
		cgnsTime = data[-1]
fid.close()


# Now we setup the sediment model
# BASEMENT object:
bm = BASEMENT(wd)
bm.setProjectTitle(modelID)
bm.setModelID(modelID)
bm.setNumThreads(7)
# Geometry
bm.set2dmFile(os.path.split(filename2dm)[1])
bm.setStringdefFile (os.path.split(td.getStringdefFilename())[1])
# Timestep
bm.setTimeStep({'total_run_time':54000.0,'start_time':'0.0','CFL':'0.95'})
# Hydraulics
bm.setHydParameters({'minimum_water_depth':'0.01','riemann_solver':'hllc'})
bm.setHydInitialCondition({'type':'continue','file':RestartFileName,'restart_solution_time':cgnsTime})
#bm.setHydInitialCondition({'type':'dry'})
bm.setHydFriction({'type':'yalin','default_friction':'0.08','index':'(1 2 3 4)','friction':'(0.08 0.08 0.08 0.08)','wall_friction':'off','input_type':'index_table','grain_size_friction':'yes','roughness_factor':'2.5','fixed_bed_d90':'50.0'})	
# Inflow input file
bm.writeTimeSeriesFile(inflowFile,[0.0,54000.0],[optsPars.getValue('Q'),optsPars.getValue('Q')])
bm.addHydBoundaryCondition('inflow',{'string_name':'inflow', 'type':'hydrograph', 'file':inflowFile, 'slope':'1.72'})
bm.addHydBoundaryCondition('outflow',{'string_name':'outflow', 'type':'hqrelation', 'slope':'1.72'})
bm.addTurbulenceModel({'type':'algebraic','turbulence_factor':'2.0'})
# Morphology
bm.setMorphInitialCondition({'type':'initial_mesh'})
bm.setMorphParameters({'local_slope_angle_repose':'30.0','porosity':'40.0','control_volume_type':'constant','control_volume_thickness':'0.125'})
bm.setGrainClass([12.5, 25.0, 37.5, 55.0, 80.0])
bm.addMixture('multi_grain',[28, 24, 16, 22, 10])
bm.addSoilDefinition('soil_mixture',[{'mixture':'multi_grain', 'bottom_elevation':-50.0}])
bm.assignSoils({'1':'soil_mixture','2':'soil_mixture','3':'soil_mixture','4':'soil_mixture'})
bm.setFixedBed(os.path.split(filename2dmFixed)[1])
# bedload transport
bm.setBedloadParameters({'limit_bedload_wetted':'off','use_cell_averaged_bedload_flux':'off','upwind_index':'(1 2 3 4)','upwind':'(0.8 0.8 0.8 0.8)'})
bm.setBedloadDirection({'lateral_transport_type':'lateral_bed_slope','lateral_transport_factor':'2.0','lateral_index':'(1 2 3 4)'})
bm.setBedloadFormula({'bedload_formula':'mpm_multi','bedload_factor':'0.7','bedload_exponent':'1.6','theta_critical_approach':'theta_critical_yalin','theta_critical_hiding':'mean','local_slope_correction':'local_slope_vanrijn'})
bm.addBedloadBoundaryCondition('outflow',{'type':'IODown','string_name':'outflow'})
bm.setGravitationTransport(
	{'index':'(1 2 3 4)',
	'angle_failure_dry':'(40 40 40 40)',
	'angle_failure_wetted':'(30 30 30 30)',
	'angle_failure_deposited':'(30 30 30 30)',
	'angle_wetted_criterion':'partially_wetted',
	'min_changed_volume': '0.000001',
	'max_delta_z':'0.001'})
# Output
bm.setRestartTimeStep(1e32)
bm.setConsoleTimeStep(600)
#bm.addBASEvizOutput(1)
bm.addBalanceOutput(600.0,['sediment','timestep','water_volume'])
# Stringdef output
bm.addHistoryOutput(600.0,'stringdef',td.getStringdefNames(),['Q','Qsed','u','wse'])
if optsPars.getValue('L') == 50.0:
	bm.addHistoryOutput(1.0,'stringdef',['b17-b18'],['Qsed']) # L = 8m: 'b31-b32'; L = 2m: 'b17-b18'
if optsPars.getValue('L') == 200.0:
	bm.addHistoryOutput(1.0,'stringdef',['b31-b32'],['Qsed']) # L = 8m: 'b31-b32'; L = 2m: 'b17-b18'
bm.addHistoryOutput(1.0,'stringdef',['top'],['Qsed'])
bm.addHistoryOutput(1.0,'stringdef',['middle'],['Qsed'])
bm.addHistoryOutput(1.0,'stringdef',['lower'],['Qsed'])
# Node history output at the breaklines
#bm.addHistoryOutput(100.0,'node',td.getStringdefNodeIDs('b1-b2'),['z_node'],True)
for ii in range(0,len(td.getStringdefNames())):
	bm.addHistoryOutput(600.0,'node',td.getStringdefNodeIDs(td.getStringdefNames()[ii]),['z_node'],True)

# Node centred output (node: node centred, element: element centred)
#bm.addCenteredOutput (300.0,'node',['z_node','wse','depth','velocity','deltaz'],'sms')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'ascii')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'qgis')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'vtk')
#bm.addCenteredOutput (300.0,'element',['z_node','wse','depth','velocity','deltaz'],'tecplot')
#bm.addCenteredOutput (100.0,'node',['z_node','wse','depth','velocity','deltaz'],'shape')

# finally run the model
bm.build()
#bm.run()

'''
# postprocessing
data = []
for name in td.getStringdefNames():
	pp = POSTPROCESSING(bm.getResultNameSuffix())
	pp.readTimeHistory('stringdef',name)
	print pp.getFilename()
	print pp.getData('Q')
	data.append(pp)


# postprocessing
pp = POSTPROCESSING(bm.getResultNameSuffix())
pp.readBalance()
print pp.getFilename()
print pp.getData('water_vol')
'''

