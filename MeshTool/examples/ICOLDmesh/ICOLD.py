#!/usr/bin/env python
import math
import numpy as np
from batchTools import *
from meshModel import NODE, MESHMODEL, RASTER

class ICOLD (MESHMODEL):
	def __init__ (self, directory,
		domainFile=None,
		maxArea=100.0,
		quality=30.0,
		demFile=None,
		landuseFile=None,
		mType='plane2x'):
		# call init of the meshmodel class
		MESHMODEL.__init__(self,directory,absolutePrecision=1e-1)
		# this class does all the meshing stuff
		self.initialization('pocICOLD',mType)
		# store input argument as member variable
		self.__A = maxArea
		self.__q = quality
		# domain information
		try:
			self.__domain = np.genfromtxt(domainFile, delimiter=',')
		except:
			print 'error reading domain file...'
			self.__domain = None
		# load the DEM data
		dem = RASTER(demFile)
		self.__DEM = dem.getInterpolator()
		# load the landuse data
		lulc = RASTER(landuseFile)
		lulc.setInterpolation('nearest')
		self.__lulc = lulc.getInterpolator()
		# activate bandwidth reduction
		self.setReduction(True)
		
	def MaterialIDFunction (self, matID, x, y):
		return self.__lulc([x,y]) if self.__lulc else 0

	def elevationFunction (self, x, y):
		return self.__DEM([x,y]) if self.__DEM else 0.0

	# this function actually does all the work
	def build (self):
		nodes = []
		for n in self.__domain:
			nodes.append( NODE(n[0],n[1]) )
		# now define the domain
		self.addLine(nodes,True)
		# add some stringdefs
		self.addStringdef( '11', [nodes[-2],nodes[0]] )
		self.addStringdef( '12', nodes[4:6] )
		self.addStringdef( '13', nodes[5:7] )
		self.addStringdef( '14', nodes[6:84] )
		self.addStringdef( '15', nodes[83:85] )
		# set triangle options
		self.setQuality(self.__q)
		self.setArea(self.__A)
		# let's go...
		self.createMesh()

		
print '*** ICOLD 2013 benchmark mesh ***'
wd = WORKINGDIRECTORY('icold_benchmark')
# 2k   -> a = 1.00e5
# 20k  -> a = 9.65e3
# 200k -> a = 9.6e2
icold = ICOLD(wd,
	maxArea=1.0e5,
	quality=30.0,
	domainFile='domain.csv',
	demFile='dem.txt',
	landuseFile='lulc.txt',
	mType='plane3x'
	)
icold.build()