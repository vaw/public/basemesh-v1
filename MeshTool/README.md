# Installation of BASEMENT-MESH

* add the BASEMENT-MESH directory to the system environment variable `PYTHONPATH`in the .bashrc file.
~~~bash
export PYTHONPATH=~/BASEMENT-MESH/MeshTool
~~~

* now you can generate your meshes wherever you want by running
~~~python
python myMesh.py
~~~


# Follow these instructions to use the meshing tool

* create a python file MyModel_mesh.py

* from module `meshModel` import the class `NODE` and `MESHMODEL`
~~~python
from meshModel import NODE, MESHMODEL
~~~

* create a python class that:
    - inherits from the `MESHMODEL` class
    - initialize this parent class with a model ID. This string will be used as identifier for all generated filenames
    - implement the function `elevationFunction(self,x,y)`. This function describes the topography of your model (with input arguments x and y coordinates, and return value z coordinate). It is held abstract in the `MESHMODEL` class and *must* be implemented here.
    - implement the function 'build(self)'. In order to define the mesh features (boundary, breaklines, regions, holes, mesh quality, ...)
    - in this function, define all x/y points you need for the definition of your mesh with `NODE()` instances.
    - define all breaklines with list of `NODE` objects
    - define regions with `NODE` objects and the optional arguments 'maxArea' and `matID`
    - define stringdefs with a name and a list of nodes that describe the segments of the stringdef
    - set the quality of your mesh (minimum angle of the triangles)
    - finally create your mesh (in the background Shewchucks `TRIANGLE` runs, the results are read, and a `.2dm-file` is written)


~~~python
class MyModel_ID (MESHMODEL):
	...
	def elevationFunction (self, x, y):
		...
	def build (self):
		#define the points needed for the definition of the mesh
		n1 = NODE(x,y)
		...
		#breakline
		self.addLine([n1,...])
		#regions
		self.addRegion(NODE(),maxArea, matID)
		#stringdefs
		self.addStringdef('name',[n1,...])
		#quality of mesh
		self.setQuality()
		#create mesh
		self.createMesh()
~~~


* three different mesh types can be generated (`mType=...`):
   - *plane2x* for 2d meshes of BASEMENT v2.x (default)
   - *plane3x* for 2d meshes of BASEMENT v3.xat
   - *plane2x3x* for 2d meshes of BASEMENT v3.x, but z-values are stored on the node (instead of cells)
   - *chain* for 1d mesh, i.e. list of cross-sections cutting the 2d topography

* use the created class to produce the `.2dm-file`. **See the files in the folder examples/MESHgenerator for illustration.**


# Mesh visualization

* for BM<=2.8: you can visualize your mesh with the viewer tool (you need openGL and pyqt installed) by running
~~~python
python viewMesh.py <your2dmFile> <optionalZScaleArgument>
~~~

* for BM=3.0: you can visualize your mesh making use of Paraview engine (you need Paraview installed) by running
~~~python
python vtkVis.py <your2dmFile>
~~~
