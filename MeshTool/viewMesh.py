#!/usr/bin/python

# by Samuel J. Peter, 2015

import sys
import random as rnd
import numpy as np
from ..pyqtgl import opengl
try:
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import pyqtSignal
except ImportError:
    print 'unable to import PyQt4 module'
from meshModel import *



class GLWidget(opengl.GLViewWidget):
    Moved = pyqtSignal()

    def __init__(self):
        opengl.GLViewWidget.__init__(self)
        # eventually implement a mouse event to move the origin ...


def viewMesh (nodeDict,cellList,zScale=1.0):
    # test function: plot the mesh in a opengl window...yeah!!!
    #write all nodes
    verts = []
    for node in nodeDict.values():
        verts.append([node.getX(), node.getY(), zScale*node.getZ()])
    # write all elements
    faces = []
    faceColor = []
    foundMatids = [0]
    matColor = [[0.5,0.5,0.5,1]]
    for eli in cellList:
        nodes = eli.getNodes()
        material = eli.getMaterial()
        faces.append([nodes[0].getID()-1, nodes[1].getID()-1, nodes[2].getID()-1]) # correct with (-1) because of sms convention
        if material not in foundMatids:
            foundMatids.append(material)
            matColor.append([rnd.random(),rnd.random(),rnd.random(),1])
        faceColor.append(matColor[foundMatids.index(material)])
    # plot that shit
    verts = np.array(verts)
    dx = 0.5*(verts[:,0].max()+verts[:,0].min())
    dy = 0.5*(verts[:,1].max()+verts[:,1].min())
    dz = 0.5*(verts[:,2].max()+verts[:,2].min())
    # create application
    app = QApplication([])
    # create widget with mesh
    view = GLWidget()
    view.setGeometry(0,0,app.desktop().width(),app.desktop().height())
    view.setWindowTitle('3D mesh view')
    view.setBackgroundColor([1,1,1])
    mesh = opengl.MeshData(vertexes=verts,faces=np.array(faces),faceColors=np.array(faceColor))
    meshItem = opengl.GLMeshItem(meshdata=mesh,drawEdges=True,edgeWidth=3,edgeColor=(0.1,0.1,0.1,1),smooth=False)
    data = meshItem.opts['meshdata']
    view.addItem(meshItem)
    view.pan(dx,dy,dz)
    view.show()
    # exit application
    sys.exit(app.exec_())


# actual script
if __name__ == "__main__":
    print ('look at your beautiful mesh ...')
    nArg = len(sys.argv)
    if nArg == 2 or nArg == 3:
        try:
            m = MESH2DM(sys.argv[1])
            nodeDict,cellList = m.getMesh()
        except:
            print ('error while loading and generating the mesh.')
            sys.exit(0)
        if nArg == 2:
            viewMesh(nodeDict,cellList)
        elif nArg == 3:
            viewMesh(nodeDict,cellList,float(sys.argv[2]))
    else:
        print ('input error: two arguments are allowed\n\t1. 2dm-file (mandatory)\n\t2. z-scale (optional)')
        sys.exit(0)