# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

import os
import sys
import platform

# common functions that are used in several scripts of this plugin
from ..tools import commonFunctions
from ..tools.meshConversion import MESHCONVERSION
from ..tools import nodeManipulation

# ui from external file
from ui_stringdef_widget import Ui_StringdefQT


class StringdefDialog (QDialog, Ui_StringdefQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
   
        # define plugin directory
        self.plugin_dir = QFileInfo(QgsApplication.qgisUserDbFilePath()).path() + "/python/plugins/BASEmesh"

        #populate combo boxes and read attribute tables
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                self.nodesComboBox.addItem( layer.name() )

        for layer in self.canvas.layers():
          if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Line:
            self.breaklinesComboBox.addItem( layer.name() )
        self.readAttributeSTRINGDEF()
        QObject.connect(self.breaklinesComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeSTRINGDEF)

        # connection of signal for definition of output file
        QObject.connect(self.browseButton, SIGNAL("clicked()"), self.defineOutputFile)
        self.connect(self.snappingToleranceCheckBox, SIGNAL("toggled(bool)"), self.onSnappingToleranceToggled)

    # ---------------------------
    # - definition of functions -
    # ---------------------------
    def defineOutputFile (self): # display file dialog for output file, return selected file path
        commonFunctions.defineOutput(self,self.outputShapeLineEdit,".txt", commonFunctions.getProjectName())
        pass

    def readAttributeSTRINGDEF (self): # as above
        self.stringdefComboBox.clear()
        commonFunctions.readAttribute (self.breaklinesComboBox, self.stringdefComboBox)
        pass

    def onSnappingToleranceToggled (self):
        self.snappingToleranceSpinBox.setReadOnly( not self.snappingToleranceCheckBox.isChecked() )


    # -------------------------------------------------------------
    # - accept - block: loaded, when "generate"-button is pressed -
    # -------------------------------------------------------------
    def accept (self):
        # do some initialization stuff
        self.textStatusBox.clear()
        self.progressBar.setValue(0)
        
        # check if output file is defined
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please specify output textfile.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            fileName, fileExtension = os.path.splitext(outputFile)
        # layer containing the nodes
        if self.nodesComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","pls define a shapefile containing the nodes of the mesh!")
            return
        else:
            nodesLayer = commonFunctions.readSelection (self.nodesComboBox)
        # layer containing the breaklines
        if self.breaklinesComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","pls define a shapefile containing the breaklines with stringdef field!")
            return
        else:
            breaklinesLayer = commonFunctions.readSelection (self.breaklinesComboBox)
        # column with stringdef names
        if self.stringdefComboBox.count() == 0 or self.stringdefComboBox.currentIndex() < 0:
            QMessageBox.warning(self.iface.mainWindows(), "Error", "pls define the column containing the stringdef field in the breakline shapefile!")
            return
        else:
            stringdefFieldName = self.stringdefComboBox.currentText()
        # initialize the progress bar
        self.textStatusBox.append ('\nInput files have been determined, finding stringdefs has started.')
        count = 0
        index = breaklinesLayer.fieldNameIndex(stringdefFieldName)
        for feat in breaklinesLayer.getFeatures():
            if feat.attributes()[index] != None:
                count += 1
        self.textStatusBox.append ('\n>Number of stringdefs found: %i' % count)
        self.progressBar.setRange(0, count)
        
        # find the stringdef nodes
        # first get the relative precision for snapping
        precisionExp = self.snappingToleranceSpinBox.value()
        result = nodeManipulation.extractStringdefNodes( nodesLayer, breaklinesLayer, stringdefFieldName, 10**precisionExp, self.progressBar )
        if type(result) in [type(str()), type(unicode())]:
            QMessageBox.warning( self.iface.mainWindow(), "Error", result )
            return
        else:
            self.textStatusBox.append ('\nAll stringdefs were successfully handled.')
            out = ''
            for name, nodeList in result.iteritems():
                out += '\n> %s: %i nodes' % (name, len(nodeList))
            self.textStatusBox.append(out)
            success = self.writeBlocks(outputFile, result)
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(),  "Error", success )
            else:
                self.textStatusBox.append ('\nResult written to file %s.'%outputFile)
        return

    # ---------------------------
    # - writing the results to the ascii-file -
    # ---------------------------
    def writeBlocks (self, filename, resultDict):
        try:
            id = open(filename, 'w')
            for name, nodeList in resultDict.iteritems():
                id.write('STRINGDEF {\n\tname = %s\n\tnode_ids = ('%name)
                for ii in range(0, len(nodeList)):
                    id.write("%i"%nodeList[ii])
                    id.write(" " if (ii <= len(nodeList)-2) else "")
                id.write(')\n\tupstream_direction = right\n}\n')
            id.close()
            return True
        except:
            return "Could not open the file '%s'" % filename


