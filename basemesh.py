# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import QAction, QIcon
from qgis.core import *

"""needed if help is displayed as html file"""
## import webbrowser, os

# Import the specific modules for each task;
from elevation.elevation_dialog import ElevMeshDialog
from quality.quality_dialog import QualMeshDialog
from interpolation.interpolation_dialog import InterpolDialog
from exportmesh.exportmesh_dialog import ExportMeshDialog
from basement_about_dialog import BasementAboutDialog
from renumber.renumber_dialog import RenumberMeshDialog
from stringdef.stringdef_dialog import StringdefDialog
from importmesh.importmesh_dialog import ImportMeshDialog
from checkMesh.checkMesh_dialog import CheckMeshDialog

# Import the function for the plugin directory name
import tools.commonFunctions
import os.path

# Initialize Qt resources from file resources.py
import resources

class BASEmesh:
    """QGIS interface implementation of the basement plugin.
    This class acts as glue between QGIS and the different steps/scripts
    required for mesh creation. A toolbar with different icons and
    connection to the sub-modules is created.
    """

    def __init__(self, iface):
        """Class constructor: on instantiation, the plugin instance will be
        assigned a copy of the QGIS interface (iface) object which allows this
        plugin to access and manipulate the running QGIS instance.
        """
        self.iface = iface

    def initGui(self):
        """Gui initialisation procedure (for QGIS plugin api).
        This method is called by QGIS and is used to set up all graphical user
        interface elements that appear in QGIS by default (before the user
        performs any explicit action with the plugin)."""

        # plugin installation directory
        self.pluginDir = tools.commonFunctions.getPluginDir()

        # create basement toolbar
        self.toolBar = self.iface.addToolBar("BASEmesh")
        self.toolBar.setObjectName("BASEmesh")

        # add entry to plugins menu
        self.menu = self.iface.pluginMenu().addMenu(QIcon(os.path.join(self.pluginDir,"icons/basement.png")), "BASEmesh")

        """ icons in normal plugin toolbar and menu entries"""
        #------------------------------------
        # quality meshing tool
        #------------------------------------
        self.qualmesh = QAction(QIcon(os.path.join(self.pluginDir,"icons/qual_meshing.svg")), u"Quality meshing", self.iface.mainWindow())
        self.qualmesh.setStatusTip("Meshing using quality parameters")
        self.toolBar.addAction(self.qualmesh)
        self.menu.addAction(self.qualmesh)
        QObject.connect(self.qualmesh, SIGNAL("triggered()"), self.run_qual_mesh) # connection of signal to slot

        #--------------------------------------
        # elevation meshing tool
        #--------------------------------------
        self.elevmesh = QAction(QIcon(os.path.join(self.pluginDir,"icons/elev_meshing.svg")), u"Elevation meshing", self.iface.mainWindow()) # this string is used in the button's tooltip
        self.elevmesh.setStatusTip("Meshing of elevation data") # this string is shown in the status area at the bottom of the QGIS main window
        self.toolBar.addAction(self.elevmesh) #creates icon in basement toolbar
        self.menu.addAction(self.elevmesh)
        QObject.connect(self.elevmesh, SIGNAL("triggered()"), self.run_elev_mesh) # connection of signal to slot

        #----------------------------------
        # interpolation tool
        #----------------------------------
        self.interpol = QAction(QIcon(os.path.join(self.pluginDir,"icons/interpolation.svg")), u"Interpolation", self.iface.mainWindow())
        self.interpol.setStatusTip("Interpolation of elevation data to mesh")
        self.toolBar.addAction(self.interpol)
        self.menu.addAction(self.interpol)
        QObject.connect(self.interpol, SIGNAL("triggered()"), self.run_interpolation) # connection of signal to slot

        #--------------------------------------
        # import mesh tool
        #--------------------------------------
        self.importmesh = QAction(QIcon(os.path.join(self.pluginDir,"icons/importmesh.svg")), u"Import mesh", self.iface.mainWindow())
        self.importmesh.setStatusTip("Import Mesh")
        self.toolBar.addAction(self.importmesh)
        self.menu.addAction(self.importmesh)
        QObject.connect(self.importmesh, SIGNAL("triggered()"), self.run_importmesh) # connection of signal to slot

        #--------------------------------------
        # renumbering tool
        #--------------------------------------
        self.renumber = QAction(QIcon(os.path.join(self.pluginDir,"icons/renumbering.svg")), u"Renumber mesh", self.iface.mainWindow())
        self.renumber.setStatusTip("Renumber mesh")
        self.toolBar.addAction(self.renumber)
        self.menu.addAction(self.renumber)
        QObject.connect(self.renumber, SIGNAL("triggered()"), self.run_renumber) # connection of signal to slot

        #--------------------------------------
        # check mesh tool
        #--------------------------------------
        self.checkmesh = QAction(QIcon(os.path.join(self.pluginDir,"icons/showmesh.svg")), u"3D view", self.iface.mainWindow())
        self.checkmesh.setStatusTip("3-dimensional plot of shape meshes")
        self.toolBar.addAction(self.checkmesh)
        self.menu.addAction(self.checkmesh)
        QObject.connect(self.checkmesh, SIGNAL("triggered()"), self.run_checkmesh) # connection of signal to slot

        #--------------------------------------
        # stringdef tool
        #--------------------------------------
        self.stringdef = QAction(QIcon(os.path.join(self.pluginDir,"icons/stringdef.svg")), u"Stringdef", self.iface.mainWindow())
        self.stringdef.setStatusTip("Stringdef")
        self.toolBar.addAction(self.stringdef)
        self.menu.addAction(self.stringdef)
        QObject.connect(self.stringdef, SIGNAL("triggered()"), self.run_stringdef) # connection of signal to slot

        #--------------------------------------
        # export mesh tool
        #--------------------------------------
        self.exportmesh = QAction(QIcon(os.path.join(self.pluginDir,"icons/exportmesh.svg")), u"Export mesh", self.iface.mainWindow())
        self.exportmesh.setStatusTip("Export of shape meshes")
        self.toolBar.addAction(self.exportmesh)
        self.menu.addAction(self.exportmesh)
        QObject.connect(self.exportmesh, SIGNAL("triggered()"), self.run_exportmesh) # connection of signal to slot

        #--------------------------
        # about gui elements
        #--------------------------
        self.about = QAction(QIcon(os.path.join(self.pluginDir,"icons/basement.png")), u"About BASEMENT", self.iface.mainWindow())
        QObject.connect(self.about, SIGNAL("triggered()"), self.run_about) # connection of signal to slot
        self.menu.addAction(self.about)

    def unload(self):
        """Gui breakdown procedure (for QGIS plugin api). Method removes
        any graphical user interface elements that were created by the plugin"""

        # Remove the plugin menu items and icons
        # self.iface.removePluginMenu("BASEMENT",self.elevmesh)
        self.menu.removeAction(self.elevmesh)
        self.iface.removeToolBarIcon(self.elevmesh)
        self.iface.unregisterMainWindowAction(self.elevmesh)

        # self.iface.removePluginMenu("BASEMENT",self.qualmesh)
        self.menu.removeAction(self.qualmesh)
        self.iface.removeToolBarIcon(self.qualmesh)
        self.iface.unregisterMainWindowAction(self.qualmesh)

        # self.iface.removePluginMenu("BASEMENT",self.interpol)
        self.menu.removeAction(self.interpol)
        self.iface.removeToolBarIcon(self.interpol)
        self.iface.unregisterMainWindowAction(self.interpol)

        # self.iface.removePluginMenu("BASEMENT",self.conv2dm)
        self.menu.removeAction(self.exportmesh)
        self.iface.removeToolBarIcon(self.exportmesh)
        self.iface.unregisterMainWindowAction(self.exportmesh)

        # self.iface.removePluginMenu("BASEMENT",self.renumber)
        self.menu.removeAction(self.renumber)
        self.iface.removeToolBarIcon(self.renumber)
        self.iface.unregisterMainWindowAction(self.renumber)

        # self.iface.removePluginMenu("BASEMENT",self.stringdef)
        self.menu.removeAction(self.stringdef)
        self.iface.removeToolBarIcon(self.stringdef)
        self.iface.unregisterMainWindowAction(self.stringdef)

        # self.iface.removePluginMenu("BASEMENT",self.checkmesh)
        self.menu.removeAction(self.checkmesh)
        self.iface.removeToolBarIcon(self.checkmesh)
        self.iface.unregisterMainWindowAction(self.checkmesh)

        # self.iface.removePluginMenu("BASEMENT",self.import2dm)
        self.menu.removeAction(self.importmesh)
        self.iface.removeToolBarIcon(self.importmesh)
        self.iface.unregisterMainWindowAction(self.importmesh)

        # self.iface.removePluginMenu("BASEMENT",self.aboutAction)
        self.menu.removeAction(self.about)
        self.iface.unregisterMainWindowAction(self.about)

        # remove BASEMENT-menu item & toolbar
        self.iface.pluginMenu().removeAction(self.menu.menuAction())
        del self.toolBar

        """definition of slots for sub-modules"""

    def run_elev_mesh(self):
        dlg = ElevMeshDialog(self.iface) #calls Dialog class in file 'elevation'
        dlg.exec_()
        return

    def run_qual_mesh(self):
        dlg = QualMeshDialog(self.iface) #calls Dialog class in file 'quality'
        dlg.exec_()
        return

    def run_interpolation(self):
        dlg = InterpolDialog(self.iface) #calls Dialog class in directory 'interpol'
        dlg.exec_()
        return

    def run_exportmesh(self):
        dlg = ExportMeshDialog(self.iface) #calls Dialog class in directory 'exportmesh'
        dlg.exec_()
        return

    def run_checkmesh(self):
        dlg = CheckMeshDialog(self.iface) #calls Dialog class in directory 'checkmesh'
        dlg.exec_()
        return

    def run_renumber(self):
        dlg = RenumberMeshDialog(self.iface) #calls Dialog class in directory 'renumber'
        dlg.exec_()
        return

    def run_stringdef(self):
        dlg = StringdefDialog(self.iface) #calls Dialog class in directory 'stringdef'
        dlg.exec_()
        return

    def run_importmesh(self):
        dlg = ImportMeshDialog(self.iface) #calls Dialog class in directory 'importmesh'
        dlg.exec_()
        return

    def run_about(self):
        dlg = BasementAboutDialog(self.iface)
        dlg.exec_()
        return
