# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

# ui from external file
from ui_exportmesh_widget import Ui_exportmeshQT

# common functions that are used in several scripts of this plugin
from ..tools import commonFunctions
from ..tools.meshConversion import MESHCONVERSION
from ..tools import crossSectionConversion


class ExportMeshDialog (QDialog, Ui_exportmeshQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
        self.mWindow = iface.mainWindow()

        QObject.connect(self.meshComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeMATID)
        QObject.connect(self.nodesComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeELEVATION)

        # connecttion of signal for the defintion of export method
        QObject.connect( self.radioButton2D, SIGNAL("toggled(bool)"), self.radioButton2DToggled )
        self.radioButton2DToggled(True)

        # connection of signal for definition of output file
        QObject.connect(self.browseButton, SIGNAL("clicked()"), self.defineOutputFile)

    # ---------------------------
    # - definition of functions -
    # ---------------------------

    def defineOutputFile (self): # display file dialog for output file, return selected file path
        ext = ""
        if self.radioButton1D.isChecked():
            if self.formatComboBox.currentText().find(".bmg") >= 0:
                ext = ".bmg"
            else:
                ext = ".g01"
        else:
            ext = ".2dm"
        commonFunctions.defineOutput(self,self.outputShapeLineEdit,ext, commonFunctions.getProjectName())
        pass

    def readAttributeMATID (self): # this is necessary as QObject doesn't allow to pass arguments to functions
        self.matidComboBox.clear()
        commonFunctions.readAttribute (self.meshComboBox, self.matidComboBox)
        pass

    def readAttributeELEVATION (self): # as above
        self.elevComboBox.clear()
        commonFunctions.readAttribute (self.nodesComboBox, self.elevComboBox)
        pass

    def radioButton2DToggled (self, bool):
        if bool:
            # 2D export
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Polygon:
                    self.meshComboBox.addItem( layer.name() )
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                    self.nodesComboBox.addItem( layer.name() )
            self.formatComboBox.clear()
            self.crossSectionsComboBox.clear()
        else:
            # 1D export
            self.meshComboBox.clear()
            self.nodesComboBox.clear()
            for layer in self.canvas.layers():
                if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Line:
                    self.crossSectionsComboBox.addItem( layer.name() )
            self.formatComboBox.addItem("BASEchain (*.bmg)")
            self.formatComboBox.addItem("HEC-RAS (*.g)")

    # -------------------------------------------------------------
    # - accept - block: loaded, when "Convert"-button is pressed -
    # -------------------------------------------------------------
    def accept (self):
        # check if output file is defined
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please specify output file.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            fileName, fileExtension = os.path.splitext(outputFile)
        # layer containing the cells
        if self.radioButton2D.isChecked():
            if self.meshComboBox.count()==0:
                QMessageBox.warning(self.iface.mainWindow(), "Error","Please define a polygon shapefile containing the mesh elements.")
                return
            else:
                meshLayer = commonFunctions.readSelection (self.meshComboBox)
                matidField = self.matidComboBox.currentText()
            # layer containing the nodes
            if self.nodesComboBox.count()==0:
                QMessageBox.warning(self.iface.mainWindow(), "Error","Please define a point shapefile containing the mesh nodes.")
                return
            else:
                nodesLayer = commonFunctions.readSelection (self.nodesComboBox)
                elevationField = self.elevComboBox.currentText()
            # initialize the progress bar
            totalFeatures = commonFunctions.featureCount(meshLayer) + commonFunctions.featureCount(nodesLayer)
            self.iface.messageBar().pushMessage("Conversion", "Number of features to be converted: %d" % totalFeatures, QgsMessageBar.INFO, 3)
            self.convProgressBar.setRange(0, totalFeatures*2.0) # because reading and writing
            self.convProgressBar.setValue(0)
            # create mesh from shape files
            meshConv = MESHCONVERSION()
            success = meshConv.readShape( nodesLayer, elevationField, meshLayer, matidField, self.convProgressBar )
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            # write mesh to 2dm-file
            success = meshConv.write2dm(fileName, self.convProgressBar)
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                iface.messageBar().pushMessage("Conversion", "Conversion to .2dm-file finished succesfully.", QgsMessageBar.INFO, 10)
        else:
            if self.crossSectionsComboBox.count() == 0:
                QMessageBox.warning(self.iface.mainWindow(), "Error", "pls define a shapefile containing the cross-sections of the 1D mesh!")
                return
            else:
                meshLayer = commonFunctions.readSelection(self.crossSectionsComboBox)
            totalFeatures = commonFunctions.featureCount(meshLayer)
            self.iface.messageBar().pushMessage("Output", "Number of cross-sections to be written: %d" % totalFeatures, QgsMessageBar.INFO, 3)
            self.convProgressBar.setRange(0, totalFeatures)
            self.convProgressBar.setValue(0)
            #create mesh from shape file
            crossConv = crossSectionConversion.CROSSSECTIONCONVERSION()
            text = []
            success = crossConv.readShape( meshLayer, text, self.convProgressBar )
            text_string = "CONVERSION WARNINGS:\r\n"
            text_string += "--------------------\r\n"
            for line in text:
                text_string += line + "\r\n"
            if success != True:
                elf.textEdit1Dexport.append(text_string)
                QMessageBox.warning( self.iface.mainWindow(), "Error", "dkdkd %s " % success )
                return
            else:
                self.textEdit1Dexport.append(text_string)
            bmgFormat = True
            if self.formatComboBox.currentText().find("HEC-RAS") >= 0:
                bmgFormat = False
            success = False
            if bmgFormat:
                success = crossConv.writeBMG(fileName, self.convProgressBar)
            else:
                success = crossConv.writeHECRAS(fileName, self.convProgressBar)
            if success != True:
                QMessageBox.warning( self.iface.mainWindow(), "Error", success )
                return
            else:
                iface.messageBar().pushMessage("Export", "Conversion to 1D file finished succesfully.", QgsMessageBar.INFO, 10)
