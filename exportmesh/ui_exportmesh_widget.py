# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'exportmesh/ui_exportmesh_widget.ui'
#
# Created: Fri Mar 11 11:06:35 2016
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_exportmeshQT(object):
    def setupUi(self, exportmeshQT):
        exportmeshQT.setObjectName(_fromUtf8("exportmeshQT"))
        exportmeshQT.resize(570, 575)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(exportmeshQT.sizePolicy().hasHeightForWidth())
        exportmeshQT.setSizePolicy(sizePolicy)
        exportmeshQT.setMinimumSize(QtCore.QSize(570, 575))
        exportmeshQT.setMaximumSize(QtCore.QSize(16777215, 575))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/BASEmesh/icons/exportmesh.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        exportmeshQT.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(exportmeshQT)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.tabWidget = QtGui.QTabWidget(exportmeshQT)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setMinimumSize(QtCore.QSize(500, 0))
        self.tabWidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.tab_3)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.radioButton2D = QtGui.QRadioButton(self.tab_3)
        self.radioButton2D.setChecked(True)
        self.radioButton2D.setObjectName(_fromUtf8("radioButton2D"))
        self.verticalLayout_5.addWidget(self.radioButton2D)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.labelQualmesh_2 = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelQualmesh_2.sizePolicy().hasHeightForWidth())
        self.labelQualmesh_2.setSizePolicy(sizePolicy)
        self.labelQualmesh_2.setMinimumSize(QtCore.QSize(140, 0))
        self.labelQualmesh_2.setObjectName(_fromUtf8("labelQualmesh_2"))
        self.horizontalLayout_8.addWidget(self.labelQualmesh_2)
        self.meshComboBox = QtGui.QComboBox(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.meshComboBox.sizePolicy().hasHeightForWidth())
        self.meshComboBox.setSizePolicy(sizePolicy)
        self.meshComboBox.setMinimumSize(QtCore.QSize(300, 0))
        self.meshComboBox.setAcceptDrops(False)
        self.meshComboBox.setObjectName(_fromUtf8("meshComboBox"))
        self.horizontalLayout_8.addWidget(self.meshComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_9.addItem(spacerItem)
        self.labelMatid_2 = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelMatid_2.sizePolicy().hasHeightForWidth())
        self.labelMatid_2.setSizePolicy(sizePolicy)
        self.labelMatid_2.setMinimumSize(QtCore.QSize(140, 0))
        self.labelMatid_2.setObjectName(_fromUtf8("labelMatid_2"))
        self.horizontalLayout_9.addWidget(self.labelMatid_2)
        self.matidComboBox = QtGui.QComboBox(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.matidComboBox.sizePolicy().hasHeightForWidth())
        self.matidComboBox.setSizePolicy(sizePolicy)
        self.matidComboBox.setMinimumSize(QtCore.QSize(250, 0))
        self.matidComboBox.setAcceptDrops(False)
        self.matidComboBox.setObjectName(_fromUtf8("matidComboBox"))
        self.horizontalLayout_9.addWidget(self.matidComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.labelNodes_2 = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelNodes_2.sizePolicy().hasHeightForWidth())
        self.labelNodes_2.setSizePolicy(sizePolicy)
        self.labelNodes_2.setMinimumSize(QtCore.QSize(140, 0))
        self.labelNodes_2.setObjectName(_fromUtf8("labelNodes_2"))
        self.horizontalLayout_10.addWidget(self.labelNodes_2)
        self.nodesComboBox = QtGui.QComboBox(self.tab_3)
        self.nodesComboBox.setMinimumSize(QtCore.QSize(290, 0))
        self.nodesComboBox.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.nodesComboBox.setAcceptDrops(False)
        self.nodesComboBox.setObjectName(_fromUtf8("nodesComboBox"))
        self.horizontalLayout_10.addWidget(self.nodesComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_11.addItem(spacerItem1)
        self.labelElevattr_2 = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelElevattr_2.sizePolicy().hasHeightForWidth())
        self.labelElevattr_2.setSizePolicy(sizePolicy)
        self.labelElevattr_2.setMinimumSize(QtCore.QSize(140, 0))
        self.labelElevattr_2.setObjectName(_fromUtf8("labelElevattr_2"))
        self.horizontalLayout_11.addWidget(self.labelElevattr_2)
        self.elevComboBox = QtGui.QComboBox(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.elevComboBox.sizePolicy().hasHeightForWidth())
        self.elevComboBox.setSizePolicy(sizePolicy)
        self.elevComboBox.setMinimumSize(QtCore.QSize(250, 0))
        self.elevComboBox.setAcceptDrops(False)
        self.elevComboBox.setObjectName(_fromUtf8("elevComboBox"))
        self.horizontalLayout_11.addWidget(self.elevComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_11)
        self.line = QtGui.QFrame(self.tab_3)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout_5.addWidget(self.line)
        self.radioButton1D = QtGui.QRadioButton(self.tab_3)
        self.radioButton1D.setMaximumSize(QtCore.QSize(16777215, 16777212))
        self.radioButton1D.setChecked(False)
        self.radioButton1D.setObjectName(_fromUtf8("radioButton1D"))
        self.verticalLayout_5.addWidget(self.radioButton1D)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.textEdit1Dexport = QtGui.QTextEdit(self.tab_3)
        self.textEdit1Dexport.setAutoFillBackground(True)
        self.textEdit1Dexport.setReadOnly(True)
        self.textEdit1Dexport.setObjectName(_fromUtf8("textEdit1Dexport"))
        self.horizontalLayout_5.addWidget(self.textEdit1Dexport)
        self.verticalLayout_5.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.labelFormat = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelFormat.sizePolicy().hasHeightForWidth())
        self.labelFormat.setSizePolicy(sizePolicy)
        self.labelFormat.setMinimumSize(QtCore.QSize(140, 0))
        self.labelFormat.setObjectName(_fromUtf8("labelFormat"))
        self.horizontalLayout_4.addWidget(self.labelFormat)
        self.formatComboBox = QtGui.QComboBox(self.tab_3)
        self.formatComboBox.setMinimumSize(QtCore.QSize(290, 0))
        self.formatComboBox.setObjectName(_fromUtf8("formatComboBox"))
        self.horizontalLayout_4.addWidget(self.formatComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.labelCrossSections = QtGui.QLabel(self.tab_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelCrossSections.sizePolicy().hasHeightForWidth())
        self.labelCrossSections.setSizePolicy(sizePolicy)
        self.labelCrossSections.setMinimumSize(QtCore.QSize(140, 0))
        self.labelCrossSections.setObjectName(_fromUtf8("labelCrossSections"))
        self.horizontalLayout_3.addWidget(self.labelCrossSections)
        self.crossSectionsComboBox = QtGui.QComboBox(self.tab_3)
        self.crossSectionsComboBox.setMinimumSize(QtCore.QSize(290, 0))
        self.crossSectionsComboBox.setObjectName(_fromUtf8("crossSectionsComboBox"))
        self.horizontalLayout_3.addWidget(self.crossSectionsComboBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.verticalLayout_7 = QtGui.QVBoxLayout(self.tab_4)
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.textBrowser_2 = QtGui.QTextBrowser(self.tab_4)
        self.textBrowser_2.setObjectName(_fromUtf8("textBrowser_2"))
        self.verticalLayout_7.addWidget(self.textBrowser_2)
        self.tabWidget.addTab(self.tab_4, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)
        self.groupBox_3 = QtGui.QGroupBox(exportmeshQT)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy)
        self.groupBox_3.setMinimumSize(QtCore.QSize(500, 0))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.outputShapeLineEdit = QtGui.QLineEdit(self.groupBox_3)
        self.outputShapeLineEdit.setObjectName(_fromUtf8("outputShapeLineEdit"))
        self.horizontalLayout.addWidget(self.outputShapeLineEdit)
        self.browseButton = QtGui.QPushButton(self.groupBox_3)
        self.browseButton.setObjectName(_fromUtf8("browseButton"))
        self.horizontalLayout.addWidget(self.browseButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.convProgressBar = QtGui.QProgressBar(self.groupBox_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.convProgressBar.sizePolicy().hasHeightForWidth())
        self.convProgressBar.setSizePolicy(sizePolicy)
        self.convProgressBar.setMinimumSize(QtCore.QSize(235, 0))
        self.convProgressBar.setProperty("value", 0)
        self.convProgressBar.setObjectName(_fromUtf8("convProgressBar"))
        self.horizontalLayout_2.addWidget(self.convProgressBar)
        self.closeButton = QtGui.QPushButton(self.groupBox_3)
        self.closeButton.setMinimumSize(QtCore.QSize(91, 23))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout_2.addWidget(self.closeButton)
        self.exportButton = QtGui.QPushButton(self.groupBox_3)
        self.exportButton.setMinimumSize(QtCore.QSize(91, 0))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.exportButton.setFont(font)
        self.exportButton.setAutoDefault(True)
        self.exportButton.setObjectName(_fromUtf8("exportButton"))
        self.horizontalLayout_2.addWidget(self.exportButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addWidget(self.groupBox_3)

        self.retranslateUi(exportmeshQT)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), exportmeshQT.reject)
        QtCore.QObject.connect(self.exportButton, QtCore.SIGNAL(_fromUtf8("clicked()")), exportmeshQT.accept)
        QtCore.QMetaObject.connectSlotsByName(exportmeshQT)

    def retranslateUi(self, exportmeshQT):
        exportmeshQT.setWindowTitle(QtGui.QApplication.translate("exportmeshQT", "BASEmesh - Export Mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton2D.setText(QtGui.QApplication.translate("exportmeshQT", "2D mesh export", None, QtGui.QApplication.UnicodeUTF8))
        self.labelQualmesh_2.setText(QtGui.QApplication.translate("exportmeshQT", "Mesh elements", None, QtGui.QApplication.UnicodeUTF8))
        self.labelMatid_2.setText(QtGui.QApplication.translate("exportmeshQT", "Material ID field", None, QtGui.QApplication.UnicodeUTF8))
        self.labelNodes_2.setText(QtGui.QApplication.translate("exportmeshQT", "Mesh nodes", None, QtGui.QApplication.UnicodeUTF8))
        self.labelElevattr_2.setText(QtGui.QApplication.translate("exportmeshQT", "Elevation field", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton1D.setText(QtGui.QApplication.translate("exportmeshQT", "1D mesh export", None, QtGui.QApplication.UnicodeUTF8))
        self.labelFormat.setText(QtGui.QApplication.translate("exportmeshQT", "Format", None, QtGui.QApplication.UnicodeUTF8))
        self.labelCrossSections.setText(QtGui.QApplication.translate("exportmeshQT", "Cross Sections", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QtGui.QApplication.translate("exportmeshQT", "INPUT MESH", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_2.setHtml(QtGui.QApplication.translate("exportmeshQT", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">PURPOSE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">In the meshing process using BASEmesh and QGIS, the mesh elements and corresponding mesh points are stored in separate shapefiles.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">For using these meshes as computational grid in BASEMENT, they have to be provided in 2dm-format (BASEplane, 2D) or in bmg-format (BASEchain, 1D). For this purpose, this tool uses the shape files and export them to a .2dm- or .bmg-file. If a computational mesh with elevations is needed, this tool should be used after the interpolation routine. </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">In some cases, a 2D-mesh might be enough: In this case, the result files of \'quality meshing\' can be used directly. Please note that all nodes of the quality meshing - output have a default elevation of 0. </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">USAGE FOR 2D MESHES:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; text-decoration: underline;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">1.</span><span style=\" font-size:8pt; color:#0000ff;\"> </span><span style=\" font-size:8pt;\">Two</span><span style=\" font-size:8pt; font-weight:600;\"> inputs layers </span><span style=\" font-size:8pt;\">have to be selected.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">The shapefiles needed have to be loaded into the QGIS-project before executing this tool. It is not possible to select files directly on the hard drive by browsing. Futhermore, the files used for meshing must be activated in the QGIS table of contents on the left side of the screen. Only data that is visible for the user can be used for meshing. </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">To prevent the selection of wrong shapetypes, the available fields are populated with the corresponding data type.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Mesh elements:</span><span style=\" font-size:8pt;\"> polygon layer containing mesh elements as polygons with their element ID and three adjacent nodes stored in the attribute table. Normally, this file has been created using the quality meshing tool.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">After selecting the correct file, the attribute field storing the material indices can be selected in the \'</span><span style=\" font-size:8pt; font-weight:600;\">material ID attribute field\'</span><span style=\" font-size:8pt;\"> below.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Mesh nodes:</span><span style=\" font-size:8pt;\"> point layer containing mesh nodes as points with their node ID and XYZ-coordinates as attributes. Normally, this file has been created using the quality meshing tool. After selecting the correct file, the attribute field storing elevation information can be selected in the box \'</span><span style=\" font-size:8pt; font-weight:600;\">elevation attribute field\'</span><span style=\" font-size:8pt;\"> below.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">2.</span><span style=\" font-size:8pt;\"> After selecting the output file name and location, the conversion process is started by clicking on \'</span><span style=\" font-size:8pt; font-weight:600;\">Export mesh</span><span style=\" font-size:8pt;\">\'. A progress bar informs about the current status of the process. Until completion, no status messages are displayed.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">ADVICE:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">The conversion process can be time-consuming and last for several minutes. Please be patient.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; text-decoration: underline; color:#da9100;\">USAGE FOR 1D MESHES:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; text-decoration: underline;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">1.</span><span style=\" font-size:8pt; color:#0000ff;\"> </span><span style=\" font-size:8pt;\">Select the desired output format. The mesh geometry geometry can be exported as BASEchain file (*.bmg) or HEC-RAS geometry file (*.g)</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; color:#0000ff;\">2.</span><span style=\" font-size:8pt; color:#0000ff;\"> </span><span style=\" font-size:8pt;\">Select the cross section-lines file which was generated at the import of a 1D mesh geometry file via BASEmesh.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">After selecting the output file name and location, the conversion process is started by clicking on \'</span><span style=\" font-size:8pt; font-weight:600;\">Export mesh</span><span style=\" font-size:8pt;\">\'. A progress bar informs about the current status of the process. Until completion, no status messages are displayed.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), QtGui.QApplication.translate("exportmeshQT", "Help", None, QtGui.QApplication.UnicodeUTF8))
