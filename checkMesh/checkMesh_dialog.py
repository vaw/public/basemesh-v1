# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

# ui from external file
from ui_checkMesh_widget import Ui_checkMeshQT

# common functions that are used in several scripts of this plugin
from ..tools import commonFunctions
from ..tools.meshConversion import MESHCONVERSION


class CheckMeshDialog (QDialog, Ui_checkMeshQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
        self.mWindow = iface.mainWindow()

        # populate combo boxes
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Polygon:
                self.meshComboBox.addItem( layer.name() )
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
                self.nodesComboBox.addItem( layer.name() )

        self.readAttributeMATID()
        QObject.connect(self.meshComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeMATID)
        self.readAttributeELEVATION()
        QObject.connect(self.nodesComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeELEVATION)

        # elevation scaling
        validator = QDoubleValidator(0.0,100.0,2,None)
        self.zScaleEdit.setValidator(validator)

    # ---------------------------
    # - definition of functions -
    # ---------------------------

    def readAttributeMATID (self): # this is necessary as QObject doesn't allow to pass arguments to functions
        self.matidComboBox.clear()
        commonFunctions.readAttribute (self.meshComboBox, self.matidComboBox)
        pass

    def readAttributeELEVATION (self): # as above
        self.elevComboBox.clear()
        commonFunctions.readAttribute (self.nodesComboBox, self.elevComboBox)
        pass

    # -------------------------------------------------------------
    # - accept - block: loaded, when "Convert"-button is pressed -
    # -------------------------------------------------------------
    def accept (self):
        # layer containing the cells
        if self.meshComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please define a polygon shapefile containing the mesh elements.")
            return
        else:
            meshLayer = commonFunctions.readSelection (self.meshComboBox)
            matidField = self.matidComboBox.currentText()
        # layer containing the nodes
        if self.nodesComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please define a point shapefile containing the mesh nodes.")
            return
        else:
            nodesLayer = commonFunctions.readSelection (self.nodesComboBox)
            elevationField = self.elevComboBox.currentText()
        # initialize the progress bar
        totalFeatures = commonFunctions.featureCount(meshLayer) + commonFunctions.featureCount(nodesLayer)
        self.iface.messageBar().pushMessage("Conversion", "Number of features to be converted: %d" % totalFeatures, QgsMessageBar.INFO, 3)
        self.convProgressBar.setRange(0, totalFeatures) # because reading and writing
        self.convProgressBar.setValue(0)
        # create mesh from shape files
        meshConv = MESHCONVERSION()
        success = meshConv.readShape( nodesLayer, elevationField, meshLayer, matidField, self.convProgressBar )
        if success != True:
            QMessageBox.warning( self.iface.mainWindow(), "Error", success )
            return
        # show your mesh in an openGL window
        zScale = float(self.zScaleEdit.text())
        success = meshConv.writeOpenGL(zScale)
        if success != True:
            QMessageBox.warning( self.iface.mainWindow(), "Error", success )
            return
        else:
            iface.messageBar().pushMessage("Conversion", "Conversion to openGL window finished succesfully.", QgsMessageBar.INFO, 10)
        # closing the window
        self.close()