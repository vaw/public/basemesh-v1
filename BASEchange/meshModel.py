# by Samuel J. Peter, 2015

import sys, os, platform, subprocess, stat, re, abc, math, linecache, shutil
import numpy as np
from scipy.interpolate import RegularGridInterpolator
from scipy import interpolate
from scipy.sparse import csc_matrix, lil_matrix, tril, find
from scipy.sparse.csgraph import reverse_cuthill_mckee
try:
    from osgeo import gdal
    from osgeo.gdalconst import *
except ImportError:
    print "\n*** WARNING ***"
    print "module 'osgeo' not found! -> loading of geotiffs as DEM is not possible\n"
try:
    from PIL import Image
except ImportError:
    print "\n*** WARNING ***"
    print "module 'PIL' not found! -> loading of images as DEM is not possible\n"


class RASTER:

    def __init__ (self, file, dtype='double'):
        self.__data = None
        self.__ncols = 0
        self.__nrows = 0
        self.__xcellsize = 0.0
        self.__ycellsize = 0.0
        self.__xllcorner = 0.0
        self.__yllcorner = 0.0
        self.__method = 'linear'
        self.__fillValue = None
        self.__loadFile(file,dtype)

    def setInterpolation (self, method):
        self.__method = method

    def setFillValue (self, value):
        self.__fillValue = value

    def getInterpolator (self):
        # create the grid
        x = self.__xllcorner + self.__xcellsize*\
            np.linspace(0.0,self.__ncols-1,self.__ncols)
        y = self.__yllcorner + self.__ycellsize*\
            np.linspace(0.0,self.__nrows-1,self.__nrows)
        # create the interpolator
        rgi = RegularGridInterpolator(
            (x,y),
            np.transpose(np.flipud(self.__data)),
            method=self.__method,
            bounds_error=False,
            fill_value=self.__fillValue)
        return rgi

    def getBounds (self):
        return {'x':[
                    self.__xllcorner,
                    self.__xllcorner+self.__xcellsize*(self.__ncols-1)],
                'y':[
                    self.__yllcorner,
                    self.__yllcorner+self.__ycellsize*(self.__nrows-1)]}

    def __loadFile (self, file, dtype):
        if dtype=='half':
            dt = np.float16
        elif dtype=='single':
            dt = np.float32
        else:
            dt = np.float64
        base,ext = os.path.splitext(file)
        if ext == '.tif':
            try:
                print 'load geotiff raster ...'
                self.__loadGEOTIFF(file,dt)
            except:
                print 'error reading "%s" ...'%file
                return None
        elif ext == '.txt':
            try:
                print 'load ascii ...'
                self.__loadASCII(file,dt)
            except:
                print 'error reading "%s" ...'%file
                return None
        else:
            try:
                print 'load picture ...'%file
                self.__loadIMAGE(file,dt)
            except:
                print 'error reading "%s" ...'%file
                return None
        print 'loaded data: %f MB'%(sys.getsizeof(self.__data)/1024.0/1024.0)


    def __loadGEOTIFF (self, file, dt):
        base,ext = os.path.splitext(file)

        gdal.AllRegister()
        ds = gdal.Open(file, GA_ReadOnly)

        rows = ds.RasterYSize
        cols = ds.RasterXSize
        print ("raster has %i bands"%ds.RasterCount)
        band = ds.GetRasterBand(1)
        
        array = np.zeros((rows, cols)).astype(dtype=dt)
        blockSizes = band.GetBlockSize()
        xBlockSize = blockSizes[0]
        yBlockSize = blockSizes[1] 
        for i in range(0, rows, yBlockSize):
            if i + yBlockSize < rows:
                numRows = yBlockSize
            else:
                numRows = rows - i
            for j in range(0, cols, xBlockSize):
                if j + xBlockSize < cols:
                    numCols = xBlockSize
                else:
                    numCols = cols - j
                array[i:i+yBlockSize,j:j+xBlockSize] = band.ReadAsArray(j, i, numCols, numRows).astype(dtype=dt)
      
        [height, width] = array.shape
        self.__data = array
        self.__ncols = width
        self.__nrows = height
        
        if os.path.exists(base+'.tfw'):
            print 'geo reference from %s.tfw'%base
            geo = np.genfromtxt(base+'.tfw')
            self.__xcellsize = geo[0]
            self.__ycellsize = -1.0*geo[3]
            self.__xllcorner = geo[4]
            self.__yllcorner = geo[5] - (self.__nrows-1)*self.__ycellsize
        else:
            print 'geo refernce from tif meta data'
            geo = ds.GetGeoTransform()
            self.__xcellsize = geo[1]
            self.__ycellsize = -1.0*geo[5]
            self.__xllcorner = geo[0]
            self.__yllcorner = geo[3] - (self.__nrows-1)*self.__ycellsize
        array = None
        band = None
        ds = None

    def __loadASCII (self, file, dt):
        raster = {}
        for i in range(1,7):
            line = linecache.getline(file,i)
            raster[line.split()[0]] = float(line.split()[1])
        self.__ncols = raster['ncols']
        self.__nrows = raster['nrows']
        self.__xcellsize = raster['cellsize']
        self.__ycellsize = raster['cellsize']
        self.__xllcorner = raster['xllcorner']
        self.__yllcorner = raster['yllcorner']
        self.__data = np.genfromtxt(file, delimiter=' ',skip_header=6,dtype=dt)

    def __loadIMAGE (self, file, dt):
        # the orientation might be wrong here ...
        img = Image.open(file).convert('L')
        width,height = img.size
        self.__data = np.array(img,dtype=dt)
        self.__ncols = width
        self.__nrows = height
        self.__xllcorner = 0.0
        self.__yllcorner = 0.0
        self.__xcellsize = 1.0/width
        self.__ycellsize = 1.0/width



class MESHMODEL:
    __metaclass__ = abc.ABCMeta

    # global class variables
    area_default = 0
    material_default = 0

    def __init__(self,directory,absolutePrecision=1e-1,snappingTolerance=1e-3):
        self.__wd = directory.getDirectory()
        self.__coordDict = COORDINATEDICTIONARY(absolutePrecision,snappingTolerance)
        self.__meshNodes = COORDINATEDICTIONARY(absolutePrecision,snappingTolerance)
        self.__meshType = None # possible are: 'plane','chain','gpu'
        self.__segments = []
        self.__stringdefNodes = {}
        self.__wStringdefDist = False
        self.__holePoints = []
        self.__regionPoints = []
        self.__stringdefs = {}
        self.__elevFunc = None
        self.__elevMethod ={}
        self.__optionTags = "-qDpaA"
        self.__crossSections = []
        self.__offset = ()
        self.__friction = {}
        self.__reverseList = False
        self.__hasBed = False
        self.__reduction = False
        self.__volume = False
		
    def setOffset (self, x, z):
        self.__offset = (x,z)

    def setCSnamePrefix (self, prefix):
        self.__prefix = prefix

    def setReverseList (self, reverseList):
        self.__reverseList = reverseList

    def setHasBed (self, hasBed, bedStart):
        self.__hasBed = hasBed
        self.__bedStart = bedStart

    def setStringdefDistance (self, trueORfalse):
        self.__wStringdefDist = trueORfalse
    
    def initialization (self, filename, mtype='plane', cellCenter='average'):
        self.__fileID = filename
        if mtype in ['plane','chain','gpu']:
            self.__meshType = mtype
        else:
            raise Exception('unknown mesh type "%s"'%mtype)
        if cellCenter in ['average','outer','inner','mid']:
            self.__elevMethod[self.material_default] = cellCenter
        else:
            raise Exception('unknown cell center calculation method "%s"'%cellCenter)
        filePath,name = os.path.split(os.path.realpath(__file__))
        # create directory to store all temporary files
        self.__tempDir = os.path.join(filePath,'triangle_tempfiles')
        if not os.path.exists(self.__tempDir):
            try:
                os.mkdir(self.__tempDir)
            except:
                print ("Failed to create temporary directory")
                return
        self.__filePath = os.path.join(self.__tempDir, self.__fileID)
        # init stuff for 2d or 1d only
        if self.__meshType in ['plane','gpu']:
            # determine triangle name
            if os.name == 'nt':
                if "32bit" in platform.architecture():
                    self.__triangleName = "triangle_32.exe"
                else:
                    # so far only a 32bit executable is provided, but should run on 64bit machines as well ?!
                    self.__triangleName = "triangle_32.exe"
            elif os.name == 'posix':
                if platform.system() == 'Darwin':
                    if "64bit" in platform.architecture():
                        self.__triangleName = "triangle_macosx_intel_64"
                else:
                    if "32bit" in platform.architecture():
                        self.__triangleName = "triangle_linux_32"
                    else:
                        self.__triangleName = "triangle_linux_64"
            else:
                print ('Operating system could not be detected!')
                return
            # check if triangle executable is in the correct directory 
            self.__triangleLocation = os.path.join(filePath,'triangle/'+self.__triangleName)
            if not os.path.exists(self.__triangleLocation):
                print ("%s not found. Please copy the executable in the correct directory."%self.__triangleLocation)
                return
            # eventually change the executable's permission (only unix system)
            if os.name == 'posix':
                os.chmod(self.__triangleLocation, stat.S_IEXEC)

    def addVertices (self, nodeList):
        # store the NODE objects in dictionary with unique key
        for node in nodeList:
            self.__addVertex(node)

    def addLines (self, lineList, nSegments=None):
        if nSegments==None:
            nSegments = [None]*len(lineList)
        for line,nSegs in zip(lineList,nSegments):
            self.__addSingleLine(line,nSegs)

    def addLine (self, pointList, connectLastFirst=False, name=None):
        if connectLastFirst:
            pointList.append(pointList[0])
        self.__addSingleLine(pointList,None,name)

    def addHole (self, point):
        self.__holePoints.append( point )

    def addRegion (self, point, maxArea=None, matID=None):
        area = maxArea if maxArea else self.area_default
        material = matID if matID else self.material_default
        self.__regionPoints.append( [point.getX(), point.getY(), material, area] )

    def addCrossSection(self, point,orientation=0.0):
        self.__crossSections.append([point,orientation])

    def addStringdef (self, name, nodeList, singleEdge=False):
        self.__stringdefs[name] = (nodeList,singleEdge)

    def getStringdefNames (self):
        return self.__stringdefs.keys()

    def getStringdefNodeIDs (self, keyString):
        ids = []
        myList = []
        for name, nodeList in self.__stringdefNodes.iteritems():
            if name == keyString:
                ids = nodeList
        for i in ids:
            if isinstance(i, int):
                myList.append(str(i))
        return myList

    def addElevationMethod (self, matid, method):
        self.__elevMethod[matid] = method
        
    def setOption (self, option, value=False):
        if value:
            optval = '%s%d'%(option,value)
            if option not in self.__optionTags:
                self.__optionTags += optval
            else:
                self.__optionTags = re.sub(r'%s[0-9]*'%option, optval, self.__optionTags)
        else:
            if option not in self.__optionTags:
                self.__optionTags += option
    
    def setQuality (self, value):
        self.setOption('q',value)

    def setArea (self, value):
        self.setOption('a',value)

    def setFriction (self, frictionDict):
        if not frictionDict.has_key('default'):
            print 'error: friction dictionary must contain a key ''default'''
        else:
            self.__friction = frictionDict

    def setReduction (self, red=True):
        self.__reduction = red

    def setVolume (self, vol=True):
        self.__volume = vol

    def getVolume (self):
        return self.__volume

    def get2dmFilename(self):
        return self.__fileID+'.2dm'

    def getStringdefFilename(self):
        return os.path.join(self.__wd,self.__fileID+'_stringdefs.txt')

    def deleteFiles (self):
        try:
            os.remove(self.__filePath+'.poly')
            os.remove(self.__filePath+'.1.poly')
            os.remove(self.__filePath+'.1.node')
            os.remove(self.__filePath+'.1.ele')
        except:
            None

    def load2dm (self, file):
        try:
            print 'loading mesh file "%s" ...'%file
            m = MESH2DM(file)
            return m.getElevationInterpolator()
        except:
            print 'error reading "%s" ...'%file
            return None
    
    def createMesh (self, polyfile=None, parDict=None):
        if self.__meshType == 'chain':
            filename = self.__writeBMG()
        elif self.__meshType in ['plane','gpu']:
            if polyfile is None:
                print 'writing the input file for TRIANGLE ...'
                self.__writePolyfile()
            else:
                self.__setPolyfile(polyfile,parDict)
            print 'calling TRIANGLE ...'
            print 'with options: %s'%self.__optionTags
            subprocess.call( [self.__triangleLocation, self.__optionTags, self.__filePath+'.poly'] )
            print 'reading the TRIANGLE results ...'
            nodes, cells = self.__readTriangleResults()
            if self.__reduction:
                print 'do bandwidth reduction ...'
                cells = self.__runReduction(cells)
            if self.__volume:
                print 'calc the volume of pond ...'
                self.__volume = self.__calcVolume(cells)
            print 'writing the final mesh file (2dm) ...'
            filename = self.__write2dm( nodes, cells )
            if len(self.__stringdefs) > 0:
                print 'writing the stringdefs ...'
                self.__writeStringdef( nodes )
                if self.__wStringdefDist:
                    self.__writeStringdefDistance( nodes )
        print '*** succesfully written "%s" ***'%self.__fileID


    def __writeBMG (self):
        filename = os.path.join(self.__wd,self.__fileID+'.bmg')
        bmgFile = open(filename, 'w')
        for jj,cs in enumerate(self.__crossSections):
            # get the cross section coordinate points
            node,orientation = cs
            phi = orientation/180.0*math.pi
            ics = self.__findIntersections(node[0],node[1],-math.sin(phi),math.cos(phi))
            # get the strickler values
            for ii,ic in enumerate(ics):
                if ii==0:
                    tag1 = ic[2]
                else:
                    tag2 = ic[2]
                    key1 = '%s-%s'%(tag1,tag2)
                    key2 = '%s-%s'%(tag2,tag1)
                    if key1 in self.__friction.keys():
                        ks = self.__friction[key1]
                    elif key2 in self.__friction.keys():
                        ks = self.__friction[key2]
                    else:
                        ks = self.__friction['default']
                    tag1 = tag2
                    # find slice indices with same strickler values
                    if ii==1:
                        ksList = [ks]
                        indCont = 1
                        sliceInd = []
                    elif ksList[-1]!=ks:
                        sliceInd.append((indCont,ii-1))
                        ksList.append(ks)
                        indCont = ii
            sliceInd.append((indCont,ii))
            # write cross section
            bmgFile.write('CROSS_SECTION {\n')
            bmgFile.write('\tname = %s%i\n'%(self.__prefix,jj+1))
            bmgFile.write('\torientation_angle = 90\n')
            bmgFile.write('\tleft_point_global_coords = (%f,%f,%f)\n'%(node[0]+self.__offset[0],ics[0][0],ics[0][1]+self.__offset[1]))
            bmgFile.write('\tdistance_coord = %f\n'%((node[0]+self.__offset[0])/1000.0))
            bmgFile.write('\treference_height = %f\n'%self.__offset[1])
            bmgFile.write('\tnode_coords = (')
            for ii,ic in enumerate(ics):
                if ii!=0:
                    bmgFile.write(',')
                bmgFile.write('(%f,%f)'%(ic[0],ic[1]))
            bmgFile.write(')\n')
            bmgFile.write('\tfriction_coefficients = (')
            for ii,ks in enumerate(ksList):
                if ii!=0:
                    bmgFile.write(',')
                bmgFile.write('%f'%ks)
            bmgFile.write(')\n')
            bmgFile.write('\tfriction_slice_indexes = (')
            for ii,inds in enumerate(sliceInd):
                if ii!=0:
                    bmgFile.write(',')
                bmgFile.write('(%i,%i)'%(inds[0],inds[1]))
            bmgFile.write(')\n')
            if self.__hasBed:
                bmgFile.write('\tbottom_range = (%f,%f)\n'%(ics[self.__bedStart-1][0],ics[self.__bedStart][0]))
                bmgFile.write('\tSOIL_DEF {\n\t\tindex = 1\n\t\trange = (%f,%f)\n\t}\n'%(ics[self.__bedStart-1][0],ics[self.__bedStart][0]))
            bmgFile.write('}\n')
        # finished
        bmgFile.close()
        # write geometry block for bmc file
        bmcfilename = os.path.join(self.__wd,self.__fileID+'.bmc')
        bmcFile = open(bmcfilename,'w')
        bmcFile.write('PROJECT {\n')
        bmcFile.write('}\n')
        bmcFile.write('DOMAIN {\n')
        bmcFile.write('\tBASECHAIN_1D {\n')
        bmcFile.write('\t\tGEOMETRY {\n')
        bmcFile.write('\t\t\ttype = basement\n')
        bmcFile.write('\t\t\tfile = geometry.bmg\n')
        bmcFile.write('\t\t\tcross_section_order = (')
        if self.__reverseList:
            for ii,cs in reversed(list(enumerate(self.__crossSections))):
                bmcFile.write('%s%i'%(self.__prefix,ii+1))
                if ii!=0:
                    bmcFile.write(' ')
        else:
            for ii,cs in enumerate(self.__crossSections):
                if ii!=0:
                    bmcFile.write(' ')
                bmcFile.write('%s%i'%(self.__prefix,ii+1))
        bmcFile.write(')\n')
        bmcFile.write('\t\t}\n')
        bmcFile.write('\t}\n')
        bmcFile.write('}\n')
        bmcFile.close()
        return filename

    def __findIntersections (self, x, y, dx, dy):
        ics = []
        l = math.sqrt(dx**2+dy**2)
        dx = dx/l
        dy = dy/l
        for s in self.__segments:
            # check if parallel
            if abs(s.getOrientation()[0])!=dx or abs(s.getOrientation()[1])!=dy:
                # lets see if they intersect
                n = np.cross( [dx, dy, 0.0], [0.0, 0.0, 1.0] )
                n1 = s.getNodes()[0]
                n2 = s.getNodes()[1]
                d1 = n[0]*(n1.getX()-x)+n[1]*(n1.getY()-y)
                d2 = n[0]*(n2.getX()-x)+n[1]*(n2.getY()-y)
                if (d1<=0.0 and d2>=0.0) or (d2<=0.0 and d1>=0.0):
                    if d1==0.0:
                        # point is on segment
                        x = n1.getX()
                        y = n1.getY()
                    elif d2==0.0:
                        # point is on segment
                        x = n2.getX()
                        y = n2.getY()
                    else:
                        # intersection found
                        r = abs(d1)/(abs(d2)+abs(d1))
                        x = n1.getX()+r*(n2.getX()-n1.getX())
                        y = n1.getY()+r*(n2.getY()-n1.getY())
                    ics.append( [y, self.elevationFunction(x,y), s.getName()] )
            else:
                pass
        # filter out duplicate entries (nodes where multiple segments cross)
        icsUnique = []
        yList = []
        for ic in ics:
            isNew = True
            for icRef in icsUnique:
                if icRef[0]==ic[0]:
                    if icRef[2]==None:
                        icRef[2] = ic[2]
                    else:
                        isNew = False
            if isNew:
                icsUnique.append(ic)
        # sort by y-coord
        return sorted(icsUnique, key=lambda ic: ic[0])
    
    def __writePolyfile (self):
        filename = self.__filePath+'.poly'
        polyfile = open(filename, 'w')
        # write nodes given in a dictionary
        dimensions = 2
        points = self.__coordDict.getPoints()
        nPoints = len(points)
        polyfile.write("%d %d 1 0\n" %(nPoints, dimensions))
        # order with ascending ID
        tempList= [None]*nPoints
        for pt in points:
            tempList[pt.getID()] = pt
        for pt in tempList:
            polyfile.write( "%d %0.15f %0.15f %0.15f\n" %(pt.getID(), pt.getX(), pt.getY(), pt.getZ()) )
        # write segment given in list
        polyfile.write("%d 1\n" %len(self.__segments))
        for ii, seg in enumerate(self.__segments):
            nodes = seg.getNodes()
            polyfile.write( "%d %d %d\n" %(ii, nodes[0].getID(), nodes[1].getID()) )
        # no holes
        polyfile.write("%d\n" %len(self.__holePoints))
        for ii, point in enumerate(self.__holePoints):
            polyfile.write("%d %0.6f %0.6f\n" %(ii, point[0], point[1]))
        # region points
        polyfile.write("%d\n" %len(self.__regionPoints))
        for ii, point in enumerate(self.__regionPoints):
            polyfile.write("%d %0.6f %0.6f %d %0.6f\n" %(ii, point[0], point[1], point[2], point[3]))
        # finished
        polyfile.close()
        # check if everything worked
        if not os.path.exists(filename):
            print ("%s.poly not found.\nConversion not successfull - meshing canceled."%self.__filePath)
            return

    def __setPolyfile (self,polyfile,parDict):
        try:
            pname,ext = os.path.splitext(os.path.realpath(polyfile))
        except:
            print ("Failed to set the polyfile")
        if parDict:
            # make a copy of the original polyfile and replace key-value pairs
            cfile = pname+'_copy'
            shutil.copyfile(polyfile, cfile+'.poly')
            contents = open(cfile+'.poly','r').read()
            for key,value in parDict.iteritems():
                contents = contents.replace(key,str(value))
            f = open(cfile+'.poly','w')
            f.write(contents)
            f.close()
            pname = cfile
        self.__filePath = pname
        

    def __readTriangleResults (self):
        nodes = {}
        cells = []
        # parse node file
        try:
            nodeFile = open(self.__filePath+'.1.node', 'r')
        except:
            print  ('could not open the file ''%s.1.node''!' % self.__fileID)
            return
        line = nodeFile.readline().split()
        N = int(float(line[0]))
        stride = int(N/10)
        print 'reading nodes:'
        if int(float(line[2])) == 1:
            for ii in range(0,N):
                line = [float(l) for l in nodeFile.readline().split()]
                nodeID = int(line[0]+1) # increase node ID by 1, because TRIANGLE starts at 0, here we need 1 (SMS convention)
                x = line[1]
                y = line[2]
                # elevation stored at nodes?
                z = 0.0
                if self.__meshType=='plane':
                    z = self.elevationFunction(x,y)
                node = NODE(x,y,z)
                node.setID(nodeID)
                nodes[nodeID] = node
                if stride > 0 :
                    if ii % stride == 0 :
                        p = ii/stride*10
                        row = "="*p + ">"
                        sys.stdout.write("%s\r%d%%" %(row,p))
                        sys.stdout.flush()
            sys.stdout.write("\n")
        else:
            print ('error in parsing .ele file')
            return
        # parse element file
        try:
            eleFile = open(self.__filePath+'.1.ele', 'r')
        except:
            print ('could not open the file ''%s.1.ele''!' % self.__fileID)
            return
        line = eleFile.readline().split()
        N = int(float(line[0]))
        stride = int(N/10)
        print 'reading elements:'
        for ii in range(0,N):
            line = [int(float(l)) for l in eleFile.readline().split()]
            cell = CELL(nodes[line[1]+1],nodes[line[2]+1],nodes[line[3]+1]) # increase node ID by 1 (see above when adding a NODE)
            matid_loc = line[4] if len(line) == 5 else self.material_default #read matID from region (or default)
            matid = self.MaterialIDFunction(matid_loc,cell.getCenter()[0],cell.getCenter()[1]) #overwrite matID by function (if defined)
            cell.setMaterial(matid)
            if matid in self.__elevMethod.keys():
                centerMethod = self.__elevMethod[matid]
            else:
                centerMethod = self.__elevMethod[self.material_default]
            cell.calcProperties(center=centerMethod)
            if self.__meshType=='gpu':
                z = self.elevationFunction( cell.getCenter()[0], cell.getCenter()[1] ) #[0]
                cell.setElevation( z )
            cells.append(cell)
            if stride > 0 :
                if ii % stride == 0:
                    p = ii/stride*10
                    row = "="*p + ">"
                    sys.stdout.write("%s\r%d%%" %(row,p))
                    sys.stdout.flush()
        sys.stdout.write("\n")
        return (nodes,cells)

    def __runReduction (self, cells):
        e2t = self.__calcEdge2Cell(cells)
        ne = e2t.shape[0]
        nt = len(cells)
        # create sparse matrix
        A = csc_matrix((np.ones((ne),dtype=np.uint32), (e2t[:,0],e2t[:,1])), shape=(nt,nt))
        # finally do the redcution
        Ai = reverse_cuthill_mckee(A)
        # and accordingly reorder the cell list
        cellsRED = []
        for ai in Ai:
            cellsRED.append( cells[ai] )
        #for now...
        #cellsRED = cells
        return cellsRED

    def __calcEdge2Cell (self,cells):
        nt = len(cells) # number of triangles
        # node connectivity
        tri = np.zeros((nt,3),dtype=np.uint32)
        for ii,ci in enumerate(cells):
            tri[ii,:] = ci.getNodeIDs()
        # edges - not unique
        e = np.vstack((tri[:,[0,1]],tri[:,[1,2]],tri[:,[2,0]]))
        # find unique edges
        e = np.sort(e,axis=1)
        b = np.ascontiguousarray(e).view(np.dtype((np.void, e.dtype.itemsize * e.shape[1])))
        _,ej,bj = np.unique(b, return_index=True, return_inverse=True)
        e = e[ej]
        # unique edges in each triangle
        ids = np.array(range(nt))
        te = np.vstack((bj[ids],bj[ids+nt],bj[ids+2*nt])).transpose()
        # edge-to-triangle connectivity
        # each row has two entries corresponding to the cell IDs associated with the edge.
        # boundary edges have e2t[i,1] = 0
        ne = e.shape[0]
        e2t = -np.ones((ne,2),dtype=np.int64)
        for ti in range(nt):
            ii = 0
            while ii<3:
                ce = te[ti,ii]
                if e2t[ce,0]==-1:
                    e2t[ce,0] = ti
                else:
                    e2t[ce,1] = ti
                ii += 1
        for ei in e2t:
            if ei[1]==-1:
                ei[1] = ei[0]
        return e2t

    def __calcVolume (self, cellList):
        V = 0.0
        for ci in cellList:
            V += ci.calcArea()*ci.getElevation()
        return abs(V)

    def __write2dm (self, nodeDict, cellList):
        # writes the mesh into a .2dm-file
        filename = os.path.join(self.__wd,self.__fileID+'.2dm')
        file2dm = open(filename,'w')
        try:
            file2dm.write("MESH2D   #created automatically via meshModel tool\n")
            # write all elements
            for ii,ci in enumerate(cellList):
                nds = ci.getNodes()
                if self.__meshType=='gpu':
                    file2dm.write( "E3T %7d %7d %7d %7d %7d %7.3f\n" % 
                        (ii+1,nds[0].getID(),nds[1].getID(),nds[2].getID(),ci.getMaterial(),ci.getCenter()[2]) )
                else:
                    file2dm.write( "E3T %7d %7d %7d %7d %7d\n" % 
                        (ii+1,nds[0].getID(),nds[1].getID(),nds[2].getID(),ci.getMaterial()) )
            # write all nodes
            for key,ni in nodeDict.iteritems():
                file2dm.write( "ND %7d %9f %9f %9f\n" % 
                    (key, ni.getX(),ni.getY(),ni.getZ()) )
        finally:
            file2dm.close()
        if not os.path.exists(filename):
            print ("2dm-file could not be created.")

    def __writeStringdef (self, nodeDict):
        # create a dictionary with all meshing nodes
        for ID,node in nodeDict.iteritems():
            self.__meshNodes.storeInDict(node,ID)
        # loop over all stringdef and find the generated nodes on the segments
        print ("looking for %i stringdefss"%len(self.__stringdefs))
        count = 0
        for name,definition in self.__stringdefs.iteritems():
            nodeList,singleEdge = definition
            nodesOfSegments = []
            for nn in nodeList:
                dictNode = self.__meshNodes.getNodeFromDict( nn.getX(), nn.getY() )
                if dictNode!=None:
                    nodesOfSegments.append(dictNode)
            # look for other nodes that are lying on the segment          
            if len(nodesOfSegments)<2:
                print ("stringdef '%s': no nodes found in mesh!"%name)
                return 
            else:
                # very first node
                nodeIDsOnBreakline = [nodesOfSegments[0].getID()]
                # loop over all segments
                for ii in range(0, len(nodesOfSegments)-1):
                    node1 = nodesOfSegments[ii]
                    node2 = nodesOfSegments[ii+1]
                    nodesOnSegment = self.__meshNodes.getNodesOnSegment(node1, node2)
                    # add node IDs to the list, sorted!
                    x = node1.getX()
                    y = node1.getY()
                    while len(nodesOnSegment)>0:
                        di = [math.sqrt(math.pow(ni.getX()-x, 2)+math.pow(ni.getY()-y, 2)) for ni in nodesOnSegment]
                        ind = di.index(min(di))
                        nodeIDsOnBreakline.append(nodesOnSegment[ind].getID())
                        x = nodesOnSegment[ind].getX()
                        y = nodesOnSegment[ind].getY()
                        nodesOnSegment.pop(ind)
                    # last node of each segment
                    nodeIDsOnBreakline.append(node2.getID())
                if singleEdge:
                    # sometimes only ONE edge is needed (e.g. the mid one)
                    n = len(nodeIDsOnBreakline)
                    nodeIDsOnBreakline = nodeIDsOnBreakline[n/2-1:n/2+1]
                self.__stringdefNodes[name] = nodeIDsOnBreakline
                count += 1
                print (" %i - verified string '%s' (%d nodes)"%(count,name,len(nodeIDsOnBreakline)))
        # write found nodes
        if self.__meshType=='plane':
            # write seperate file containing the bmc-block
            filename = os.path.join(self.__wd,'%s_stringdefs.txt'%self.__fileID)
            fid = open(filename, 'w')
            try:
                for name, nodeList in self.__stringdefNodes.iteritems():
                    fid.write('STRINGDEF {\n\tname = %s\n\tnode_ids = ('%name)
                    for ii in range(0, len(nodeList)):
                        fid.write("%i"%nodeList[ii])
                        fid.write(" " if (ii <= len(nodeList)-2) else "")
                    fid.write(')\n\tupstream_direction = right\n}\n')
            finally:
                fid.close()
        elif self.__meshType=='gpu':
            # append 2dm file with NS
            filename = os.path.join(self.__wd,self.__fileID+'.2dm')
            fid = open(filename, 'a')
            try:
                for name, nodeList in self.__stringdefNodes.iteritems():
                    fid.write('NS')
                    for ii in range(0, len(nodeList)-1):
                        fid.write(" %i"%nodeList[ii])
                    fid.write(" %i %s\n"%(-nodeList[-1],name))
            finally:
                fid.close()
        if not os.path.exists(filename):
            print "stringdef file could not be created!"

    def __writeStringdefDistance (self, nodeDict):
        # create a dictionary with all meshing nodes
        for ID,node in nodeDict.iteritems():
            self.__meshNodes.storeInDict(node,ID)
        # open file to write coordinates and distance of nodes
        filename = os.path.join(self.__wd,'%s_stringdefs_distance.txt'%self.__fileID)
        fid = open(filename, 'w')
        # loop over all stringdef and find the generated nodes on the segments
        for name,definition in self.__stringdefs.iteritems():
            nodeList,singleEdge = definition
            nodesOfSegments = []
            for nn in nodeList:
                dictNode = self.__meshNodes.getNodeFromDict( nn.getX(), nn.getY() )
                if dictNode!=None:
                    nodesOfSegments.append(dictNode)
            # look for other nodes that are lying on the segment
            if len(nodesOfSegments)<2:
                print ("stringdef '%s': no nodes found in mesh!"%name)
                return 
            else:
                # very first node
                nodeIDsOnBreakline = [nodesOfSegments[0].getID()]
                nodeOnBreakline = [nodesOfSegments[0]]
                # write
                fid.write('STRINGDEF \n\tname = %s'%name)
                # loop over all segments
                for ii in range(0, len(nodesOfSegments)-1):
                    node1 = nodesOfSegments[ii]
                    node2 = nodesOfSegments[ii+1]
                    nodesOnSegment = self.__meshNodes.getNodesOnSegment(node1, node2)
                    # add node IDs to the list, sorted!
                    x = node1.getX()
                    y = node1.getY()
                    while len(nodesOnSegment)>0:
                        di = [math.sqrt(math.pow(ni.getX()-x, 2)+math.pow(ni.getY()-y, 2)) for ni in nodesOnSegment]
                        ind = di.index(min(di))
                        nodeIDsOnBreakline.append(nodesOnSegment[ind].getID())
                        nodeOnBreakline.append(nodesOnSegment[ind])
                        x = nodesOnSegment[ind].getX()
                        y = nodesOnSegment[ind].getY()
                        nodesOnSegment.pop(ind)
                    # last node of each segment
                    nodeIDsOnBreakline.append(node2.getID())
                    nodeOnBreakline.append(node2)
                self.__stringdefNodes[name] = nodeIDsOnBreakline
                xCoord = []
                yCoord = []
                for tt in range(0,len(nodeOnBreakline)):
                    xCoord.append(nodeOnBreakline[tt].getX())
                    yCoord.append(nodeOnBreakline[tt].getY())
                # calculate distance
                dist = []
                for ii in range(0, len(xCoord)):
                    dist.append(math.sqrt(math.pow(xCoord[0]-xCoord[ii], 2)+math.pow(yCoord[0]-yCoord[ii], 2)))
                fid.write('\n\tx-coordinate = ')
                for ii in range(0, len(xCoord)):
                    fid.write("%s"%xCoord[ii])
                    fid.write(" " if (ii <= len(xCoord)-2) else "")
                fid.write('\n\ty-coordinate = ')
                for ii in range(0, len(yCoord)):
                    fid.write("%s"%yCoord[ii])
                    fid.write(" " if (ii <= len(yCoord)-2) else "")    
                fid.write('\n\tdistance = ')
                for ii in range(0, len(dist)):
                    fid.write("%s"%dist[ii])
                    fid.write(" " if (ii <= len(dist)-2) else "") 
                fid.write('\n')          
        fid.close()
        if not os.path.exists(filename):
            print "stringdef coordinate file could not be created!"

    def __addVertex (self,v):
        # check if this vertex already exists
        if self.__coordDict.storeInDict(v):
            # new vertex
            segmentsToSplit = {}
            for ii,s in enumerate(self.__segments):
                # check if new vertex splits an existing segment
                if self.__coordDict.isNodeOnSegment(v,s):
                    # ok, we have to replace that segment by two new ones...
                    segmentsToSplit[ii] = v
            for ii,v in segmentsToSplit.iteritems():
                e1 = EDGE( [self.__segments[ii].getNodes()[0],v] )
                e2 = EDGE( [v,self.__segments[ii].getNodes()[1]] )
                self.__segments[ii] = e1
                self.__segments.append(e2)

    def __addSingleLine (self, line, nSegs=None, name=None):
        if nSegs!=None and nSegs>0 and nSegs>len(line)-1:
            # here we have to divide the line into a given number of segments
            # calc the total length of the line
            length = 0.0
            for ii in range(0,len(line)-1):
                length += self.__2pointDistance( line[ii],line[ii+1] )
            dx = length/nSegs # this is the target segment size
            numCells = {}
            # first guess ...
            for ii in range(0,len(line)-1):
                l = self.__2pointDistance( line[ii],line[ii+1] )
                numCells[ii]= max(1,round(l/dx)) # at least one segment is needed
            # check for corrections due to rounding effects
            dN = nSegs-int(sum(numCells.values()))
            if dN != 0:
                count = 0
                for v in sorted(numCells, key=numCells.get, reverse=True): # correction at the largest segments
                    if count<abs(dN):
                        numCells[v] += math.copysign(1,dN)
                        count += 1
                    else:
                        break
            # dividing the line
            oldLine = line
            line = [ (oldLine[0][0],oldLine[0][1]) ]
            for ii,n in numCells.iteritems():
                dx = (oldLine[ii+1][0]-oldLine[ii][0])/n
                dy = (oldLine[ii+1][1]-oldLine[ii][1])/n
                for ni in range(0,int(n)):
                    line.append( (oldLine[ii][0]+dx*(ni+1), oldLine[ii][1]+dy*(ni+1)) )
        # regular line without special treatment
        for ii, point in enumerate(line):
            if ii < len(line) - 1:
                # first point of segment
                node1 = NODE( point[0], point[1] )
                # second point of segment
                nextPoint = line[ii+1]
                node2 = NODE( nextPoint[0], nextPoint[1] )
                # create new segment and store it in list
                self.__addSegment( node1, node2, name )

    def __addSegment (self,n1,n2,name=None):
        self.__addVertex(n1)
        self.__addVertex(n2)
        # eventually get the original nodes at the segment end points
        n1 = self.__coordDict.getNodeFromDict(n1.getX(),n1.getY())
        n2 = self.__coordDict.getNodeFromDict(n2.getX(),n2.getY())
        # and build a new segment out of these
        newSeg = EDGE([n1,n2])
        # do we really have a new segment?
        new = True
        for ii,s in enumerate(self.__segments):
            if s==newSeg:
                new = False 
        if new:
            newSeg.setName(name)
            self.__segments.append( newSeg )

    def __2pointDistance (self, p1, p2):
        return math.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)
	
    
    #@abc.abstractmethod
    def elevationFunction (self, x, y):
        # this function must be implemented by the derived model class
        #raise NotImplementedError
        return 0.0
    #@abc.abstractmethod
    def MaterialIDFunction (self, matID, x, y):
        # this function must be implemented by the derived model class
        #raise NotImplementedError
        return matID

class COORDINATEDICTIONARY:
    # this class stores nodes in a dictionary.
    # the key of the node is generated from the x- and y- coordinates
    def __init__(self,absolutePrecision=1e-6,snappingTolerance=1e-6):
        # setting the precision, which is a double, defines the precsision of the key
        # e.g. 0.00001254 means, that the coordinates include decimal numbers until 10-5
        self.__precision = absolutePrecision
        self.__tolerance = snappingTolerance
        # initialize the dictionary
        self.clear()

    def clear(self):
        self.__points = {}
        self.__pointID = 0

    # returns all points that are stored in the dictionary (without keys)
    def getPoints(self):
        return self.__points.values()

    # return all keys
    def getKeys(self):
        return self.__points.keys()
    
    # retreive the node from the dictionary with the coordinates (x,y)
    # returns None, if no such node is stored
    def getNodeFromDict(self, x, y):
        keyString = self.__getKeyString(x, y)
        return self.getNodeFromDictByKey(keyString)

    #retreive the node from the dictionary using the keyString
    # returns None, if no such node is stored        
    def getNodeFromDictByKey(self, keyString):
        if self.__points.has_key(keyString):
            return self.__points[keyString]
        else:
            return None
    
    # returns False if a point with the coordinates of 'node' is already stored in the dictionary
    # returns True if if the 'node' is new (gets a new ID as well)
    def storeInDict(self, node, id=None): # stores in dictionary using a unique keystring
        keyString = self.__getKeyString(node.getX(), node.getY())
        if not self.__points.has_key(keyString):
            if id==None:
                node.setID(self.__pointID)
                self.__pointID += 1
            else:
                node.setID(id)
            self.__points[keyString] = node
            return True
        else:
            return False
    
    # returns a list of nodes that are lying on the line between the two nodes given as arguments
    def getNodesOnSegment(self, n1, n2):
        nodeList = []
        segment = EDGE([n1,n2])
        for node in self.__points.values():
            if self.isNodeOnSegment(node,segment):
                nodeList.append(node)
        return nodeList

    def isNodeOnSegment(self,node,segment):
        x1, y1 = segment.getNodes()[0].getX(), segment.getNodes()[0].getY()
        ks1 = self.__getKeyString(x1, y1)
        x2, y2 = segment.getNodes()[1].getX(), segment.getNodes()[1].getY()
        ks2 = self.__getKeyString(x2, y2)
        keyString = self.__getKeyString(node.getX(), node.getY())
        if (keyString == ks1) or (keyString == ks2): # startpoint and endpoint -> do nothing
            return False
        else:         
            xBool = False
            yBool = False
            xp, yp = node.getX(), node.getY() # possible midpoint
                                              
            if ks1.split('_')[0]==ks2.split('_')[0]:
                xBool = True
            else:
                xBool = (min(x1, x2) <= xp <= max(x1, x2))
            if ks1.split('_')[1]==ks2.split('_')[1]:
                yBool = True
            else:
                yBool = (min(y1, y2) <= yp <= max(y1, y2))
            if xBool and yBool:
                a = np.array([xp-x1, yp-y1, 0.0])
                b = np.array([x2-xp, y2-yp, 0.0])
                cp = np.cross(a, b)
                if int(round(np.linalg.norm(cp)/self.__tolerance,2)) == 0:
                    return True
        return False

    # builds a unique key out of the objects x and y-coordinates
    def __getKeyString(self, x, y):
        # convert coordinates to integer, according to the precision defined earlier
        intX = long(round(x/self.__precision,2))
        intY = long(round(y/self.__precision,2))
        return "%i_%i" % (intX, intY)



class NODE:
    def __init__ (self, x=0.0, y=0.0, z=0.0):
        self.__coords = [x, y, z]
        self.__index = None
        self.__attribute = None

    def __getitem__ (self,ind):
        return self.__coords[ind]
    
    def setID (self, index):
        self.__index = index
    
    def getID (self):
        return self.__index

    def getX (self):
        return self.__coords[0]

    def getY (self):
        return self.__coords[1]

    def getZ (self):
        return self.__coords[2]
    
    def getPointXY(self):
        return [self.__coords[0], self.__coords[1]]
        
    def getPointXYZ(self):
        return [self.__coords[0], self.__coords[1], self.__coords[2]]
    
    def setAttribute(self, attribute):
        self.__attribute = attribute

    def getAttribute(self):
        return self.__attribute


class EDGE:
    # CLASS of segment, defined by 2 nodes
    def __init__ (self, nodes):
        self.__name = None
        if len(nodes)==2:
            self.__nodes = nodes # list of node objects
            self.calcProperties()
        else:
            print ('to define a segment, exactly two nodes are needed!')
            return

    def __eq__ (self, other):
        out = 0
        for ni in self.__nodes:
            for nj in other.getNodes():
                if ni==nj:
                    out = out+1
        if out==len(self.__nodes):
            return True
        else:
            return False

    def __str__ (self):
        return ("(%f/%f) -> (%f/%f)"%
            (self.__nodes[0].getX(),self.__nodes[0].getY(),self.__nodes[1].getX(),self.__nodes[1].getY()))

    def getNodes (self):
        return self.__nodes

    def calcProperties (self):
        n1 = self.__nodes[0]
        n2 = self.__nodes[1]
        # calculate edge length
        self.__length = math.sqrt( math.pow(n1.getX()-n2.getX(),2) + math.pow(n1.getY()-n2.getY(),2) )
        # calcualte normal vector
        #vec = np.array( [np.cross( [n2.getX()-n1.getX(), n2.getY()-n1.getY(), 0.0], [0.0, 0.0, 1.0] )] ).transpose()
        #self.__normal = np.divide( vec, self.__length )
        # calculate the orientation
        self.__orientation = [(n2.getX()-n1.getX())/self.__length, (n2.getY()-n1.getY())/self.__length]

    def getOrientation (self):
        return self.__orientation

    def setName (self, name):
        self.__name = name

    def getName (self):
        return self.__name


class CELL:

    def __init__ (self, node1, node2, node3):
        self.__nodes = [node1,node2,node3]
        self.__matID = None
        self._center = [0.0,0.0,0.0]

    def calcProperties (self,center): 
        nodes = self.getNodes()         
        # calculate the element center coordinates
        x = 0.0
        y = 0.0
        N = len(nodes)
        if center=='average':
            # average of triangle nodes coordinates as interpolation point
            for nn in nodes:
                x = x+nn.getX()
                y = y+nn.getY()
            x = x/3.0
            y = y/3.0
        elif center=='outer':
            # outer circle center as interpolation point
            d =  0.0
            for ii in range(N):
                n1 = nodes[(ii-1)%N]
                n2 = nodes[ii]
                n3 = nodes[(ii+1)%N]
                dx = n3.getX()-n1.getX()
                dy = n3.getY()-n1.getY()
                d = d + n2.getX()*dy
                v = n2.getX()**2 + n2.getY()**2
                x = x + v*dy
                y = y - v*dx
            x = x / (2.0*d)
            y = y / (2.0*d)
        elif center=='inner':
            # inner circle cener as interpolation point
            lsum = 0.0
            for ii in range(N):
                n1 = nodes[(ii+1)%N]
                n2 = nodes[(ii+2)%N]
                l = math.sqrt((n2.getX()-n1.getX())**2 + (n2.getY()-n1.getY())**2)
                lsum += l
                x = x + l*nodes[ii].getX()
                y = y + l*nodes[ii].getY()
            x = x/lsum
            y = y/lsum
        elif center=='mid':
            # mid of xmax and xmin (ymax and ymin)
            xmax = max(nodes[0].getX(),max(nodes[1].getX(),nodes[2].getX()))
            xmin = min(nodes[0].getX(),min(nodes[1].getX(),nodes[2].getX()))
            ymax = max(nodes[0].getY(),max(nodes[1].getY(),nodes[2].getY()))
            ymin = min(nodes[0].getY(),min(nodes[1].getY(),nodes[2].getY()))
            x = 0.5*(xmin+xmax)
            y = 0.5*(ymin+ymax)

        self._center = [x, y, 0.0]

    def getCenter (self):
        return self._center

    def setElevation (self, z):
        self._center[2] = z

    def getElevation (self):
        return self._center[2]
    
    #def getElementCenterCoords (self, elID):
     #   return self._elements[elID].getCenter()		
    
    def getNodes (self):
        return self.__nodes

    def getNodeIDs (self):
        nids = []
        for ni in self.__nodes:
            nids.append(ni.getID())
        return nids

    def setMaterial (self, value):
        self.__matID = value

    def getMaterial (self):
        return self.__matID

    def calcArea (self):
        n1,n2,n3 = self.getNodes()
        return abs((n1.getX()*(n2.getY()-n3.getY()) + n2.getX()*(n3.getY()-n1.getY())+ n3.getX()*(n1.getY()-n2.getY()))/2.0)

    def isInside (self, x, y):
        whichSide = lambda pt, n1, n2: True if (pt[0] - n2.getX()) * (n1.getY() - n2.getY()) - (pt[1] - n2.getY())*(n1.getX() - n2.getX()) >= 0.0 else False
        side1 = whichSide((x, y), self.__nodes[0], self.__nodes[1])
        N = len(self.__nodes)
        for ii in range(1, N):
            side2 = whichSide((x, y), self.__nodes[ii], self.__nodes[(ii+1)%N])
            if side2 != side1:
                return False
            else:
                side1 = side2
        return True

    def interpolate (self, x, y):
        P = self.__nodes[0]
        Q = self.__nodes[1]
        R = self.__nodes[2]
        a =  -(R.getY()*Q.getZ() - P.getY()*Q.getZ() - R.getY()*P.getZ() + Q.getY()*P.getZ() + P.getY()*R.getZ() - Q.getY()*R.getZ())
        b =   P.getY()*R.getX() + Q.getY()*P.getX() + R.getY()*Q.getX() - Q.getY()*R.getX() - P.getY()*Q.getX() - R.getY()*P.getX()
        c =   Q.getZ()*R.getX() + P.getZ()*Q.getX() + R.getZ()*P.getX() - P.getZ()*R.getX() - Q.getZ()*P.getX() - Q.getX()*R.getZ()
        d = -a*P.getX() - b*P.getZ() - c*P.getY()
        if b == 0.0:
            if ((x == P.getX()) and (y == P.getY)):
                return P.getZ()
            if (x == Q.getX() and y == Q.getY()):
                return Q.getZ()
            if (x == R.getX() and y == R.getY()):
                return R.getZ()
            return 0.0
        return -(a*x+c*y+d) / b


class MESH2DM:

    def __init__ (self, filename):
        n,c = self.__read2dm(filename)
        self.__nodeDict = n
        self.__cellList = c

    def getMesh (self):
        return (self.__nodeDict,self.__cellList)

    def getElevationInterpolator (self):
        # most efficient way of interpolation so far:
        # trying to do everything with vector operations!
        print 'create mesh interpolation function ...'
        coords,c2n = self.__mesh2array()
        e2nDict,e2cDict,_ = self.__getMeshMaps(coords.shape[0],c2n)
        # create matrix with edges x (n1x,n1y,n2x,n2y)
        Ne = len(e2nDict)
        edgeCoords = np.zeros((Ne,4),dtype=np.float)
        for eid,nodes in e2nDict.iteritems():
            # we substract 1 because we have to start at zero in a array
            edgeCoords[eid-1,:] = [coords[nodes[0],0],coords[nodes[0],1],coords[nodes[1],0],coords[nodes[1],1]]
        delta = edgeCoords[:,2:4]-edgeCoords[:,:2]
        l2 = np.sum(np.power(delta,2),axis=1)
        print l2.shape
        def interp(x,y):
            # Consider the line extending the segment, parameterized as v + t (w - v).
            # We find projection of point p onto the line. 
            # It falls where t = [(p-v) . (w-v)] / |w-v|^2
            # We clamp t from [0,1] to handle points outside the segment vw.
            dp = [x,y] - edgeCoords[:,:2]
            t = (dp[:,0]*delta[:,0] + dp[:,1]*delta[:,1]) / l2
            # do anything further only with edges that 0.0<t<1.0
            inds = np.where( np.logical_and( t >= 0.0, t <= 1.0) )[0]
            if not np.sum(inds):
                # if no edge found with 0.0<t<1.0, then take the globally closest one
                inds = np.ones((Ne,),dtype=np.int)
            t = t[inds]
            # Projection falls on the segment
            projection = edgeCoords[inds,:2] + np.vstack((t,t)).transpose() * delta[inds,:]
            distance = np.sum(np.power([x,y]-projection,2),axis=1)
            # get the edge with minimum distance
            minind = inds[distance.argsort()[0]] + 1 # increasing by one because the first edge ID i s 1
            # try left and right cell
            for ci in e2cDict[minind]:
                if self.__cellList[ci].isInside(x,y):
                    return self.__cellList[ci].interpolate(x,y)
        return interp

    def getElevationInterpolator2 (self):
        # experimentl interpolation, is working, but quite non-intuitive...and slow!
        print 'create mesh interpolation function ...'
        coords,c2n = self.__mesh2array()
        _,e2cDict,n2e = self.__getMeshMaps(coords.shape[0],c2n)
        def interp(x,y):
            d = np.power(coords[:,0]-x,2)+np.power(coords[:,1]-y,2)
            indices = d.argsort()
            # now look for the cell
            nC = 2
            while nC>1:
                # create list of potential cells
                cList = []
                n2 = nC-1
                for n1 in range(n2):
                    eid = n2e[indices[n1],indices[n2]]
                    if eid:
                        for ci in e2cDict[eid]:
                            if (ci is not None):
                                cList.append(ci)
                for ci in cList:
                    if self.__cellList[ci].isInside(x,y):
                        return self.__cellList[ci].interpolate(x,y)
                nC += 1
        return interp

    def __getMeshMaps (self,nNodes,connArr):
        # init sparse matrix to map nodes->edges
        n2e = lil_matrix((nNodes,nNodes),dtype=np.int)
        e2cDict = {} # dict map edge->cells
        edgeID = 1 # we start at 1 because 0 means 'no-data'
        for ic,ns in enumerate(connArr):
            for ii in range(len(ns)):
                n1 = ns[ii]
                n2 = ns[(ii+1)%len(ns)]
                eid = n2e[n1,n2]
                if not eid:
                    # new edge
                    n2e[n1,n2] = edgeID
                    n2e[n2,n1] = edgeID
                    e2cDict[edgeID] = [ic,None]
                    edgeID += 1
                else:
                    # existing edge
                    e2cDict[eid][1] = ic
        n1,n2,ei = find(tril(n2e))
        e2nDict = {}
        for ii in range(ei.shape[0]):
            e2nDict[ei[ii]] = [n1[ii],n2[ii]]
        return (e2nDict,e2cDict,n2e)

    def __read2dm (self,filename2dm):
        # import .2dm file, generated by SMS or QGIS
        try:
            smsFile = open(filename2dm,'r')
        except:
            print ("could not open the file '%s'!" % filename2dm)
            return
        lineCounter = 0
        elData = []
        cellList = []
        nodeDict = {}
        for line in smsFile:
            # parse line
            tokens = line.strip().split()
            if len(tokens) < 1:
                continue
            if tokens[0] in ["MESH2d", "MESH2D", "MESHNAME"]:
                continue
            # store triangle data in list
            if tokens[0] == "E3T":
                if len(tokens) < 6:
                    print ("Error found in 2dm file at cell-definition at line #%d. Cannot proceed." % lineCounter+1)
                    return
                else:
                    elData.append( [int(tokens[2]), int(tokens[3]), int(tokens[4]), int(tokens[5])] )
            # store quadrilateral data in list
            elif tokens[0] == "E4Q":
                print ("Error found in 2dm file at cell-definition at line #%d. No support for quadrilateral elements." % lineCounter+1)
                return
            # add node to mesh
            elif tokens[0] == "ND":
                if len(tokens) < 5:
                    print ("Error found in 2dm file at node-definition at line #%d. Cannot proceed." % lineCounter+1)
                    return
                else:
                    nodeID = int(tokens[1])
                    node = NODE( float(tokens[2]), float(tokens[3]), float(tokens[4]) )
                    node.setID(nodeID)
                    nodeDict[nodeID] = node
            # go to next line
            lineCounter = lineCounter + 1
        smsFile.close()
        # create the cells
        for eli in elData:
            cell = CELL( nodeDict[eli[0]],nodeDict[eli[1]],nodeDict[eli[2]] )
            cell.calcProperties()
            cell.setMaterial( eli[3] )
            cellList.append( cell )
        # return the results
        return (nodeDict,cellList)

    def __mesh2array (self):
        NN = len(self.__nodeDict)
        NC = len(self.__cellList)
        coords = np.zeros((NN,3),dtype=np.float)
        c2n = np.zeros((NC,3),dtype=np.int)
        # correct all ids because in 2dm we start from 1
        for key,ni in self.__nodeDict.iteritems():
            coords[key-1,:] = [ni.getX(),ni.getY(),ni.getZ()]
        for ii,ci in enumerate(self.__cellList):
            nodes = ci.getNodes()
            c2n[ii,:] = [nodes[0].getID()-1, nodes[1].getID()-1, nodes[2].getID()-1]
        return (coords,c2n)
