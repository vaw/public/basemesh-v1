#!/usr/bin/env python

# (c) VAW ETH Zurich, 2018
# Authors: Samuel J. Peter and others 

from batchTools import *
from meshModel import NODE, MESHMODEL
import math


class NUMMODCHANNEL (MESHMODEL):
	"""class defines the geometry of a trapezoidal channel"""
	def __init__ (self, directory,
		channelLength,
		channelBedWidth,
		channelHeight,
		bedSlope,
		bankSlope,
		nQP,
		ksBed,
		ksBank,
		mpShift):
		# call init of the meshmodel (base)-class, this class does all the meshing stuff
		MESHMODEL.__init__(self, directory)
		# store parameters
		self.__lc = channelLength
		self.__h = channelHeight
		self.__wc = channelBedWidth
		self.__s = bedSlope
		self.__b = bankSlope
		self.__N = nQP
		self.__ksBed = ksBed
		self.__ksBank = ksBank
		self.__mpShift = mpShift

	def buildModel(self):
		# mid point shift regarding to bottom left/right
		mps = self.__mpShift
		# needed coordinates
		xB,xU = (0.0, self.__lc)
		ybL,ybR = (-0.5*self.__wc, 0.5*self.__wc)
		ytL,ytR = (-(0.5*self.__wc+self.__b*self.__h), 0.5*self.__wc+self.__b*self.__h)
		ymL, ymR = (ybL-mps*abs(ytL-ybL), ybR+mps*abs(ytR-ybR))
		# bed lines
		self.addLine( [NODE(xB,ybL),NODE(xU,ybL)], name='bottomLeft' )
		self.addLine( [NODE(xB,ybR),NODE(xU,ybR)], name='bottomRight' )
		# bank lines
		self.addLine( [NODE(xB,ytL),NODE(xU,ytL)], name='topLeft' )
		self.addLine( [NODE(xB,ymL),NODE(xU,ymL)], name='midLeft' )
		self.addLine( [NODE(xB,ytR),NODE(xU,ytR)], name='topRight' )
		self.addLine( [NODE(xB,ymR),NODE(xU,ymR)], name='midRight' )

		# boundary
		boundaryLine = [NODE(0.0,0.0),NODE(xB,ybL),NODE(xB,ymL),NODE(xB,ytL)]
		boundaryLine.extend( [NODE(xU,ytL),NODE(xU,ybL),NODE(xU,ybR),NODE(xU,ytR)] )
		boundaryLine.extend( [NODE(xB,ytR),NODE(xB,ymR),NODE(xB,ybR),NODE(0.0,0.0)] )
		self.addLine(boundaryLine)			
		# matid only for illustration purposes
		self.addRegion(NODE(0.5*self.__lc,0.0),matID=1)
		self.addRegion(NODE(0.5*self.__lc,0.5*(self.__wc+self.__h*self.__b)),matID=2)
		self.addRegion(NODE(0.5*self.__lc,-0.5*(self.__wc+self.__h*self.__b)),matID=2)
		# define number of cross sections
		dx = self.__lc/self.__N
		for ii in range(self.__N+1):
			self.addCrossSection(NODE(ii*dx,0.0))
		# define the friction ranges
		friction = {'default': self.__ksBank, 
					'topLeft-midLeft': self.__ksBank,
					'midLeft-bottomLeft': self.__ksBank,
					'bottomLeft-bottomRight': self.__ksBed,
					'bottomRight-midRight': self.__ksBank,
					'midRight-topRight': self.__ksBank}
		self.setFriction(friction)
		# write the .bmg file
		self.createMesh()

	def elevationFunction(self, x, y):
		zBed = x*self.__s
		# because of symmetry
		x = min(x,self.__lc-x)
		dy = abs(y)-0.5*self.__wc
		if dy > 0.0:
			zBank = min( self.__h, dy/self.__b)
		else:
			zBank = 0.0
		return zBed + zBank


class NUMMODWIDENING (MESHMODEL):
	"""class defines the geometry of a trapezoidal channel with widening"""
	def __init__ (self, directory,
		channelLength,
		channelBedWidth,
		channelHeight,
		bedSlope,
		bankSlope,
		nQP,
		ksBed,
		ksBank,
		wideningLength,
		transitionLength,
		wideningBedWidth,
		ksWidening,
		mpShift):
		# call init of the meshmodel (base)-class, this class does all the meshing stuff
		MESHMODEL.__init__(self, directory)
		# store parameters
		self.__lc = channelLength
		self.__h = channelHeight
		self.__wc = channelBedWidth
		self.__s = bedSlope
		self.__b = bankSlope
		self.__lw = wideningLength
		self.__lt = transitionLength
		self.__ww = wideningBedWidth
		self.__ksWidening = ksWidening
		self.__N = nQP
		self.__ksBed = ksBed
		self.__ksBank = ksBank
		self.__mpShift = mpShift

	def buildModel(self):
		# do some checks
		if 2.0*self.__lt+self.__lw > self.__lc:
			print 'ERROR: channel length too small to model the widening including the transition!'
		else:
			# mid point shift regarding to bottom left/right
			mps = self.__mpShift
			# needed coordinates
			xB,xU = (0.0, self.__lc)
			ybL,ybR = (-0.5*self.__wc, 0.5*self.__wc)
			ytL,ytR = (-(0.5*self.__wc+self.__b*self.__h), 0.5*self.__wc+self.__b*self.__h)
			xtB,xtU = (0.5*(self.__lc-self.__lw)-self.__lt, 0.5*(self.__lc+self.__lw)+self.__lt)
			xwB,xwU = (0.5*(self.__lc-self.__lw), 0.5*(self.__lc+self.__lw))
			ybwL,ybwR = (-0.5*self.__ww, 0.5*self.__ww)
			ytwL,ytwR = (-(0.5*self.__ww+self.__b*self.__h), 0.5*self.__ww+self.__b*self.__h)
			ymL, ymR = (ybL-mps*abs(ytL-ybL), ybR+mps*abs(ybR-ytR))
			ymwL, ymwR = (ybwL-mps*abs(ytwL-ybwL), ybwR+mps*abs(ytwR-ybwR))
			# bed lines
			self.addLine( [NODE(xB,ybL),NODE(xtB,ybL)], name='bottomLeft' )
			self.addLine( [NODE(xtB,ybL), NODE(xwB,ybwL), NODE(xwU,ybwL), NODE(xtU,ybL)], name='wideningLeft' )
			self.addLine( [NODE(xtU,ybL),NODE(xU,ybL)], name='bottomLeft' )
			self.addLine( [NODE(xB,ybR),NODE(xtB,ybR)], name='bottomRight' )
			self.addLine( [NODE(xtB,ybR), NODE(xwB,ybwR), NODE(xwU,ybwR), NODE(xtU,ybR)], name='wideningRight' )
			self.addLine( [NODE(xtU,ybR),NODE(xU,ybR)], name='bottomRight' )
			# bank lines
			self.addLine( [NODE(xB,ytL),NODE(xtB,ytL)], name='topLeft' )
			self.addLine( [NODE(xtB,ytL),NODE(xwB,ytwL),NODE(xwU,ytwL),NODE(xtU,ytL)], name='WtopLeft' )
			self.addLine( [NODE(xtU,ytL),NODE(xU,ytL)], name='topLeft' )
			self.addLine( [NODE(xB,ytR),NODE(xtB,ytR)], name='topRight' )
			self.addLine( [NODE(xtB,ytR),NODE(xwB,ytwR),NODE(xwU,ytwR),NODE(xtU,ytR)], name='WtopRight' )
			self.addLine( [NODE(xtU,ytR),NODE(xU,ytR)], name='topRight' )
			self.addLine( [NODE(xB,ymL),NODE(xtB,ymL)], name='midLeft' )
			self.addLine( [NODE(xtB,ymL),NODE(xwB,ymwL),NODE(xwU,ymwL),NODE(xtU,ymL)], name='WmidLeft' )			
			self.addLine( [NODE(xtU,ymL),NODE(xU,ymL)], name='midLeft' )
			self.addLine( [NODE(xB,ymR),NODE(xtB,ymR)], name='midRight' )
			self.addLine( [NODE(xtB,ymR),NODE(xwB,ymwR),NODE(xwU,ymwR),NODE(xtU,ymR)], name='WmidRight' )
			self.addLine( [NODE(xtU,ymR),NODE(xU,ymR)], name='midRight' )
			# boundary
			boundaryLine = [NODE(0.0,0.0),NODE(xB,ybL),NODE(xB,ymL), NODE(xB,ytL)]
			boundaryLine.extend( [NODE(xtB,ytL),NODE(xwB,ytwL),NODE(xwU,ytwL),NODE(xtU,ytL)] )
			boundaryLine.extend( [NODE(xU,ytL), NODE(xU, ymL), NODE(xU,ybL),NODE(xU,ybR), NODE(xU, ymR), NODE(xU,ytR)] )
			boundaryLine.extend( [NODE(xtU,ytR),NODE(xwU,ytwR),NODE(xwB,ytwR),NODE(xtB,ytR)] )
			boundaryLine.extend( [NODE(xB,ytR), NODE(xB, ymR), NODE(xB,ybR),NODE(0.0,0.0)] )
			self.addLine(boundaryLine)			
			# matid only for illustration purposes
			self.addLine( [NODE(xtB,ybL),NODE(xtB,ybR)] )
			self.addLine( [NODE(xtU,ybL),NODE(xtU,ybR)] )
			self.addRegion(NODE(0.5*xtB,0.0),matID=1)
			self.addRegion(NODE(0.5*(xtU+self.__lc),0.0),matID=1)
			self.addRegion(NODE(0.5*self.__lc,0.0),matID=3)
			self.addRegion(NODE(0.5*self.__lc,0.5*(self.__ww+self.__h*self.__b)),matID=2)
			self.addRegion(NODE(0.5*self.__lc,-0.5*(self.__ww+self.__h*self.__b)),matID=2)
			# define number of cross sections
			dx = self.__lc/self.__N
			for ii in range(self.__N+1):
				self.addCrossSection(NODE(ii*dx,0.0))
			# define the friction ranges
			friction = {'default': self.__ksBank, 
						'topLeft-midLeft': self.__ksBank,
						'midLeft-bottomLeft': self.__ksBank,
						'bottomLeft-bottomRight': self.__ksBed,
						'bottomRight-midRight': self.__ksBank,
						'midRight-topRight': self.__ksBank,
						'wideningLeft-wideningRight': self.__ksWidening,
						'wideningLeft-WmidLeft': self.__ksBank,
						'WmidLeft-topLeft': self.__ksBank,
						'wideningRight-WmidRight': self.__ksBank,
						'WmidRight-topRight': self.__ksBank}
			self.setFriction(friction)
			# write the .bmg file
			self.createMesh()

	def elevationFunction(self, x, y):
		zBed = x*self.__s
		# because of symmetry
		x = min(x,self.__lc-x)
		x1 = 0.5*(self.__lc-self.__lw)-self.__lt
		x2 = 0.5*(self.__lc-self.__lw)
		if x<x1:
			dy = abs(y)-0.5*self.__wc
		elif x>x2:
			dy = abs(y)-0.5*self.__ww
		else:
			w = self.__wc + (self.__ww-self.__wc)/(x2-x1)*(x-x1)
			dy = abs(y)-0.5*w
		if dy > 0.0:
			zBank = min( self.__h, dy/self.__b)
		else:
			zBank = 0.0
		return zBed + zBank


if __name__ != 'main':
	import argparse

	intro = '\n\
	---------------------------------------\n\
	BASEchange - Version 31.01.2018\n\
	Generation of simple channel geometries\n\
	---------------------------------------\n'
	print(intro)

	# default input parameters
	sketch = '\n\
    <------------------------LC------------------------>\n\
                       **************\n\
                    **        |       **\n\
    ***************           |          ***************\n\
          |                   |                 |\n\
          WC                  WW                WC\n\
          |                   |                 |\n\
    ***************           |          ***************\n\
                    **        |       **\n\
                       **************\n\
                   <LT><-----LW-----><LT>'
	parser = argparse.ArgumentParser(prog='BASEchange',
		formatter_class=argparse.RawDescriptionHelpFormatter,
		description=sketch)
	parser.add_argument('-LC', metavar='channelLength', default=500, type=float, help='total length of the channel [m] (default: %(default)s)')
	parser.add_argument('-WC', metavar='channelBedWidth', default=50, type=float, help='bed width of the channel [m] (default: %(default)s)')
	parser.add_argument('-S', metavar='bedSlope', default=0.0015, type=float, help='slope of the channel [-] (default: %(default)s)')
	parser.add_argument('-B', metavar='bankSlope', default=1, type=float, help='slope of the banks [h:v] (default: %(default)s)')
	parser.add_argument('-H', metavar='height', default=5, type=float, help='height of the floodplain above the bed [m] (default: %(default)s)')
	parser.add_argument('-KS', metavar='kstrickler', default=35, type=float, help='strickler value of bed [m^(1/3)/s] (default: %(default)s)')
	parser.add_argument('-KSB', metavar='kstrickler', default=20, type=float, help='strickler value of bank [m^(1/3)/s] (default: %(default)s)')
	parser.add_argument('-X', metavar='referenceDistance', default=0, type=float, help='offset in x-direction [m] (default: %(default)s)')
	parser.add_argument('-Z', metavar='referenceElevation', default=0, type=float, help='offset in z-direction [m] (default: %(default)s)')
	parser.add_argument('-N', metavar='nCrossSections', default=100, type=int, help='number of cross sections to generate [-] (default: %(default)s)')
	parser.add_argument('-CS', metavar='csNamePrefix', default='CS', type=str, help='prefix of cross section names (default: %(default)s)')
	parser.add_argument('--R', dest='reverseList', action='store_true', help='reverse cross section order (default: %(default)s)')
	parser.add_argument('-MPS', metavar='mpShift', default=0.5, type=float, help='shift of bank mid point regarding bottom [-] (default: %(default)s)')
	parser.add_argument('--widening', dest='widening', action='store_true', help='tag to model a widening (default: %(default)s)')
	parser.add_argument('-LW', metavar='wideningLength', default=100, type=float, help='length of the widening only [m] (default: %(default)s)')
	parser.add_argument('-LT', metavar='transitionLength', default=20, type=float, help='length of the transition between channel and widenening (>0) [m] (default: %(default)s)')
	parser.add_argument('-WW', metavar='wideningBedWidth', default=100, type=float, help='bed width of the widening [m] (default: %(default)s)')
	parser.add_argument('-KSW', metavar='kstrickler', default=30, type=float, help='strickler value of widening [m^(1/3)/s] (default: %(default)s)')
	parser.add_argument('--2D', dest='2d', action='store_true', help='tag to generate 2d mesh (default: %(default)s)')
	parser.add_argument('--BED', dest='bed', action='store_true', help='tag to define a bottom with soil 1 (default: %(default)s)')
	parser.add_argument('-BS', metavar='bedStart', default=3, type=int, help='index of node where bed starts [-] (default: %(default)s)')
	args = vars(parser.parse_args())
	# create model directory
	wd = WORKINGDIRECTORY('Channel_Geometry')
	# cretae the mesh model
	if args['widening']:
		nm = NUMMODWIDENING(wd,
			channelLength = args['LC'],
			wideningLength = args['LW'],
			transitionLength = args['LT'],
			channelBedWidth = args['WC'],
			wideningBedWidth = args['WW'],
			channelHeight = args['H'],
			bedSlope = args['S'],
			bankSlope = args['B'],
			nQP = args['N'],
			ksBed = args['KS'],
			ksBank = args['KSB'],
			ksWidening = args['KSW'],
			mpShift = args['MPS'])
	else:
		nm = NUMMODCHANNEL(wd,
			channelLength = args['LC'],
			channelBedWidth = args['WC'],
			channelHeight = args['H'],
			bedSlope = args['S'],
			bankSlope = args['B'],
			nQP = args['N'],
			ksBed = args['KS'],
			ksBank = args['KSB'],
			mpShift = args['MPS'])
	modID = 'geometry'
	mesh = 'plane' if args['2d'] else 'chain'
	nm.initialization(modID,mtype=mesh)
	nm.setOffset(args['X'],args['Z'])
	nm.setCSnamePrefix(args['CS'])
	nm.setReverseList(args['reverseList'])
	nm.setHasBed(args['bed'],args['BS'])
	nm.buildModel()