# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import platform, os, math

from ..tools import commonFunctions
from ..tools.runTriangle import COORDINATEDICTIONARY
from ..tools.mesh import MESH
from ..tools.mesh import NODE


def extractStringdefNodes (nLayer, blLayer, stringdefFieldName, precision=10**(-6), progressBar=None):
    # do some work before entering the main loop
    if nLayer.geometryType()==QGis.Point:
       nodeIDindex = nLayer.fieldNameIndex('NODE_ID')
    else:
        return ("layer '%s' not of type 'Point'!"%nLayer.name())
    if blLayer.geometryType()!=QGis.Line:
        return ("layer '%s' not of type 'Line'!"%blLayer.name())
    else:
        stringdefIndex = blLayer.fieldNameIndex(stringdefFieldName)
    # creating the node dictionary
    rect = nLayer.extent()
    minExtent = min(rect.width(),rect.height())
    nodesCoordDict = COORDINATEDICTIONARY(precision*minExtent)
    nodes = commonFunctions.extractPoints (nLayer,withIDs=True)
    if type(nodes) != list:
        return nodes
    else:
        for n in nodes:
            nodesCoordDict.storeInDict(n,id=n.getID())
    # loop over all nodes of the stringdef breaklines
    out = {} # dictionary of the stringdef names and their nodes
    for feat in blLayer.getFeatures():
        stringdefName = feat.attributes()[stringdefIndex]
        if stringdefName != None:
            if feat.geometry().type()==QGis.Line: # double check for feature geometry
                # get the node list of all nodes that define the breakline
                nodesOfBreakline = []
                line = feat.geometry().asPolyline()
                for ii, point in enumerate(line):
                    dictNode = nodesCoordDict.getNodeFromDict( point[0], point[1] )
                    if dictNode!=None:
                        nodesOfBreakline.append(dictNode)
                # look for other nodes that are lying on the break line
                if len(nodesOfBreakline)<2:
                    return ("stringdef '%s': no nodes found in mesh!"%stringdefName)
                else:
                    # very first node
                    nodeIDsOnBreakline = [nodesOfBreakline[0].getID()]
                    # loop over all segments of the breakline
                    for ii in range(0, len(nodesOfBreakline)-1):
                        node1 = nodesOfBreakline[ii]
                        node2 = nodesOfBreakline[ii+1]
                        nodesOnSegment = nodesCoordDict.getNodesOnSegment(node1, node2)
                        # add node IDs to the list, sorted!
                        x = node1.getX()
                        y = node1.getY()
                        while len(nodesOnSegment)>0:
                            di = [math.sqrt(math.pow(ni.getX()-x, 2)+math.pow(ni.getY()-y, 2)) for ni in nodesOnSegment]
                            ind = di.index(min(di))
                            nodeIDsOnBreakline.append(nodesOnSegment[ind].getID())
                            x = nodesOnSegment[ind].getX()
                            y = nodesOnSegment[ind].getY()
                            nodesOnSegment.pop(ind)
                        # last node of each segment
                        nodeIDsOnBreakline.append(node2.getID())
                    out[stringdefName] = nodeIDsOnBreakline
            else:
                return ("Feature in %s layer not of type 'line'!"%blLayer)
        else:
            pass # breakline feature which is not a stringdef
        # report progress
        if progressBar!=None:
            commonFunctions.updateProgressBar(progressBar)
    return out
    

def renumber (nLayer, elevationField, cLayer, materialField, precision=10**(-6), progressBar=None):
    # check input layers
    if nLayer.geometryType()!=QGis.Point:
        return ("layer '%s' is not of type 'point'"%nLayer.name())
    else:
        elevationFieldIndex = nLayer.fieldNameIndex(elevationField)
    if cLayer.geometryType()!=QGis.Polygon:
        return ("layer %s is not of type 'polygon'"%cLayer.name())
    else:
        materialFieldIndex = cLayer.fieldNameIndex(materialField)
    # check that each node has z-information, otherwise inform user and exit
    missingZList = []
    for featNode in nLayer.getFeatures():
        z = featNode[elevationFieldIndex]
        if z == None:
            missingZList.append(featNode[0])
    if len(missingZList) > 0:
        return ("Not all nodes have z-values (node-ID = %s). Please correct the input." % ', '.join(map(str, missingZList)))
    #check that each cell has a material index set, otherwise inform user and exit
    missingIndexList = []
    for featCell in cLayer.getFeatures():
        index = featCell[materialFieldIndex]
        if index == None:
            missingIndexList.append(featCell[0])
    if len(missingIndexList) > 0:
        return ("Not all cells have material indices set (cell-ID = %s). Please correct the input." % ', '.join(map(str, missingIndexList)))

    # create a coordinate dictionary that has the control of the uniqueness of the nodes
    # and which allows for comparisons using x-y-coordinates
    rect = nLayer.extent()
    minExtent = min(rect.width(),rect.height())
    precision = 10**(-6)
    nodesCoordDict = COORDINATEDICTIONARY(precision*minExtent)
    nodes = commonFunctions.extractPoints (nLayer,attrField=elevationField)
    if type(nodes) != list:
        return nodes
    else:
        for n in nodes:
            nodesCoordDict.storeInDict(n)

    # main loop over all cells
    cells= {}
    elementListWithoutID = []
    maxID = 0
    ascendingID = 1
    for featCell in cLayer.getFeatures():
        elid = ascendingID
        ascendingID = ascendingID + 1
        matid = featCell.attributes()[materialFieldIndex]

        cellsNodeList = []  # list which stores all nodes of the cell
        ring = featCell.geometry().asPolygon()[0]
        nrRingNodes = len(ring) - 1
        if nrRingNodes != 3 and nrRingNodes != 4:
            return ("Cell #%d with #%d vertices found. Cells must have 3 or 4 vertices!" %(elid, nrRingNodes) )
        # loop over all nodes of the cell
        for ii in range(nrRingNodes):
            point = ring[ii+1]

            # compare point coordinates with all x-y-node coordinates
            # to find the corresponding node
            nodeFound = nodesCoordDict.getNodeFromDict(point[0], point[1])
            if nodeFound == None:
                return ( "Cell point (x = %f, y= %f) does not match any node! Please correct the mesh." % (point[0], point[1]) )
            else:
                cellsNodeList.append(nodeFound)
                nodeFound.setAttribute(True)

        # check orientation of cell (should be clockwise)
        # if orientation is not correct, simply reverse the node order
        [x1, y1] = cellsNodeList[0].getPointXY()
        [x2, y2] = cellsNodeList[1].getPointXY()
        [x3, y3] = cellsNodeList[2].getPointXY()
        crossProd = (x2-x1)*(y3-y2) - (x3-x2)*(y2-y1)
        if crossProd > 0: # umlaufsinn positive
            pass
        elif crossProd < 0:  # umlaufsinn negativ
            cellsNodeList.reverse()
        else:
            return ("Polygon without extension found? Cannot proceed.")

        # set the attributes of the cell in a list
        attributesList = []
        for node in cellsNodeList:
            attributesList.append(node.getID() + 1) #  node numbers in coord list start with 0, but we want to start with 1
        attributesList.append(matid)
        if elid == -1: # new element without ID
            elementListWithoutID.append(attributesList)
        else: # existing element with ID
            cells[elid] = attributesList
            maxID = max(maxID, elid)
        # report progress
        if progressBar!=None:
            commonFunctions.updateProgressBar(progressBar)

    # assign new elementID to all the cells without elementID yet
    for elData in elementListWithoutID:
        maxID = maxID+1
        cells[maxID] = elData

    # check if there are nodes which do not belong to any element
    for key in nodesCoordDict.getKeys():
        node = nodesCoordDict.getNodeFromDictByKey(key)
        if node.getAttribute() == False:
            return ("The node (x = %f, y = %f) has no associated cell. Please correct the mesh data." % (node.getX(), node.getY()))

    # create mesh object from dictionaries
    newMesh = MESH()
    for key in nodesCoordDict.getKeys():
        node = nodesCoordDict.getNodeFromDictByKey(key)
        newMesh.addNode(node.getID() + 1, node.getX(), node.getY(), node.getZ())
    newMesh.newAttribute('matid')
    for elid, elData in cells.iteritems():
        newMesh.addElement(elid, elData[0:-1])
        newMesh.setAttr(elid, ('matid', elData[-1]) )
    newMesh.init(1)

    return newMesh


def interpolationFromRaster (outputFileID, elevRaster, band, samplePointLayer, progressBar=None):
    # strategy to fill not idntified points
    notValid = -999
    # get coordinate reference system of the project
    crs = iface.mapCanvas().mapRenderer().destinationCrs()
    # create the new shape file for nodes
    fields = QgsFields()
    fields.append(QgsField("NODE_ID", QVariant.Int))
    fields.append(QgsField("X", QVariant.Double))
    fields.append(QgsField("Y", QVariant.Double))
    fields.append(QgsField("Z", QVariant.Double))
    try:
        # create writer for new shape layer
        writer = QgsVectorFileWriter(outputFileID, "CP1250", fields, QGis.WKBPoint, crs, "ESRI Shapefile")
        # create data provider of the raster layer contatining the elevation information
        prov=elevRaster.constDataProvider()
        # get the index of the "NODE_ID" field
        nodeIDindex = samplePointLayer.fieldNameIndex('NODE_ID')
        # loop over all points that get new elevation data
        notFound = 0
        for ptFeat in samplePointLayer.getFeatures():
            attrs = ptFeat.attributes()
            # get and set point ID
            ptID = attrs[nodeIDindex]
            newFeat = QgsFeature(fields) # create destination feature
            newFeat.setFeatureId(ptID)
            # get and set the geometry
            pt = ptFeat.geometry().asPoint()
            newFeat.setGeometry(QgsGeometry.fromPoint( pt ))
            # get and set the attributes
            newFeat.setAttribute(0, ptID)
            newFeat.setAttribute(1, pt[0])
            newFeat.setAttribute(2, pt[1])
            # get and set the elevation data
            resDict = prov.identify(pt, 1).results()
            if resDict[band] != None:
                ptFeat.setAttribute(3, resDict[band])
            else:
                ptFeat.setAttribute(3, notValid)
                notFound += 1
            # add feature to the layer
            writer.addFeature(ptFeat)
            if progressBar != None:
                commonFunctions.updateProgressBar(progressBar)
    finally:
        # delete the writer to flush features to disk (optional)
        del writer
    if not os.path.exists(outputFileID+'.shp'):
        return ("shape-file with interpolated elevation could not be created.")
    else:
        return notFound


def interpolationFromMesh (outputFileID, elevMesh, samplePointLayer, precision=10**(-6), progressBar=None):
    rect = samplePointLayer.extent()
    minExtent = min(rect.width(),rect.height())
    tolerance = precision*minExtent
    # for each element in the source-shapefile, the whole list of elements is scanned -> performance-issue!!!
    # do some preparation stuff
    elIDs = elevMesh.getElementIDs()
    notFound = 0 # number of points which could not be interpolated
    listID = []
    listX = []
    listY = []
    listZ = []
    listFound = []
    # get coordinate reference system of the project
    crs = iface.mapCanvas().mapRenderer().destinationCrs()
    # create the new shape file for nodes
    fields = QgsFields()
    fields.append(QgsField("NODE_ID", QVariant.Int))
    fields.append(QgsField("X", QVariant.Double))
    fields.append(QgsField("Y", QVariant.Double))
    fields.append(QgsField("Z", QVariant.Double))
    # get the index of the "NODE_ID" field
    nodeIDindex = samplePointLayer.fieldNameIndex('NODE_ID')
    #first loop over all points:
    # - check if point lies within an element
    # - if it is the case -> interpolate the z-value
    for feat in samplePointLayer.getFeatures():
        listID.append( feat.attributes()[nodeIDindex] )
        x, y = feat.geometry().asPoint()
        listX.append(x)
        listY.append(y)
        # loop over all elements
        found = False
        for ei in elIDs:
            #check if point lies within one of the elements
            if elevMesh.pointInElement(ei, x, y):
                #point lies within element
                zValue = elevMesh.interpolateElevation(ei, x, y)
                if zValue > -99999:
                    found = True
                    listFound.append(True)
                    # interpolate elevation
                    listZ.append( zValue )
                    break
        if not found:
            # point does not lie within any element
            notFound += 1
            listZ.append(-10000.0)
            listFound.append(False)
        # update progress bar
        if progressBar != None:
            commonFunctions.updateProgressBar(progressBar)
    # second loop over all points:
    # - interpolate z-values for all points which were not found by finding the nearest neighbour
    # - create the new feature
    try:
        # create writer for new shape layer
        writer = QgsVectorFileWriter(outputFileID, "CP1250", fields, QGis.WKBPoint, crs, "ESRI Shapefile")
        index = 0
        error = True
        for feat in samplePointLayer.getFeatures():
            if not listFound[index]:
            	for ei in elIDs:
            		#check if point is close to one of the elements: (True/False, xn and yn foot coordinates)
            		(check, xn, yn) = elevMesh.pointCloseToElement(ei, listX[index], listY[index], tolerance)
            		if check:
                        #if True interpolate with the foot coordinates
                		listZ[index] = elevMesh.interpolateElevation(ei,  xn, yn)
                		error = False
        #        min_distance = 1E32
        #        for ii in range(len(listX)):
        #            if listFound[ii]:
        #                distance = math.sqrt((listX[index]-listX[ii])**2 + (listY[index]-listY[ii])**2)
        #                if distance < min_distance:
        #                    min_distance = distance
        #                    listZ[index] = listZ[ii]
        #                    error = False
            else:
                error =False
            if error:
                return "A node (%i: %f/%f) could not be assigned a height. This should never happen, please contact the developers." % (listID[index],listX[index],listY[index])
            # store information
            newFeat = QgsFeature(fields) # create destination feature
            newFeat.setFeatureId (listID[index]) # original id of point is written to new shapefile
            newFeat.setGeometry(QgsGeometry.fromPoint(QgsPoint(listX[index],listY[index])))
            newFeat.setAttribute(0, listID[index])
            newFeat.setAttribute(1, listX[index])
            newFeat.setAttribute(2, listY[index])
            newFeat.setAttribute(3, listZ[index])
            writer.addFeature(newFeat)
            index += 1
    finally:
        # delete the writer to flush features to disk (optional)
        del writer
    if not os.path.exists(outputFileID+'.shp'):
        return ("shape-file with interpolated elevation could not be created.")
    else:
        return notFound

def compareTwoPointsCoords(p1, p2, precision):
    # convert coordinates
    coordInts = []
    points = [p1, p2]
    for p in points:
        for ii in range(2):
           coordInts.append(int(p[ii]/precision))
    # now compare
    if coordInts[0]==coordInts[2] and coordInts[1]==coordInts[3]:
        return True
    else:
        return False
