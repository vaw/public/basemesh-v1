# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

import numpy as np
import math



class MESH:
    # CLASS of 2D mesh (triangles and quadrilaterlas)
    # ... or even a container of 1D cross sections
    def __init__ (self):
        self.__nodes = {}
        self.__edges = {}
        self.__elements = {}
        self.__attrNames = [] # list of element attributes
    
    def addNode (self, nodeID, x=0.0, y=0.0,  z=0.0):
        self.__nodes[ nodeID ] = NODE(x, y, z)
    
    def addElement (self, elementID, nodeIDs):
        # create edges
        edges = []
        for ii in range(0, len(nodeIDs) ):
            i1 = nodeIDs[ii]
            i2 = nodeIDs[int(math.fmod(ii+1,len(nodeIDs)))]
            edges.append( EDGE( [self.__nodes[i1], self.__nodes[i2]] ) )
        # add edges to the mesh
        if len(self.__edges)==0:
            for ii in range(0, len(edges)):
                self.__edges[ii+1] = edges[ii]
        else:
            maxID = int(max(self.__edges.keys()))
            for ii in range(0, len(edges)):
                self.__edges[maxID+1+ii] = edges[ii]
        # create element
        self.__elements[ elementID ] = ELEMENT( edges, nodeIDs )
        # assign element ID to the nodes
        for nid in nodeIDs:
            self.__nodes[nid].attachElementID( elementID )
        # assign element to the nodes
        for ee in edges:
            ee.setElement(self.__elements[elementID])
    
    def init (self, level=0):
        success = True
        if level>0:
            # do some checks
            success = self.checks()
            if level>1:
                # find neighbours
                success = self.findNeighbours()
        return success
        
    def getElementIDs (self):
        # list of ids (key of dictionary) of all elements in the mesh, needed for riemann solver in main loop
        return self.__elements.keys()
    
    def getElement (self, elID):
        return self.__elements[elID]

    def getElementCenterCoords (self, elID):
        return self.__elements[elID].getCenter()

    def getElementSize (self, elID):
        # in case of 1d this is dx, in case of 2d this is the element area
        return self.__elements[elID].getElementSize()
    
    def getElementBoundingBox (self, elID):
        return self.__elements[elID].getBoundingBox()
    
    def pointInElement (self, elID, x, y):
        return self.__elements[elID].isInside(x, y)
    
    def pointCloseToElement (self, elID, x, y, tolerance):
        return self.__elements[elID].isClose(x, y, tolerance)
    
    def interpolateElevation (self, elID, x, y):
        return self.__elements[elID].interpolate(x, y)

    def newAttribute (self, attrName):
        if attrName in self.__attrNames:
            return
        else:
            self.__attrNames.append(attrName)
        pass

    def setAttr (self, elID, attrTupel):
        if attrTupel[0] in self.__attrNames:
            self.__elements[elID].setAttr( attrTupel[0], attrTupel[1])
        else:
            return False

    def getAttr (self, elID,  attrName):
        if attrName in self.__attrNames:
            return self.__elements[elID].getAttr( attrName )
        else:
            return False

    def getEdges (self, elID):
        return self.__elements[elID].getEdges()

    def getElementNodeIDs (self, elID):
        return self.__elements[elID].getNodeIDs()

    def getNodeIDs (self):
        # list of ids (key of dictionary) of all nodes in the mesh
        return self.__nodes.keys()

    def getNode (self, nodeID):
        return self.__nodes[nodeID]

    def getAllNodes (self):
        nodeIDs = self.__nodes.keys()
        nodeIDs.sort()
        nodes = []
        for ii in nodeIDs:
            nodes.append(self.__nodes[ii])
        return nodes

    def rotate (self, rotCenter=[0.0, 0.0], rotAngle=0.0):
        # create rotation center as node
        center = NODE( rotCenter[0], rotCenter[1], 0.0 )
        # rotate each node of the mesh
        for point in self.__nodes.values():
            point.rotate (center, rotAngle) # angle in radian
        # recalculate the properties of the edges
        for edge in self.__edges.values():
            edge.calcProperties()
        # recalculate the properties of the elements
        for element in self.__elements.values():
            element.calcProperties()
        return self.init(1)

    def findNeighbours (self):
        # loop over all elements
        for ei in self.__elements.keys():
            # pick a single edge
            for edge1 in self.__elements[ei].getEdges():
                # check if neighbour already asigned
                if edge1.getTwinEdge() is None:
                    # if not, get an arbitrary node belonging to that edge
                    node = edge1.getNodes()[0]
                    # and loop over all elements attached to that node
                    for ej in node.getAttachedElementIDs():
                        # check whether the two selected elements are not the same
                        if ei!=ej:
                            # pick again a single edge
                            for edge2 in self.__elements[ej].getEdges():
                                # now check if the two edges are defined by the same point(s)
                                if edge1==edge2:
                                    # found neighbouring elements
                                    edge1.setTwinEdge(edge2)
                                    edge2.setTwinEdge(edge1)
                                    self.__elements[ei].addNeighbour(ej)
                                    self.__elements[ej].addNeighbour(ei)
                                    break
                        if edge1.getTwinEdge()!=None:
                            break
        return True

    def getNeighbours (self, elID):
        return self.__elements[elID].getNeighbours()

    def checks (self):
        # delete all nodes that do not belong to any element
        toDelete = []
        for id in self.__nodes.keys():
            if len(self.__nodes[id].getAttachedElementIDs())==0:
                toDelete.append(id)
        for id in toDelete:
            del self.__nodes[id]
        return True
        # check if sum( norm.vec * edge.length ) = 0
        #totalLength = 0.0
        #for ele in self.__elements.values():
        #    sumLengthX = 0.0
        #    sumLengthY = 0.0
        #    for edge in ele.getEdges():
        #        sumLengthX = sumLengthX + edge.getLength()*edge.getNormalVector()[0]
        #        sumLengthY = sumLengthY + edge.getLength()*edge.getNormalVector()[1]
        #    totalLength = totalLength + math.sqrt( math.pow(sumLengthX,2) + math.pow(sumLengthY,2) )
        #if totalLength > 1e-12:
        #    return ('consistency of generated mesh NOT fullfilled!')
        #else:
        #    return True


class NODE:

    def __init__ (self, x, y=0.0, z=0.0):
        self.__coords = [x, y, z]
        self.__attachedElementIDs = []
        self.__index = None
        self.__attribute = None

    def setID (self, index):
        self.__index = index

    def getID (self):
        return self.__index

    def getX (self):
        return self.__coords[0]

    def getY (self):
        return self.__coords[1]

    def getZ (self):
        return self.__coords[2]

    def getPointXY(self):
        return [self.__coords[0], self.__coords[1]]

    def getPointXYZ(self):
        return [self.__coords[0], self.__coords[1], self.__coords[2]]

    def setAttribute(self, attribute):
        self.__attribute = attribute

    def getAttribute(self):
        return self.__attribute

    def attachElementID (self, elID):
        self.__attachedElementIDs.append( elID )

    def getAttachedElementIDs (self):
        return self.__attachedElementIDs

    def rotate (self, center, angle):
        # trigonometric values
        cosAngle = math.cos(angle)
        sinAngle = math.sin(angle)
        # translate node by -center
        xTmp = self.getX() - center.getX()
        yTmp = self.getY() - center.getY()
        # do the rotation
        xRes = xTmp*cosAngle - yTmp*sinAngle
        yRes = xTmp*sinAngle + yTmp*cosAngle
        # translate node back
        self.__coords[0] = xRes + center.getX()
        self.__coords[1] = yRes + center.getY()
        pass


class EDGE:
    # CLASS of 2D edge, defined by 2 nodes
    def __init__ (self, nodes):
        if len(nodes)==2:
            self.__nodes = nodes # list of node objects
            self.__element = None
            self.__twinEdge = None
            self.calcProperties()
        else:
            return ('to define an edge, exactly two nodes are needed!')

    def __eq__ (self, other):
        out = 0
        for ni in self.__nodes:
            for nj in other.getNodes():
                if ni==nj:
                    out = out+1
        if out==len(self.__nodes):
            return True
        else:
            return False

    def __str__ (self):
        return ("(%f/%f) -> (%f/%f)"%
            (self.__nodes[0].getX(),self.__nodes[0].getY(),self.__nodes[1].getX(),self.__nodes[1].getY()))

    def getNodes (self):
        return self.__nodes

    def getCoords (self):
        out = []
        for node in self.__nodes:
            out.append( [node.getX(), node.getY(), node.getZ()] )
        return out

    def calcProperties (self):
        n1 = self.__nodes[0]
        n2 = self.__nodes[1]
        # calculate edge length
        self.__length = math.sqrt( math.pow(n1.getX()-n2.getX(),2) + math.pow(n1.getY()-n2.getY(),2) )
        # calcualte normal vector
        vec = np.array( [np.cross( [n2.getX()-n1.getX(), n2.getY()-n1.getY(), 0.0], [0.0, 0.0, 1.0] )] ).transpose()
        self.__normal = np.divide( vec, self.__length )
        pass

    def getLength (self):
        return self.__length

    def getNormalVector (self):
        return self.__normal

    def setElement (self, element):
        self.__element = element
        pass

    def getElement (self):
        return self.__element

    def setTwinEdge (self, other):
        self.__twinEdge = other
        pass

    def getTwinEdge (self):
        return self.__twinEdge

    def getVector (self):
        n1 = self.__nodes[0]
        n2 = self.__nodes[1]
        return np.array( [n2.getX()-n1.getX(), n2.getY()-n1.getY(), n2.getZ()-n1.getZ()] )


class ELEMENT:

    def __init__ (self, edges, nodeIDs):
        self.__edges = edges # list of edges
        nodes = []
        for ee in edges:
            for nn in ee.getNodes():
                if nn not in nodes:
                    nodes.append(nn)
        self.__nodes = nodes
        self.__nodeIDs = nodeIDs
        self.__neighbours = []
        self.__attrs = {} # dict of attributes
        self.__crossSection = None
        self.calcProperties()

    def getEdges (self):
        return self.__edges

    def getNodes (self):
        return self.__nodes

    def getNodeIDs (self):
        return self.__nodeIDs

    def getCenter (self):
        return self.__center

    def isNeighbour (self, other):
        for e1 in self.__edges:
            for e2 in other.getEdges():
                if e1==e2:
                    return True
        return False

    def addNeighbour (self, elID):
        self.__neighbours.append(elID)

    def getNeighbours (self):
        return self.__neighbours

    def setAttr (self, attrName, value):
        self.__attrs[attrName] = value

    def getAttr (self, attrName):
        return self.__attrs[attrName]

    def getElementSize (self):
        return self.__area

    def getBoundingBox (self):
        x = self.__nodes[0].getX()
        minX = x
        maxX = x
        y = self.__nodes[0].getY()
        minY = y
        maxY = y
        for ii in range(1, len(self.__nodes)):
            x = self.__nodes[ii].getX()
            minX = min(minX, x)
            maxX = max(maxX, x)
            y = self.__nodes[ii].getY()
            minY = min(minY, y)
            maxY = max(maxY, y)
        return [minX, minY, maxX, maxY]

    def isInside (self, x, y):
        # # function to calculate the area of a triangle, defined via three nodes
        # calcTriangleArea = lambda n1,n2,n3: abs((n1.getX()*(n2.getY()-n3.getY()) + n2.getX()*(n3.getY()-n1.getY())+ n3.getX()*(n1.getY()-n2.getY()))/2.0)
        # np = NODE(x,y)
        # n1,n2,n3 = self.__nodes
        # # calculate area of triangle ABC
        # A = calcTriangleArea(n1,n2,n3)
        # # calculate area of triangle PBC
        # A1 = calcTriangleArea(np,n2,n3)
        # # calculate area of triangle APC
        # A2 = calcTriangleArea(n1,np,n3)
        # # calculate area of triangle ABP
        # A3 = calcTriangleArea(n1,n2,np)
        # # check if sum of A1, A2 and A3 is same as A
        # return (A == A1 + A2 + A3)
        whichSide = lambda pt, n1, n2: True if (pt[0] - n2.getX()) * (n1.getY() - n2.getY()) - (pt[1] - n2.getY())*(n1.getX() - n2.getX()) >= 0.0 else False
        side1 = whichSide((x, y), self.__nodes[0], self.__nodes[1])
        N = len(self.__nodes)
        for ii in range(1, N):
            side2 = whichSide((x, y), self.__nodes[ii], self.__nodes[(ii+1)%N])
            if side2 != side1:
                return False
            else:
                side1 = side2
        return True

    def dist(self,x1,y1, x2,y2, x3,y3):
        ##function to get distance of point (x3,y3) from the segment given by P1 and P2
        ##it returns also the point foot (x,y)
    	px = x2-x1
    	py = y2-y1

	denominator = px*px + py*py

    	u =  ((x3 - x1) * px + (y3 - y1) * py) / float(denominator)

    	if u > 1:
        	u = 1
    	elif u < 0:
        	u = 0

    	x = x1 + u * px
    	y = y1 + u * py

    	dx = x - x3
    	dy = y - y3

    	dist = math.sqrt(dx*dx + dy*dy)

    	return (dist, x, y)
    
        
    def isClose (self, x, y, min_distance):
        ## returns True and foot coordinates (xp,yp) if the given point is closer than min_distance from the self element
        
        N = len(self.__nodes)
        for ii in range(0, N):      	
        	(distance, xp, yp) = self.dist(self.__nodes[ii].getX(),self.__nodes[ii].getY(),self.__nodes[(ii+1)%N].getX(),self.__nodes[(ii+1)%N].getY(),x,y)
        	if distance < min_distance:       		
        		return (True, xp, yp)       		
        return (False, -9999, -9999)
        
    
    def interpolate (self, x, y):
        N = len(self.__nodes)
        if N == 3:
            P = self.__nodes[0]
            Q = self.__nodes[1]
            R = self.__nodes[2]
            a =  -(R.getY()*Q.getZ() - P.getY()*Q.getZ() - R.getY()*P.getZ() + Q.getY()*P.getZ() + P.getY()*R.getZ() - Q.getY()*R.getZ())
            b =   P.getY()*R.getX() + Q.getY()*P.getX() + R.getY()*Q.getX() - Q.getY()*R.getX() - P.getY()*Q.getX() - R.getY()*P.getX()
            c =   Q.getZ()*R.getX() + P.getZ()*Q.getX() + R.getZ()*P.getX() - P.getZ()*R.getX() - Q.getZ()*P.getX() - Q.getX()*R.getZ()
            d = -a*P.getX() - b*P.getZ() - c*P.getY()
            if b == 0.0:
                if ((x == P.getX()) and (y == P.getY)):
                    return P.getZ()
                if (x == Q.getX() and y == Q.getY()):
                    return Q.getZ()
                if (x == R.getX() and y == R.getY()):
                    return R.getZ()
                return -9999999 # cv: hack - to prevent division by zero - better do something other?
            return -(a*x+c*y+d) / b
        elif N == 4:
            # quadrilateral mapping
            A = np.array([[1,0,0,0],[1,1,0,0],[1,1,1,1],[1,0,1,0]])
            AI = np.array([[1,0,0,0],[-1,1,0,0],[-1,0,0,1],[1,-1,1,-1]])
            px = []
            py = []
            pz = []
            for nn in self.__nodes:
                px.append(nn.getX())
                py.append(nn.getY())
                pz.append(nn.getZ())
            a = np.dot(AI, px)
            b = np.dot(AI, py)
            # convert global (x,y) to logical (l,m)
            # quadratic equation coeffs, aa*m^2+bb*m+cc=0
            aa = a[3]*b[2] - a[2]*b[3]
            bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3]
            cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1]
            # compute m = (-b+sqrt(b^2-4ac))/(2a)
            try:
                det = math.sqrt(bb*bb - 4.0*aa*cc)
            except:
                return -999
            m = (-bb+det)/(2*aa)
            # compute l
            l = (x-a[0]-a[2]*m)/(a[1]+a[3]*m)
            # finally do interpolation
            if l>=0 and l<=1 and m>=0 and m<=1:
                dl, dm = l, m
                return (1-dl)*(1-dm)*pz[0] + dl*(1-dm)*pz[1] + dl*dm*pz[2] +(1-dl)*dm*pz[3]

    def calcProperties (self):
        N = len(self.__nodes)
        # calculate the element area
        if N==4:
            # quadrilateral element
            a = self.__nodes[0]
            b = self.__nodes[1]
            c = self.__nodes[2]
            d = self.__nodes[3]
            self.__area = abs( 0.5 * np.cross( [c.getX()-a.getX(),c.getY()-a.getY()], [d.getX()-b.getX(),d.getY()-b.getY()] ) )
        elif N==3:
            # triangular element
            a = self.__nodes[0]
            b = self.__nodes[1]
            c = self.__nodes[1]
            d = self.__nodes[2]
            self.__area = abs( 0.5 * np.cross( [c.getX()-a.getX(),c.getY()-a.getY()], [d.getX()-b.getX(),d.getY()-b.getY()] ) )
        else:
            # cross section: area has no meaning
            # but calculate the width of the cross section instead
            length = 0.0
            for ii in range(0, len(self.__edges)-1):
                length = length + self.__edges[ii].getLength()
            self.__area = length
        # calculate the element center coordinates
        x = 0.0
        y = 0.0
        for nn in self.__nodes:
            x = x+nn.getX()
            y = y+nn.getY()
        self.__center = [x/N, y/N, 0.0]
