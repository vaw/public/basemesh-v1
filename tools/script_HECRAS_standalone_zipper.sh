#!/bin/bash
#
# extremely simple bash-script generating the HECRAS2basement.zip file
# for stand-alone 1D mesh conversions
#

echo "Zipping all needed files for the stand-alone HECRAS-2-BASEMENT convert..."
zip HECRAS2basement.zip HECRAS2basement.py CrossSection.py crossSectionConversion.py meshParser1D.py mesh.py 
echo "Zip-file 'HECRAS2basement.zip was created!"
