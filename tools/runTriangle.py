# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

# import of needed libaries / modules
#from qgis.core import *
#from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from ..tools import commonFunctions
from ..tools.mesh import NODE
from ..tools.mesh import EDGE
import numpy as np
import sys, os, platform, re, stat, math, copy

class RUNTRIANGLE:

    def __init__(self, someLayer, precision=10**(-6)):
        # init the coordinate dictionary, which controls the snapping of points etc.
        rect = someLayer.extent()
        minExtent = min(rect.width(),rect.height())
        self.__coordDict = COORDINATEDICTIONARY(precision*minExtent)
        self.__segments = []
        self.__holePoints = []
        self.__regionPoints = []
        self.__optionTags = "-p"
    
    def initialization (self, filename):
        self.__fileID = filename
        # determine triangle name
        if os.name == 'nt':
            if "32bit" in platform.architecture():
                self.__triangleName = "triangle_32.exe"
            else:
                # so far only a 32bit executable is provided, but should run on 64bit machines as well ?!
                self.__triangleName = "triangle_32.exe"
        elif os.name == 'posix':
            if "32bit" in platform.architecture():
                self.__triangleName = "triangle_linux_32"
            else:
                self.__triangleName = "triangle_linux_64"
        else:
            return ('Operating system could not be detected!')
        # check if triangle executable is in the correct directory    
        self.__triangleLocation = commonFunctions.getPluginDir() + '/triangle/' + self.__triangleName
        if not os.path.exists(self.__triangleLocation):
            return ("%s not found. Please copy the executable in the correct directory."%self.__triangleLocation)
        # eventually change the executable's permission (only unix system)
        if os.name == 'posix':
            os.chmod(self.__triangleLocation, stat.S_IEXEC)
        # create directory to store all temporary files
        self.__tempDir = os.path.join( commonFunctions.getPluginDir(), 'triangle_tempfiles')
        try:
            QDir().mkdir(self.__tempDir)
            self.__filePath = os.path.join(self.__tempDir, self.__fileID)
        except OSError:
            return ("Could not create temporary directory '%s'"%self.__tempDir)
        except:
            return ("Failed to create temporary directory")
        # return success
        return True
    
    def writePolyfile (self):
        self.__polyfile = open(self.__filePath+'.poly', 'w')
        # write nodes given in a dictionary
        dimensions = 2
        points = self.__coordDict.getPoints()
        nPoints = len(points)
        self.__polyfile.write("%d %d 1 0\n" %(nPoints, dimensions))
        # order with ascending ID
        tempList= [None]*nPoints
        for pt in points:
            tempList[pt.getID()] = pt
        for pt in tempList:
            self.__polyfile.write( "%d %0.15f %0.15f %0.15f\n" %(pt.getID(), pt.getX(), pt.getY(), pt.getZ()) )
        # write segment given in list
        self.__polyfile.write("%d 1\n" %len(self.__segments))
        for ii, seg in enumerate(self.__segments):
            nodes = seg.getNodes()
            self.__polyfile.write( "%d %d %d\n" %(ii, nodes[0].getID(), nodes[1].getID()) )
        # no holes
        self.__polyfile.write("%d\n" %len(self.__holePoints))
        for ii, point in enumerate(self.__holePoints):
            self.__polyfile.write("%d %0.6f %0.6f\n" %(ii, point.getX(), point.getY()))
        # region points
        self.__polyfile.write("%d\n" %len(self.__regionPoints))
        for ii, point in enumerate(self.__regionPoints):
            self.__polyfile.write("%d %0.6f %0.6f %d %0.6f\n" %(ii, point[0], point[1], point[2], point[3]))
        # finished
        self.__polyfile.close()
        # check if everything worked
        if not os.path.exists(self.__filePath+'.poly'):
            return ("%s.poly not found.\nConversion not successfull - meshing canceled."%self.__filePath)
        else:
            return True
    
    def setOption (self, option, value=True):
        if value:
            if option not in self.__optionTags:
                self.__optionTags += option
        else:
            self.__optionTags.replace(option,'')
    
    def setQuality (self, value):
        if 'q' not in self.__optionTags:
            self.__optionTags += 'q%d'%value
        else:
            self.__optionTags = re.sub(r'q[0-9]*', '', self.__optionTags)
    
    def addExpertArgs (self, argumentString):
        self.__optionTags += argumentString
    
    def getParameters (self):
        return [self.__optionTags, self.__filePath+".poly"]
    
    def getExecutable (self):
        return self.__triangleLocation
    
    def getResultfiles (self):
        nodeFile = "%s.1.node" % self.__filePath
        eleFile = "%s.1.ele" % self.__filePath
        return (nodeFile, eleFile)
    
    def deleteFiles (self):
        try:
            QFile().remove(self.__filePath+'.poly')
            QFile().remove(self.__filePath+'.1.poly')
            QFile().remove(self.__filePath+'.1.node')
            QFile().remove(self.__filePath+'.1.ele')
        except:
            return ('Could not delete temporary files created by TRIANGLE')
        return True

    def addVertices (self, nodeList):
        # store the NODE objects in dictionary with unique key
        for node in nodeList:
            self.__addVertex(node)

    def addLines (self, lineList, nSegments=None):
        if nSegments==None:
            nSegments = [None]*len(lineList)
        for line,nSegs in zip(lineList,nSegments):
            if nSegs!=None and nSegs>0 and nSegs>len(line)-1:
                # here we have to divide the line into a given number of segments
                # calc the total length of the line
                length = 0.0
                for ii in range(0,len(line)-1):
                    length += self.__2pointDistance( line[ii],line[ii+1] )
                dx = length/nSegs # this is the target segment size
                numCells = {}
                # first guess ...
                for ii in range(0,len(line)-1):
                    l = self.__2pointDistance( line[ii],line[ii+1] )
                    numCells[ii]= max(1,round(l/dx)) # at least one segment is needed
                # check for corrections due to rounding effects
                dN = nSegs-int(sum(numCells.values()))
                if dN != 0:
                    count = 0
                    for v in sorted(numCells, key=numCells.get, reverse=True): # correction at the largest segments
                        if count<abs(dN):
                            numCells[v] += math.copysign(1,dN)
                            count += 1
                        else:
                            break
                # dividing the line
                oldLine = line
                line = [ (oldLine[0][0],oldLine[0][1]) ]
                for ii,n in numCells.iteritems():
                    dx = (oldLine[ii+1][0]-oldLine[ii][0])/n
                    dy = (oldLine[ii+1][1]-oldLine[ii][1])/n
                    for ni in range(0,int(n)):
                        line.append( (oldLine[ii][0]+dx*(ni+1), oldLine[ii][1]+dy*(ni+1)) )
            # regular line without special treatment
            for ii, point in enumerate(line):
                if ii < len(line) - 1:
                    # first point of segment
                    node1 = NODE( point[0], point[1] )
                    # second point of segment
                    nextPoint = line[ii+1]
                    node2 = NODE( nextPoint[0], nextPoint[1] )
                    # create new segment and store it in list
                    self.__addSegment( node1, node2 )

    def addRegions (self, regionList):
        for r in regionList:
            if r[3]:
                self.addHole(r[0])
            else:
                self.addRegion(r[0],r[1],r[2])

    def addRegion (self, point, maxArea=None, matID=None):
        area = maxArea if maxArea != None else 0
        material = matID if matID != None else 0
        self.__regionPoints.append( [point.getX(), point.getY(), material, area] )

    def addHole (self, point):
        self.__holePoints.append( point )

    def __addVertex (self,v):
        # check if this vertex already exists
        if self.__coordDict.storeInDict(v):
            # new vertex
            segmentsToSplit = {}
            for ii,s in enumerate(self.__segments):
                # check if new vertex splits an existing segment
                if self.__coordDict.isNodeOnSegment(v,s):
                    # ok, we have to replace that segment by two new ones...
                    segmentsToSplit[ii] = v
            for ii,v in segmentsToSplit.iteritems():
                e1 = EDGE( [self.__segments[ii].getNodes()[0],v] )
                e2 = EDGE( [v,self.__segments[ii].getNodes()[1]] )
                self.__segments[ii] = e1
                self.__segments.append(e2)

    def __addSegment (self,n1,n2):
        self.__addVertex(n1)
        self.__addVertex(n2)
        # eventually get the original nodes at the segment end points
        n1 = self.__coordDict.getNodeFromDict(n1.getX(),n1.getY())
        n2 = self.__coordDict.getNodeFromDict(n2.getX(),n2.getY())
        # and build a new segment out of these
        newSeg = EDGE([n1,n2])
        # do we really have a new segment?
        new = True
        for ii,s in enumerate(self.__segments):
            if s==newSeg:
                new = False
        if new:
            self.__segments.append( newSeg )

    def __2pointDistance (self, p1, p2):
        return math.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)


class COORDINATEDICTIONARY:
    # this class stores nodes in a dictionary.
    # the key of the node is generated from the x- and y- coordinates
    def __init__(self, precision=10**(-6)):
        # setting the precision, which is a double, defines the precsision of the key
        # e.g. 0.00001254 means, that the coordinates include decimal numbers until 10-5
        self.__precision = precision
        # initialize the dictionary
        self.__points = {}
        self.__pointID = 0

    # returns all points that are stored in the dictionary (without keys)
    def getPoints(self):
        return self.__points.values()

    # return all keys
    def getKeys(self):
        return self.__points.keys()

    def getKeyOfNode(self,node):
        return self.__getKeyString(node.getX(), node.getY())
    
    # retreive the node from the dictionary with the coordinates (x,y)
    # returns None, if no such node is stored
    def getNodeFromDict(self, x, y):
        keyString = self.__getKeyString(x, y)
        if self.__points.has_key(keyString):
            return self.__points[keyString]
        else:
            return None

    #retreive the node from the dictionary using the keyString
    # returns None, if no such node is stored        
    def getNodeFromDictByKey(self, keyString):
        if self.__points.has_key(keyString):
            return self.__points[keyString]
        else:
            return None
    
    # returns False if a point with the coordinates of 'node' is already stored in the dictionary
    # returns True if if the 'node' is new (gets a new ID as well)
    def storeInDict(self, node, id=None): # stores in dictionary using a unique keystring
        keyString = self.__getKeyString(node.getX(), node.getY())
        if not self.__points.has_key(keyString):
            if id==None:
                node.setID(self.__pointID)
                self.__pointID += 1
            else:
                node.setID(id)
            self.__points[keyString] = node
            return True
        else:
            return False
    
    # returns a list of nodes that are lying on the line between the two nodes given as arguments
    def getNodesOnSegment(self, n1, n2):
        nodeList = []
        segment = EDGE([n1,n2])
        for node in self.__points.values():
            if self.isNodeOnSegment(node,segment):
                nodeList.append(node)
        return nodeList

    def isNodeOnSegment(self,node,segment):
        x1, y1 = segment.getNodes()[0].getX(), segment.getNodes()[0].getY()
        ks1 = self.__getKeyString(x1, y1)
        x2, y2 = segment.getNodes()[1].getX(), segment.getNodes()[1].getY()
        ks2 = self.__getKeyString(x2, y2)
        keyString = self.__getKeyString(node.getX(), node.getY())
        if (keyString == ks1) or (keyString == ks2): # startpoint and endpoint -> do nothing
            return False
        else:
            xBool = False
            yBool = False
            xp, yp = node.getX(), node.getY() # possible midpoint
            if ks1.split('_')[0]==ks2.split('_')[0]:
                xBool = True
            else:
                xBool = (min(x1, x2) <= xp <= max(x1, x2))
            if ks1.split('_')[1]==ks2.split('_')[1]:
                yBool = True
            else:
                yBool = (min(y1, y2) <= yp <= max(y1, y2))
            if xBool and yBool:
                a = np.array([xp-x1, yp-y1, 0.0])
                b = np.array([x2-xp, y2-yp, 0.0])
                cp = np.cross(a, b)
                if int(np.linalg.norm(cp)/self.__precision) == 0:
                    return True
        return False

    # builds a unique key out of the objects x and y-coordinates
    def __getKeyString(self, x, y):
        # convert coordinates to integer, according to the precision defined earlier
        intX = int(x/self.__precision)
        intY = int(y/self.__precision)
        return "%i_%i" % (intX, intY)