# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import platform, os
from random import random

from ..tools.mesh import MESH,EDGE,NODE

# return the QGIS project name
# if no project name is set, then set it to "Project1"
def getProjectName():
    if QgsProject.instance().title() == "":
        QgsProject.instance().title("Project1")
    return QgsProject.instance().title()


# return the plugin directory.
def getPluginDir():
    return os.path.join(QFileInfo(QgsApplication.qgisUserDbFilePath()).path(), "python/plugins/BASEmesh")


def readAttribute (parent, child):
    child.clear()
    if parent.currentText() != "":
        layername = parent.currentText()
        for name, layer in QgsMapLayerRegistry.instance().mapLayers().iteritems():
            if layer.name() == layername:
                if layer.type() == 0:
                    for index in range(layer.dataProvider().fields().size()):
                        child.addItem(layer.dataProvider().fields()[index].name())
                elif layer.type() == 1:
                    for index in range(1, layer.bandCount()+1):
                        child.addItem(str(index))
    return


def baseInputToggled (checkBox, comboBox, baseInputState):
    if baseInputState:
        checkBox.setEnabled(True)
    else:
        checkBox.setCheckState(False)
        checkBox.setEnabled(False)
        comboBox.setEnabled(False)


def readSelection (boxname):
    for layer in iface.mapCanvas().layers():
        if layer.name() == str(boxname.currentText() ):
            item_found = layer # QgsVectorLayer object
    return item_found


def featureCount (shapes):
    provider = shapes.dataProvider()
    total_features = int(provider.featureCount())
    return total_features


def updateProgressBar (progressBar):
    current_value = progressBar.value()
    progressBar.setValue(current_value + 1)
    return


def defineOutput (widgetObj, lineEdit,  extensionString, outputName = ""): # display file dialog for output shapefile, return selected file path
    lineEdit.clear()
    settings = QSettings()
    key = '/UI/lastShapeDir'
    lastdir = settings.value(key)
    fileDialog = QFileDialog()
    fileDialog.setConfirmOverwrite(False)
    if extensionString==".shp":
        try:    #this try-except block is needed, because QGIS has sometimes seems to return NoneType as project-title (bug)
            if outputName != "" :
                fileName, fileExtension = os.path.splitext(outputName)
                if (fileExtension != ".shp"):
                    outputName = outputName + ".shp"
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output Shapefile", lastdir + "//" + outputName, 'Shapefiles (*.shp)')
            else:
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output Shapefile", lastdir, 'Shapefiles (*.shp)')
        except:
            output_filename = fileDialog.getSaveFileName(widgetObj, "Output Shapefile", lastdir, 'Shapefiles (*.shp)')
    elif extensionString==".2dm":
        try:     #this try-except block is needed, because QGIS has sometimes seems to return NoneType as project-title (bug)
            if outputName != "":
                fileName, fileExtension = os.path.splitext(outputName)
                if (fileExtension != ".2dm"):
                    outputName = outputName + ".2dm"
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output 2dm-file", lastdir + "//" + outputName, '2dm-files (*.2dm)')
            else:
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output 2dm-file", lastdir, '2dm-files (*.2dm)')
        except:
            output_filename = fileDialog.getSaveFileName(widgetObj, "Output 2dm-file", lastdir, '2dm-files (*.2dm)')
    elif extensionString==".bmg":
        try:     #this try-except block is needed, because QGIS has sometimes seems to return NoneType as project-title (bug)
            if outputName != "":
                fileName, fileExtension = os.path.splitext(outputName)
                if (fileExtension != ".bmg"):
                    outputName = outputName + ".bmg"
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output BASEchain-file", lastdir + "//" + outputName, 'BASEchain-files (*.bmg)')
            else:
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output BASEchain-file", lastdir, 'BASEchain-files (*.bmg)')
        except:
            output_filename = fileDialog.getSaveFileName(widgetObj, "Output BASEchain-file", lastdir, 'BASEchain-files (*.bmg)')
    elif extensionString==".g01":
        try:     #this try-except block is needed, because QGIS has sometimes seems to return NoneType as project-title (bug)
            if outputName != "":
                fileName, fileExtension = os.path.splitext(outputName)
                if (fileExtension != ".g01"):
                    outputName = outputName + ".g01"
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output HEC-RAS-file", lastdir + "//" + outputName, 'HEC-RAS-files (*.g01)')
            else:
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output HEC-RAS-file", lastdir, 'HEC-RAS-files (*.g01)')
        except:
            output_filename = fileDialog.getSaveFileName(widgetObj, "Output HEC-RAS-file", lastdir, 'HEC-RAS-files (*.g01)')
    elif (extensionString==".txt"):
        try:     #this try-except block is needed, because QGIS has sometimes seems to return NoneType as project-title (bug)
            if outputName != "":
                fileName, fileExtension = os.path.splitext(outputName)
                if (fileExtension != ".txt"):
                    outputName = outputName + ".txt"
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output textfile", lastdir + "//" + outputName, 'text-files (*.txt)')
            else:
                output_filename = fileDialog.getSaveFileName(widgetObj, "Output textfile", lastdir, 'text-files (*.txt)')
        except:
            output_filename = fileDialog.getSaveFileName(widgetObj, "Output textfile", lastdir, 'text-files (*.txt)')
    
    output_path = QFileInfo(output_filename).absoluteFilePath()
    b = output_path[-4:]
    if b != extensionString:
        output_path = output_path + extensionString
    if output_filename != "":
        lineEdit.clear()
        lineEdit.insert(output_path)
        settings.setValue(key, QFileInfo(output_filename).absolutePath()) # last save folder is saved for next execution
    return


def defineInput (widgetObj, lineEdit,  extensionStrings):
    lineEdit.clear()
    settings = QSettings()
    key = '/UI/lastShapeDir'
    lastdir = settings.value(key)
    fileDialog = QFileDialog()
    fileDialog.setConfirmOverwrite(False)
    formatString = "("
    for ext in extensionStrings:
        formatString = formatString + "*" + ext + " "
    formatString = formatString[0:-1] + ")"
    input_filename = fileDialog.getOpenFileName(widgetObj, "input file", lastdir, formatString)
    path = QFileInfo(input_filename).absoluteFilePath()
    if input_filename != "":
        lineEdit.clear()
        lineEdit.insert(path)
        settings.setValue(key, QFileInfo(input_filename).absolutePath()) # last save folder is saved for next execution
    return


def createMatidRenderer (layer):
    matids = set()
    for f in layer.getFeatures():
        matids.add(f.attribute('MATID'))
    # categorize the color dependent on the material indices
    categories = []
    for value in matids:
        symbol = QgsFillSymbolV2()
        symbol.setColor( QColor.fromRgb(random()*255,random()*255,random()*255) )
        categories.append(QgsRendererCategoryV2(value,symbol,str(value)))
    return QgsCategorizedSymbolRendererV2('MATID',categories)


def extractPoints (layer, withIDs=False, attrField=None):
    # get all points of layer
    # and store the NODE objects in return list
    nodeList = []
    # get index of attribute field
    if attrField!=None:
        attrIndex = layer.fieldNameIndex(attrField)
    else:
        attrIndex = -1
    # get the index of the node ID attribute
    if withIDs:
        IdIndex = layer.fieldNameIndex('NODE_ID')
    else:
        IdIndex = -1
    # get geometry type of features
    if layer.geometryType()==QGis.Point:
        for feat in layer.getFeatures():
            if feat.geometry().type()==QGis.Point: # double check for feature geometry
                x, y = feat.geometry().asPoint()
                z = feat.attributes()[attrIndex] if attrIndex>=0 else 0.0
                node = NODE(x, y, z)
                if IdIndex>=0:
                    node.setID( feat.attributes()[IdIndex] )
                nodeList.append( node )
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()))
    elif layer.geometryType()==QGis.Line:
        for feat in layer.getFeatures():
            if feat.geometry().type()==QGis.Line: # double check for feature geometry
                line = feat.geometry().asPolyline()
                for x, y in line:
                    z =  feat.attributes()[attrIndex] if attrIndex>=0 else 0.0
                    nodeList.append( NODE(x, y, z) )
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()))
    elif layer.geometryType()==QGis.Polygon: # polygons have a specific ring-datastructure
        for feat in layer.getFeatures():
            if feat.geometry().type()==QGis.Polygon: # double check for feature geometry
                ring = feat.geometry().asPolygon()[0]
                for x, y in ring:
                    z = feat.attributes()[attrIndex] if attrIndex>=0 else 0.0
                    nodeList.append( NODE(x, y, z) )
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()))
    else:
        return ("Unknown geometry type in layer '%s'!"%layer.name())
    return nodeList


def addPointsFromNodeLayer(nLayer, elFieldIndex=None, idFromLayer=False):
    if nLayer.geometryType() != QGis.Point:
        return ("layer must be of type 'Point'! no points were added to the CoordinateDictionary...")
    else:
        # storing all nodes in the dictionnary, renumbering IDs automatically
        nodeIdFieldIndex = nLayer.fieldNameIndex('NODE_ID')
        for featNode in nLayer.getFeatures():
            x, y = featNode.geometry().asPoint()
            z = 0.0 if (elFieldIndex == None) else featNode.attributes()[elFieldIndex]
            nodeObject = NODE(x,y,z)
            nodeObject.setAttribute(False)
            id = featNode.attributes()[nodeIdFieldIndex] if idFromLayer else None
            #if not self.storeInDict(nodeObject, id):
            #    return ("node (x = %f, y = %f) already in COORDINATEDICTIONARY; there must not be two nodes with identical x-y coordinates!" % (x, y))
    return True


def extractLines (layer, nCellsField=None):
    # define all segments that are between two points of polygon or line layer features
    lineList = []
    nSegList = []
    # eventually we have to split up some features into a given number of segments
    nCellsIndex = layer.fieldNameIndex(nCellsField) if nCellsField != None else -1
    # determine all segments and their corresponding nodes
    if layer.geometryType()==QGis.Line:
        nSegList = []
        for feat in layer.getFeatures():
            g = feat.geometry()
            if feat.geometry().type()==QGis.Line: # double check for feature geometry
                line = g.asPolyline()
                if nCellsIndex<0:
                    nCells = None
                else:
                    nCells = feat.attributes()[nCellsIndex]
                lineList.append( line )
                nSegList.append( nCells )
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()),None)
    elif layer.geometryType()==QGis.Polygon:
        for feat in layer.getFeatures():
            g = feat.geometry()
            if g.type()==QGis.Polygon: # double check for feature geometry
                lineList.append( g.asPolygon()[0] )
                nSegList.append( None ) # we do not provide dividing the boundary
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()),None)
    else:
        return ("geometry type in layer '%s' not of type 'Line' or 'Polygon'!"%layer.name(),None)
    return (lineList,nSegList)


def extractRegions (layer, areaField, materialField, holeField):
    nodeList = []
    areaList = []
    matidList = []
    holeList = []
    # get index of attribute fields
    areaIndex = layer.fieldNameIndex(areaField) if areaField != None else -1
    materialIndex = layer.fieldNameIndex(materialField) if materialField != None else -1
    holeIndex = layer.fieldNameIndex(holeField) if holeField != None else -1
    # extract geometric points from region layer
    if layer.geometryType()==QGis.Point:
        for feat in layer.getFeatures():
            if feat.geometry().type()==QGis.Point: # double check for feature geometry
                point = feat.geometry().asPoint()
                nodeList.append( NODE(point[0],point[1]) )
                # check if this region is a hole
                hole = None if holeIndex<0 else feat.attributes()[holeIndex]
                if hole!=None and hole>0:
                    # this region is a hole and all elements will be eaten ...
                    areaList.append(None)
                    matidList.append(None)
                    holeList.append(True)
                else:
                    # it is a region that has to be meshed
                    area = None if areaIndex<0 else feat.attributes()[areaIndex]
                    material = None if materialIndex<0 else feat.attributes()[materialIndex]
                    areaList.append(area)
                    matidList.append(material)
                    holeList.append(False)
            else:
                return ("non-matching geometry type in layer '%s' and feature '%s'!"%(layer.name(), feat.id()))
        return map(list, zip(*[nodeList,areaList,matidList,holeList]))
    else:
        return ("geometry type in layer '%s' not of type 'Point'!"%layer.name())