#-------------------------------------------------------------------------------
# Name:        meshParser1D
# Purpose:     Import and Export of 1D cross sections in different formats
#
# Author:      cv
#
# Created:     29.07.2014
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import math
import sys
import os
import CrossSection

try:
	from PyQt4.QtCore import *
	qgisEnvironment = True
except ImportError:
	# the HECras to BASEMENT conversion shall also work as standalone script
	# Hence, we must take care of the case, that no QT-environment is installed on the PC
	qgisEnvironment = False
        
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
# BASEchain-CrossSection converter
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class BASEchainParser:
    _crossSectionsList = []
    
    def __init__(self):
        self._crossSectionsList = []
        pass
    def getNumberCrossSections(self):
        return len(self._crossSectionsList)
    
    def getCrossSection(self, ii):
        return self._crossSectionsList[ii]
        
    def getCrossSections(self):
        return self._crossSectionsList
            
    def parseBASEchain(self, linesFile, successText):
        self._crossSectionsList = []
        # first, extract the cross section blocks from the file (lines)
        crossSectionLinesList = self.__extractCrossSections(linesFile, successText)
        # now, parse each cross section block and create cross section objects
        for crossSectionLines in crossSectionLinesList:
            tagsDict = {}
            self.__readTagsFromCrossSection(crossSectionLines, tagsDict, successText)
            self._crossSectionsList.append( self.createCrossSection(tagsDict, successText) )   
        return True
            
    def writeCrossSectionToBasement(self, fobj, crossSection, reference_dist = -1E32 ):
        # first, check if there are some geometry points
        if crossSection.getNrPoints() <= 0:
            return "no points within cross-section '%s'. This must not be the case." % self.name

        # write some general cross section data
        # we need to determine the distance to the reference point,
        # here we expect the name to be the "Kilometrierung"
        fobj.write("CROSS_SECTION {\n")
        fobj.write("name \t = %s\n" % crossSection.getName())
        if reference_dist < -9999999:
            distance = crossSection.getDistance() / 1000.0  #distance must be given in km in BASEchain!
        else:
            distance = abs(float(crossSection.getName()) - reference_dist) / 1000.0 #distance must be given in km in BASEchain!
        fobj.write("distance_coord \t = %f\n" % distance)
        self.__writeGeoReferencedData(fobj, crossSection)
        self.__writeBanks(fobj, crossSection)
        self.__writeLevees(fobj, crossSection)
        self.__writeFlowRange(fobj, crossSection)
        self.__writeFriction(fobj, crossSection)     
        self.__writeGeometry(fobj, crossSection)   
        fobj.write("\n}\n\n")
        return True
        
        
    def __writeGeoReferencedData(self, fobj, crossSection):
        # write the data for global reference coordinates, if given
        # determine the orientation (angle) of the cross section (only possible if geo referenced!)
        if crossSection.isGeoReferenced():
            x,y,z = crossSection.getLeftCoordinate()
            fobj.write("left_point_global_coords \t = (%f, %f, %f)\n" % (x, y, crossSection.getPointZ(0)))
            fobj.write("orientation_angle = %f\n" % crossSection.getOrientationAngle())
                
    def __writeBanks(self, fobj, crossSection):
        if crossSection.hasBanks():
            # write the main channel range 
            # this is quite easy
            leftBank, rightBank = crossSection.getBanks()
            fobj.write("main_channel_range \t = (%f, %f)\n" % (leftBank, rightBank))
        
    def __writeLevees(self, fobj, crossSection):
        # write Levees
        # left and right levee must be given --> the active range is set in between
        # since the levee coordinates in HECRAS m ust not be the same as the node coordinates
        # we are using the nearest neighbouring nodes
        if crossSection.hasLevees():
            levee_left, levee_right = crossSection.getLevees()
            indices_left = crossSection.findNeighborPoints(levee_left)
            index_left_levee = -1
            if crossSection.getPointZ(indices_left[0]) > crossSection.getPointZ(indices_left[1]):
                index_left_levee = indices_left[0]
            else:
                index_left_levee = indices_left[1]
            indices_right = crossSection.findNeighborPoints(levee_right)
            index_right_levee = -1
            if crossSection.getPointZ(indices_right[0]) > crossSection.getPointZ(indices_right[1]):
                index_right_levee = indices_right[0]
            else:
                index_right_levee = indices_right[1]
            fobj.write("active_range \t = (%f, %f)\n" % (crossSection.getPointX(index_left_levee), crossSection.getPointX(index_right_levee)))
            
    def __writeFlowRange(self, fobj, crossSection):
                # write flow range
        # this concept differs from HECRAS 
        # - in HECRAS the "inneffectiv flow range" is specified
        # - in BASEMENT we defined the "effective flow range"
        if crossSection.hasFlowRange():
            flow_range_left, flow_range_right = crossSection.getFlowRange()
            indices_left = crossSection.findNeighborPoints(flow_range_left)
            index_left_flow = -1
            if crossSection.getPointZ(indices_left[0]) > crossSection.getPointZ(indices_left[1]):
                index_left_flow = indices_left[0]
            else:
                index_left_flow = indices_left[1]
            indices_right = crossSection.findNeighborPoints(flow_range_right)
            index_right_flow = -1
            if crossSection.getPointZ(indices_right[0]) > crossSection.getPointZ(indices_right[1]):
                index_right_flow = indices_right[0]
            else:
                index_right_flow = indices_right[1]
            fobj.write("water_flow_range \t = (%f, %f)\n" % (crossSection.getPointX(index_left_flow), crossSection.getPointX(index_right_flow)))
            
    def __writeFriction(self, fobj, crossSection):
         # write friction ranges and friction values
        # HECRAS allows for arbitrary numbers of friction ranges/values (not only left floodplain, main, right floodplain!)
        if crossSection.hasFriction():        
            friction_ranges = ""
            first = True
            for ii, location in enumerate(crossSection.getFrictionRanges()):
                nextLocation = 0.0
                if ii < len(crossSection.getFrictionRanges())-1:
                    nextLocation = crossSection.getFrictionRange(ii+1)
                else:
                    nextLocation = crossSection.getPointX(crossSection.getNrPoints()-1)
                if location - nextLocation != 0.0:
                    if not first:
                        friction_ranges += ", "
                    else:
                        first = False
                    friction_ranges += "(%f,%f)" % (location, nextLocation)
            if friction_ranges != "":
                fobj.write("friction_ranges \t = (\n %s \n)\n" % friction_ranges)
             
            friction_coefficients = ""
            first = True
            for ii,friction in enumerate(crossSection.getFrictions()):
                location = crossSection.getFrictionRange(ii)
                nextLocation = 0.0
                if ii < len(crossSection.getFrictionRanges())-1:
                    nextLocation = crossSection.getFrictionRange(ii+1)
                else:
                    nextLocation = crossSection.getPointX(crossSection.getNrPoints()-1)
                if location - nextLocation != 0.0:
                    if not first:
                        friction_coefficients += ", "
                    else:
                        first = False
                    friction_coefficients += "%f" % friction
            if friction_coefficients != "":
                fobj.write("friction_coefficients \t = ( %s )\n" % friction_coefficients)
                
    def __writeGeometry(self, fobj, crossSection):
        # write geometry of the cross section
        # this is quite easy
        node_string = ""
        for ii in range(0,crossSection.getNrPoints()):
            node_string += "(%f,%f)" % (crossSection.getPointX(ii),crossSection.getPointZ(ii))
            if ii == crossSection.getNrPoints()-1:
                node_string += "\n"
            else:
                node_string += ",\n"
        fobj.write("node_coords \t =(\n  %s )" % node_string)            
            
        
    def __extractCrossSections(self, linesFile, successText):
        # split the lines into cross sections
        # assumption: each cross section starts with "CROSS_SECTION" and ends with "}"
        list_cs = []
        for ii,line in enumerate(linesFile):
            if line.find("CROSS_SECTION") >= 0:
                crossSectionLines = [] 
                jj = ii
                counter = 0
                openBrackets = 0
                found = False
                while (not found):
                    for char in line:
                        if char == "{":
                            openBrackets += 1
                        if char == "}":
                            openBrackets -= 1
                    if len(crossSectionLines) > 2 and openBrackets == 0:
                        found = True
                    crossSectionLines.append(line)   
                    jj += 1                    
                    if (jj >= len(linesFile)-1):
                        found = True
                    else:
                        line = linesFile[jj]
                    if counter > 1000:
                        return "no closing '}' could be found."
                list_cs.append(crossSectionLines)
            if line.find("SOIL_DEF") >= 0:
                successText.append("Soil definition found --> is ignored!")
        return list_cs
                
    def __readTagsFromCrossSection(self, crossSectionLines, tagsDict, successText):
        tagsDict["name"]  = self.__readTag(crossSectionLines, "name", successText)
        tagsDict["distance_coord"] = self.__readTag(crossSectionLines, "distance_coord", successText)
        tagsDict["node_coords"] = self.__readTag(crossSectionLines, "node_coords", successText)
        tagsDict["main_channel_range"] = self.__readTag(crossSectionLines, "main_channel_range", successText)
        tagsDict["friction_ranges"] = self.__readTag(crossSectionLines, "friction_ranges", successText)
        tagsDict["friction_coefficients"] = self.__readTag(crossSectionLines, "friction_coefficients", successText)  
        tagsDict["active_range"] = self.__readTag(crossSectionLines, "active_range", successText)
        tagsDict["left_point_global_coords"] = self.__readTag(crossSectionLines, "left_point_global_coords", successText)
        tagsDict["orientation_angle"] = self.__readTag(crossSectionLines, "orientation_angle", successText)
        
    def createCrossSection(self, tagsDict, successText):
        #create new cross section object
        crossSection = CrossSection.CrossSection()
        # parce cross section for attributes
        result = tagsDict["name"]
        try:
            result = result.strip()
            if result.find("*") >= 0:
                result = result.replace("*", "")
                successText.append("The char '*' is not alowed in cross section names and is replaced by ''")
        except AttributeError as e:
            successText.append("TypeError '%s'.\n Cross section name cannot be read!" % str(e) )
            pass
        crossSection.setName(result)
        result = tagsDict["distance_coord"]
        crossSection.setDistance( float(result))
        result = tagsDict["node_coords"]
        listX, listY = self.__splitString2D(result)
        for ii in range(0,len(listX)):
            crossSection.addPoint(float(listX[ii]), float(listY[ii]))
        result = tagsDict["main_channel_range"]
        try:
            if result != "" and len(result) > 0:
                leftBank, rightBank = self.__splitString2D(result)
                crossSection.setBanks(float(leftBank[0]), float(rightBank[0]))
                crossSection.setHasBanks(True)
        except TypeError as e:
            successText.append("TypeError '%s'.\n Occured at conversion of main channel range at cross section '%s'. No main_channel_range considered." % (str(e), crossSection.getName()) )
            crossSection.setHasBanks(False)
            pass
        result = tagsDict["friction_ranges"]
        try:
            if result != "" and len(result) > 0:
                listX, listX2 = self.__splitString2D(result)
                result = tagsDict["friction_coefficients"]
                listFric = self.__splitString1D(result)
                for ii in range(0,len(listFric)):
                    crossSection.addManningFriction(float(listX[ii]), float(listFric[ii]))
                crossSection.setHasFriction(True)
        except TypeError as e:
            successText.append("TypeError '%s'.\n Occured at conversion of friction ranges at cross section '%s'. No friction considered." % (str(e), crossSection.getName()) )
            crossSection.setHasFriction(False)
            pass
        result = tagsDict["active_range"]
        try:
	    if qgisEnvironment:
		    if not type(result) is QPyNullVariant:
			if len(result) > 0:
			    left, right = self.__splitString2D(result)
			    crossSection.setLevees(float(left[0]), float(right[0]))
	    else:
		    if len(result) > 0:
			left, right = self.__splitString2D(result)
			crossSection.setLevees(float(left[0]), float(right[0]))
        except TypeError as e:
            successText.append("TypeError '%s'.\n Occured at conversion of active_range at cross section '%s'. No active_range considered." %  (str(e), crossSection.getName()) )
            pass
        result = tagsDict["left_point_global_coords"]
        try:
            if result != "":
                x,y,z = self.__splitString3D(result)
                crossSection.setLeftCoordinate( float(x[0]), float(y[0]))
                crossSection.setGeoReferencing(True)
            else:
                crossSection.setGeoReferencing(False)
        except TypeError as e:
            successText.append("TypeError '%s'.\n Occured at conversion of global coordinates at cross section '%s'. No geo referencing considered." % (str(e), crossSection.getName()) )
            crossSection.setGeoReferencing(False)
            pass
        result = tagsDict["orientation_angle"]
        try:
            if result != "":
                angle = self.__splitString1D(result)
                crossSection.setOrientationAngle( float(angle[0]) )
                crossSection.setGeoReferencing(True)
            else:
                crossSection.setOrientationAngle(90)
                crossSection.setGeoReferencing(False)
        except TypeError as e:
            successText.append("TypeError '%s'.\n Occured at conversion of orientation angle at cross section '%s'. No geo referencing considered." % (str(e), crossSection.getName()) )
            crossSection.setOrientationAngle(90)
            crossSection.setGeoReferencing(False)
            pass
        
        return crossSection
        
        
    def __readTag(self, cs_list, keyword, successText):
        keyline = ""
        found = False
        for ii, line in enumerate(cs_list):
            if line.find(keyword) >= 0 or found:
                if not (found):
                    tokens = line.split("=")
                    keyline += tokens[1]
                    found = True
                else:
                    keyline += line
                nrOpenBrackets = 0          
                nrCloseBrackets = 0
                for letter in keyline:
                    if letter == '(':
                        nrOpenBrackets += 1
                    if letter == ')':
                        nrCloseBrackets += 1
                if nrOpenBrackets == nrCloseBrackets:
                    # remove zeilenumbrueche
                    keyline.replace("\\n", "")
                    keyline.replace("\r\n", "")
                    # removing leading and ending spaces
                    keyline.rstrip()
                    keyline.lstrip()
                    break                
        if keyline == "":
            successText.append("WARNING: tag '%s' not found" % keyword)
        return keyline
            
    def __splitString2D(self, stringWord):
        list1 = []
        list2 = []
        result = stringWord.replace("(", " ")
        result = result.replace(")", " ")        
        tokens = result.split(",")
        last1 = False
        for token in tokens:
            token = token.strip()
            token = token.replace("\r\n", "")
            if not last1:
                list1.append(token)
                last1 = True
            else:
                list2.append(token)
                last1 = False
        return list1, list2
                
    def __splitString1D(self, stringWord):
        list1 = []
        result = stringWord.replace("(", " ")
        result = result.replace(")", " ")        
        tokens = result.split(",")
        for token in tokens:
            token = token.strip()
            token.replace("\r\n", "")
            list1.append(token)
        return list1
        
    def __splitString3D(self, stringWord):
        list1 = []
        list2 = []
        list3 = []
        result = stringWord.replace("(", " ")
        result = result.replace(")", " ")        
        tokens = result.split(",")
        last1 = False
        last3 = True
        for token in tokens:
            token = token.strip()
            token = token.replace("\r\n", "")
            if last3:
                list1.append(token)
                last1 = True
                last3 = False
            elif last1:
                list2.append(token)
                last1 = False
                last3 = False
            else:
                list3.append(token)
                last1 = False
                last3 = True
        return list1, list2, list3
    
    
    
    
    
    
    
    
    
    
            
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# HECRAS cross section converter
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class HECRASParser:
    class Keywords():
        PROFILE = "#Sta/Elev"
        FRICTION = "#Mann"
        GIS = "XS GIS Cut Line"
        MAIN_CHANNEL = "Bank Sta"
        REACH = "Reach XY"
        LEVEE = "Levee"
        INACTIVE_RANGE = "XS Ineff"
        
    __KEYWORDS = Keywords()
    __BLOCK_TYPES = {}
    _crossSectionsList = []
    _originalNrCrossSections = []
    listIgnored = []
    listIgnored2 = []
    listMustNotHave = []
    listMustNotMissing = []
    streamlineList = []     
    __SINGLE = 8
    __DOUBLE = 16

    """A class parsing HECRAS-blocks """
    def __init__(self):
        # HEC-RAS geometry block types
        # we do only support type 1 = cross section!
        self.__BLOCK_TYPES[1] = "cross section"
        self.__BLOCK_TYPES[2] = "culvert"
        self.__BLOCK_TYPES[3] = "bridge" 
        self.__BLOCK_TYPES[4] = "multiple opening"
        self.__BLOCK_TYPES[5] = "lateral weir"
        self.__BLOCK_TYPES[6] = "inline weir"
        # initializations
        self._crossSectionsList = []
        self._originalNrCrossSections = []
        self.listIgnored = []
        self.listIgnored2 = []
        self.listMustNotHave = []
        self.listMustNotMissing = []
        self.streamlineList = []  
        pass
        
    def getNumberCrossSections(self):
        return len(self._crossSectionsList)
        
    def getCrossSections(self):
        return self._crossSectionsList
        
    def getNumberOriginalCrossSections(self):
        return self._originalNrCrossSections
        
    def getCrossSection(self,ii):
        return self._crossSectionsList[ii]
    
    def parseHECRAS(self, linesFile, successText):
        self._crossSectionsList = []
        self.streamlineList = []         
        #split file into cross sections
        crossSectionsLinesList = self.__extractCrossSections(linesFile, successText)
        xList = []
        yList = []        
        if self.__extractStreamLine(linesFile, successText):
            self.__parseBlockXY(self.streamlineList, self.Keywords.REACH, xList, yList, 2, 16, successText)
            successText.append("Reach XY with '%d' points found in HEC-RAS data." % len(xList))
        else:
            successText.append("No Reach XY found in HEC-RAS data.")
             
        # parse all the cross sections
        distance = 0.0
        for ii, cs_lines in enumerate(crossSectionsLinesList): 
            #create new cross section
            cs = CrossSection.CrossSection()
            #add cross section to the cross section list
            self._crossSectionsList.append(cs)
            
            line = cs_lines[0]
            if line.find(",") == -1:
                successText.append( "ERROR: line %s has no name defined" % line )
                return "Error"
            tokensEqual = line.split("=")
            if len(tokensEqual) < 2:
                successText.append("ERROR: line '%s' has no right side defined" & line)
            tokens = tokensEqual[1].split(",")
            try:
                cs_type = int(tokens[0])
            except ValueError as e:
                successText.append("TypeError '%s'.\nType of object '%s' could not be determined, something is wrong here" % (str(e), tokens[0]))
                
            cs_name = tokens[1].rstrip()
            cs_name = cs_name.replace("*","")
            cs.setName(cs_name)
            for ii in range(0, len(xList)):
                cs.addReachPointXY(xList[ii], yList[ii])
                
            try:
                cs_downstream_length = float(tokens[3]) # tokens[2] and tokens[4] are downstream lengths of floodplains, which we ignore
            except ValueError as e:
                # no downstream length found, this may be correct e.g. at the last (downstream) cross-section
                if ii < len(crossSectionsLinesList) - 1:
                    successText.append("TypeError '%s'.\nNo downstream length given in cross-section '%s', someting is wrong here. (set to 100)" % (str(e), cs_name))
                    cs_downstream_length = 100.0
            cs.setDistance(distance)
            distance += cs_downstream_length    
                
            # extract all elevation data
            if not self.__parseCrossSectionProfile(cs_lines, cs, successText):
                successText.append( "...cross section '%s' contains no profile data" % cs.getName() )
                return "No cross section profile data found in %s..." % cs.getName()
                
            # try to extract geoferencing information of cross section (optional)
            # this must be done AFTER reading the profile
            self.__parseGeoReferencing(cs_lines, cs, successText)
                
            # extract all manning friction values (optional)
            self.__parseManningFriction(cs_lines, cs, successText)
                
            # extract main channel information  (optional)
            self.__parseMainChannel(cs_lines, cs, successText)
    
            # extrect Levees - optional
            self.__parseLevees(cs_lines, cs, successText)
    
            # extract ineffective flow area - attention: the concept differs from BASEMENT!
            self.__parseFlowRange(cs_lines, cs, successText)
            
          # try do determine the global coordinates using the Reach XY
        if not self._crossSectionsList[0].isGeoReferenced() and self._crossSectionsList[0].hasReachXY(): 
            self.__determineGlobalCoordinatesFromReach(successText)
            
        return True
        
        
    def __determineGlobalCoordinatesFromReach(self, successText):
        # no geo referencing was found
        # try to determine global coordinates using the river reach!
        # 1. determine the total length of reach
        lengthReach = self._crossSectionsList[len(self._crossSectionsList)-1].getDistance()
        # 2. determine total length of reach in XY "screen" coordinates
        lengthReachScreen = 0.0
        for ii in range (0, self._crossSectionsList[0].getNrReachXY()-1):
            x,y = self._crossSectionsList[0].getReachPointXY(ii)
            x2,y2 = self._crossSectionsList[0].getReachPointXY(ii+1)
            delta_x = x2 - x
            delta_y = y2 - y
            lengthReachScreen += (delta_x**2 +delta_y**2)**0.5
        
        for crossSection in self._crossSectionsList:
            successText.append("nr reach = %d" % crossSection.getNrReachXY())
            dist_scaled = 0.0
            dist_scaled_old = 0.0
            dist_real_scaled = crossSection.getDistance() / lengthReach # 0-1
            for ii in range(0, crossSection.getNrReachXY()-1):
                x,y = crossSection.getReachPointXY(ii)
                x2,y2 = crossSection.getReachPointXY(ii+1)
                x_scaled = x * lengthReach / lengthReachScreen
                y_scaled = y * lengthReach / lengthReachScreen
                x2_scaled = x2 * lengthReach / lengthReachScreen
                y2_scaled = y2 * lengthReach / lengthReachScreen
                dist_scaled_old = dist_scaled
                delta_x = abs(x2_scaled - x_scaled)
                delta_y = abs(y2_scaled - y_scaled)
                length =  (delta_x**2 +delta_y**2)**0.5
                length_scaled = (delta_x**2 +delta_y**2)**0.5 / lengthReach
                dist_scaled += length_scaled  # 0-1
                if (dist_scaled >= dist_real_scaled):
                    m = (dist_real_scaled - dist_scaled_old) / length_scaled
                    if m > 1.0:
                        successText.append("ATTENTION: m = %f, there is something strange." % m)
                    x_res = x_scaled  + m * delta_x
                    y_res = y_scaled  + m * delta_y
                    
                    x_left = x_res + 0.5 * crossSection.getWidth()  * delta_y / length
                    y_left = y_res - 0.5 * crossSection.getWidth()  * delta_x / length
                    x_right = x_res  - 0.5 * crossSection.getWidth()  * delta_y / length
                    y_right = y_res + 0.5 * crossSection.getWidth()  * delta_x / length
                    crossSection.setLeftCoordinate(x_left, y_left)
                    crossSection.setRightCoordinate(x_right, y_right)
                    crossSection.updateOrientationAngle()
                    crossSection.setGeoReferencing(True)
        return True
        
    def writeHECRASheader(self, fobj, cs_list):
        # write header
        fobj.write("Geom Title=%s\r\n\r\n" % "Converted_BASEmesh")
        fobj.write("Program Version=4.10\r\n")
        # write river reach
        fobj.write("River Reach=%s  ,1\r\n" % "River_Reach")
        cs = cs_list[0]
        if not cs.isGeoReferenced():
            fobj.write("%s= 2\r\n" % self.Keywords.REACH)
            fobj.write("%16.6f%16.6f" % (0.0, 0.0))
            fobj.write("%16.6f%16.6f\r\n" % (1.0, 1.0))
        else:
            fobj.write("%s= %d\r\n" % (self.Keywords.REACH, len(cs_list)))
            for ii, cs in enumerate(cs_list):
                x,y,z = cs.getLeftCoordinate()
                fobj.write("%16.7f%16.7f" % (x, y))
                if (ii > 0) and ((ii+1) % 2) == 0:
                    fobj.write("\r\n")
        fobj.write("\r\n\r\n")

    def writeCrossSectionToHECRAS(self, fobj, ii, crossSectionList):
        crossSection = crossSectionList[ii]
        # first, check if there are some geometry points
        if (crossSection.getNrPoints() <= 0):
            return "no points within cross-section '%s'. This must not be the case." % crossSection.getName()
        
        # determine the downstream reach length and write it to file
        downstream_reach_length = 0.0
        index_downstream = ii + 1
        if index_downstream < len(crossSectionList):
            downstream_reach_length = abs(crossSection.getDistance() - crossSectionList[index_downstream].getDistance())
        fobj.write("Type RM Length L Ch R = 1 ,%f,%f,%f,%f\r\n" % (crossSection.getDistance(), downstream_reach_length,downstream_reach_length,downstream_reach_length))
        self.__writeGeoReferencedData(fobj, crossSection)
        self.__writeProfileData(fobj, crossSection)
        self.__writeFrictionData(fobj, crossSection)        
        self.__writeLeveeData(fobj, crossSection)
        self.__writeBankStation(fobj, crossSection)      
      
        # footer stuff
        fobj.write("XS Rating Curve= 0 ,0\r\n")
        fobj.write("Exp/Cntr=0.3,0.1\r\n")
        fobj.write("\r\n")
        return True
        
    def __writeGeoReferencedData(self, fobj, crossSection):
        # determine and write geo-referenced data
        if crossSection.isGeoReferenced():
            fobj.write("%s=2\r\n" % self.__KEYWORDS.GIS)
            x1,y1,z = crossSection.getLeftCoordinate()
            crossSection.updateRightCoordinate()
            x2,y2,z = crossSection.getRightCoordinate()
            fobj.write("%16.7f%16.7f%16.7f%16.7f\r\n" % (x1,y1,x2,y2))
        
    def __writeProfileData(self, fobj, crossSection):
         #write elevation data
        fobj.write("%s= %d\r\n" % (self.Keywords.PROFILE, crossSection.getNrPoints()))
        for ii in range(0, crossSection.getNrPoints()):
            # if we write 8.4 and we have >= 4 digits before the commata, 
            # than the output has 9 digits! Strange? I just output with .3 to prevent such problems...
            fobj.write("%8.3f%8.3f" % (crossSection.getPointX(ii), crossSection.getPointZ(ii)))
            if (ii < crossSection.getNrPoints()-1) and ((ii+1)  % 5  == 0):
                fobj.write("\r\n")
        fobj.write("\r\n")
        
    def __writeFrictionData(self, fobj, crossSection):
        #write friction data
        if crossSection.hasFriction():
            fobj.write("%s= %d,0,-1\r\n" % (self.__KEYWORDS.FRICTION, crossSection.getNrFrictionRanges()))
            last = False
            for ii in range(0, crossSection.getNrFrictionRanges()): 
                last = False
                fobj.write("%8.4f%8.4f%8.4f" % (crossSection.getFrictionRange(ii), crossSection.getFriction(ii), 0.0))
                if (ii > 0) and ((ii+1) % 3 == 0):
                    fobj.write("\r\n")
                    last = True
            if not last:
                fobj.write("\r\n")
                
    def __writeLeveeData(self, fobj, crossSection):
        # Levee
        if crossSection.hasLevees():
            levee_left, levee_right = crossSection.getLevees()
            indices_left = crossSection.findNeighborPoints(levee_left)
            index_left_levee = -1
            if crossSection.getPointZ(indices_left[0]) > crossSection.getPointZ(indices_left[1]):
                index_left_levee = indices_left[0]
            else:
                index_left_levee = indices_left[1]
            indices_right = crossSection.findNeighborPoints(levee_right)
            index_right_levee = -1
            if crossSection.getPointZ(indices_right[0]) > crossSection.getPointZ(indices_right[1]):
                index_right_levee = indices_right[0]
            else:
                index_right_levee = indices_right[1]
            fobj.write("active_range \t = (%f, %f)\n" % (crossSection.getPointX(index_left_levee), crossSection.getPointX(index_right_levee)))
            fobj.write("%s=-1,%6.2f,%6.2f,-1,%6.2f,%6.2f,,\r\n" % (self.__KEYWORDS.LEVEE, crossSection.getPointX(index_left_levee), crossSection.getPointZ(index_left_levee), crossSection.getPointX(index_right_levee), crossSection.getPointZ(index_right_levee)))
            
    def __writeBankStation(self, fobj, crossSection):
        if crossSection.hasBanks():
            #Bank Station
            leftBank, rightBank = crossSection.getBanks()
            fobj.write("%s=%f,%f\r\n" % (self.Keywords.MAIN_CHANNEL, leftBank, rightBank))
    
    def __extractStreamLine(self, linesFile, successText):
        for ii,line in enumerate(linesFile):
            jj = ii
            if line.find(self.__KEYWORDS.REACH) >= 0:
                while (linesFile[jj].strip() != ""):
                    self.streamlineList.append(linesFile[jj])
                    jj += 1
                return True
        return False
    
    # split the lines into cross sections
     #!! Assumption: a cross section starts with "Type" and ends with an empty line !!
    def __extractCrossSections(self, linesFile, successText):
        crossSectionsLinesList = []
        self._originalNrCrossSections = 0
        for ii,line in enumerate(linesFile):
            jj = ii
            crossSectionLines = [] 
            blockType = 0
            error = False
            if line.find("Type") >= 0:
                tokensEqual = line.split("=")
                if len(tokensEqual) > 1:
                    tokensCommata = tokensEqual[1].split(",")
                    if len(tokensCommata) > 1:
                        try:
                            blockType = int(tokensCommata[0])
                        except ValueError as e:
                            successText.append("ATTENTION: Block '%s' is unknown and ignored. (Error: '%s')" % (tokensEqual[1], str(e)))
                            error = True
                        if not error:
                            if blockType > 6 or blockType < 1:
                                successText.append("ATTENTION: Block Type = '%d' is unknown and ignored." % blockType)
                                error = True
                            elif blockType != 1:
                                successText.append("ATTENTION: Block Type = '%s' is not supported and will be ignored." % self.__BLOCK_TYPES[blockType])
                            else:
                                while (linesFile[jj].strip() != ""):
                                    crossSectionLines.append(linesFile[jj])
                                    jj += 1
                                crossSectionsLinesList.append(crossSectionLines)
        self._originalNrCrossSections = len(crossSectionsLinesList)
              
        # delete all "cross sections" which do not have each of these obligatory key-words       
        self.listMustNotMissing = [ self.__KEYWORDS.PROFILE ]
        self.listIgnoredNr = [0,0,0]
        for ii,listName in enumerate(self.listMustNotMissing):
            indexList = self.__crossSectionsMissingData(crossSectionsLinesList, listName)
            if len(indexList) > 0:
                successText.append( "...%d cross sections without '%s' data were found. These cross sections will be ignored." % (len(indexList), listName) )
                self.listIgnoredNr[ii] = len(indexList)
                for jj in indexList:
                    crossSectionsLinesList[jj] = []
                self.__delEmptyItemsFromList(crossSectionsLinesList)
        return crossSectionsLinesList
        
    def __parseBlockXY(self, stringList, keyword, xList, yList, nrLineData, precisionInt, successText):
        # parse coordinates of River Reach
        for ii, line in enumerate(stringList):
            if line.find(keyword) >= 0:
                tokens1 = line.split("=")
                if len(tokens1) < 2:
                    successText.apppend("Data could not be parsed. This block is ignored.")
                    return False
                tokens2 = tokens1[1].split(",")
                try:
                    nrPoints = int(tokens2[0])
                except ValueError as e:
                    successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is ignored." % (str(e), keyword) )
                    return False            
                counterPoints = 0
                jj = ii + 1
                while (True):
                    lineXY = stringList[jj]
                    start = 0
                    for kk in range(0,nrLineData):
                        x = lineXY[start:start+precisionInt]
                        start += precisionInt
                        y = lineXY[start:start+precisionInt]
                        start += precisionInt
                        try:
                            xList.append(float(x))
                            yList.append(float(y))
                        except ValueError as e:
                            successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is ignored." % (str(e), keyword ))
                            xList = []
                            yList = []       
                            return False
                        counterPoints += 1
                        if counterPoints >= nrPoints:
                            return True	
                    jj += 1
        return False
        
        
    def __parseBlockXYZ(self, stringList, keyword, xList, yList, zList, nrLineData, precisionInt, successText):
        # parse coordinates of River Reach
        for ii, line in enumerate(stringList):
            if line.find(keyword) >= 0:
                tokens1 = line.split("=")
                if len(tokens1) < 2:
                    successText.apppend("Data could not be parsed. This block is ignored.")
                    return False
                tokens2 = tokens1[1].split(",")
                try:
                    nrPoints = int(tokens2[0])
                except ValueError as e:
                    successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is ignored." % (str(e), keyword) )
                    return False            
                counterPoints = 0
                jj = ii + 1
                while (True):
                    lineXY = stringList[jj]
                    start = 0
                    for kk in range(0,nrLineData):
                        x = lineXY[start:start+precisionInt]
                        start += precisionInt
                        y = lineXY[start:start+precisionInt]
                        start += precisionInt
                        z = lineXY[start:start+precisionInt]
                        start += precisionInt
                        try:
                            xx = float(x)
                            xList.append(xx)
                        except ValueError as e:
                            successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is set to 0.0." % (str(e), keyword ))
                            xList.append(0.0)
                        try:
                            yy =  float(y)
                            yList.append(yy)
                        except ValueError as e:
                            successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is set to 0.0." % (str(e), keyword ))
                            yList.append(0.0)
                        try:
                            zz = float(z)
                            zList.append(zz)
                        except ValueError as e:
                            successText.append( "ValueError '%s'.\nWARNING. %s data could not be parsed and is set to 0.0." % (str(e), keyword ))
                            zList.append(0.0)
                        counterPoints += 1
                        if counterPoints >= nrPoints:
                            return True	
                    jj += 1
        return False
        
        
    #parse main channel information
    # return True if main channel information was found, else False
    def __parseMainChannel(self, cs_lines, crossSection, successText):
        for ii, bank_line in enumerate(cs_lines):
            if bank_line.find(self.__KEYWORDS.MAIN_CHANNEL) >= 0:           
                tokens = bank_line.split("=")
                tokens2 = tokens[1].split(",")
		station1 = 0.0
		station2 = 0.0
		try:
                	station1 = float(tokens2[0]) 
		except ValueError as e:
			successText.append("ValueError '%s' at parsing of %s.\nWARNING. Left bank station '%s' is not a number. It is set to 0.0." % (str(e), self.__KEYWORDS.MAIN_CHANNEL, tokens2[0]))
			station1 = 0.0
		try:	
			station2 = float(tokens2[1])
		except ValueError as e:
			successText.append("ValueError '%s' at parsing of %s.\nWARNING. Right bank station '%s' is not a number. It is set to 0.0." % (str(e), self.__KEYWORDS.MAIN_CHANNEL, tokens2[1]))
			station2 = 0.0
			
		crossSection.setBanks( station1, station2)                
		crossSection.setHasBanks(True)
		return True
        return False
        
    #parse for geo-referencing data
    # HEC-RAS specifies hier a list of points, representing the cross section
    # we simply assume a straigt line and there take only the first and the last point of the cross section
    # using these data, we can compute the orientation angle for BASEchain
    # return True if geo referencing information was found, else False
    def __parseGeoReferencing(self, cs_lines, crossSection, successText):
        xList = []
        yList = []
        # determine total number of geo-references points
        # in HEC-RAS this can be a arbirtrary number
        # however, we are interested only in the first and the last point
        # (since we assume that each cross-section is a straight line!)
        if self.__parseBlockXY(cs_lines, self.__KEYWORDS.GIS, xList, yList, 2, self.__DOUBLE, successText):
            if len(xList) < 2:
                successText.append("Nr of XS GIS points '%d' is not sufficient. No geo-referencing taken into account for cross section '%s'" % (len(xList), crossSection.getName()))
                crossSection.setOrientationAngle(90)
                crossSeFalsection.setGeoReferencing(False)
                return False
            crossSection.setLeftCoordinate(xList[0], yList[0])
            crossSection.setRightCoordinate(xList[len(xList)-1], yList[len(yList)-1])
            crossSection.updateOrientationAngle()
            crossSection.setGeoReferencing(True)
            return True
        else:
            # i give up, there is no geo-referencing...
            crossSection.setOrientationAngle(90)
            crossSection.setGeoReferencing(False)
            return False
    
    #parse cross section profile data
    # return True if cross section data was found, else False
    def __parseCrossSectionProfile(self, cs_lines, crossSection, successText):
         # extract all elevation data
        xList = []
        yList = []
        if self.__parseBlockXY(cs_lines, self.__KEYWORDS.PROFILE, xList, yList, 5, self.__SINGLE, successText):
            for ii in range(0, len(xList)):
                crossSection.addPoint(float(xList[ii]), float(yList[ii]))
            return True
        return False
        
    # parse Manning friction values
    # return True if manning values were found, else False
    def __parseManningFriction(self,  cs_lines, crossSection, successText):
        locationList = []
        manningList = []
        dummyList = []
        if self.__parseBlockXYZ(cs_lines, self.__KEYWORDS.FRICTION, locationList, manningList, dummyList, 3, self.__SINGLE, successText):        
            for ii in range(0, len(locationList)):
                if manningList[ii] != 0.0:
                    crossSection.addManningFriction(locationList[ii], manningList[ii])
            # special treatment if only 1 friction point found
            # here, we set the same friction up to the end of the cross section
            if crossSection.getNrFrictionRanges() == 1:
                crossSection.addManningFriction(crossSection.getPointX(crossSection.getNrPoints()-1), manningList[0])
            crossSection.setHasFriction(True)
            return True
        else:
            successText.append( "WARNING. Strange with manning friction at cross section '%s'. Friction is ignored." % crossSection.getName() )
            crossSection.setHasFriction(False)
            return False
                
#        for ii, man_line in enumerate(cs_lines):
#            if man_line.find("Mann") == 1:
#                tokens = man_line.split("=")
#                tokens2 = tokens[1].split(",")
#                nrManningValues = int(tokens2[0])
#                if nrManningValues < 0:
#                    successText.append( "Error parsing file. Something is wrong with Manning values in cross section '%s'. Cannot proceed." % crossSection.getName() )
#                    return False
#                if nrManningValues == 0:
#                    successText.append( "Cross section '%s' has 0 Manning values." % crossSection.getName() )
#                    break
#                crossSection.setHasFriction(True)
#                jj = ii + 1
#                while (True):
#                    nextline = cs_lines[jj]
#                    start = 0
#                    for kk in range(3):
#                        try:
#                            # first value (8 letters) = x-coordinate
#                            location = float(nextline[start:start + 8])
#                        except ValueError as e:
#                            successText.append( "ValueError '%s'.\nWARNING. Strange manning location at cross section '%s'. Set to 0.0." % (str(e), crossSection.getName()) )
#                            location = 0
#                        start += 8
#                        try:
#                            # seconde value (8 letters) = manning value
#                            friction = float(nextline[start:start + 8])
#                        except ValueError as e:
#                            successText.append( "ValueError '%s'.\nWARNING. Strange with manning friction at cross section '%s'. Set to 0.0." % (str(e), crossSection.getName()) )
#                            friction = 0
#                        start += 8
#                        # third value (8 letters) = ??? 
#                        start += 8
#                        
#                        # add values to cross section
#                        crossSection.addManningFriction(location, friction)
#                        # stop parsing if all manning values were found
#                        if crossSection.getNrManningValues() >= nrManningValues:
#                            crossSection.setHasFriction(True)
#                            return True
#                    jj += 1
#        successText.append( "WARNING. Strange with manning friction at cross section '%s'. Set to 0.0." % crossSection.getName() )
#        crossSection.setHasFriction(False)
#        return False
                       
    # parse Levee-Information
    # return True if levees were found, else False
    def __parseLevees(self, cs_lines, crossSection, successText):
        for ii, levee_line in enumerate(cs_lines):
            if levee_line.find(self.__KEYWORDS.LEVEE) == 0:
                successText.append( "Levee was found in cross section '%s'..." % crossSection.getName() )
                tokens1 = levee_line.split("=")
                tokens2 = tokens1[1].split(",")
                if tokens2[0] <> "-1" or tokens2[3] <> "-1":
                    successText.append( "Both two levees must be defined in cross section '%s'. Cannot use levee data." % crossSection.getName() )
                    return False
                crossSection.setLevees(float(tokens2[1]), float(tokens2[4]))
                return True
        return False
                
    # parse ineffective flow range
    # return True if ineffective flow range was found, else False            
    def __parseFlowRange(self, cs_lines, crossSection, successText):
        for ii, flowrange_line in enumerate(cs_lines):
            if flowrange_line.find("%s= 1 ,-1" % self.__KEYWORDS.INACTIVE_RANGE) >= 0:
                nextline = cs_lines[ii+1]
                successText.append( "Water flow range was found in cross section '%s'..." % crossSection.getName() )
                crossSection.setFlowRange(float(nextline[0:8]), float(nextline[9:16]))
                return True
        return False
                
                
    #check if there is a line beginning with data, at EACH cross section
    #if not, then add the index to a list and return the list
    def __crossSectionsMissingData(self, crossSectionsList,data):
        notFoundList = []
        for ii,cs_lines in enumerate(crossSectionsList):
            found = False
            for line in cs_lines:
                if line.find(data) == 0:
                    found = True
                    break
            if not found:
                notFoundList.append(ii)
        return notFoundList
        
    #check if there is a section with this data type
    #if yes, then add the index to a list and return the list
    def __crossSectionsHavingData(self, crossSectionsList,data):
        foundList = []
        for ii,cs_lines in enumerate(crossSectionsList):
            for line in cs_lines:
                if line.find(data) == 0:
                    foundList.append(ii)
                    break
        return foundList
        
    # delete all empty lists [] contained within another list
    def __delEmptyItemsFromList(self, List):
        # determine number of empty entries which shall be deleted
        nrDelets = 0    
        for subList in List:
            if len(subList) <= 0:
                nrDelets += 1
        # delete all empty entries from the list
        ii = 0
        delCounter = 0
        while (delCounter < nrDelets):
            if len(List[ii]) == 0:
                del List[ii]
                delCounter += 1
            else:
                ii += 1

