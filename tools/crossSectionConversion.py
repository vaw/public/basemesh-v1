# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""    
import math
import CrossSection
import meshParser1D
import commonFunctions
from mesh import MESH

try:
	from qgis.core import *
	from qgis.utils import *
	from PyQt4.QtCore import *
	from PyQt4.QtGui import *
	qgisEnvironment = True
except ImportError:
	# the HECras to BASEMENT conversion shall also work as standalone script
	# Hence, we must take care of the case, that no QT-environment is installed on the PC
	qgisEnvironment = False
        


class CROSSSECTIONCONVERSION:
    __mesh = None
    __crossSectionList = []    
    
    # class to create BASEchain "meshes", that is:
    # one cross section is an element. containing all the cross sectional information (stored in the attributes)
    # nodes contain the elevation information
    def __init__ (self):
        self.__mesh = None
        self.__crossSectionList = []    
        pass
    
    def getMesh (self):
        return self.__mesh
    
    def setMesh (self, mesh):
        self.__mesh = mesh
        pass
    
    def readShape(self, meshLayer, successText, progressBar=None):
        # clear the cross section list
        self.__crossSectionList = []  
        # determine the indices of the attribute table
        idNameIndex = meshLayer.fieldNameIndex("name")
        idDistanceIndex = meshLayer.fieldNameIndex("distance_c")
        idLeftCoord = meshLayer.fieldNameIndex("left_point")
        idOrientationAngle = meshLayer.fieldNameIndex("orientatio")
        idMainChannel = meshLayer.fieldNameIndex("main_chann")
        idFrictionRanges = []
        idFrictionCoefficients = []
        for ii in range(1,11):
            idFrictionRanges.append( meshLayer.fieldNameIndex("fric_r%d" % ii) )
            idFrictionCoefficients.append( meshLayer.fieldNameIndex("fric_c%d" % ii) )
        idActiveRange = meshLayer.fieldNameIndex("active_ran")
        idNodeCoords = []        
        for ii in range(1,100):
            idNodeCoords.append( meshLayer.fieldNameIndex("node_co%d" % ii) )
        
        # do all the work
        # first, we store the values of the attribute table in a dictionary
        # the, we parse the dictionary with the same methods 
        # as used for parsing bmg-files (this saves a lot of work)
        parser = meshParser1D.BASEchainParser()
        for feat in meshLayer.getFeatures():
            attrs = feat.attributes()
      
            # store attributes in dictionary
            tagsDict = {}
            tagsDict["name"] = attrs[idNameIndex]
            tagsDict["distance_coord"] = str(attrs[idDistanceIndex])
            tagsDict["left_point_global_coords"] = attrs[idLeftCoord]
            tagsDict["orientation_angle"] = str(attrs[idOrientationAngle])
            tagsDict["main_channel_range"] = attrs[idMainChannel]
            tmp_string = ""
            for ii in range(1,11):
                text = attrs[idFrictionRanges[ii-1]]
                if text != None:
                    tmp_string += text
            tagsDict["friction_ranges"] = tmp_string
            tmp_string = ""
            for ii in range(1,11):
                text = attrs[idFrictionCoefficients[ii-1]]
                if text != None:
                    tmp_string += text
            tagsDict["friction_coefficients"] = tmp_string
            tagsDict["active_range"] = attrs[idActiveRange]
            tmp_string = ""
            for ii in range(1,100):
                text = attrs[idNodeCoords[ii-1]]
                if text != None:
                    tmp_string += text
            tagsDict["node_coords"] = tmp_string
            # create the cross section from the dictionary data using the BASEchain-parser
            self.__crossSectionList.append( parser.createCrossSection(tagsDict, successText) )
            
            # update progress bar
            if progressBar!=None:
                commonFunctions.updateProgressBar(progressBar)
        return True


    def readBMG (self,  fileID, successText):
        # import .bmg file, generated by BASEchain
        try:
            bmgFile = open(fileID+'.bmg', 'r')
            # read whole file into a list and close the file
            data = bmgFile.readlines()
            bmgFile.close()
        except:
            return ("Error: could not open the file '%s.bmg'!" % fileID)
    
        parser = meshParser1D.BASEchainParser()
        success = parser.parseBASEchain(data, successText)
        if not success:
            return "parsing failed: %s" % success
        self.__determineStatTextBASEchain(parser, successText)
	self.__crossSectionList = parser.getCrossSections()
        return self.__createMeshFromCrossSections(parser.getCrossSections())
        
    def readHECRAS (self,  fileID, successText):
        # import .bmg file, generated by BASEchain
        try:
            HECRASfile = open(fileID, 'r')
            # read whole file into a list and close the file
            data = HECRASfile.readlines()
            HECRASfile.close()
        except:
            return ("Error: could not open the file '%s'!" % fileID)
    	
        parser = meshParser1D.HECRASParser()
        success = parser.parseHECRAS(data, successText)
        if not success:
            for line in successText:
                success += line
            return "parsing failed:\n%s" % success
        self.__determineStatTextHECRAS(parser, successText)
	self.__crossSectionList = parser.getCrossSections()
        return self.__createMeshFromCrossSections(parser.getCrossSections())
        
    def __createMeshFromCrossSections(self, crossSectionsList):
        nodeCounter = 1
        elementCounter = 1
        self.__mesh = MESH()
        self.__mesh.newAttribute('name')
        self.__mesh.newAttribute('distance')
        for crossSection in crossSectionsList:
            nodeIDs = []
            # create nodes
            if  crossSection.isGeoReferenced():
                for x, y, z in crossSection.getGlobalPoints():
                    self.__mesh.addNode( nodeCounter, x, y, z )
                    nodeIDs.append(nodeCounter)
                    nodeCounter = nodeCounter+1
            else:
                for x,y,z in crossSection.getPoints():
                    self.__mesh.addNode( nodeCounter, x, y, z )
                    nodeIDs.append(nodeCounter)
                    nodeCounter = nodeCounter+1
            # create element
            self.__mesh.addElement( elementCounter, nodeIDs )
            # define attributes
            self.__mesh.setAttr( elementCounter, ('name', crossSection.getName()) )
            self.__mesh.setAttr( elementCounter, ('distance', crossSection.getDistance()) )
            self.__mesh.getElement(elementCounter).__crossSection = crossSection
            elementCounter = elementCounter + 1  
        # return success
        success = self.__mesh.init()
        if success != True:
            return success
        else:
            return True
    
    def writeBMG(self, fileName, progressBar=None):
        if len(self.__crossSectionList) <= 0:
            return "error, there are no cross sections present. "
        parser = meshParser1D.BASEchainParser()
        # write bmg-file
        fobj = open("%s.bmg" % fileName,"w")
        try:
            for cs in  self.__crossSectionList:
                parser.writeCrossSectionToBasement(fobj, cs)
        finally:
            fobj.close()
        # write additional text-file with cross-section names
        # this eases the preparation of the command-file .bmc
        fobj = open("%s_cross_section_names.txt" % fileName, "w")
        try:	
            counter = 1
            for cs in self.__crossSectionList:
                if counter > 10:
                    fobj.write("\r\n")
                    counter = 1
                fobj.write("%s " % cs.getName())
        finally:
            fobj.close()

        return True
        
    def writeHECRAS(self, fileName, progressBar=None):
        if len(self.__crossSectionList) <= 0:
            return "error, there are no cross sections present. "
        parser = meshParser1D.HECRASParser()
        fobj = open("%s.g01" % fileName,"w")
        try:
            parser.writeHECRASheader(fobj, self.__crossSectionList)
            for ii, cs in enumerate(self.__crossSectionList):
                parser.writeCrossSectionToHECRAS(fobj, ii, self.__crossSectionList)
        finally:
            fobj.close()
 
        return True
        
    def __determineStatTextBASEchain(self, parser, text):
        messages = []        
        for line in text:
            messages.append(line)
        text = []
        text.append("\n---------------------------------------------------")
        text.append("Conversion finished.")
        text.append("Messages:\n")
        for line in messages:
            text.append(line)
        text.append("\n---------------------------------------------------")
        text.append("A total number of %d sections were found." % parser.getNumberCrossSections())
        text.append( "A total number of %d cross sections were converted." % parser.getNumberCrossSections())
        text.append( "Have fun with BASEMENT!")
        text.append( "---------------------------------------------------")
        
    def __determineStatTextHECRAS(self, parser, text):
        messages = []        
        for line in text:
            messages.append(line)
        text.append("\n---------------------------------------------------")
        text.append("Conversion finished.")
        text.append("Messages:\n")
        for line in messages:
            text.append(line)
        text.append("\n---------------------------------------------------")
        text.append("A total number of %d sections were found." % parser.getNumberOriginalCrossSections())
        for ii,Type in enumerate(parser.listMustNotMissing):
            if parser.listIgnoredNr[ii] > 0:
                text.append( "Data '%s' is missin in %d cross sections. These were ignored. " % (Type, parser.listIgnoredNr[ii]))
        for ii,Type in enumerate(parser.listMustNotHave):
             if parser.listIgnoredNr2[ii] > 0:
                text.append( "Type '%s' is not supported. %d structures were ignored. " % (Type, parser.listIgnoredNr2[ii]))     
        text.append( "A total number of %d cross sections were converted." % parser.getNumberCrossSections())
        text.append( "Have fun with BASEMENT!")
        text.append( "---------------------------------------------------")
                
    def writeShape (self, outputFileID,  progressBar=None):
        # get coordinate reference system of the project
        crs = iface.mapCanvas().mapRenderer().destinationCrs()
        # create the new shape file for nodes
        fields = QgsFields()
        fields.append(QgsField("NODE_ID", QVariant.Int))
        fields.append(QgsField("X", QVariant.Double))
        fields.append(QgsField("Y", QVariant.Double))
        fields.append(QgsField("Z", QVariant.Double))
        try:
            writer = QgsVectorFileWriter(outputFileID+'_points', "CP1250", fields, QGis.WKBPoint, crs, "ESRI Shapefile")
            for ptID in self.__mesh.getNodeIDs():
                ptFeat = QgsFeature(fields) # create destination feature
                ptFeat.setFeatureId(ptID)
                # define feature geometry
                pt = self.__mesh.getNode(ptID)
                ptFeat.setGeometry(QgsGeometry.fromPoint( QgsPoint( pt.getX(), pt.getY() ) ))
                # fill fields with contents of nodefile
                ptFeat.setAttribute(0, ptID)
                ptFeat.setAttribute(1, pt.getX())
                ptFeat.setAttribute(2, pt.getY())
                ptFeat.setAttribute(3, pt.getZ())
                writer.addFeature(ptFeat)
                if progressBar!=None:
                    commonFunctions.updateProgressBar(progressBar)
        finally:
            # delete the writer to flush features to disk (optional)
            del writer
        if not os.path.exists(outputFileID+'_points.shp'):
            return ("shape-file with nodes could not be created.")
        # create the new shape file for cells
        fields = QgsFields()
        fields.append(QgsField("ELEMENT_ID", QVariant.Int))
        # here we have a problem:
        # large cross sections have tons of node_ids -->
        # these do not fit in a 255 character-attribute-field
        # hence, we need to populate LOTS of attribute fields
        for ii in range(1,20):
            fields.append(QgsField("NODE_IDs%d" % ii, QVariant.String, "test", 254))
        fields.append(QgsField("name", QVariant.String))
        fields.append(QgsField("distance_coord", QVariant.Double))
        fields.append(QgsField("left_point_global_coords", QVariant.String, "test", 254))
        fields.append(QgsField("orientation_angle", QVariant.Double))
        fields.append(QgsField("main_channel_range", QVariant.String, "test", 254))
        # again, for the friction and the node-coordinates we populate
        # many attribute fields, to ensure that there is enough space
        for ii in range(1,11):
            fields.append(QgsField("fric_r%d" % ii, QVariant.String, "test", 254))
        for ii in range(1,11):
            fields.append(QgsField("fric_c%d" % ii, QVariant.String, "test", 254))
        fields.append(QgsField("active_range", QVariant.String, "test", 254))
        for ii in range(1,100):
            fields.append(QgsField("node_co%d" % ii, QVariant.String, "text", 254))

        try:
            writer = QgsVectorFileWriter(outputFileID+'_crossSections', "CP1250", fields, QGis.WKBLineString, crs, "ESRI Shapefile")
            for elID in self.__mesh.getElementIDs():
                elFeat = QgsFeature(fields) # create destination feature
                elFeat.setFeatureId(elID)
                attribute_index = 0
                elFeat.setAttribute(attribute_index, elID) # write ID to attribute field
                attribute_index += 1
                qgsPointList = []
                #write node id list to attribute field
                # attention: shapefiles have a limit of 254 characters per attribute
                # hence, we must split the node coordiantes into many attributes fields (here: 20) 
                idString = ""
                idStringList = []
                for ii, nid in enumerate(self.__mesh.getElementNodeIDs(elID)):
                    if ii < len(self.__mesh.getElementNodeIDs(elID))-1:
                        idString += str(nid) + ","
                        if len(idString) > 230:
                            idStringList.append(idString)
                            idString = ""
                    else:
                        idString += str(nid)
                        idStringList.append(idString)
         
                    pt = self.__mesh.getNode(nid)
                    qgsPointList.append( QgsPoint(pt.getX(), pt.getY()) )
                attribute_index_store = attribute_index
                for ii, text in enumerate(idStringList):
                    elFeat.setAttribute(attribute_index, text)
                    attribute_index += 1
                attribute_index = attribute_index_store + 19
                
                # cross section name
                elFeat.setAttribute(attribute_index, self.__mesh.getAttr(elID, 'name'))
                attribute_index += 1
                # distance_coord
                elFeat.setAttribute(attribute_index, self.__mesh.getAttr(elID, 'distance'))
                attribute_index += 1
                # global left coordinate
                crossSection = self.__mesh.getElement(elID).__crossSection
                x,y,z = crossSection.getLeftCoordinate()
                elFeat.setAttribute(attribute_index, '(%s,%s,%s)' % (x,y,z))
                if not crossSection.isGeoReferenced():
                    x,y,z = crossSection.getPoints()[0]
                    elFeat.setAttribute(attribute_index, '(%s,%s,%s)' % (x,y,z) )
                attribute_index += 1
                # orientation angle
                elFeat.setAttribute(attribute_index, '%f' % crossSection.getOrientationAngle())
                attribute_index += 1
                # main channel
                if crossSection.hasBanks():
                    left, right = crossSection.getBanks()
                    elFeat.setAttribute(attribute_index, '(%s,%s)' % (left, right))
                attribute_index += 1
                # friction ranges
                if crossSection.hasFriction():
                    strings_list = []
                    tmp_string = "("
                    for ii,item in enumerate(crossSection.getFrictionRanges()):
                        if ii < len(crossSection.getFrictionRanges())-1:
                            tmp_string += "(%s,%s)," % (crossSection.getFrictionRange(ii), crossSection.getFrictionRange(ii+1))
                        else:
                            tmp_string += "(%s,%s))" % (crossSection.getFrictionRange(ii), crossSection.getPointX(crossSection.getNrPoints()-1))
                        if len(tmp_string) > 230:
                            strings_list.append(tmp_string)
                            tmp_string = ""
                    strings_list.append(tmp_string)
                    for ii,line in enumerate(strings_list):
                        elFeat.setAttribute(attribute_index + ii, line)
                attribute_index += 10
                # friction coefficients
                if crossSection.hasFriction():
                    strings_list = []
                    tmp_string = "("
                    for ii,item in enumerate(crossSection.getFrictions()):
                        tmp_string += "%s" % item
                        if ii < len(crossSection.getFrictions())-1:
                            tmp_string += ","
                        if len(tmp_string) > 230:
                            strings_list.append(tmp_string)
                            tmp_string = ""
                    tmp_string += ")"
                    strings_list.append(tmp_string)
                    for ii,line in enumerate(strings_list):
                        elFeat.setAttribute(attribute_index + ii, line)
                attribute_index += 10
                #active range
                if crossSection.hasLevees():
                    left, right = crossSection.getLevees()
                    elFeat.setAttribute(attribute_index, "(%s,%s)" % (left, right))
                attribute_index += 1
                # node_coordinates
                # attention: shapefiles have a limit of 254 characters per attribute
                # hence, we must split the node coordiantes into many attributes fields (here: 100)
                tmp_string = "("
                stringList = []
                for ii in range(0, crossSection.getNrPoints()):
                    x = crossSection.getPointX(ii)
                    z = crossSection.getPointZ(ii)
                    if ii < crossSection.getNrPoints()-1:
                        tmp_string += "(%f,%f)," % (x, z)
                        if len(tmp_string) > 200:
                            stringList.append(tmp_string)
                            tmp_string = ""
                    else:
                        tmp_string += "(%f,%f))" % (x, z)
                        stringList.append(tmp_string)
       
                for ii,string in enumerate(stringList):
                    elFeat.setAttribute(attribute_index, string)
                    attribute_index += 1
                
                # define feature geometry
                elFeat.setGeometry(QgsGeometry.fromPolyline( qgsPointList ))
                # finally add the feature
                writer.addFeature(elFeat)
                if progressBar!=None:
                    commonFunctions.updateProgressBar(progressBar)
        finally:
            # delete the writer to flush features to disk (optional)
            del writer
        if not os.path.exists(outputFileID+'_crossSections.shp'):
            return ("shape-file with cells could not be created.")
        else:
            return True
