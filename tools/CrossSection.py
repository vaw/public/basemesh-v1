#-------------------------------------------------------------------------------
# Name:        CrossSection.py
# Purpose:     Contains information about BASEchain cross sections
#
# Author:      cv
#
# Created:     29.07.2014
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import math



# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Cross Section class
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class CrossSection:
    """A class containing the cross section information
       and writing the information in *.bmg-format  """
    __name = "empty"
    __listX = []
    __listZ = []
    __manningX = []
    __manningFric = []
    __hasFriction = False
    __geo_x = 0.0
    __geo_y = 0.0
    __geo_x2 = 0.0
    __geo_y2 = 0.0
    __leftBank = 0.0
    __rightBank = 0.0
    __hasBanks = False
    __levees_found = False
    __levee_left = 0.0
    __levee_right = 0.0
    __flow_range_found = False
    __flow_range_left = 0.0
    __flow_range_right = 0.0
    __distance = 0.0
    __orientationAngle = -1000.0
    __isGeoReferenced = False
    __listReachX = []
    __listReachY = []

    def __init__(self):
        self.__name = "empty"
        self.__listX = []
        self.__listZ = []
        self.__manningX = []
        self.__manningFric = []
        self.__hasFriction = False
        self.__geo_x = 0.0
        self.__geo_y = 0.0
        self.__geo_x2 = 0.0
        self.__geo_y2 = 0.0
        self.__leftBank = 0.0
        self.__rightBank = 0.0
        self.__hasBanks = False
        self.__levees_found = False
        self.__levee_left = 0.0
        self.__levee_right = 0.0
        self.__flow_range_found = False
        self.__flow_range_left = 0.0
        self.__flow_range_right = 0.0
        self.__distance = 0.0
        self.__orientationAngle = -1000.0
        self.__isGeoReferenced = False
        self.__listReachX = []
        self.__listReachY = []
        
    def hasReachXY(self):
        return len(self.__listReachX) > 0
        
    def getNrReachXY(self):
        return len(self.__listReachX)
        
    def addReachPointXY(self, x, y):
        self.__listReachX.append(x)
        self.__listReachY.append(y)
    
    def getReachPointXY(self, ii):
        return self.__listReachX[ii], self.__listReachY[ii]  
        
    def getReachLength(self):
        dist = 0.0
        for ii in range(self.getNrReachXY()-1):
            dist += ((self.__listReachX[ii+1] - self.__listReachX[ii])**2 + (self.__listReachY[ii+1] - self.__listReachY[ii])**2)**0.5
        return dist

    def addPoint(self, x,y):
        self.__listX.append(x)
        self.__listZ.append(y)
        
    def getNrPoints(self):
        return len(self.__listX)
        
    def getPointX(self, ii):
        return self.__listX[ii]
        
    def getPointZ(self, ii):
        return self.__listZ[ii]
        
    def getWidth(self):
        return abs(self.__listX[self.getNrPoints()-1] - self.__listX[0])
        
    def getBanks(self):
        return self.__leftBank, self.__rightBank
        
    def setBanks(self, leftBank, rightBank):
        self.__leftBank = leftBank
        self.__rightBank = rightBank
        
    def hasBanks(self):
        return self.__hasBanks
        
    def setHasBanks(self, banks):
        self.__hasBanks = banks
        
    def addManningFriction(self, x, manning):
        self.__manningX.append(x)
        self.__manningFric.append(manning)
        
    def getNrFrictionRanges(self):
        return len(self.__manningFric)
    
    def getFrictionRanges(self):
        return self.__manningX
        
    def getFrictionRange(self, ii):
        return self.__manningX[ii]
    
    def getFriction(self, ii):
        return self.__manningFric[ii]
        
    def getFrictions(self):
        return self.__manningFric
        
    def hasFriction(self):
        return self.__manningFric
    
    def setHasFriction(self, fric):
        self.__hasFriction = fric
        
    def setLevees(self, left, right):
        self.__levees_found = True
        self.__levee_left = left
        self.__levee_right = right
    
    def hasLevees(self):
        return self.__levees_found
    
    def getLevees(self):
        return self.__levee_left, self.__levee_right
    
    def setFlowRange(self, left, right):
        self.__flow_range_found = True
        self.__flow_range_left = left
        self.__flow_range_right = right
        
    def getFlowRange(self):
        return self.__flow_range_left, self.__flow_range_right
        
    def hasFlowRange(self):
        return self.__flow_range_found
        
    def addBanks(self, left, right):
        self.__leftBank = left
        self.__rightBank = right
        
    def getNrManningValues(self):
        return len(self.__manningX)
        
    def getName(self):
        return self.__name
        
    def setName(self, name):
        self.__name = name
        
    def setDistance(self, distance):
        self.__distance = distance   
    
    def getDistance(self):
        return self.__distance
        
    def isGeoReferenced(self):
        return self.__isGeoReferenced #(self.__geo_x != 0.0 or self.__geo_y != 0.0 or self.__orientationAngle != -1000.0)

    def getLeftCoordinate(self):
        return self.__geo_x, self.__geo_y, self.__listZ[0]   
        
    def setLeftCoordinate(self, x, y):
        self.__geo_x = x
        self.__geo_y = y
    
    def setRightCoordinate(self, x, y):
        self.__geo_x2 = x
        self.__geo_y2 = y
        
    def getRightCoordinate(self):
        return self.__geo_x2, self.__geo_y2, self.__listZ[self.getNrPoints()-1]
        
    def setGeoReferencing(self, geo):
        self.__isGeoReferenced = geo
    
    def updateRightCoordinate(self):
        x1 = self.__geo_x
        y1 = self.__geo_y
        s = self.__listX[len(self.__listX)-1] - self.__listX[0]
        rad = math.radians(self.__orientationAngle)
        self.setRightCoordinate(x1 + math.cos( rad ) * s, y1 + math.sin( rad ) * s)
        
    def updateOrientationAngle(self):
        angle = 0.0
        if (self.__geo_x2 - self.__geo_x != 0.0):
            angle = math.atan2((self.__geo_y2 - self.__geo_y), (self.__geo_x2 - self.__geo_x))
            angle = math.degrees(angle)
        if angle < 0.0:
            angle = 360 + angle
        self.__orientationAngle = angle
        
    def getOrientationAngle(self):
        return self.__orientationAngle
        
    def setOrientationAngle(self, orientationAngle):
        self.__orientationAngle = orientationAngle
    
    def getGlobalPoints(self):
        xi = []
        yi = []
        zi = []
        for xx, zz in zip( self.__listX, self.__listZ):
            radiant = math.radians(self.__orientationAngle)
            dx = xx-self.__listX[0]
            xi.append( self.__geo_x + math.cos( radiant ) * dx )
            yi.append( self.__geo_y + math.sin( radiant ) * dx )
            zi.append( zz )
        return zip(xi, yi, zi)    
        
    def getPoints(self):
        xi = []
        yi = []
        zi = []
        index = 0
        half = int(len(self.__listX) / 2)
        for xx,zz in zip( self.__listX, self.__listZ):
            radiant = math.radians(self.__orientationAngle)
            xi.append( self.getDistance() + math.cos( radiant ) * xx )
            yi.append(0.0 + math.sin(radiant) * (xx - self.__listX[half]))
            zi.append( zz )
            index += 1
        return zip(xi, yi, zi)
    
    # find the index of the points within the cross section
    # which is next (left and right) to the given coordinates
    def findNeighborPoints(self, point):
        min_index_left = -1
        min_distance_left = 999999999
        for ii in range(0,self.getNrPoints()):
            distance_left = point - self.__listX[ii]
            if distance_left >= 0 and distance_left < min_distance_left:
                min_index_left = ii
                min_distance_left = distance_left
        min_index_right = -1
        min_distance_right = 999999999
        for ii in range(0,self.getNrPoints()):
            distance_right = self.__listX[ii] - point
            if distance_right >= 0 and distance_right < min_distance_right:
                min_index_right = ii
                min_distance_right = distance_right
        return (min_index_left, min_index_right)
        
 