#-------------------------------------------------------------------------------
# Name:        HECRAS2basement
# Purpose:     Conversion of HECRAS input files to BASEchain grid files
#
# Author:      cv
#
# Created:     29.07.2014
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import math
import sys
import os
import CrossSection
import crossSectionConversion
#import meshParser1D              
        
#************************************************
# main routine
#************************************************
def main():
    # check user given input filename
    filename = ""
    if (len(sys.argv) <= 1):
        print "No HEC-RAS geometry input file specified. Nothing can be done."
        sys.exit()
    else:
        filename = sys.argv[1]
    if not os.path.isfile(filename):
        print "The filename '%s' is no valid file" % filename
        sys.exit()
    print "\n"
    print "/   |  \_   _____/\_   ___ \______   \  /  _  \  /   _____/ "
    print "/   |   \    __)_ /    \  \/|       _/ /  /_\  \ \_____  \  "
    print "\   |   /        \\     \___|    |   \/    |    \/        \ "
    print "\___|_ /_______  / \______  /____|_  /\____|__  /_______  / "
    print ""
    print "                        to                                 "
    print ""
    print " ______   _____    ______ ____   _____   ____   _____/  |_ "
    print "|    |  _/\__  \  /  ___// __ \ /     \_/ __ \ /    \   __\ "
    print "|    |   \ / __ \_\___ \\  ___/|  | |  \  ___/|   |  \  |   "
    print "|______  /(____  /____  >\___  >__|_|  /\___  >___|  /__|   \n"


    # determine file extension
    fileName, fileExtension = os.path.splitext(filename)
    
    # Convert from HECRAS --> BASEchain
    if fileExtension.find('.g') >= 0:
         # create mesh from .bmg-file
        csConv = crossSectionConversion.CROSSSECTIONCONVERSION()
        text = []
        success = csConv.readHECRAS( fileName + fileExtension, text )
        if not success:
           print success
	   print "hallo"
           exit(1)
        messages = "MESSAGES:\n"
        for line in text:
            messages += line + "\n"
        print messages
        
        filename = "converted"
        success = csConv.writeBMG(filename)
        if not success:
            print success
            exit(1)

    # Convert from BASEchain --> HECRAS
    elif fileExtension == '.bmg':
        csConv = crossSectionConversion.CROSSSECTIONCONVERSION()
        text = []
        success = csConv.readBMG( fileName + fileExtension, text )
        if not success:
            print success
            exit(1)
        messages = "MESSAGES:\n"
        for line in text:
            messages += line + "\n"
        print messages   
        
        filename = "converted"
        success = csConv.writeHECRAS(filename)
        if not success:
            print success
            exit(1)
    else:
        print "The file format '%s' is unknwon. Script terminated." % fileExtension
        
    print "\n---------------------------------------------------"
    print "Conversion finished."    
    print "---------------------------------------------------"

    pass

if __name__ == '__main__':
    main()
