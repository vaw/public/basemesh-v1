# -*- coding: utf-8 -*-
"""
 BASEmesh: a QGIS plugin for the creation of computational meshes
 for the numerical modelling software BASEMENT using Jonathan Shewchuk's mesh
 generator 'Triangle'.

 Copyright (C) 2013 Florian Hinkelammert and Christian Volz, ETH Zurich
 hinkelammert@vaw.baug.ethz.ch / volz@vaw.baug.ethz.ch

 This QGIS Plugin is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 For the mesh generator Triangle, the licence regulations are not obvious. It's
 author Jonathan Shevchuk has the copyright on the software and want's to be
 informed if the software is used. For more information, visit
 http://www.cs.cmu.edu/~quake/triangle.html.
"""

from qgis.core import *
from qgis.utils import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.gui import QgsMessageBar

import os
import sys
import platform

# common functions that are used in several scripts of this plugin
from ..tools import commonFunctions
from ..tools.meshConversion import MESHCONVERSION
from ..tools import nodeManipulation

# ui from external file
from ui_renumber_widget import Ui_renumberMeshQT


class RenumberMeshDialog (QDialog, Ui_renumberMeshQT):

    def __init__(self, iface):
        # Set up the user interface
        QDialog.__init__(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.setupUi(self)
   
        # define plugin directory
        self.plugin_dir = QFileInfo(QgsApplication.qgisUserDbFilePath()).path() + "/python/plugins/BASEmesh"

        #populate combo boxes and read attribute tables
        for layer in self.canvas.layers():
            if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Polygon:
                self.elementsComboBox.addItem( layer.name() )
        self.readAttributeMATID()
        QObject.connect(self.elementsComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeMATID)

        for layer in self.canvas.layers():
          if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
            self.nodesComboBox.addItem( layer.name() )
        self.readAttributeELEVATION()
        QObject.connect(self.nodesComboBox, SIGNAL("currentIndexChanged(QString)"), self.readAttributeELEVATION)

        # connection of signal for definition of output file
        QObject.connect(self.browseButton, SIGNAL("clicked()"), self.defineOutputFile)
        self.connect(self.snappingToleranceCheckBox, SIGNAL("toggled(bool)"), self.onSnappingToleranceToggled)

    # ---------------------------
    # - definition of functions -
    # ---------------------------

    def defineOutputFile (self): # display file dialog for output file, return selected file path
        commonFunctions.defineOutput(self,self.outputShapeLineEdit,".shp", commonFunctions.getProjectName())
        pass
    
    def readAttributeMATID (self): # this is necessary as QObject doesn't allow to pass arguments to functions
        self.materialComboBox.clear()
        commonFunctions.readAttribute (self.elementsComboBox, self.materialComboBox)
        pass

    def readAttributeELEVATION (self): # as above
        self.zComboBox.clear()
        commonFunctions.readAttribute (self.nodesComboBox, self.zComboBox)
        pass

    def onSnappingToleranceToggled (self):
        self.snappingToleranceSpinBox.setReadOnly( not self.snappingToleranceCheckBox.isChecked() )

    # -------------------------------------------------------------
    # - accept - block: loaded, when "generate"-button is pressed -
    # -------------------------------------------------------------
    def accept (self):
        # do some initialization stuff
        self.textStatusBox.clear()
        self.progressBar.setValue(0)
        
        # used for definition of Triangle output filenames
        renumberingName = 'Renumbered'
        
        # check if output file is defined
        if self.outputShapeLineEdit.text() == "":
            QMessageBox.warning(self.iface.mainWindow(), "Error","Please specify output shapefile.")
            return
        else:
            outputFile = str(self.outputShapeLineEdit.text())
            fileName, fileExtension = os.path.splitext(outputFile)
        # layer containing the cells
        if self.elementsComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","pls define a shapefile containing the cells of the mesh!")
            return
        else:
            elementsLayer = commonFunctions.readSelection (self.elementsComboBox)
        # column with material index
        if self.materialComboBox.count() == 0 or self.materialComboBox.currentIndex() < 0:
            QMessageBox.warning(self.iface.mainWindows(), "Error", "pls define the column containing the material index in the elements shapefile!")
            return
        else:
            materialField = self.materialComboBox.currentText()
        # layer containing the nodes
        if self.nodesComboBox.count()==0:
            QMessageBox.warning(self.iface.mainWindow(), "Error","pls define a shapefile containing the nodes of the mesh!")
            return
        else:
            nodesLayer = commonFunctions.readSelection (self.nodesComboBox)
        # column with elevations
        if self.zComboBox.count() == 0 or self.zComboBox.currentIndex() < 0:
            QMessageBox.warning(self.iface.mainWindows(), "Error", "pls define the column containing the z-elevation in the nodes shapefile!")
            return
        else:
            elevationField = self.zComboBox.currentText()
        # initialize the progress bar
        self.textStatusBox.append ('\nInput files have been determined, mesh renumbering is started.')
        totalFeatures = 2*commonFunctions.featureCount(elementsLayer) +  commonFunctions.featureCount(nodesLayer)
        self.textStatusBox.append ('\nNumber of features to be renumbered: %d' % totalFeatures)
        self.iface.messageBar().pushMessage("Conversion", "Number of features to be converted: %d" % totalFeatures, QgsMessageBar.INFO, 3)
        self.progressBar.setRange(0, totalFeatures)
        
        # do the renumbering
        precisionExp = self.snappingToleranceSpinBox.value()
        result = nodeManipulation.renumber( nodesLayer, elevationField, elementsLayer, materialField, 10**precisionExp, self.progressBar )
        if type(result) in [type(str()), type(unicode())]:
            QMessageBox.warning( self.iface.mainWindow(), "Error", result )
            return
        else:
            self.textStatusBox.append ('\nThe selected mesh was succesfully renumbered.')
            iface.messageBar().pushMessage("Renumbering", "The selected mesh was succesfully renumbered.", QgsMessageBar.INFO, 10) 
        # convert the mesh to shape files
        meshConv = MESHCONVERSION()
        meshConv.setMesh(result)
        outputFileName = fileName + "_" + renumberingName;
        success = meshConv.writeShape(outputFileName, self.progressBar)
        if success != True:
            self.textStatusBox.append ('\nAn error occured during mesh conversion. We are sorry for the problems.')
            QMessageBox.warning( self.iface.mainWindow(), "Error", success )
            return
        else:
            shapefileNodes = outputFileName+'_nodes.shp'
            shapefileCells = outputFileName+'_elements.shp'
            self.textStatusBox.append ('\nWriting shape files finished succesfully!')
            iface.messageBar().pushMessage ("Renumbering",'Writing shape files finished succesfully!')
        # loading layer containing the cells (optional)
        if self.loadMeshPolygonCheckBox.checkState() == Qt.Checked:
            meshlayer = QgsVectorLayer(shapefileCells, unicode(os.path.basename(shapefileCells)), "ogr")
            if meshlayer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(meshlayer)
                renderer = commonFunctions.createMatidRenderer(meshlayer)
                meshlayer.setRendererV2(renderer)
            else:
                QMessageBox.warning(self.iface.mainWindow(), "Error", "loading mesh polygon layer %s"%shapefileCells)
                pass
        # loading layer containing the nodes (optional)
        if self.loadMeshPointsCheckBox.checkState() == Qt.Checked:
            meshlayer = QgsVectorLayer(shapefileNodes, unicode(os.path.basename(shapefileNodes)), "ogr")
            if meshlayer.isValid():
                QgsMapLayerRegistry.instance().addMapLayer(meshlayer)
            else:
                QMessageBox.warning(self.iface.mainWindow(), "Error", "loading mesh point layer %s"%shapefileNodes)
                pass
        return


